

<?php


//daoDoctorantFormationSecuriteNonFaite()
//daoStagiaireFormationSecuriteNonFaite()
//daoPersonnelFormationSecuriteNonFaite()

// TRAITEMENT STAGIAIRE
require_once '../entites/Model.php';
$liste = Model::daoStagiaireFormationSecuriteNonFaite();

foreach ($liste as $row) {
// meta_messageSecurite ????
  $to = Model::meta_respSec();

$subject = '[HYBISCUS] FORMATION SECURITE  '.$row['p_nom']. ' '.$row['p_prenom'];
$message = 'Le stagiaire '.$row['p_nom']. ' '.$row['p_prenom'].' n\'a toujours pas fait sa formation securité';
$message .= $row['mailEncadrant'];
$headers = 'From: no-reply@umontpellier.fr' . "\r\n" .
    'Reply-To: no-reply@umontpellier.Fr' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);
}

// TRAITEMENT DOCTORANT
require_once '../entites/Model.php';
$liste = Model::daoDoctorantFormationSecuriteNonFaite();

foreach ($liste as $row) {
  $to = Model::meta_respSec();

$subject = '[HYBISCUS] FORMATION SECURITE  '.$row['p_nom']. ' '.$row['p_prenom'];
$message = 'Le doctorant '.$row['p_nom']. ' '.$row['p_prenom'].' n\'a toujours pas fait sa formation securité';
$message .= $row['mailDirecteur1'];
$message .= $row['mailDirecteur2'];
$headers = 'From: no-reply@umontpellier.fr' . "\r\n" .
    'Reply-To: no-reply@umontpellier.Fr' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
}

// TRAITEMENT Personnels
require_once '../entites/Model.php';
$liste = Model::daoPersonnelFormationSecuriteNonFaite();

foreach ($liste as $row) {
  $to = Model::meta_respSec();

$subject = '[HYBISCUS] FORMATION SECURITE  '.$row['p_nom']. ' '.$row['p_prenom'];
$message = 'Le personnel '.$row['p_nom']. ' '.$row['p_prenom'].' n\'a toujours pas fait sa formation securité';
$headers = 'From: no-reply@umontpellier.fr' . "\r\n" .
    'Reply-To: no-reply@umontpellier.Fr' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
}


?>
