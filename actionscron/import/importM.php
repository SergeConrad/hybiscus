<?php
/***************************************************************************\
 *  GES UMR : gestion rh pour les umr                                      *
 *                                                                         *
 *  Copyright (c) 2018                                                     *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/
// exemple PDO OBJ
// self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
// $rq = Model::$pdo->prepare ( "SELECT pl_id FROM t_piece,t_place WHERE t_piece.pi_id LIKE t_place.pi_id AND t_piece.pi_id LIKE :pi_id");
   //     $rq->bindParam ( ':pi_id', $id_piece );
   //                     $rq->execute ();
// return $rq;
require_once 'config.php';
//recuperation_piece_noms_et_ids($site);
class importM {
	public static $pdo;
	public static function set_static() {
		$host = Conf::getHostname ();
		$dbname = Conf::getDatabase ();
		$login = Conf::getLogin ();
		$pass = Conf::getPassword ();
		
		try {
			// Connexion à la base de données
			// Le dernier argument sert à ce que toutes les chaines de charactères
			// en entrée et sortie de MySql soit dans le codage UTF-8
			self::$pdo = new PDO ( "mysql:host=$host;dbname=$dbname", $login, $pass, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8" 
			) );
			
			// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
			self::$pdo->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $ex ) {
			echo $ex->getMessage ();
			die ( 'Problème lors de la connexion à la base de données' );
		}
	}

	public static $pdo2;
	public static function set_static_2() {
		$host = Conf2::getHostname ();
		$dbname = Conf2::getDatabase ();
		$login = Conf2::getLogin ();
		$pass = Conf2::getPassword ();
		
		try {
			// Connexion à la base de données
			// Le dernier argument sert à ce que toutes les chaines de charactères
			// en entrée et sortie de MySql soit dans le codage UTF-8
			self::$pdo2 = new PDO ( "mysql:host=$host;dbname=$dbname", $login, $pass, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8" 
			) );
			
			// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
			self::$pdo2->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $ex ) {
			echo $ex->getMessage ();
			die ( 'Problème lors de la connexion à la base de données Spip' );
		}
	}
	


  static function req_sql_dst($req) {
                try {
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                        $requeteSelectsite = self::$pdo->prepare ( $req);
                        $requeteSelectsite->execute ();
                        return $requeteSelectsite;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }


  static function req_sql_ori($req) {
                try {
                        self::$pdo2->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                        $requeteSelectsite = self::$pdo2->prepare ( $req);
                        $requeteSelectsite->execute ();
                        return $requeteSelectsite;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }


}

importM::set_static ();
importM::set_static_2 ();
?>
