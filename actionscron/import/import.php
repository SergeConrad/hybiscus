
<?php



require_once './importM.php';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$reqsql="SELECT  *             FROM t_personne ";
$origine = importM::req_sql_ori($reqsql);
$destination = importM::req_sql_dst($reqsql);

foreach ($origine as $row) {
	echo "traitement de ".$row->p_id."\n";
	$rq= 'INSERT INTO t_personne (
					p_id,
					p_nom,
			              	p_prenom,
					dateNaissance,
					p_mail,
					p_tel,
					p_tel_perso,
					p_page_perso,
					p_commentaire,
					nationalite,
					personneaprevenir,
					horaireacces,
					p_article_spip,
					p_date_depart,
					sit_id,
					ty_id,
					st_id,
					t_id,
					permanent_c_id,
					pi_id,
					ed_id,
					loginspip,
					demandeIntranet
					) 
	  VALUES (
			"'.$row->p_id.'",
			"'.$row->p_nom.'",
			"'.$row->p_prenom.'",
			"'.$row->dateNaissance.'",
			"'.$row->p_mail.'",
			"'.$row->p_tel.'",
			"'.$row->p_tel_perso.'",
			"'.$row->p_page_perso.'",
			"'.$row->p_commentaire.'",
			"'.$row->nationalite.'",
			"'.$row->personneaprevenir.'",
			"'.$row->horaireacces.'",
			"'.$row->p_article_spip.'",
			"'.$row->p_date_depart.'",
			"'.$row->sit_id.'",
			"'.$row->ty_id.'",
			"'.$row->st_id.'",
			"'.$row->t_id.'",
			"'.$row->permanent_c_id.'",
			"'.$row->pi_id.'",
			"'.$row->ed_id.'",
			"'.$row->loginspip.'",
			"'.$row->demandeIntranet.'"
		)';
	$destination = importM::req_sql_dst($rq);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$reqsql="SELECT  *             FROM droitsspip ";
$origine = importM::req_sql_ori($reqsql);
$destination = importM::req_sql_dst($reqsql);

foreach ($origine as $row) {
        $rq= 'INSERT INTO droitsadmin (
					user,
					admin,
					ordi,
					equipe,
					personnes,
					reservations
                                        )
          VALUES (
                        "'.$row->user.'",
                        "'.$row->admin.'",
                        "'.$row->ordi.'",
                        "'.$row->equipe.'",
                        "'.$row->personnes.'",
                        "'.$row->reservations.'"
	)';
	$destination = importM::req_sql_dst($rq);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$reqsql="SELECT  *             FROM meta ";
$origine = importM::req_sql_ori($reqsql);
$destination = importM::req_sql_dst($reqsql);

foreach ($origine as $row) {
        $rq= 'INSERT INTO meta (
					nom,
					valeur
                                        )
          VALUES (
                        "'.$row->nom.'",
                        "'.$row->valeur.'"
	)';
	$destination = importM::req_sql_dst($rq);
}

///16///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$reqsql="SELECT  *             FROM stage ";
$origine = importM::req_sql_ori($reqsql);
$destination = importM::req_sql_dst($reqsql);

foreach ($origine as $row) {
        $rq= 'INSERT INTO stage (
					stageId,
					p_id,
					p_stag_encadrant,
					eq_id,
					dateDebut,
					dateFin,
					etablissement,
					f_id,
					autreformation,
					t_conv_id,
					gratification,
					financementCredit,
					missionFrance,
					dateMissionFrance,
					missionEtranger,
					dateMissionEtranger,
					demandeBureau,
					demandePoste,
					conventionRevenue,
					scanConvention,
					dossierGratif,
					dateDossierGratif,
					parDossierGratif,
					stageModifiePar,
					stageModifieDate
					
                                        )
          VALUES (
                        "'.$row->stageId.'",
                        "'.$row->p_id.'",
                        "'.$row->p_stag_encadrant.'",
                        "'.$row->eq_id.'",
                        "'.$row->dateDebut.'",
                        "'.$row->dateFin.'",
                        "'.$row->etablissement.'",
                        "'.$row->f_id.'",
                        "'.$row->autreformation.'",
                        "'.$row->t_conv_id.'",
                        "'.$row->gratification.'",
                        "'.$row->financement.'",
                        "'.$row->missionFrance.'",
                        "'.$row->dateMissionFrance.'",
                        "'.$row->missionEtranger.'",
                        "'.$row->dateMissionEtranger.'",
                        "'.$row->demandeBureau.'",
                        "'.$row->demandePoste.'",
                        "'.$row->convention.'",
                        "'.$row->scanConvention.'",
                        "'.$row->dossierGratif.'",
                        "'.$row->dateDossierGratif.'",
                        "'.$row->parDossierGratif.'",
                        "'.$row->stageModifiePar.'",
                        "'.$row->stageModifieDate.'"
	)';
	$destination = importM::req_sql_dst($rq);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$reqsql="SELECT  *             FROM theses ";
$origine = importM::req_sql_ori($reqsql);
$destination = importM::req_sql_dst($reqsql);

foreach ($origine as $row) {
        $rq= 'INSERT INTO theses (
				theseId,
				p_id,
				p_these_directeur1,
				p_these_directeur2,
				ecoleDoctorale,
				intitule,
				tfinancement,
				m2obtenu,
				laboaccueil,
				etsInscription,
				theseDebut,
				theseSoutenance,
				abandon,
				apresThese,
				modifiePar,
				modifieDate
                                        )
          VALUES (
				"'.$row->theseId.'",
				"'.$row->p_id.'",
				"'.$row->p_these_directeur1.'",
				"'.$row->p_these_directeur2.'",
				"'.$row->ecoleDoctorale.'",
				"'.$row->intitule.'",
				"'.$row->tfinancement.'",
				"'.$row->m2obtenu.'",
				"'.$row->laboaccueil.'",
				"'.$row->etsInscription.'",
				"'.$row->theseDebut.'",
				"'.$row->theseSoutenance.'",
				"'.$row->abandon.'",
				"'.$row->apresThese.'",
				"'.$row->modifiePar.'",
				"'.$row->modifieDate.'"
	)';
	$destination = importM::req_sql_dst($rq);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$reqsql="SELECT  *             FROM t_equipe ";
$origine = importM::req_sql_ori($reqsql);
$destination = importM::req_sql_dst($reqsql);

foreach ($origine as $row) {
        $rq= 'INSERT INTO t_equipe (
				eq_id,
				resp_id,
				eq_nom,
				eq_nom_long,
				eq_verticale
                                        )
          VALUES (
				"'.$row->eq_id.'",
				"'.$row->resp_id.'",
				"'.$row->eq_nom.'",
				"'.$row->eq_nom_long.'",
				"'.$row->eq_verticale.'"
	)';
	$destination = importM::req_sql_dst($rq);
}

///16///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$reqsql="SELECT  *             FROM t_membres_equipe ";
$origine = importM::req_sql_ori($reqsql);
$destination = importM::req_sql_dst($reqsql);

foreach ($origine as $row) {
        $rq= 'INSERT INTO t_membres_equipe (
					eq_id,
					p_id
                                        )
          VALUES (
                        "'.$row->eq_id.'",
                        "'.$row->p_id.'"
	)';
	$destination = importM::req_sql_dst($rq);
}





///16///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$reqsql="SELECT  *             FROM t_ordinateur ";
$origine = importM::req_sql_ori($reqsql);
$destination = importM::req_sql_dst($reqsql);

foreach ($origine as $row) {

        $rq= 'INSERT INTO t_ordinateur (

				o_id,
				p_id,
				hostname,
			adresse_ipv4,
			adresse_mac,
			type,
			serviceTag,
			numero_inventaire,
			date_achat,
			garantie,
			processeur,
			memoire,
			dhcp,
			commentaire
                                        )
          VALUES (
			"'.$row->o_id.'",
			"'.$row->p_id.'",
			"'.$row->hostname.'",
			"'.$row->adresse_ipv4.'",
			"'.$row->adresse_mac.'",
			"'.$row->type.'",
			"'.$row->serviceTag.'",
			"'.$row->numero_inventaire.'",
			"'.$row->date_achat.'",
			"'.$row->garantie.'",
			"'.$row->processeur.'",
			"'.$row->memoire.'",
			"'.$row->dhcp.'",
			"'.$row->commentaire.'"
	)';
	$destination = importM::req_sql_dst($rq);
}
///16///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$reqsql="SELECT  *             FROM t_piece ";
$origine = importM::req_sql_ori($reqsql);
$destination = importM::req_sql_dst($reqsql);

foreach ($origine as $row) {
        $rq= 'INSERT INTO t_piece (
			pi_id,
			pi_numero,
			pi_nb_places,
			pi_nb_places_dispo,
			sit_id

                                        )
          VALUES (
			"'.$row->pi_id.'",
			"'.$row->pi_numero.'",
			"'.$row->pi_nb_places.'",
			"'.$row->pi_nb_places_dispo.'",
			"'.$row->sit_id.'"
	)';
	$destination = importM::req_sql_dst($rq);
}
///16///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$reqsql="SELECT  *             FROM t_place ";
$origine = importM::req_sql_ori($reqsql);
$destination = importM::req_sql_dst($reqsql);

foreach ($origine as $row) {
        $rq= 'INSERT INTO t_place (
					pl_id,
					pi_id,
					o_id
                                        )
          VALUES (
                        "'.$row->pl_id.'",
                        "'.$row->pi_id.'",
                        "'.$row->o_id.'"
	)';
	$destination = importM::req_sql_dst($rq);
}
///16///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$reqsql="SELECT  *             FROM t_reservation2 ";
$origine = importM::req_sql_ori($reqsql);
$destination = importM::req_sql_dst($reqsql);

foreach ($origine as $row) {
        $rq= 'INSERT INTO t_reservation2 (
					r_id,
					r_date_debut,
					r_date_fin,
			p_id,
			pl_id
                                        )
          VALUES (
					"'.$row->r_id.'",
					"'.$row->r_date_debut.'",
					"'.$row->r_date_fin.'",
			"'.$row->p_id.'",
			"'.$row->pl_id.'"
	)';
	$destination = importM::req_sql_dst($rq);
}
?>


