<?php
require_once '../entites/Model.php';

require_once("../web/session_start.php");

class PagePersonneNavigationResponsable {

  private $selectSite;
  private $selectStatut;
  private $selectType;
  private $selectEquipe;


public function __construct($selectSite,$selectStatut,$selectType,$selectEquipe)  {
        if (isset($selectSite)) $this->selectSite = $selectSite;
        else  $this->selectSite = 'T';
        if (isset($selectStatut)) $this->selectStatut = $selectStatut;
        else  $this->selectStatut = 'T';
        if (isset($selectType)) $this->selectType = $selectType;
        else  $this->selectType = 'T';
        if (isset($selectEquipe)) $this->selectEquipe = $selectEquipe;
        else  $this->selectEquipe = 'T';




}



      public function afficher($listePers,$type,$id,$listeSite,$listeStatut,$listeType,$listeEquipe,$eq_id,$affarch,$yeardebut,$yearfin){
echo '<div id="aide-general">';
echo '<ul>';
if ($type=="stagiaire") $phrase = ' stagiaires existant';
elseif ($type=="chercheurstagiaire") $phrase = ' stagiaires existant';
elseif ($type=="chercheurstag") $phrase = ' stagiaires existant';
elseif ($type=="chercheurstagiairearchive") $phrase = ' stagiaires existant';
elseif ($type=="chercheurdoctorant") $phrase = ' doctorants existant';
elseif ($type=="chercheurdoct") $phrase = ' doctorants existant';
elseif ($type=="chercheurdoctorantarchive") $phrase = ' doctorants existant';
elseif ($type=="chercheurcdd") $phrase = ' contractuels existant';
elseif ($type=="chercheurcont") $phrase = ' contractuels existant';
elseif ($type=="chercheurinvite") $phrase = ' invites existant';
elseif ($type=="chercheurinvi") $phrase = ' invites existant';
elseif ($type=="chercheurcddarchive") $phrase = ' contractuels existant';
elseif ($type=="chercheurequipe") $phrase = ' équipes existantes';
elseif ($type=="adminequipe") $phrase = ' équipes existantes';
elseif ($type=="responsablestagiaire") $phrase = ' stagiaires existant';
elseif ($type=="responsablestagiairearchive") $phrase = ' stagiaires existant';
elseif ($type=="responsabledoctorant") $phrase = ' doctorants existant';
elseif ($type=="responsabledoctorantarchive") $phrase = ' doctorants existant';
elseif ($type=="responsablecdd") $phrase = ' contractuels existant';
elseif ($type=="responsablecddarchive") $phrase = ' contractuels existant';
elseif ($type=="doctorant") $phrase = ' doctorants existant';
elseif ($type=="cdd") $phrase = ' contractuels existant';
elseif ($type=="permanent") $phrase = ' permanents existant';
elseif ($type=="chercheurperm") $phrase = ' permanents existant';
elseif ($type=="chercheurperma") $phrase = ' permanents existant';
elseif ($type=="laboratoire") $phrase = ' personnels';
elseif ($type=="chercheurlabo") $phrase = ' personnels';
elseif ($type=="archive") $phrase = ' archivés';
elseif ($type=="chercheurarch") $phrase = ' archivés';
elseif ($type=="anonyme") $phrase = ' anonymisés';
elseif ($type=="chercheuranon") $phrase = ' anonymisés';
elseif ($type=="invite") $phrase = ' invités';
elseif ($type=="validation") $phrase = ' en attente validation';
elseif ($type=="admincahier") $phrase = ' cahiers de laboratoire';
elseif ($type=="chercheurcahier") $phrase = ' cahiers de laboratoire';
elseif ($type=="responsableequipe") $phrase = ' membres';
else $phrase ='';
echo '<li id="non-lien"> '.$listePers->rowCount().' '.$phrase .'<input id="searchInput" value="Taper pour filtrer"></li>';
echo '<form  name="check" id="check" action="../control/ControleurPersonne.php?numAction=300" method="post">';

echo '<input type="hidden" name="selectSite" id="selectSite" value="'.$this->selectSite.'">';
echo '<input type="hidden" name="selectStatut"  id="selectStatut" value="'.$this->selectStatut.'">';
echo '<input type="hidden" name="selectType"  id="selectType" value="'.$this->selectType.'">';
echo '<input type="hidden" name="selectEquipe"  id="selectEquipe" value="'.$this->selectEquipe.'">';
echo '<input type="hidden" name="menuDestination"  id="menuDestination" value="'.$type.'">';


if (($type=="laboratoire")||($type=="chercheurlabo")) {
		////////////////////////////////////////////////////////////////////////////////////////////////
		// Site
		////////////////////////////////////////////////////////////////////////////////////////////////
		echo '<label> Site:</label>';
		echo '<select name="selectSite" onchange="this.form.submit()">';
		echo '<option value="T">Tous</option>';
		     foreach ( $listeSite as $row ) {
		                        if ($this->selectSite== $row->sit_id) {
		                           echo '<option value="'.$row->sit_id.'" selected>'. $row->sit_nom.'</option>';
		                        } else {
		                           echo '<option value="'.$row->sit_id.'">'. $row->sit_nom.'</option>';
		                        }
		                }
		echo '</select><br>';
		////////////////////////////////////////////////////////////////////////////////////////////////
		// statut
		////////////////////////////////////////////////////////////////////////////////////////////////
		echo '<label> Statut:</label>';
		// Select par statut
		echo '<select name="selectStatut" onchange="this.form.submit()">';
		echo '<option value="T">Tous </option>';
		     foreach ( $listeStatut as $row ) {
		                        if ($this->selectStatut== $row->st_id) {
		                           echo '<option value="'.$row->st_id.'" selected>'. $row->st_nom.'</option>';
		                        } else {
		                           echo '<option value="'.$row->st_id.'">'. $row->st_nom.'</option>';
		                        }
		                }
		echo '</select><br>';
		////////////////////////////////////////////////////////////////////////////////////////////////
		// type
		////////////////////////////////////////////////////////////////////////////////////////////////
		echo '<label> Type:</label>';
		echo $GLOBALS['visiteur_session']['nom'];
		
		          echo '<select  name="selectType" onchange="this.form.submit()" >';
		        echo '<option value="T">Tous </option>';
		
		                foreach ( $listeType as $rowType ) {
		
		                        if ( ( $this->selectType != 'T' ) && ($this->selectType == $rowType->ty_id) && ($this->selectStatut == $rowType->st_id)) {
		                                echo '<option selected="selected" value=" ' . $rowType->ty_id . '"> ' . $rowType->ty_nom . '</option>';
		                        }
		                        elseif ($this->selectStatut == $rowType->st_id)    echo '<option value=" ' . $rowType->ty_id . '"> ' . $rowType->ty_nom . '</option>';
		
		                }

		                echo '</select><br>';
		////////////////////////////////////////////////////////////////////////////////////////////////
		// Equipe 
		////////////////////////////////////////////////////////////////////////////////////////////////
		echo '<label> Equipe:</label>';
		echo '<select name="selectEquipe" onchange="this.form.submit()">';
		echo '<option value="T">Tous</option>';
		     foreach ( $listeEquipe as $row ) {
		                        if ($this->selectEquipe== $row->eq_id) {
		                           echo '<option value="'.$row->eq_id.'" selected>'. $row->eq_nom.'</option>';
		                        } else {
		                           echo '<option value="'.$row->eq_id.'">'. $row->eq_nom.'</option>';
		                        }
		                }
		echo '</select><br>';
}







echo '</ul>';
echo '</div>';



if (($type=="laboratoire")||($type=="chercheurlabo")) echo '<div id="NavigationBarLabo">';
else echo '<div id="NavigationBarPersonnes">';
if($listePers->rowCount() >0) {
echo '<nav>';
echo '<ul id="ce">';


if ($type=="laboratoire") $suite= '&selectSite='.$this->selectSite.'&selectStatut='.$this->selectStatut.'&selectType='.$this->selectType.'&selectEquipe='.$this->selectEquipe;
elseif (($type=="responsablestagiaire")||($type=="responsabledoctorant")||($type=="responsablecdd")||($type=="responsablestagiairearchive")||($type=="responsabledoctorantarchive")||($type=="responsablecddarchive")) $suite = '&eq_id='.$eq_id;
else $suite = "";


if (($type=="admincahier")||($type=="chercheurcahier")) {
foreach ( $listePers as $row )  {
echo '<li  class="cemenu"><a href="ControleurCahier.php?numAction=172&menuDestination='.$type.'&cahierNum=' .$row->cahierNum .'">'.$row->cahierNum.' '.$row->p_nom .'</a></li>';
} // fin  while

}
elseif (($type=="adminequipe")||($type=="chercheurequipe")||($type=="responsableequipe")) {
foreach ( $listePers as $row )  {
echo '<li  class="cemenu"><a href="ControleurEquipe.php?numAction=112&menuDestination='.$type.'&id=' .$row->eq_id .'">'.$row->eq_nom .'</a></li>';
} // fin  while
}
else {
foreach ( $listePers as $row )  { 
if ($affarch==0) $plusarch="";
else $plusarch="&affarch=1&yeardebut=".$yeardebut."&yearfin=".$yearfin;


	  if ($row->p_id==$id) echo '<li class="cemenu"><a href="ControleurPersonne.php?numAction=303&menuDestination='.$type.'&id=' .$row->p_id .$suite.$plusarch.'" style="color:#FF0000;">'.$row->p_nom .' '.$row->p_prenom. '</a></li>';
	  else echo '<li class="cemenu"><a href="ControleurPersonne.php?numAction=303&menuDestination='.$type.'&id=' .$row->p_id .$suite.$plusarch.'">'.$row->p_nom .' '.$row->p_prenom. '</a></li>';

//	  if ($row->demandeintranet==0) echo '<li class="cemenu"><a href="ControleurPersonne.php?numAction=303&menuDestination='.$type.'&id=' .$row->p_id .$suite.$plusarch.'">'.$row->p_nom .' '.$row->p_prenom. '</a></li>';
//	  else echo '<li class="cemenu"><a href="ControleurPersonne.php?numAction=303&menuDestination='.$type.'&id=' .$row->p_id .$suite.$plusarch.'" style="color:#FF0000;">'.$row->p_nom .' '.$row->p_prenom. '</a></li>';

	} // fin  while

}



echo '</ul>';
echo '</nav>';
}
echo '</form>';

echo '</div>';
}
}



?>




