﻿<?php
class PageResponsableSuiviRh {
public function __construct()  {
}


public function afficher($nomEquipe,$eq_id,$listeEquipeEffectif,$listeEquipePermanent,$listeEquipeNonPermanent,$listeEquipeEtudiant,$listeDepart3mois,$listeArrivee3mois,$listeTypeParAnnee,$message){

	


echo '   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>';
echo '    <script type="text/javascript">';
echo "      google.charts.load('current', {'packages':['corechart']});";
echo "      google.charts.load('current', {'packages':['bar']});";

echo "      google.charts.setOnLoadCallback(equipeEffectif);";
echo "      google.charts.setOnLoadCallback(equipePermanent);";
echo "      google.charts.setOnLoadCallback(equipeNonPermanent);";
echo "      google.charts.setOnLoadCallback(equipeDynamique);";

echo "      function equipeEffectif() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Statut', 'Nombres'],";
$nb=0;
foreach ($listeEquipeEffectif as $row) {
        echo "          ['".$row->st_nom."',      $row->nb],";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "    title:  '".$nb." Effectif de équipe'";
echo "  };";
echo "  var chart = new google.visualization.PieChart(document.getElementById('equipeEffectif'));";
echo "  chart.draw(data, options);";
echo "}";

echo "      function equipePermanent() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Statut', 'Nombres'],";
$nb=0;
foreach ($listeEquipePermanent as $row) {
        echo "          ['".$row->ty_nom."',      $row->nb],";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "    title:  '".$nb." Permanents de équipe'";
echo "  };";
echo "  var chart = new google.visualization.PieChart(document.getElementById('equipePermanent'));";
echo "  chart.draw(data, options);";
echo "}";

echo "      function equipeNonPermanent() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Statut', 'Nombres'],";
$nb=0;
foreach ($listeEquipeNonPermanent as $row) {
        echo "          ['".$row->ty_nom."',      $row->nb],";
        $nb=$nb+$row->nb;
}
foreach ($listeEquipeEtudiant as $row) {
        echo "          ['".$row->ty_nom."',      $row->nb],";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "    title:  '".$nb." Non Permanents de équipe'";
echo "  };";
echo "  var chart = new google.visualization.PieChart(document.getElementById('equipeNonPermanent'));";
echo "  chart.draw(data, options);";
echo "}";



echo "      function equipeDynamique() {";
echo "     var data = google.visualization.arrayToDataTable([";
//echo "        ['Genre', 'Etudiants', 'Doctorants','1','2', { role: 'annotation' } ],";
echo "        ['Dynamique RH', ";
//foreach ($liste1 as $row) echo "'".$row->ty_nom."',";
//echo "'stagiaire','doctorant','post doctorant','cdd chercheur','ater','ita','chercheur','enseignant chercheur / past',";
echo "'chercheur','enseignant chercheur / past','ita',";
echo "'post doctorant','cdd chercheur','ater',";
echo "'doctorant','stagiaire',";
echo "        { role: 'annotation' } ],";
foreach($listeTypeParAnnee as $year=>$liste) {
      $listeEquipeCurrent= array();
      foreach ($liste as $row) {
              $listeEquipeCurrent[$row->ty_nom]= $row->nb;
              }
  //    foreach ($listeNonPermanentParAnnee[$year] as $row) {
  //            $listeEquipeCurrent[$row->ty_nom]= $row->nb;
  //            }
  //    foreach ($listePermanentParAnnee[$year] as $row) {
  //            $listeEquipeCurrent[$row->ty_nom]= $row->nb;
  //            }

	echo "        ['".$year."',";
      echo $listeEquipeCurrent['chercheur'].",".$listeEquipeCurrent['enseignant chercheur / past'].",".$listeEquipeCurrent['ita'].",";

      echo $listeEquipeCurrent['post doctorant'].",".$listeEquipeCurrent['cdd chercheur'].",".$listeEquipeCurrent['ater'].",";
      echo $listeEquipeCurrent['doctorant'].",".$listeEquipeCurrent['stagiaire'].",";
//	foreach ($liste as $row) echo $row->nb.",";
	echo "         ''],";
}


echo "      ]);";

echo "      var options = {";
echo "        width: 600,";
echo "        height: 400,";
echo "        legend: { position: 'top', maxLines: 3 },";
echo "        bar: { groupWidth: '75%' },";
echo "        isStacked: true,";

echo "series: {";
echo "    0:{color:'#FFBF00'},";
echo "    1:{color:'#2E64FE'},";
echo "    2:{color:'#FA5858'},";
echo "    3:{color:'#01DF3A'},";
echo "    4:{color:'#BF00FF'},";
echo "    5:{color:'DarkSeaGreen'},";
echo "    6:{color:'Lavender'},";
echo "    7:{color:'Turquoise'},";
echo "  }";


echo "      };";

echo "     var chart = new google.charts.Bar(document.getElementById('equipeDynamique'));";

echo "        chart.draw(data, google.charts.Bar.convertOptions(options));";
echo "}";






echo "    </script>";



echo '<div id="FormulairesModificationPersonne">';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurPersonne.php">';


echo '<table><tr>';
echo '    <td><div id="equipeEffectif" style="width: 500px; height: 300px;"></div></td>';
echo '    <td><div id="equipePermanent" style="width: 500px; height: 300px;"></div></td>';
echo '    <td><div id="equipeNonPermanent" style="width: 500px; height: 300px;"></div></td>';
echo '</tr></table>';


echo "<h2> prochains départs à 3 mois</h2>";
echo '<table>';
foreach ($listeDepart3mois as $row) {
   echo '<tr><td><a href="ControleurPersonne.php?numAction=303&eq_id='.$eq_id.'&menuDestination=responsablestagiaire&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->dateFinH.'</td></tr>';
}
echo '</table>';

echo "<h2> prochaines arrivées à 3 mois</h2>";
echo '<table>';
foreach ($listeArrivee3mois as $row) {
   echo '<tr><td><a href="ControleurPersonne.php?numAction=303&eq_id='.$eq_id.'&menuDestination=responsablestagiaire&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->dateDebutH.'</td></tr>';
}
echo '</table>';

echo "<h2> Dynamique RH</h2>";
echo '    <div id="equipeDynamique" style="width: 500px; height: 300px;"></div>';



echo "<h2> DEBUG</h2>";
echo "=======================<br>";
echo $message;

echo "=======================<br>";
foreach($listeTypeParAnnee as $year=>$liste) {
      $listeEquipeCurrent= array();
      foreach ($liste as $row) {
              echo $year.'-'.$row->ty_nom.'-'.$row->nb.':'.$row->debug.'<br>';
              $listeEquipeCurrent[$row->ty_nom]= $row->nb;
              }
     //echo $listeEquipeCurrent['stagiaire'].",".$listeEquipeCurrent['doctorant'].",";
}
echo "=======================";


echo '</div>';

echo '</body>';
echo '</html>';
}
}
?>
