<?php
// Dans les versions de PHP antiéreures à 4.1.0, la variable $HTTP_POST_FILES
// doit être utilisée à la place de la variable $_FILES.

$uploaddir = '/var/www/html/spiphsm/AppUmrTest/uploads/';
$uploadfile = $uploaddir . basename($_FILES['uploadConvention']['name']);

echo '<pre>';
echo '1'.$_FILES['uploadConvention']['name'].'<br>';
echo '2'.basename($_FILES['uploadConvention']['name']);
if (move_uploaded_file($_FILES['uploadConvention']['tmp_name'], $uploadfile)) {
    echo "Le fichier est valide, et a été téléchargé
           avec succès. Voici plus d'informations :\n";
} else {
    echo "Attaque potentielle par téléchargement de fichiers.
          Voici plus d'informations :\n";
}

echo 'Voici quelques informations de débogage :';
print_r($_FILES);

echo '</pre>';

?>
