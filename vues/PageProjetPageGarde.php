﻿<?php
class PageProjetPageGarde {
public function __construct()  {
}




public function afficher($menuDestination,$selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$listeProjet,$listeProjetSoumisPorteur,$listeProjetSoumisPartenaire,$listeProjetAcceptePorteur,$listeProjetAcceptePartenaire,$listeProjetRefusePorteur,$listeProjetRefusePartenaire,$listeFin12mois,$listeProjetParAnneeQuinquennal1,$listeProjetParAnneeQuinquennal2,$listeProjetParAnneeMontantQuinquennal1,$listeProjetParAnneeMontantQuinquennal2,$listeProjetParAnneeMontantParTutelleQuinquennal1,$listeProjetParAnneeMontantParTutelleQuinquennal2,$listeProjetParAnneeFinanceur,$listeProjetSoumis,$listeProjetAccepte,$listeProjetRefuse,$listeProjetTermine,$listeTutelle,$listeFinanceur,$listeProjetParAnneeType,$listeType,$arrayEquipe,$arrayPersonne,$arrayFinanceur,$arrayType,$arrayTutelle,$selectPersonnel,$selectAssocie) {



echo '   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>';
echo '    <script type="text/javascript">';
echo "      google.charts.load('current', {'packages':['corechart']});";
echo "      google.charts.load('current', {'packages':['bar']});";




echo "      google.charts.setOnLoadCallback(soumisPorteur);";
echo "      google.charts.setOnLoadCallback(soumisPartenaire);";
echo "      google.charts.setOnLoadCallback(acceptePorteur);";
echo "      google.charts.setOnLoadCallback(acceptePartenaire);";
echo "      google.charts.setOnLoadCallback(refusePorteur);";
echo "      google.charts.setOnLoadCallback(refusePartenaire);";
echo "      google.charts.setOnLoadCallback(projetDynamique1);";
echo "      google.charts.setOnLoadCallback(projetDynamique2);";
echo "      google.charts.setOnLoadCallback(projetDynamique5);";
echo "      google.charts.setOnLoadCallback(projetDynamique6);";
//echo "      google.charts.setOnLoadCallback(projetDynamique3);";
//echo "      google.charts.setOnLoadCallback(projetDynamique4);";
//echo "      google.charts.setOnLoadCallback(projetDynamiqueFinanceur);";
//echo "      google.charts.setOnLoadCallback(projetDynamiqueType);";






//========================================================================================

// Attention on ne met que 3 tutelles dans la première ligne de arraytoDataTable
//echo " 'CNRS', 'IRD', 'UM', ";
// Ceci est lié à ce que la requete select group by ne renvoit que les tutelles qui ont effectivement une participation dans les projets.
//on aurait pu mettre
//foreach ($listeTutelle as $row) echo "'$row->t_nom',";

echo "function projetDynamique5() {";
            // Define the chart to be drawn.


echo "            var data = google.visualization.arrayToDataTable([";

echo "          ['YEAR',";
echo " 'CNRS', 'IRD', 'UM', 'IMT',";
echo " 'MONTANT PROJET'],";

      $projetParTutelle= array();
      foreach($listeProjetParAnneeMontantParTutelleQuinquennal1 as $row) {
              $projetParTutelle[$row->year][$row->t_nom]= $row->hsm;

        }

foreach ($listeProjetParAnneeMontantQuinquennal1 as $row) {
        echo "          ['".$row->year."', ";
        echo intval($projetParTutelle[$row->year]['CNRS'])  ;
	echo ",";
        echo intval($projetParTutelle[$row->year]['IRD'])  ;
	echo ",";
        echo intval($projetParTutelle[$row->year]['UM'])  ;
	echo ",";
        echo intval($projetParTutelle[$row->year]['IMT'])  ;
	echo ",";
//	echo "    $row->sum],\n";
	echo "    0],\n";
}
echo "            ]);";


echo "      var options = {";
echo "          title : 'Montant des projets par tutelles',";
echo "          vAxis: {title: 'Montant'},";
echo "          hAxis: {title: 'Annéee'},";
echo "          seriesType: 'bars',";
//echo "          series: {4: {type: 'line'}}";
echo "        };";


            // Instantiate and draw the chart.
echo "            var chart = new google.visualization.ComboChart(document.getElementById('projetDynamique5'));";

echo "            chart.draw(data, options);";
echo "         }";

//========================================================================================

// Attention on ne met que 3 tutelles dans la première ligne de arraytoDataTable
//echo " 'CNRS', 'IRD', 'UM', ";
// Ceci est lié à ce que la requete select group by ne renvoit que les tutelles qui ont effectivement une participation dans les projets.
//on aurait pu mettre
//foreach ($listeTutelle as $row) echo "'$row->t_nom',";

echo "function projetDynamique6() {";
            // Define the chart to be drawn.


echo "            var data = google.visualization.arrayToDataTable([";

echo "          ['YEAR',";
echo " 'CNRS', 'IRD', 'UM', 'IMT', ";
echo " 'MONTANT PROJET'],";

      $projetParTutelle= array();
      foreach($listeProjetParAnneeMontantParTutelleQuinquennal2 as $row) {
              $projetParTutelle[$row->year][$row->t_nom]= $row->hsm;

        }

foreach ($listeProjetParAnneeMontantQuinquennal2 as $row) {
        echo "          ['".$row->year."', ";
        echo intval($projetParTutelle[$row->year]['CNRS'])  ;
        echo ",";
        echo intval($projetParTutelle[$row->year]['IRD'])  ;
        echo ",";
        echo intval($projetParTutelle[$row->year]['UM'])  ;
        echo ",";
        echo intval($projetParTutelle[$row->year]['IMT'])  ;
        echo ",";
//        echo "    $row->sum],\n";
        echo "    0],\n";
}
echo "            ]);";


echo "      var options = {";
echo "          title : 'Montant des projets par tutelles',";
echo "          vAxis: {title: 'Montant'},";
echo "          hAxis: {title: 'Annéee'},";
echo "          seriesType: 'bars',";
//echo "          series: {4: {type: 'line'}}";
echo "        };";


            // Instantiate and draw the chart.
echo "            var chart = new google.visualization.ComboChart(document.getElementById('projetDynamique6'));";

echo "            chart.draw(data, options);";
echo "         }";

//=================================================================================================================

//echo "function projetDynamique6() {";
//            // Define the chart to be drawn.
//echo "            var data = google.visualization.arrayToDataTable([";
//echo "               ['Année', 'Montant'],";
//foreach ($listeProjetParAnneeMontantQuinquennal2 as $row) {
//        echo "          ['".$row->year."',      $row->sum],\n";
//}
//echo "            ]);";
//
//echo "            var options = {title: 'Montant (in millions)'}; ";
//
//            // Instantiate and draw the chart.
//echo "            var chart = new google.visualization.ColumnChart(document.getElementById('projetDynamique6'));";
//echo "            chart.draw(data, options);";
//echo "         }";






//=================================================================================================================
echo "\n";
echo "function soumisPorteur() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Type', 'NB'],";
$nb=0;
foreach ($listeProjetSoumisPorteur as $row) {
        echo "          ['".$row->type_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('soumisporteur'));";
echo "        chart.draw(data, options);";
echo "      }";


echo "\n";
echo "function soumisPartenaire() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Type', 'NB'],";
$nb=0;
foreach ($listeProjetSoumisPartenaire as $row) {
        echo "          ['".$row->type_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('soumispartenaire'));";
echo "        chart.draw(data, options);";
echo "      }";


echo "\n";
echo "function acceptePorteur() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Type', 'NB'],";
$nb=0;
foreach ($listeProjetAcceptePorteur as $row) {
        echo "          ['".$row->type_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('accepteporteur'));";
echo "        chart.draw(data, options);";
echo "      }";


echo "\n";
echo "function acceptePartenaire() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Type', 'NB'],";
$nb=0;
foreach ($listeProjetAcceptePartenaire as $row) {
        echo "          ['".$row->type_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('acceptepartenaire'));";
echo "        chart.draw(data, options);";
echo "      }";


echo "\n";
echo "function refusePorteur() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Type', 'NB'],";
$nb=0;
foreach ($listeProjetRefusePorteur as $row) {
        echo "          ['".$row->type_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('refuseporteur'));";
echo "        chart.draw(data, options);";
echo "      }";



echo "\n";
echo "function refusePartenaire() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Type', 'NB'],";
$nb=0;
foreach ($listeProjetRefusePartenaire as $row) {
        echo "          ['".$row->type_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('refusepartenaire'));";
echo "        chart.draw(data, options);";
echo "      }";

echo "\n";




//=====================================================================================
echo "function projetDynamique2() {";
            // Define the chart to be drawn.


echo "            var data = google.visualization.arrayToDataTable([";

echo "          ['YEAR',";
echo " 'CNRS', 'IRD', 'UM', 'IMT', ";
echo " 'MONTANT PROJET'],";

//      $projetParTutelle= array();
//      foreach($listeProjetParAnneeMontantParTutelleQuinquennal2 as $row) {
//              $projetParTutelle[$row->year][$row->t_nom]= $row->hsm;
      $projetParTutelle= array();
      foreach($listeProjetParAnneeQuinquennal2 as $row)        $projetParTutelle[$row->year][$row->t_nom]= $row->nb;

        //}

foreach ($listeProjetParAnneeMontantQuinquennal2 as $row) {
        echo "          ['".$row->year."', ";
        echo intval($projetParTutelle[$row->year]['CNRS'])  ;
        echo ",";
        echo intval($projetParTutelle[$row->year]['IRD'])  ;
        echo ",";
        echo intval($projetParTutelle[$row->year]['UM'])  ;
        echo ",";
        echo intval($projetParTutelle[$row->year]['IMT'])  ;
        echo ",";
//        echo "    $row->sum],\n";
        echo "    0],\n";
}
echo "            ]);";


echo "      var options = {";
echo "          title : 'NB projets par tutelles',";
echo "          vAxis: {title: 'NB'},";
echo "          hAxis: {title: 'Annéee'},";
echo "          seriesType: 'bars',";
//echo "          series: {4: {type: 'line'}}";
echo "        };";


            // Instantiate and draw the chart.
echo "            var chart = new google.visualization.ComboChart(document.getElementById('projetDynamique2'));";

echo "            chart.draw(data, options);";
echo "         }";

//=====================================================================================
//=====================================================================================
echo "function projetDynamique1() {";
            // Define the chart to be drawn.


echo "            var data = google.visualization.arrayToDataTable([";

echo "          ['YEAR',";
echo " 'CNRS', 'IRD', 'UM', 'IMT', ";
echo " 'MONTANT PROJET'],";

//      $projetParTutelle= array();
//      foreach($listeProjetParAnneeMontantParTutelleQuinquennal2 as $row) {
//              $projetParTutelle[$row->year][$row->t_nom]= $row->hsm;
      $projetParTutelle= array();
      foreach($listeProjetParAnneeQuinquennal1 as $row)        $projetParTutelle[$row->year][$row->t_nom]= $row->nb;

        //}

foreach ($listeProjetParAnneeMontantQuinquennal1 as $row) {
        echo "          ['".$row->year."', ";
        echo intval($projetParTutelle[$row->year]['CNRS'])  ;
        echo ",";
        echo intval($projetParTutelle[$row->year]['IRD'])  ;
        echo ",";
        echo intval($projetParTutelle[$row->year]['UM'])  ;
        echo ",";
        echo intval($projetParTutelle[$row->year]['IMT'])  ;
        echo ",";
//        echo "    $row->sum],\n";
        echo "    0],\n";
}
echo "            ]);";


echo "      var options = {";
echo "          title : 'NB projets par tutelles',";
echo "          vAxis: {title: 'NB'},";
echo "          hAxis: {title: 'Annéee'},";
echo "          seriesType: 'bars',";
//echo "          series: {4: {type: 'line'}}";
echo "        };";


            // Instantiate and draw the chart.
echo "            var chart = new google.visualization.ComboChart(document.getElementById('projetDynamique1'));";

echo "            chart.draw(data, options);";
echo "         }";


//===========================================================

echo "\n";
echo "      function projetDynamique3() {";
echo "     var data = google.visualization.arrayToDataTable([";
echo "        ['Tutelle gestionnaire', ";
foreach ($listeTutelle as $row) echo "'$row->t_nom',";
echo "        { role: 'annotation' } ],";

//year(dateDebut) AS year, t_tutelle.t_nom,sum(partHsm) as hsm ,GROUP_CONCAT(projets_recherche.acronyme) AS debug

foreach($listeProjetParAnneeMontantParTutelleQuinquennal1 as $row) {
        echo "        ['".$row->year."',";
	 echo $row->hsm.",";
        echo "         ''],";
}
echo "      ]);";

//foreach($listeProjetParAnneeMontantParTutelleQuinquennal1 as $year=>$liste) {
//      $listeEquipeCurrent= array();
//      foreach ($liste as $row) {
//              $listeEquipeCurrent[$row->t_nom]= $row->hsm;
//              }

//        echo "        ['".$year."',";
//foreach ($listeTutelle as $row) echo $listeEquipeCurrent[$row->t_nom].",";
//        echo "         ''],";
//}
//echo "      ]);";

echo "      var options = {";
echo "        width: 600,";
echo "        height: 400,";
echo "        legend: { position: 'top', maxLines: 3 },";
echo "        bar: { groupWidth: '75%' },";
echo "        isStacked: true,";

echo "series: {";
echo "    0:{color:'#FFBF00'},";
echo "    1:{color:'#2E64FE'},";
echo "    2:{color:'#FA5858'},";
echo "    3:{color:'#01DF3A'},";
echo "    4:{color:'#33F0FF'},";
echo "    5:{color:'#9E5998'},";
echo "  }";

echo "      };";

echo "     var chart = new google.charts.Bar(document.getElementById('projetDynamique3'));";
echo "        chart.draw(data, google.charts.Bar.convertOptions(options));";
echo "}";


echo "\n";
echo "      function projetDynamique4() {";
echo "     var data = google.visualization.arrayToDataTable([";
echo "        ['Tutelle gestionnaire', ";
foreach ($listeTutelle as $row) echo "'$row->t_nom',";
echo "        { role: 'annotation' } ],";

foreach($listeProjetParAnneeMontantParTutelleQuinquennal2 as $year=>$liste) {
      $listeEquipeCurrent= array();
      foreach ($liste as $row) {
              $listeEquipeCurrent[$row->t_nom]= $row->hsm;
              }

        echo "        ['".$year."',";
foreach ($listeTutelle as $row) echo $listeEquipeCurrent[$row->t_nom].",";
        echo "         ''],";
}
echo "      ]);";

echo "      var options = {";
echo "        width: 600,";
echo "        height: 400,";
echo "        legend: { position: 'top', maxLines: 3 },";
echo "        bar: { groupWidth: '75%' },";
echo "        isStacked: true,";

echo "series: {";
echo "    0:{color:'#FFBF00'},";
echo "    1:{color:'#2E64FE'},";
echo "    2:{color:'#FA5858'},";
echo "    3:{color:'#01DF3A'},";
echo "    4:{color:'#33F0FF'},";
echo "    5:{color:'#9E5998'},";
echo "  }";

echo "      };";

echo "     var chart = new google.charts.Bar(document.getElementById('projetDynamique4'));";
echo "        chart.draw(data, google.charts.Bar.convertOptions(options));";
echo "}";



//echo "\n";
///////////////////////////////////////////////////////////////////////
//echo "      function projetDynamiqueFinanceur() {";
//echo "     var data = google.visualization.arrayToDataTable([";
//echo "        ['Financeurs', ";
//foreach ($listeFinanceur as $row) echo "'$row->financeur_nom',";
//echo "        { role: 'annotation' } ],";

//foreach($listeProjetParAnneeFinanceur as $year=>$liste) {
//      $listeEquipeCurrent= array();
//      foreach ($liste as $row) {
//              $listeEquipeCurrent[$row->financeur_nom]= $row->nb;
//              }

//echo "\n";
//        echo "        ['".$year."',";
//foreach ($listeFinanceur as $row) echo $listeEquipeCurrent[$row->financeur_nom].",";
//        echo "         ''],";
//}
//echo "      ]);";

//echo "\n";
//echo "      var options = {";
//echo "        width: 600,";
//echo "        height: 400,";
//echo "        legend: { position: 'top', maxLines: 3 },";
//echo "        bar: { groupWidth: '75%' },";
//echo "        isStacked: true,";


//echo "\n";
//echo "series: {";
//	$i=0;
//      foreach ($listeFinanceur as $row) {
//	$color=  sprintf("#%02X%02X%02X", rand(0,255), rand(0,255), rand(0,255));
//	echo "    $i:{color:'$color'},";
//	$i++;
//	}
//echo "  }";

//echo "      };";

//echo "     var chart = new google.charts.Bar(document.getElementById('projetDynamiqueFinanceur'));";
//echo "        chart.draw(data, google.charts.Bar.convertOptions(options));";
//echo "}";

echo "\n";
///////////////////////////////////////////////////////////////////////
//echo "      function projetDynamiqueType() {";
//echo "     var data = google.visualization.arrayToDataTable([";

//echo "        ['Types', ";
//foreach ($listeType as $row) echo "'$row->type_nom',";
//echo "        { role: 'annotation' } ],";
//echo "\n_______\n";

//foreach($listeProjetParAnneeType as $year=>$liste) {
//      $listeEquipeCurrent= array();
//      foreach ($liste as $row) {
//              $listeEquipeCurrent[$row->type_nom]= $row->nb;
//              }

//        echo "        ['".$year."',";
//foreach ($listeType as $row) echo $listeEquipeCurrent[$row->financeur_nom].",";
//        echo "         ''],";
//}
//echo "      ]);";

//echo "      var options = {";
//echo "        width: 600,";
//echo "        height: 400,";
//echo "        legend: { position: 'top', maxLines: 3 },";
//echo "        bar: { groupWidth: '75%' },";
//echo "        isStacked: true,";


//echo "series: {";
//	$i=0;
//      foreach ($listeType as $row) {
//	$color=  sprintf("#%02X%02X%02X", rand(0,255), rand(0,255), rand(0,255));
//	echo "    $i:{color:'$color'},";
//	$i++;
//	}
//echo "  }";

//echo "      };";

//echo "     var chart = new google.charts.Bar(document.getElementById('projetDynamiqueType'));";
//echo "        chart.draw(data, google.charts.Bar.convertOptions(options));";
//echo "}";

//echo "\n";


echo "    </script>";



echo "\n";
echo '<div id="FormulairesModificationPersonne">';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
//$listeProjetSoumis,$listeProjetAccepte,$listeProjetRefuse,$listeProjetTermine);
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurProjet.php">';



echo '<input type="hidden" name="menuDestination" id="menuDestination" value='.$menuDestination.'>';
echo '<input type="hidden" name="selecteq_id" id="selecteq_id" value='.$selecteq_id.'>';
echo '<input type="hidden" name="selectpreselection" id="selectpreselection" value='.$selectpreselection.'>';
echo '<input type="hidden" name="selectDate" id="selectDate" value='.$selectDate.'>';
echo '<input type="hidden" name="selectDateDebut" id="selectDateDebut" value='.$selectDateDebut.'>';
echo '<input type="hidden" name="selectPersonnel" id="selectPersonnel" value='.$selectPersonnel.'>';
echo '<input type="hidden" name="selectAssocie" id="selectAssocie" value='.$selectAssocie.'>';


echo '<button type="submit" name="numAction" value="300" id="submit_click"> Exporter CSV </button>';


//echo "===========\n";
//var_dump($listeProjetParAnneeType);
//foreach ($listeProjetParAnneeType as $row) var_dump($row);
//echo "===========\n";
//foreach ($listeType as $row) var_dump($row);

   $listeEquipeCurrent= array();
      foreach ($liste as $row) {
              $listeEquipeCurrent[$row->ty_nom]= $row->nb;
              }


echo "<h2>Les projets soumis du laboratoire: </h2>";
echo '<table>';
echo '<tr><th>acronyme</th><th>Equipe</th><th>Année soumission</th><th>Selection</th><th>nom porteur </th><th>coord Ou partenaire</th><th>Coordinateur</th><th>Financeur</th><th>Type</th><th>Tutelle gestionnaire</th></tr>';
echo '<tr>';
foreach ( $listeProjetSoumis as $row ) {
//serge
$dates = explode("-", $row->dateSoumission);



// Un projet peut faire partie de plusieurs équipes
$affeq = '';
$equipes = explode(",", $row->equipe_id);
foreach($equipes as $equipe)    $affeq .= $arrayEquipe[$equipe]." "  ;

  echo '<tr><td><a href="ControleurProjet.php?numAction=112&selectAssocie='.$selectAssocie.'&selectPersonnel='.$selectPersonnel.'&selecteq_id='.$selecteq_id.'&selectpreselection='.$selectpreselection.'&selectDate='.$selectDate.'&selectDateDebut='.$selectDateDebut.'&menuDestination=admin&projet_id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$affeq.'</td><td>'.$dates[0].'</td><td>'.$row->preselection.'</td><td>'.$arrayPersonne[$row->nom_porteur].'</td><td>'.$row->coordOuPartenaire.'</td><td>'.$row->coordinateurProjet.'</td><td>'.$arrayFinanceur[$row->financeur].'</td><td>'.$arrayType[$row->type].'</td><td>'.$arrayTutelle[$row->organisme_gestionnaire].'</td></tr>';
}
echo '</tr></table>';


echo "<h2>Les projets en cours: </h2>";
echo '<table>';
echo '<tr><th>acronyme</th><th>Equipe</th><th>Année soumission</th><th>Selection</th><th>nom porteur </th><th>coord Ou partenaire</th><th>Coordinateur</th><th>Financeur</th><th>Type</th><th>Tutelle gestionnaire</th></tr>';
echo '<tr>';
foreach ( $listeProjetAccepte as $row ){
$dates = explode("-", $row->dateSoumission);
// Un projet peut faire partie de plusieurs équipes
$affeq = '';
$equipes = explode(",", $row->equipe_id);
foreach($equipes as $equipe)    $affeq .= $arrayEquipe[$equipe]." "  ;

  echo '<tr><td><a href="ControleurProjet.php?numAction=112&selectAssocie='.$selectAssocie.'&selectPersonnel='.$selectPersonnel.'&selecteq_id='.$selecteq_id.'&selectpreselection='.$selectpreselection.'&selectDate='.$selectDate.'&selectDateDebut='.$selectDateDebut.'&menuDestination=admin&projet_id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$affeq.'</td><td>'.$dates[0].'</td><td>'.$row->preselection.'</td><td>'.$arrayPersonne[$row->nom_porteur].'</td><td>'.$row->coordOuPartenaire.'</td><td>'.$row->coordinateurProjet.'</td><td>'.$arrayFinanceur[$row->financeur].'</td><td>'.$arrayType[$row->type].'</td><td>'.$arrayTutelle[$row->organisme_gestionnaire].'</td></tr>';
}
echo '</tr></table>';

echo "<h2>Les projets refusés du laboratoire: </h2>";
echo '<table>';
echo '<tr><th>acronyme</th><th>Equipe</th><th>Année soumission</th><th>Selection</th><th>nom porteur </th><th>coord Ou partenaire</th><th>Coordinateur</th><th>Financeur</th><th>Type</th><th>Tutelle gestionnaire</th></tr>';
echo '<tr>';
foreach ( $listeProjetRefuse as $row )  {
$dates = explode("-", $row->dateSoumission);
// Un projet peut faire partie de plusieurs équipes
$affeq = '';
$equipes = explode(",", $row->equipe_id);
foreach($equipes as $equipe)    $affeq .= $arrayEquipe[$equipe]." "  ;
  echo '<tr><td><a href="ControleurProjet.php?numAction=112&selectAssocie='.$selectAssocie.'&selectPersonnel='.$selectPersonnel.'&selecteq_id='.$selecteq_id.'&selectpreselection='.$selectpreselection.'&selectDate='.$selectDate.'&selectDateDebut='.$selectDateDebut.'&menuDestination=admin&projet_id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$affeq.'</td><td>'.$dates[0].'</td><td>'.$row->preselection.'</td><td>'.$arrayPersonne[$row->nom_porteur].'</td><td>'.$row->coordOuPartenaire.'</td><td>'.$row->coordinateurProjet.'</td><td>'.$arrayFinanceur[$row->financeur].'</td><td>'.$arrayType[$row->type].'</td><td>'.$arrayTutelle[$row->organisme_gestionnaire].'</td></tr>';
}
echo '</tr></table>';

echo "<h2>Les projets terminés du laboratoire: </h2>";
echo '<table>';
echo '<tr><th>acronyme</th><th>Equipe</th><th>Année soumission</th><th>Selection</th><th>nom porteur </th><th>coord Ou partenaire</th><th>Coordinateur</th><th>Financeur</th><th>Type</th><th>Tutelle gestionnaire</th></tr>';
echo '<tr>';
foreach ( $listeProjetTermine as $row ){
$dates = explode("-", $row->dateSoumission);
// Un projet peut faire partie de plusieurs équipes
$affeq = '';
$equipes = explode(",", $row->equipe_id);
foreach($equipes as $equipe)    $affeq .= $arrayEquipe[$equipe]." "  ;
  echo '<tr><td><a href="ControleurProjet.php?numAction=112&selectAssocie='.$selectAssocie.'&selectPersonnel='.$selectPersonnel.'&selecteq_id='.$selecteq_id.'&selectpreselection='.$selectpreselection.'&selectDate='.$selectDate.'&selectDateDebut='.$selectDateDebut.'&menuDestination=admin&projet_id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$affeq.'</td><td>'.$dates[0].'</td><td>'.$row->preselection.'</td><td>'.$arrayPersonne[$row->nom_porteur].'</td><td>'.$row->coordOuPartenaire.'</td><td>'.$row->coordinateurProjet.'</td><td>'.$arrayFinanceur[$row->financeur].'</td><td>'.$arrayType[$row->type].'</td><td>'.$arrayTutelle[$row->organisme_gestionnaire].'</td></tr>';
}
echo '</tr></table>';
      echo '<button type="Submit" name="numAction" value="110" id="submit_click"> Ajouter </button>&nbsp;&nbsp;';
echo '</form>';


echo '<form id="modifier_equipe" method="post"  action="../control/ControleurPersonne.php">';



echo '<h2> Projets soumis classés par Type</h2>';
echo '<table>';
echo '<tr><th>HSM Porteur</th><th>Partenaire</th></tr><tr>';
echo '    <td><div id="soumisporteur" style="width: 500px; height: 300px;"></div></td>';
echo '    <td><div id="soumispartenaire" style="width: 500px; height: 300px;"></div></td>';
echo '</tr></table>';
echo '<h2> Projets acceptés classés par Type</h2>';
echo '<table>';
echo '<tr><th>HSM Porteur</th><th>Partenaire</th></tr><tr>';
echo '    <td><div id="accepteporteur" style="width: 500px; height: 300px;"></div></td>';
echo '    <td><div id="acceptepartenaire" style="width: 500px; height: 300px;"></div></td>';
echo '</tr></table>';
echo '<h2> Projets refusés classés par Type</h2>';
echo '<table>';
echo '<tr><th>HSM Porteur</th><th>Partenaire</th></tr><tr>';
echo '    <td><div id="refuseporteur" style="width: 500px; height: 300px;"></div></td>';
echo '    <td><div id="refusepartenaire" style="width: 500px; height: 300px;"></div></td>';
echo '</tr></table>';


echo '<h2> Projets acceptés se terminant dans les 12 mois </h2>';
///////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurProjet.php">';
echo '<table>';
echo '<tr><th>acronyme</th><th>Date FIN</th></tr>';
echo '<tr>';
foreach ( $listeFin12mois as $row )   echo '<tr><td><a href="ControleurProjet.php?numAction=112&menuDestination=responsableequipe&projet_id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$row->dateFin.'</td></tr>';

echo '</tr></table>';

  //    echo '<button type="Submit" name="numAction" value="601" id="submit_click"> Exporter csv </button></td>';
echo '</form>';


echo '<form id="modifier_equipe" method="post"  action="../control/ControleurPersonne.php">';


echo "<h2> Dynamique Projets par Tutelle (acceptés et terminés)</h2>";

echo '<table>';
echo '<tr><th colspan=2>Nb projets</th></tr>';
echo '<tr><td>Quinquennal 2015 2020</td><td>Quinquennal 2021 2026</td></tr>';
echo '<tr>';
//echo '    <td><div id="projetDynamique1" style="width: 500px; height: 300px;"></div></td>';
//echo '    <td><div id="projetDynamique2" style="width: 500px; height: 300px;"></div></td>';
echo '    <td width="50%"><div id="projetDynamique1" ></div></td>';
echo '    <td width="50%"><div id="projetDynamique2" ></div></td>';
echo '</tr>';
echo '<tr><th colspan=2> Montant projets</th></tr>';
echo '<tr>';
echo '    <td width="50%"><div id="projetDynamique5" ></div></td>';
echo '    <td width="50%"><div id="projetDynamique6" ></div></td>';
echo '</tr>';
//echo '<tr><th colspan=2>Montant projets</th></tr>';
//echo '<tr>';
//echo '    <td><div id="projetDynamique3" ></div></td>';
//echo '    <td><div id="projetDynamique4" ></div></td>';
//echo '</tr>';


//echo '<tr><th colspan=2> DEBUG CHART</th></tr>';
//echo '<tr><td>';
//      $projetParTutelle= array();
//      foreach($listeProjetParAnneeMontantParTutelleQuinquennal1 as $row) {
//              $projetParTutelle[$row->year][$row->t_nom]= $row->hsm;
	
//        }

//	foreach ($listeProjetParAnneeMontantQuinquennal1 as $row) {
//echo "$row->year $row->debug<br>";
//        	echo "  CNRS ";
//        	echo $projetParTutelle[$row->year]['CNRS'] ;
//		echo ".<br>";
//       	echo "  IRD ";
//        	echo $projetParTutelle[$row->year]['IRD'] ;
//		echo ".<br>";
//        	echo " UM ";
//        	echo $projetParTutelle[$row->year]['UM'] ;
//		echo ".<br>";
//	}
//echo '</td>';
//echo '<td>';
//echo '</td></tr>';





//echo '<tr><th colspan=2> DEBUG</th></tr>';
//echo '<tr><td>';
//foreach ($listeProjetParAnneeMontantParTutelleQuinquennal1 as $row) echo "$row->year $row->t_nom $row->hsm $row->debug <br>";
//echo '</td>';
//echo '<td>';

//foreach ($listeProjetParAnneeMontantParTutelleQuinquennal2 as $row) echo "$row->year $row->t_nom $row->hsm $row->debug <br>";
//echo '</td></tr>';
//echo '<tr><th colspan=2> DEBUG listeProjetParAnneeMontantQuinquennal2</th></tr>';
//echo '<td>';
//      foreach ($listeProjetParAnneeMontantQuinquennal1 as $row) echo "$row->year $row->sum $row->debug <br>";
//echo '</td>';
//echo '<td>';
//      foreach ($listeProjetParAnneeMontantQuinquennal2 as $row) echo "$row->year $row->sum $row->debug <br>";
//echo '</td>';




echo '</table>';




echo '</div>';

echo '</body>';
echo '</html>';
}
}
?>
