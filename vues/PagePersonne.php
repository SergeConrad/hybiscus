﻿<?php
require_once("../web/session_start.php");
class PagePersonne {

  private $p_id;
  private $sexe;
  private $p_nom;
  private $p_prenom;
  private $dateNaissance;
  private $p_mail;
  private $p_mail_perso;
  private $p_tel;
  private $p_tel_perso;
  private $p_page_perso;
  private $orcid;
  private $IdHAL;
  private $hdr;
  private $p_commentaire;
  private $nationalite;
  private $personneaprevenir;
  private $horaireacces;
  private $demandeIntranet;
  private $archive;
private $sectionCNRS;
private $collegeCNRSId;
private $sectionCNU;
private $collegeCNUId;
private $commissionIRD;
private $collegeIRDId;

private $historique;
private $historiquen;
private $listeEquipePersonne;


 private $listeStage;
 private $listeContrat;
 private $listePermanent;
 private $listeInvite;


// Objet these
  private $theseId;
  private $p_these_directeur1;
  private $p_these_directeur2;
  private $ecoleDoctorale;
  private $intitule;
  private $tfinancement;
  private $m2obtenu;
  private $laboaccueil;
  private $etsInscription;
  private $etsCoTutelle;
  private $theseDebut;
  private $theseSoutenance;
  private $abandon;
  private $apresThese;
  private $modifiePar;
  private $modifieDate;
  private $dateDebut;
  private $dateFin;



// Appelant pour les refresh
// private $numActionOrigine;

// Menu de destination pour les submits
private $menuDestination;




// parcours nouvel arrivant
 private $chartesSignees;
 private $formationSecurite;
 private $numeroCle;
 private $ficheNouveauArrivant;
 private $sessionInformatique;




// les tags
private $creation; // administrateur en mode creation
private $ajouthistorique; //on ajoute un historique
private $ajoutfinancementstage; // on ajoute un financement de stage
private $chercheur; // le chercheur peut ajouter supprimer un historique
private $droitadminPersonne; // La personne a le droit administrer les personnes
private $droitadminResa; // La personne a le droit administrer les reservations


  private $selectSite;
  private $selectStatut;
  private $selectType;
  private $selectEquipe;
  private $selectDate;

public function __construct($creation,$ajouthistorique,$chercheur,$droitadminPersonne,$droitadminResa,$tab,$historique,$listeEquipePersonne,$historiquen,$tabstage,$tabthese,$tabcontractuel,$tabinvite,$tabpermanent,$menuDestination,$listeFinancementCredit,$listeFinancementCreditNb,$ajoutfinancementstage,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate)  {




	if (isset($selectSite)) $this->selectSite = $selectSite;
        else  $this->selectSite = 'T';
        if (isset($selectStatut)) $this->selectStatut = $selectStatut;
        else  $this->selectStatut = 'T';
        if (isset($selectType)) $this->selectType = $selectType;
        else  $this->selectType = 'T';
        if (isset($selectEquipe)) $this->selectEquipe = $selectEquipe;
        else  $this->selectEquipe = 'T';
        if (isset($selectDate)) $this->selectDate = $selectDate;
        else  $this->selectDate = date('Y-m-d');


$this->creation = $creation;
$this->ajouthistorique = $ajouthistorique;
$this->ajoutfinancementstage = $ajoutfinancementstage;
$this->chercheur = $chercheur;
$this->droitadminPersonne = $droitadminPersonne;
$this->droitadminResa = $droitadminResa; 





        $this->p_id = $tab['p_id'];




        $this->sexe = $tab['sexe'];
        $this->p_nom = $tab['p_nom'];
        $this->p_prenom = $tab['p_prenom'];
	$this->dateNaissance = $tab['dateNaissance'];
        $this->p_mail = $tab['p_mail'];
        $this->p_mail_perso = $tab['p_mail_perso'];
        $this->p_tel = $tab['p_tel'];
        $this->p_tel_perso = $tab['p_tel_perso'];
        $this->orcid = $tab['orcid'];
        $this->IdHAL = $tab['IdHAL'];
        $this->hdr = $tab['hdr'];
        $this->p_page_perso = $tab['p_page_perso'];
        $this->p_commentaire = $tab['p_commentaire'];
        $this->nationalite = $tab['nationalite'];
        $this->personneaprevenir = $tab['personneaprevenir'];
        $this->horaireacces = $tab['horaireacces'];
        $this->demandeIntranet = $tab['demandeIntranet'];
	$this->archive= $tab['archive'];
$this->sectionCNRS = $tab['sectionCNRS'];
$this->collegeCNRSId = $tab['collegeCNRSId'];
$this->sectionCNU = $tab['sectionCNU'];
$this->collegeCNUId = $tab['collegeCNUId'];
$this->commissionIRD = $tab['commissionIRD'];
$this->collegeIRDId = $tab['collegeIRDId'];

$this->historique = $historique;
$this->historiquen = $historiquen;
$this->listeEquipePersonne = $listeEquipePersonne;


 $this->chartesSignees = $tab['chartesSignees'];
 $this->formationSecurite = $tab['formationSecurite'];
 $this->numeroCle = $tab['numeroCle'];
 $this->ficheNouveauArrivant = $tab['ficheNouveauArrivant'];
 $this->sessionInformatique = $tab['sessionInformatique'];

$this->listeStage = $tabstage;
$this->listeContrat = $tabcontractuel;
$this->listeInvite = $tabinvite;
$this->listePermanent = $tabpermanent;




$this->theseId = $tabthese['theseId'];
$this->p_these_directeur1 = $tabthese['p_these_directeur1'];
$this->p_these_directeur2 = $tabthese['p_these_directeur2'];
$this->ecoleDoctorale = $tabthese['ecoleDoctorale'];
$this->intitule = $tabthese['intitule'];
$this->tfinancement = $tabthese['tfinancement'];
$this->m2obtenu = $tabthese['m2obtenu'];
$this->laboaccueil = $tabthese['laboaccueil'];
$this->etsInscription = $tabthese['etsInscription'];
$this->etsCoTutelle = $tabthese['etsCoTutelle'];
$this->theseDebut = $tabthese['theseDebut'];
$this->theseSoutenance = $tabthese['theseSoutenance'];
$this->abandon = $tabthese['abandon'];
$this->apresThese = $tabthese['apresThese'];
$this->modifiePar = $tabthese['modifiePar'];
$this->modifieDate = $tabthese['modifieDate'];
$this->dateDebut = $tabthese['dateDebut'];
$this->dateFin = $tabthese['dateFin'];


//if ($_POST['numActionOrigine']=='') $this->numActionOrigine = $numActionOrigine;
//else $this->numActionOrigine = $_POST['numActionOrigine'];
$this->menuDestination = $menuDestination;


if ((($menuDestination =="chercheurstagiaire")||($menuDestination =="arrivantstagiaire"))&&($this->ajouthistorique==1)) {
	$this->historiquen->st_idn=3;
	$this->historiquen->ty_idn=2;
//        $this->p_stag_encadrant = $_SESSION['p_id'];
}
if ((($menuDestination =="chercheurdoctorant")||($menuDestination =="arrivantdoctorant"))&&($this->ajouthistorique==1)) {
	$this->historiquen->st_idn=3;
	$this->historiquen->ty_idn=1;
//        $this->p_these_directeur1 = $_SESSION['p_id'];
}
if ((($menuDestination =="chercheurcdd")||($menuDestination =="arrivantcdd"))&&($this->ajouthistorique==1)) {
	$this->historiquen->st_idn=2;
//        $this->contractuel_sup_id = $_SESSION['p_id'];
}
if ((($menuDestination =="chercheurinvite")||($menuDestination =="arrivantinvite"))&&($this->ajouthistorique==1)) {
	$this->historiquen->st_idn=4;
//        $this->invite_par_id = $_SESSION['p_id'];
}

if ($this->chercheur=="1") {
        $this->p_these_directeur1 = $_SESSION['p_id'];
}

}


      public function afficher($erreur,$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listereservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahiers, $listeRessource,$listeCollege,$tab){




   echo '<div id="FormulairesModificationPersonne">';



?>
 <!-- Tab links -->
<div class="tab">
  <button class="tablinks" onclick="openTab(event, 'info')" id="tabinfo">Informations Personnelles </button>
<?php
//  <button class="tablinks" onclick="openTab(event, 'actions')" id="tabactions">Actions </button>
if ($this->creation == 0) echo '  <button class="tablinks" onclick="openTab(event, \'admin\')" id="tabadmin">Informations administratives</button>';
if ($this->creation == 0) echo '  <button class="tablinks" onclick="openTab(event, \'historique\')" id="tabhistorique">Historique </button>';
foreach ($this->historique as $row)   {
	//if ($row->stageId !=0) $h= "Stage ".$row->dateDebutH;
	if (($row->st_id==3)&&($row->ty_id==2)) $h= "Stage ".$row->dateDebutH;
	elseif (($row->st_id==3)&&($row->ty_id==1)) $h= "Thèse ";
	elseif ($row->st_id==2) $h= "Contrat ".$row->dateDebutH;
	elseif ($row->st_id==4) $h= "Invité ".$row->dateDebutH;
	elseif ($row->st_id ==1) $h= "Permanent";
	else $h= "Autre";
	echo '  <button class="tablinks" onclick="openTab(event, \'historique'.$row->numHistorique.'\')" id="tabhistorique'.$row->numHistorique.'">'.$h.'</button>';
}

if (($this->menuDestination != "arrivantstagiaire") &&($this->menuDestination != "arrivantdoctorant")&&($this->menuDestination != "arrivantcdd")&&($this->menuDestination != "arrivantinvite")){
   if ($this->creation==0) echo '  <button class="tablinks" onclick="openTab(event, \'reservation\')" id="tabreservation">Réservations</button>';
   if ($this->creation==0) echo '  <button class="tablinks" onclick="openTab(event, \'cahier\')" id="tabreservation">Cahiers de laboratoire</button>';
}

echo '</div>';





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

foreach ($this->historique as $rowh) {
echo '<div id="historique'.$rowh->numHistorique.'" class="tabcontent" >';
		echo '<form enctype="multipart/form-data" id="modifier_personne" method="post"  action="../control/ControleurPersonne.php?numAction=303AFF">';


// LES TAGS
echo '<input type="hidden" name="ajouthistorique" id="ajouthistorique" value="'.$this->ajouthistorique.'">';
echo '<input type="hidden" name="creation" id="creation" value="'.$this->creation.'">';
echo '<input type="hidden" name="ajoutfinancementstage" id="ajoutfinancementstage" value="'.$this->ajoutfinancementstage.'">';
echo '<input type="hidden" name="chercheur" id="chercheur" value="'.$this->chercheur.'">';

echo '<input type="hidden" name="demandeIntranet" value="'.$this->demandeIntranet.'">';

echo '<input type="hidden" name="p_id" id="p_id" value="'.$this->p_id.'">';
echo '<input type="hidden" name="numHistorique" id="numHistorique" value="'.$rowh->numHistorique.'">';
//echo '<input type="hidden" name="numActionOrigine" id="numActionOrigine" value="'.$this->numActionOrigine.'">';
echo '<input type="hidden" name="menuDestination" id="menuDestination" value="'.$this->menuDestination.'">';
echo '<input type="hidden" name="selectSite" id="selectSite" value="'.$this->selectSite.'">';
echo '<input type="hidden" name="selectStatut" id="selectStatut" value="'.$this->selectStatut.'">';
echo '<input type="hidden" name="selectType" id="selectType" value="'.$this->selectType.'">';
echo '<input type="hidden" name="selectEquipe" id="selectEquipe" value="'.$this->selectEquipe.'">';
echo '<input type="hidden" name="selectDate" id="selectDate" value="'.$this->selectDate.'">';

	// LES STAGES
	if (($rowh->st_id ==3)&&($rowh->ty_id==2)) {

if ( ( (($this->menuDestination!="chercheurlabo")&&($this->menuDestination!="chercheurstag")&&($this->menuDestination!="chercheurdoct")&&($this->menuDestination!="chercheurcont")&&($this->menuDestination!="chercheurperm")&&($this->menuDestination!="chercheurinvi")&&($this->menuDestination!="chercheurarch")&&($this->menuDestination!="chercheuranon")) ) || ($_SESSION['p_id']==  $this->p_id) ) {
if (($this->demandeIntranet=="0")||($this->chercheur=="1"))echo '<button type="submit" name="numAction" value="306" id="submit_click"> Enregistrer </button>';
}
echo '    &nbsp;&nbsp;&nbsp;        ';
echo '<span class="error">'. $erreur.'</span>';


		include ("TableauStageModifie.php");
		include ("TableauStageModifiePart2.php");
	}
	// LES THESES
	elseif (($rowh->st_id ==3)&&($rowh->ty_id==1)) {

if ( ( (($this->menuDestination!="chercheurlabo")&&($this->menuDestination!="chercheurstag")&&($this->menuDestination!="chercheurdoct")&&($this->menuDestination!="chercheurcont")&&($this->menuDestination!="chercheurperm")&&($this->menuDestination!="chercheurinvi")&&($this->menuDestination!="chercheurarch")&&($this->menuDestination!="chercheuranon")) ) || ($_SESSION['p_id']==  $this->p_id)) {
if (($this->demandeIntranet=="0")||($this->chercheur=="1"))echo '<button type="submit" name="numAction" value="307" id="submit_click"> Enregistrer </button>';
}
echo '    &nbsp;&nbsp;&nbsp;        ';
echo '<span class="error">'. $erreur.'</span>';
 		include("TableauTheseModifie.php");
	}

	// CONTRACTUELS
	elseif ($rowh->st_id==2) {
if ( ( (($this->menuDestination!="chercheurlabo")&&($this->menuDestination!="chercheurstag")&&($this->menuDestination!="chercheurdoct")&&($this->menuDestination!="chercheurcont")&&($this->menuDestination!="chercheurperm")&&($this->menuDestination!="chercheurinvi")&&($this->menuDestination!="chercheurarch")&&($this->menuDestination!="chercheuranon")) ) || ($_SESSION['p_id']==  $this->p_id)) {
if (($this->demandeIntranet=="0")||($this->chercheur=="1"))echo '<button type="submit" name="numAction" value="308" id="submit_click"> Enregistrer </button>';
}
echo '    &nbsp;&nbsp;&nbsp;        ';
echo '<span class="error">'. $erreur.'</span>';
 		include("TableauContrat.php");
	}

	// INVITES
	elseif ($rowh->st_id==4) {
if ( ( (($this->menuDestination!="chercheurlabo")&&($this->menuDestination!="chercheurstag")&&($this->menuDestination!="chercheurdoct")&&($this->menuDestination!="chercheurcont")&&($this->menuDestination!="chercheurperm")&&($this->menuDestination!="chercheurinvi")&&($this->menuDestination!="chercheurarch")&&($this->menuDestination!="chercheuranon")) ) || ($_SESSION['p_id']==  $this->p_id)) {
if (($this->demandeIntranet=="0")||($this->chercheur=="1"))echo '<button type="submit" name="numAction" value="309" id="submit_click"> Enregistrer </button>';
}
echo '    &nbsp;&nbsp;&nbsp;        ';
echo '<span class="error">'. $erreur.'</span>';
 		include("TableauInvite.php");
	}

	//PERMANENTS
	elseif ($rowh->st_id==1) {
if ( ( (($this->menuDestination!="chercheurlabo")&&($this->menuDestination!="chercheurstag")&&($this->menuDestination!="chercheurdoct")&&($this->menuDestination!="chercheurcont")&&($this->menuDestination!="chercheurperm")&&($this->menuDestination!="chercheurinvi")&&($this->menuDestination!="chercheurarch")&&($this->menuDestination!="chercheuranon")) ) || ($_SESSION['p_id']==  $this->p_id)) {
if (($this->demandeIntranet=="0")||($this->chercheur=="1"))echo '<button type="submit" name="numAction" value="313" id="submit_click"> Enregistrer </button>';
}
echo '    &nbsp;&nbsp;&nbsp;        ';
echo '<span class="error">'. $erreur.'</span>';
 		include("TableauPermanent.php");
	}
echo '</form>';
echo '</div>';





}


//INFORMATIONS ADMIN
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo '<div id="admin" class="tabcontent">';
echo '<form enctype="multipart/form-data" id="modifier_personne" method="post"  action="../control/ControleurPersonne.php?numAction=303">';


// LES TAGS
echo '<input type="hidden" name="ajouthistorique" id="ajouthistorique" value="'.$this->ajouthistorique.'">';
echo '<input type="hidden" name="creation" id="creation" value="'.$this->creation.'">';
echo '<input type="hidden" name="ajoutfinancementstage" id="ajoutfinancementstage" value="'.$this->ajoutfinancementstage.'">';
echo '<input type="hidden" name="chercheur" id="chercheur" value="'.$this->chercheur.'">';
// A VERIFIER
echo '<input type="hidden" name="demandeIntranet" value="'.$this->demandeIntranet.'">';
echo '<input type="hidden" name="menuDestination" id="menuDestination" value="'.$this->menuDestination.'">';
echo '<input type="hidden" name="selectSite" id="selectSite" value="'.$this->selectSite.'">';
echo '<input type="hidden" name="selectStatut" id="selectStatut" value="'.$this->selectStatut.'">';
echo '<input type="hidden" name="selectType" id="selectType" value="'.$this->selectType.'">';
echo '<input type="hidden" name="selectEquipe" id="selectEquipe" value="'.$this->selectEquipe.'">';
echo '<input type="hidden" name="selectDate" id="selectDate" value="'.$this->selectDate.'">';


if ( ( (($this->menuDestination!="chercheurlabo")&&($this->menuDestination!="chercheurstag")&&($this->menuDestination!="chercheurdoct")&&($this->menuDestination!="chercheurcont")&&($this->menuDestination!="chercheurperm")&&($this->menuDestination!="chercheurinvi")&&($this->menuDestination!="chercheurarch")&&($this->menuDestination!="chercheuranon")) ) || ($_SESSION['p_id']==  $this->p_id)) {
if (($this->demandeIntranet=="0")||($this->chercheur=="1"))echo '<button type="submit" name="numAction" value="314" id="submit_click"> Enregistrer </button>';
}


echo '<span class="error">'. $erreur.'</span>';





echo "<H2> Informations Administratives </h2>";

echo '<input type="hidden" name="p_id" id="p_id" value="'.$this->p_id.'">';
echo '<label> Habilitation à Diriger les Recherches :</label>';
if ($this->hdr==1) echo ' <input  type="checkbox" name="hdr" id="hdr"  value="1" checked >';
else echo ' <input  type="checkbox" name="hdr"   value="1" >';
echo '<br><br>';

echo ' <label>Horaire Acces:</label>';
if ($this->horaireacces== "H1") echo '<INPUT type= "radio" name="horaireacces" id="horaireacces1" value="H1" checked> Horaire H1 8h-19h';
else echo '<INPUT type= "radio" name="horaireacces" id="horaireacces1"  value="H1" > Horaire H1 8h-19h';
if ($this->horaireacces== "H2") echo '<INPUT type= "radio" name="horaireacces" id="horaireacces2" value="H2" checked> Horaire H2 24h sur 24h';
else echo '<INPUT type= "radio" name="horaireacces" id="horaireacces2" value="H2" > Horaire H2 24-24h';

include("TableauParcoursNA.php");


echo '</form>';

echo '</div>';


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//INFORMATIONS PERSO
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo '<div id="info" class="tabcontent">';
echo '<form enctype="multipart/form-data" id="modifier_personne" method="post"  action="../control/ControleurPersonne.php?numAction=303">';


// LES TAGS
echo '<input type="hidden" name="ajouthistorique" id="ajouthistorique" value="'.$this->ajouthistorique.'">';
echo '<input type="hidden" name="creation" id="creation" value="'.$this->creation.'">';
echo '<input type="hidden" name="ajoutfinancementstage" id="ajoutfinancementstage" value="'.$this->ajoutfinancementstage.'">';
echo '<input type="hidden" name="chercheur" id="chercheur" value="'.$this->chercheur.'">';
// A VERIFIER
echo '<input type="hidden" name="demandeIntranet" value="'.$this->demandeIntranet.'">';
echo '<input type="hidden" name="menuDestination" id="menuDestination" value="'.$this->menuDestination.'">';
echo '<input type="hidden" name="selectSite" id="selectSite" value="'.$this->selectSite.'">';
echo '<input type="hidden" name="selectStatut" id="selectStatut" value="'.$this->selectStatut.'">';
echo '<input type="hidden" name="selectType" id="selectType" value="'.$this->selectType.'">';
echo '<input type="hidden" name="selectEquipe" id="selectEquipe" value="'.$this->selectEquipe.'">';
echo '<input type="hidden" name="selectDate" id="selectDate" value="'.$this->selectDate.'">';

//echo 'creation'.$this->creation;


if ( ( (($this->menuDestination!="chercheurlabo")&&($this->menuDestination!="chercheurstag")&&($this->menuDestination!="chercheurdoct")&&($this->menuDestination!="chercheurcont")&&($this->menuDestination!="chercheurperm")&&($this->menuDestination!="chercheurinvi")&&($this->menuDestination!="chercheurarch")&&($this->menuDestination!="chercheuranon")&&($this->menuDestination!="chercheurmafiche")) ) || ($_SESSION['p_id']==  $this->p_id)||($_SESSION['p_id']=="NEW") ) {
// Le dernier test permet a un chercheur de modifier ses informations personnelles
//serge
   if ($this->creation==0) {
//echo 'A'.$this->creation;
if (($this->demandeIntranet=="0")||($this->chercheur=="1"))echo '<button type="submit" name="numAction" value="304" id="submit_click"> Enregistrer </button>';
else echo '<button type="submit" name="numAction" value="304VAL" id="submit_click"> Valider la demande</button>';

if ($this->menuDestination!="chercheurmafiche") {
if ($this->archive==0) echo '&nbsp;&nbsp;&nbsp;<button type="submit" name="numAction" value="310" id="submit_click" onclick="return confirm (\'Etes vous sur de vouloir archiver la personne?\')" > Archiver la personne</button>';
else echo '<b> Personne archivée</b><button type="submit" name="numAction" value="311" id="submit_click" onclick="return confirm (\'Etes vous sur ?\')" > Désarchiver la personne</button>';
echo '        <button type="submit" name="numAction" value="315" id="submit_click" onclick="return confirm (\'Etes vous sur ?\')" > Supprimer</button>';
}
//echo '&nbsp;&nbsp;&nbsp;<button type="submit" name="numAction" value="303REC" id="submit_click"  > Recharger</button>';
}
else {
//echo 'B'.$this->creation;

	if (substr( $erreur, 0, 11 ) == "Un homonyme") {
	echo '<button type="submit" name="numAction" value="304FORCE" id="submit_click"> Enregistrer </button>';
	echo '   <br><button type="submit" name="numAction" value="304RECUP" id="submit_click"> Récupérer les données</button>';
	}
	else {
	echo '<button type="submit" name="numAction" value="304" id="submit_click"> Enregistrer </button>';
	}

}
}
echo '    &nbsp;&nbsp;&nbsp;        ';

//if (($erreur!="enregistré")||($erreur!="")||(substr( $erreur, 0, 11 ) == "Un homonyme")) echo    '<span class="error">* Champs requis:'.$erreur.'</span>';
echo '<span class="error">'. $erreur.'</span>';




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////::
// PREMIERE COLONNE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////::

////////////////////////////////////////////////
//PREMIERE LIGNE PREMIERE COLONNE
echo "<H2> Informations Personnelles </h2>";

echo '<input type="hidden" name="p_id" id="p_id" value="'.$this->p_id.'">';
//echo '<input type="hidden" name="numActionOrigine" id="numActionOrigine" value="'.$this->numActionOrigine.'">';
echo ' <label>H/F:</label> ';


if ($this->sexe== "H") echo '<INPUT type= "radio" name="sexe" id="sexe1" value="H" checked> H';
else echo '<INPUT type= "radio" name="sexe" id="sexe1"  value="H" > H';
if ($this->sexe== "F") echo '<INPUT type= "radio" name="sexe" id="sexe2" value="F" checked> F';
else echo '<INPUT type= "radio" name="sexe" id="sexe2" value="F" > F';

echo '<br><br>';



echo ' <label>Nom:</label> <input size=10 type="text" name="p_nom" id="p_nom" value="'. $this->p_nom.'"> <span  class="error">* </span> <br> <br>';
echo ' <label>Prenom:</label> <input size=10 type="text" name="p_prenom" id="p_prenom" value="'. $this->p_prenom.'"> <span  class="error">* </span> <br> <br>';




echo ' <label>Date de naissance : </label><input  type="text" name="dateNaissance" id="dateNaissance" class="dateNaiss" value="'. $this->dateNaissance.'">';
if (($this->creation==1) &&(($this->historiquen->st_idn==2)||($this->historiquen->st_idn==3)||($this->historiquen->st_idn==4)))echo '<span  class="error">* </span>';
if ($this->creation==0) echo '<span  class="error">* </span>';
echo '<br><br>';


echo ' <label>Email:</label> <input type="text" name="p_mail" id="p_mail" value="'. $this->p_mail.'"> <span  class="error">* </span> <br> <br>';
echo ' <label>Email perso:</label> <input type="text" name="p_mail_perso" id="p_mail_perso" value="'. $this->p_mail_perso.'">  <br> <br>';



echo '<label>Nationalité :</label> <span  class="error">* </span>' ;
require_once("../vues/pays.php");
echo '<select  name="nationalite" id="nationalite" >';
        foreach($countries as $key => $value) {

        if (isset ( $this->nationalite ) && $this->nationalite == $key) echo '<option value="'. $key .'" selected>'. htmlspecialchars($value) .'</option>';
        else echo '<option value="'. $key .'" >'. htmlspecialchars($value) .'</option>';

        }

echo '</select>';


echo '<br><br><hr>';



echo ' <label>Télephone:</label> <input size=15 type="text" name="p_tel" id="p_tel" value="'. $this->p_tel.'">  <br> <br>';
echo ' <label>Télephone Personnel:</label> <input size=15 type="text" name="p_tel_perso" id="p_tel_perso" value="'. $this->p_tel_perso.'"> <br> <br>';
echo ' <label>Identifiant Orcid:</label> <input size=19 type="text" name="orcid" id="orcid" value="'. $this->orcid.'"> <br> <br>';
echo ' <label>Identifiant HAL:</label> <input size=50 type="text" name="IdHAL" id="IdHAL" value="'. $this->IdHAL.'"> <br> <br>';



echo ' <label>Page Perso:</label> <input type="text" name="p_page_perso" id="p_page_perso" value="'. $this->p_page_perso.'"> <br> <br>';
echo ' <label>Commentaires:</label> <input type="text" name="p_commentaire" id="p_commentaire" value="'. $this->p_commentaire.'">  <br> <br>';

echo ' <label>Personne a Prevenir:</label> <input type="text" name="personneaprevenir" id="personneaprevenir" size="20" value="'. $this->personneaprevenir.'">  <br> <br>';
//serge
echo '<br><br><hr>';
echo '<h3> COLLEGES ELECTORAUX </h3>';

echo ' <label>Section CNRS:</label> <input type="text" name="sectionCNRS" id="sectionCNRS" value="'. $this->sectionCNRS.'">  ';

        echo '<td> <label> collège CNRS: </label><select  name="collegeCNRSId" id="collegeCNRSId" >';
	echo '<option value="" disabled selected >Choisissez</option>';

                                foreach ( $listeCollege as $row ) {
$collegeNom= $row->collegeNom;
$collegeId= $row->collegeId;


                                        if ($collegeId == $this->collegeCNRSId  )  {
                                                echo '<option selected="selected" value=" ' . $collegeId . '"> ' . $collegeNom . '</option>';
                                        } else {
                                                echo '<option value=" ' . $collegeId . '"> ' . $collegeNom . '</option>';
                                        }

                                }
       echo '</select></td><br>';




echo ' <label>Section CNU:</label> <input type="text" name="sectionCNU" id="sectionCNU" value="'. $this->sectionCNU.'">  ';
        echo '<td><label> Collège CNU : </label><select  name="collegeCNUId" id="collegeCNUId" >';
	echo '<option value="" disabled selected >Choisissez</option>';

                                foreach ( $listeCollege as $row ) {
$collegeNom= $row->collegeNom;
$collegeId= $row->collegeId;


                                        if ($collegeId == $this->collegeCNUId  )  {
                                                echo '<option selected="selected" value=" ' . $collegeId . '"> ' . $collegeNom . '</option>';
                                        } else {
                                                echo '<option value=" ' . $collegeId . '"> ' . $collegeNom . '</option>';
                                        }

                                }
       echo '</select><br></td>';
echo ' <label>Commission IRD:</label> <input type="text" name="commissionIRD" id="commissionIRD" value="'. $this->commissionIRD.'">  ';
        echo '<td><label> Collège IRD: </label> <select  name="collegeIRDId" id="collegeIRDId" >';
	echo '<option value="" disabled selected >Choisissez</option>';

                                foreach ( $listeCollege as $row ) {
$collegeNom= $row->collegeNom;
$collegeId= $row->collegeId;


                                        if ($collegeId == $this->collegeIRDId  )  {
                                                echo '<option selected="selected" value=" ' . $collegeId . '"> ' . $collegeNom . '</option>';
                                        } else {
                                                echo '<option value=" ' . $collegeId . '"> ' . $collegeNom . '</option>';
                                        }

                                }
       echo '</select><br></td>';















echo '</form>';

echo '</div>';

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HISTORIQUE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo '<div id="historique" class="tabcontent">';
echo '<form enctype="multipart/form-data" id="modifier_personne" method="post"  action="../control/ControleurPersonne.php?numAction=303">';

echo '<input type="hidden" name="ajouthistorique" id="ajouthistorique" value="'.$this->ajouthistorique.'">';
echo '<input type="hidden" name="creation" id="creation" value="'.$this->creation.'">';
echo '<input type="hidden" name="ajoutfinancementstage" id="ajoutfinancementstage" value="'.$this->ajoutfinancementstage.'">';
echo '<input type="hidden" name="chercheur" id="chercheur" value="'.$this->chercheur.'">';


// A VERIFIER
echo '<input type="hidden" name="demandeIntranet" value="'.$this->demandeIntranet.'">';

echo '<input type="hidden" name="p_id" id="p_id" value="'.$this->p_id.'">';
//echo '<input type="hidden" name="numActionOrigine" id="numActionOrigine" value="'.$this->numActionOrigine.'">';
echo '<input type="hidden" name="menuDestination" id="menuDestination" value="'.$this->menuDestination.'">';
echo '<input type="hidden" name="selectSite" id="selectSite" value="'.$this->selectSite.'">';
echo '<input type="hidden" name="selectStatut" id="selectStatut" value="'.$this->selectStatut.'">';
echo '<input type="hidden" name="selectType" id="selectType" value="'.$this->selectType.'">';
echo '<input type="hidden" name="selectEquipe" id="selectEquipe" value="'.$this->selectEquipe.'">';
echo '<input type="hidden" name="selectDate" id="selectDate" value="'.$this->selectDate.'">';



if ( ( (($this->menuDestination!="chercheurlabo")&&($this->menuDestination!="chercheurstag")&&($this->menuDestination!="chercheurdoct")&&($this->menuDestination!="chercheurcont")&&($this->menuDestination!="chercheurperm")&&($this->menuDestination!="chercheurinvi")&&($this->menuDestination!="chercheurarch")&&($this->menuDestination!="chercheuranon")) ) || ($_SESSION['p_id']==  $this->p_id)) {
// Le dernier test permet a un chercheur de modifier ses informations personnelles

   if ($this->creation==0) {
if (($this->demandeIntranet=="0")||($this->chercheur=="1"))echo '<button type="submit" name="numAction" value="305" id="submit_click"> Enregistrer </button>';


}
else {
echo '<button type="submit" name="numAction" value="305" id="submit_click"> Enregistrer </button>';
if (substr( $erreur, 0, 11 ) == "Un homonyme") echo '   <br><button type="submit" name="numAction" value="304FORCE" id="submit_click"> Forcer la création </button>';

}
}
echo '    &nbsp;&nbsp;&nbsp;        ';

//if (($erreur!="enregistré")||($erreur!="")||(substr( $erreur, 0, 11 ) == "Un homonyme")) echo    '<span class="error">* Champs requis:'.$erreur.'</span>';
echo '<span class="error">'. $erreur.'</span>';

echo '<table  border="0">';
echo "<H2> Historique de la présence sur site:</h2>";
echo '<tr><td>Actions</td><td>Site <span class="error">* </span></td><td>Statut <span class="error">* </span></td><td>Type <span class="error">* </span></td><td>Date Debut Présence HSM <span class="error">* </span></td>';
if ($this->historiquen->st_idn==1) echo '<td>Date Fin</td>';
else echo '<td>Date Fin Présence HSM<span class="error">* </span></td>';
//$lignes = $this->historique->fetch(PDO::FETCH_OBJ);
//echo $lignes->numHistorique;
echo '<td colspan=3>Equipes<span class="error">* </span></td></tr>';


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// On vient de cliquer sur le bouton + pour ajouter une ligne historique
// on ou ajoute 
if (($this->ajouthistorique==1) || $this->creation==1)  {
echo '<tr><td>a sauvegarder</td>';
//serge
//        echo ' <td>   '.  $this->historiquen->stageIdn.'</td>';
        echo '                <input type="hidden" name="stageIdn" id="stageIdn" value="'. $this->historiquen->stageIdn.'"> ';
        echo '                <input type="hidden" name="theseIdn" id="theseIdn" value="'. $this->historiquen->theseIdn.'"> ';
        echo '                <input type="hidden" name="contractuel_idn" id="contractuel_idn" value="'. $this->historiquen->contractuel_idn.'"> ';
        echo '                <input type="hidden" name="invite_idn" id="invite_idn" value="'. $this->historiquen->invite_idn.'"> ';
        echo '                <input type="hidden" name="permanent_idn" id="permanent_idn" value="'. $this->historiquen->permanent_idn.'"> ';





	echo '<td><select name="sit_idn" id="sit_idn" >';
	echo '<option value="" disabled selected >Choisissez</option>';
	foreach ( $listeSite as $row ) {
	        if (isset ( $this->historiquen )&&  $this->historiquen->sit_idn == $row->sit_id) {
	                echo '<option value="'.$row->sit_id.'" selected>'. $row->sit_nom.'</option>';
	        	} else {
		            echo '<option value="'.$row->sit_id.'">'. $row->sit_nom.'</option>';
		}
			
	}
	echo '</select></td>';



 	 echo '<td><select  name="st_idn" id="st_idn" onchange="this.form.submit()">';

	echo '<option value="" disabled selected >Choisissez</option>';
                                foreach ( $listeStatut as $rowStatut ) {


	        			if (isset ( $this->historiquen )&&  $this->historiquen->st_idn == $rowStatut->st_id) {
                                                echo '<option selected="selected" value=" ' . $rowStatut->st_id . '"> ' . $rowStatut->st_nom . '</option>';
                                        } else {
                                                echo '<option value=" ' . $rowStatut->st_id . '"> ' . $rowStatut->st_nom . '</option>';
                                        }

                                }
                                echo '</select></td>';


		 echo '<td><select  name="ty_idn" id="ty_idn" onchange="this.form.submit()">';
echo '<option value="" disabled selected >Choisissez</option>';

                foreach ( $listeType as $rowType ) {
			// on affiche les types de personnels uniquement pour le statut sélectionné
			if ($rowType->st_id == $this->historiquen->st_idn) {

	        	if (isset ( $this->historiquen )&&  $this->historiquen->ty_idn == $rowType->ty_id ) {

                                echo '<option selected="selected" value=" ' . $rowType->ty_id . '"> ' . $rowType->ty_nom.'</option>';
                        } else {
                                echo '<option value=" ' . $rowType->ty_id . '"> ' . $rowType->ty_nom . '</option>';
                        }
			}
                }

                echo '</select></td>';



        echo '                <td><input type="text" size="5" class="date" name="dateDebutHn" id="dateDebutHn" value="'. $this->historiquen->dateDebutHn.'"></td> ';
        echo '                <td><input type="text" size="5" class="date" name="dateFinHn" id="dateFinHn" value="'. $this->historiquen->dateFinHn.'"></td> ';




      echo '                <td>';
        echo '<select name="listeEquipePersonnen[]" id="listeEquipePersonnen" size="2" multiple="multiple">';
        foreach ( $listeEquipe as $rowe )  {
		foreach ($this->historiquen->eq_idn as $value) {
			
               if  ($rowe->eq_id == $value) echo "<option value= ".$rowe->eq_id.">". $rowe->eq_nom ." </option>";
		}
           }
        echo"</select>";
	// DANS un select on ne peut récupérer que les valeurs sélectionnées, ici on veut récupérer toutes les données dans le formulaire
	// https://stackoverflow.com/questions/6547209/passing-an-array-using-an-html-form-hidden-element
foreach($this->historiquen->eq_idn as $value)
{
    echo '<input type="hidden" name="eq_idn[]" value="'. $value. '">';
}

        echo '                </td>';
        echo '                <td>';
        echo '                <button type="submit" name="numAction" value="303AEQn" class="button" > <- </button>';
        echo '                        <br>';
        echo '                 <button type="submit" name="numAction" value="303SEQn" class="button" > -> </button>';
        echo '                </td>';
        echo '                <td>';
        echo '<select name="listeEquipen[]" id="listeEquipen" size="2" multiple="multiple">';
        foreach ( $listeEquipe as $rowe )  {
                echo "<option value= ".$rowe->eq_id.">". $rowe->eq_nom ." </option>";
           }
        echo"</select>";
        echo '                </td>';

echo '</tr>';



//var_dump($this->historiquen->ressourcen);

echo ' <tr><td colspan=9> <label> Utilisation des ressources suivantes :</label>';
echo '<ul>';
foreach ($listeRessource as $ressource) if ($ressource->rid != 1) {
	echo '<li> <label>'.$ressource->nom.'</label>';



if (in_array($ressource->rid,$this->historiquen->ressourcen))	echo '<input  type="checkbox" name="'.$ressource->rid.'n"   value="1" checked>';
else	echo '<input  type="checkbox" name="'.$ressource->rid.'n"   value="1" >';

	}
echo '</ul>';
echo '</td></tr>';


}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$i=0;
foreach ($this->historique as $row)  {
echo '<tr>';
        echo '                <input type="hidden" name="numHistorique'.$i.'" id="numHistorique'.$i.'" value="'. $row->numHistorique.'"> ';
        echo '                <input type="hidden" name="stageId'.$i.'" id="stageId'.$i.'" value="'. $row->stageId.'"> ';
        echo '                <input type="hidden" name="theseId'.$i.'" id="theseId'.$i.'" value="'. $row->theseId.'"> ';
        echo '                <input type="hidden" name="contractuel_id'.$i.'" id="contractuel_id'.$i.'" value="'. $row->contractuel_id.'"> ';
        echo '                <input type="hidden" name="invite_id'.$i.'" id="invite_id'.$i.'" value="'. $row->invite_id.'"> ';
        echo '                <input type="hidden" name="permanent_id'.$i.'" id="permanent_id'.$i.'" value="'. $row->permanent_id.'"> ';


$suite= '&'.$this->selectSite.'&'.$this->selectStatut.'&'.$this->selectType.'&'.$this->selectEquipe.'&'.$this->selectDate;

echo '<td>';
//	echo '<td><button type="submit" name="numAction" value="303AFF'.$i.$suite.'" class="btn-link">Voir détails</button>';
//	echo '<td><button type="submit" name="numAction" value="303AFF'.$i.'" class="btn-link">Voir détails</button>';


	if (($this->droitadminPersonne==1)||($this->chercheur == "1")) echo '<button type="submit" name="numAction" value="303DEH'.$i.'" onclick="return confirm (\'Etes vous sur de vouloir supprimer cet historique?\')" class="btn-link">SUPPRIMER</button>';
	if (($this->droitadminPersonne==1)||($this->chercheur == "1")) echo '<button type="submit" name="numAction" value="303DUP'.$i.'" onclick="return confirm (\'Etes vous sur de vouloir dupliquer cet historique?\')" class="btn-link">DUPLIQUER</button>';


	   echo '<td><select name="sit_id'.$i.'" id="sit_id'.$i.'">';
                foreach ( $listeSite as $rowSite ) {
                        if (isset ( $row->sit_id ) && $row->sit_id == $rowSite->sit_id) {
                           echo '<option value="'.$rowSite->sit_id.'" selected>'. $rowSite->sit_nom.'</option>';
                        } else {
                           echo '<option value="'.$rowSite->sit_id.'">'. $rowSite->sit_nom.'</option>';
                        }
                }
                echo '</select></td>';

 	 echo '<td><select  name="st_id'.$i.'" id="st_id'.$i.'" onchange="this.form.submit()">';

                                foreach ( $listeStatut as $rowStatut ) {


                                        if (isset ( $row->st_id ) && $row->st_id == $rowStatut->st_id) {
                                                echo '<option selected="selected" value=" ' . $rowStatut->st_id . '"> ' . $rowStatut->st_nom . '</option>';
                                        } else {
                            //                    echo '<option value=" ' . $rowStatut->st_id . '"> ' . $rowStatut->st_nom . '</option>';
                                        }

                                }
                                echo '</select></td>';


		 echo '<td><select  name="ty_id'.$i.'" id="ty_id'.$i.'">';

                foreach ( $listeType as $rowType ) {

                        if (isset ( $row->ty_id ) && ($row->ty_id == $rowType->ty_id) && ($row->st_id == $rowType->st_id)) {
                                echo '<option selected="selected" value=" ' . $rowType->ty_id . '"> ' . $rowType->ty_nom . '</option>';
                        } else {
                          //      echo '<option value=" ' . $rowType->ty_id . '"> ' . $rowType->ty_nom . '</option>';
                        }
                }

                echo '</select></td>';





        echo '                <td><input type="text" size="5" class="date" name="dateDebutH'.$i.'" id="dateDebutH'.$i.'" value="'. $row->dateDebutH.'"></td> ';
        echo '                <td><input type="text" size="5" class="date" name="dateFinH'.$i.'" id="dateFinH'.$i.'" value="'. $row->dateFinH.'"></td> ';


	echo '                <td>';
	echo '<select name="listeEquipePersonne'.$i.'[]" id="listeEquipePersonne"  size="2" multiple="multiple">';
	foreach ( $this->listeEquipePersonne[$row->numHistorique] as $eq_id => $eq_nom )  echo "<option value= ".$eq_id.">". $eq_nom ." </option>";
	echo"</select>";
//        echo '<select  name="listeEquipePersonne'.$i.'[]" id="listeEquipePersonne'.$i.'" multiple="multiple">';
//	echo '<select name="listeEquipePersonne'.$i.'" id="listeEquipePersonne'.$i.'" size="2" >';
//	foreach ( $this->listeEquipePersonne[$row->numHistorique] as $eq_id => $eq_nom )  echo "<option value= ".$eq_id.">". $eq_nom ." </option>";
//	echo"</select>";

	echo '                </td>';
	echo '                <td>';
	echo '                <button type="submit" name="numAction" value="303AEQ'.$i.'" class="button" > <- </button>';
	echo '                        <br>';
	echo '                 <button type="submit" name="numAction" value="303SEQ'.$i.'" class="button" > -> </button>';
	echo '                </td>';
	echo '                <td>';
	echo '<select name="listeEquipe'.$i.'[]" id="listeEquipe" size="2" multiple="multiple">';
	foreach ( $listeEquipe as $rowe )  {
   		echo "<option value= ".$rowe->eq_id.">". $rowe->eq_nom ." </option>";
           }
  	echo"</select>";
	echo '                </td>';


echo '</tr>';

echo ' <tr><td colspan=9> <label> Utilisation des ressources suivantes :</label>';
echo '<ul>';

$lister=array();
// explode separately 
$arr = explode(';', $row->ressources);
foreach ($arr as $val) {array_push($lister,$val);}



foreach ($listeRessource as $ressource) if ($ressource->rid != 1) {
        echo '<li> <label>'.$ressource->nom.'</label>';



if (in_array($ressource->rid,$lister))   echo '<input  type="checkbox" name="'.$ressource->rid.$i.'"   value="1" checked>';
else    echo '<input  type="checkbox" name="'.$ressource->rid.$i.'"   value="1" >';

        }
echo '</ul>';
echo '</td></tr>';

        $i++;


}

 echo '<input type="hidden" name="nbh" value="'.$i.'">';




if ((($this->droitadminPersonne==1)||($this->chercheur=="1")) && ($this->ajouthistorique==0) && ($this->creation==0)) echo ' <tr><td colspan= 9><button type="submit" name="numAction" value="303ADH" class="btn-link">Ajouter une période</button></tr>';

echo '</table>';
echo '</form>';
echo '</div>';



// CAHIERS DE LABORATOIRE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo '<div id="cahier" class="tabcontent">';
echo '<form enctype="multipart/form-data" id="modifier_personne" method="post"  action="../control/ControleurPersonne.php?numAction=303">';

echo '<input type="hidden" name="ajouthistorique" id="ajouthistorique" value="'.$this->ajouthistorique.'">';
echo '<input type="hidden" name="creation" id="creation" value="'.$this->creation.'">';
echo '<input type="hidden" name="ajoutfinancementstage" id="ajoutfinancementstage" value="'.$this->ajoutfinancementstage.'">';
echo '<input type="hidden" name="chercheur" id="chercheur" value="'.$this->chercheur.'">';


// A VERIFIER
echo '<input type="hidden" name="demandeIntranet" value="'.$this->demandeIntranet.'">';

echo '<input type="hidden" name="p_id" id="p_id" value="'.$this->p_id.'">';
//echo '<input type="hidden" name="numActionOrigine" id="numActionOrigine" value="'.$this->numActionOrigine.'">';
echo '<input type="hidden" name="menuDestination" id="menuDestination" value="'.$this->menuDestination.'">';
echo '<input type="hidden" name="selectSite" id="selectSite" value="'.$this->selectSite.'">';
echo '<input type="hidden" name="selectStatut" id="selectStatut" value="'.$this->selectStatut.'">';
echo '<input type="hidden" name="selectType" id="selectType" value="'.$this->selectType.'">';
echo '<input type="hidden" name="selectEquipe" id="selectEquipe" value="'.$this->selectEquipe.'">';
echo '<input type="hidden" name="selectDate" id="selectDate" value="'.$this->selectDate.'">';



        echo "<H2> Cahiers de laboratoire </h2>";
 if ($this->creation==0) {
     if ($listeCahiers->rowCount () > 0) {
                echo '<table>';
                echo '<tr><th>Numéro</th><th>Date début</th><th>Date fin</th></tr>';

// Pour les chercheurs, on reste en mode "chercheur" lorsqu'on clique sur un cahier
if (substr( $this->menuDestination, 0, 9 ) == 'chercheur') $plus="&menuDestination=chercheurcahier";
else $plus="&menuDestination=admincahier";
                foreach ($listeCahiers as $row) {
                                echo '<tr>';
                                echo '<td><a href="ControleurCahier.php?numAction=172&menuDestination=admincahier'.$plus.'&cahierNum=' .$row->cahierNum .'">'.$row->cahierNum.'</a></td><td>'.$row->dateDotation.'</td><td>'.$row->dateRetour.'</td>';
                                echo '</tr>';
                        }
                echo '</table>';

        }
else echo "<i> aucun cahier </i>";
}

echo '</form>';
echo '</div>';

//RESERVATIONS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




echo '<div id="reservation" class="tabcontent">';
echo '<form enctype="multipart/form-data" id="modifier_personne" method="post"  action="../control/ControleurPersonne.php?numAction=303">';

echo '<input type="hidden" name="ajouthistorique" id="ajouthistorique" value="'.$this->ajouthistorique.'">';
echo '<input type="hidden" name="creation" id="creation" value="'.$this->creation.'">';
echo '<input type="hidden" name="ajoutfinancementstage" id="ajoutfinancementstage" value="'.$this->ajoutfinancementstage.'">';
echo '<input type="hidden" name="chercheur" id="chercheur" value="'.$this->chercheur.'">';


// A VERIFIER
echo '<input type="hidden" name="demandeIntranet" value="'.$this->demandeIntranet.'">';

echo '<input type="hidden" name="p_id" id="p_id" value="'.$this->p_id.'">';
//echo '<input type="hidden" name="numActionOrigine" id="numActionOrigine" value="'.$this->numActionOrigine.'">';
echo '<input type="hidden" name="menuDestination" id="menuDestination" value="'.$this->menuDestination.'">';
echo '<input type="hidden" name="selectSite" id="selectSite" value="'.$this->selectSite.'">';
echo '<input type="hidden" name="selectStatut" id="selectStatut" value="'.$this->selectStatut.'">';
echo '<input type="hidden" name="selectType" id="selectType" value="'.$this->selectType.'">';
echo '<input type="hidden" name="selectEquipe" id="selectEquipe" value="'.$this->selectEquipe.'">';
echo '<input type="hidden" name="selectDate" id="selectDate" value="'.$this->selectDate.'">';

//dateDebutH0 sit_id0


foreach ($this->historique as $row) {
  echo '                <input type="hidden" name="dateDebutH0" id="dateDebutH0" value="'. $row->dateDebutH.'"> ';
  echo '                <input type="hidden" name="dateFinH0" id="dateFinH0" value="'. $row->dateFinH.'"> ';
  echo '                <input type="hidden" name="sit_id0" id="sit_id0" value="'. $row->sit_id.'"> ';

break;
}




        echo "<H2> Réservations </h2>";
echo '<span class="error">'. $erreur.'</span>';
 if ($this->creation==0) {
     if ($listereservation->rowCount () > 0) {


  echo '<p id="reservation_existante">Réservations existantes pour cette personne :</p>';


                        echo '<table id="reservations" class="table resultats-requete">';
                        echo "<tr class='PremierReservation'><th>Date de début</th><th>Date de fin</th><th>Numéro de salle</th><th>Site géographique</th><th>Option</th></tr>";
// Pour les chercheurs, on reste en mode "chercheur" lorsqu'on clique sur une salle
if (substr( $this->menuDestination, 0, 9 ) == 'chercheur') $plus="&menuDestination=chercheursalle";
else $plus="";
                        while ( $row = $listereservation->fetch ( PDO::FETCH_ASSOC ) ) {
                                echo '<tr>
                                <td>' . $row ['r_date_debut'] . '<input type="hidden" name="dateDebut'.$row[r_id].'" value="'.$row['r_date_debut'].'"</td>';
                                //<td>' . $row ['r_date_debut'] . '<input type="hidden" name="r_date_debut" value="'.$row['r_date_debut'].'"</td>';

                        if ($row['r_date_fin']== '9999-12-31') echo ' <td><input type="hidden" name="r_id" value="'.$row['r_id'].'"><input type="text" size="10" name="dateFin'.$row[r_id].'" class="date" placeholder="Insérer une date de fin"></td>';
                                else {
                                        echo '<td><input type="text" size="8" class="date" name="dateFin'.$row[r_id].'" value="' . $row ['r_date_fin'] . '"></td>';
                                        echo '<input type="hidden" class="date" name="dateFinOri'.$row[r_id].'" value="' . $row ['r_date_fin'] . '">';
                                }





                                echo '<td><a href="ControleurSalles.php?numAction=162'.$plus.'&id='.$row['pi_id'].'">'.$row['pi_numero'] .'</a></td>
                                <td>' . $row ['sit_nom'] . '</td>';
                                if ($this->droitadminResa==1) {
                                        echo '<td>' ;
                                        echo '<button type="submit" name="numAction" value="402'.$row["r_id"].'" id="submit_click"> Supprimer </button>';
                                        if ($row['r_date_fin']== '9999-12-31')
                                                echo '<button type="submit" name="numAction" value="403'.$row["r_id"].'" id="submit_click"> Insérer Date de Fin </button>';
                                        else    echo '<button type="submit" name="numAction" value="404'.$row["r_id"].'" id="submit_click"> Modifier Date de Fin </button>';



                               echo '</td>';
                                }
                        echo '</tr>';
                        }
                        echo "</table>";
                }
if ($this->droitadminResa==1) echo '<br><button type="submit" name="numAction" value="400" id="submit_click"> Faire une nouvelle réservation </button>';
}

echo '</form>';
echo '</div>';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////:




echo '</div>';









echo "<script type='text/javascript'>";

/////////////////////////////////////////////////////////////////////////////////////
echo ' function clicked() {';
echo '        if (confirm("Do you want to submit?")) {';
echo '           yourformelement.submit();';
echo '       } else {';
echo '           return false;';
echo '       }';
echo '    }';


echo 'function openTab(evt, cityName) {';
  // Declare all variables
echo '  var i, tabcontent, tablinks;';

  // Get all elements with class="tabcontent" and hide them
echo '  tabcontent = document.getElementsByClassName("tabcontent");';
echo '  for (i = 0; i < tabcontent.length; i++) {';
echo '    tabcontent[i].style.display = "none";';
echo '  }';

  // Get all elements with class="tablinks" and remove the class "active"
echo '  tablinks = document.getElementsByClassName("tablinks");';
echo '  for (i = 0; i < tablinks.length; i++) {';
echo '    tablinks[i].className = tablinks[i].className.replace(" active", "");';
echo '  }';

  // Show the current tab, and add an "active" class to the button that opened the tab
echo '  createCookie("tab", cityName, "10");';
echo '  document.getElementById(cityName).style.display = "block";';
echo '  evt.currentTarget.className += " active";';
echo '}';




// Get the element with id="defaultOpen" and click on it
//echo 'document.getElementById("defaultOpen").click();';
//echo 'document.getElementById("info").click();';
//echo 'document.getElementById("tabhistorique").click();';
//echo 'document.getElementById("'.$tab.'").click();';

/// SELECTION DE ONGLET A OUVRIR
// Get the element with id="defaultOpen" and click on it
//echo 'document.getElementById("tabinfo").click();';
//$val="tab".$_COOKIE["tab"];
$val="tab".$tab;
//$val="tabhistorique";
echo 'document.getElementById("'.$val.'").click();';







echo '$(document).ready(function () {';
echo '  createCookie("height", $(window).height(), "10");';
echo '});';

echo 'function createCookie(name, value, days) {';
echo '  var expires;';
echo '  if (days) {';
echo '    var date = new Date();';
echo '    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));';
echo '    expires = "; expires=" + date.toGMTString();';
echo '  }';
echo '  else {';
echo '    expires = "";';
echo '  }';
echo '  document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";';
echo '}';

echo '    function checkMe() {';

echo '        alert("Selected text is " );';
echo '    }';
?>


function klikaj() {
alert ("toto");
}
<?php
echo "</script>";













}
}
?>
