﻿<?php
class PageCahierModifie {


  private $cahierNum;
  private $modele;
  private $p_id;
  private $dateDotation;
  private $fonction;
  private $eq_id;
  private $dateRetour;
  private $localisation;
  private $commentaires;

private $listeutilisateur;
private $utilisateurnouveau;


private $creation;
private $ajoututil;
private $menuDestination;

public function __construct($creation,$ajoututil,$tab,$listeutilisateur,$utilisateurnouveau,$menuDestination)  {
        $this->cahierNum = $tab['cahierNum'];
        $this->modele = $tab['modele'];
        $this->fonction = $tab['fonction'];
        $this->eq_id = $tab['eq_id'];
        $this->localisation = $tab['localisation'];
        $this->commentaires = $tab['commentaires'];

$this->listeutilisateur = $listeutilisateur;
$this->utilisateurnouveau = $utilisateurnouveau;
$this->menuDestination = $menuDestination;


$this->creation = $creation;
$this->ajoututil = $ajoututil;
}


public function afficher($erreur,$listePersonne,$listeEquipe,$listeProjets,$listeCahierlaboProjetListe){



        $arrayProjets=array();
        foreach ($listeProjets as $row) $arrayProjets[$row->projet_id]= $row->acronyme;



echo '<div id="FormulairesModificationPersonne">';
if ($this->creation==1) echo "<h2>Ajouter un cahier: </h2>";
else echo "<h2>Modifier un cahier: </h2>";
echo '  <p>';
if (!($erreur=="")) echo '              <span class="error"> '.$erreur.'</span>';
echo '  </p>';
echo '<form method="post"  action="../control/ControleurCahier.php" >';

echo '<input type="hidden" name="menuDestination" id="menuDestination" value="'.$this->menuDestination.'">';


echo '<table>';







echo '                <tr><td><label> Cahier numéro :</label></td><td colspan=3> <input type="text" name="cahierNum" id="cahierNum" value="'. $this->cahierNum.'"> <span class="error">* </span></td></tr>';
echo '                <tr><td><label> Modèle:</label> </td><td colspan=3><input type="text" name="modele" id="modele" value="'. $this->modele.'"> </td></tr>';

echo "<tr><td><label> Attribué à :</label></td><td>Date Attribution</td><td>Date Retour</td><td>Suppression ?</td></tr> ";


$i=0;
foreach ($this->listeutilisateur as $row)  {
	echo '<tr><td><select name="p_id'.$i.'" id="p_id'.$i.'" size="1"  >';
	echo '<option value="" disabled selected >Choisissez</option>';
	foreach ($listePersonne as $rowPersonne)  {
	        if ($rowPersonne->p_id ==$row->p_id)  echo "<option value= ".$rowPersonne->p_id." selected>". $rowPersonne->p_nom .' '.$rowPersonne->p_prenom." </option>";
	         else      echo "<option value= ".$rowPersonne->p_id.">". $rowPersonne->p_nom .' '.$rowPersonne->p_prenom." </option>";
	}
	 echo"</select></td>";
	echo '                <td><input type="text" class="date" name="dateDotation'.$i.'" id="dateDotation'.$i.'" value="'. $row->dateDotation.'"> </td>';
	echo '                <td><input type="text" class="date" name="dateRetour'.$i.'" id="dateRetour'.$i.'" value="'. $row->dateRetour.'"> </td>';
	echo ' <td><button type="submit" name="numAction" value="172DEL'.$i.'" class="btn-link">-</button></td></tr>';
	//echo '<td><a href="ControleurCahier.php?numAction=172DEL&cahierNum=' .$row->cahierNum .'&supp='.$rowPersonne->p_id.'">Supprimer</a></td></tr>';
	$i++;
}




 echo '<input type="hidden" name="nblisteutilisateur" value="'.$i.'">';




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// LES NOUVEAUX
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (($this->creation==1)||($this->ajoututil==1)) {
	echo '<tr><td><select name="np_id" id="np_id" size="1"  >';
	echo '<option value="" disabled selected >Choisissez</option>';
	foreach ($listePersonne as $rowPersonne)  {
	         if ($rowPersonne->p_id ==$this->utilisateurnouveau->p_id)  {
			echo "<option value= ".$rowPersonne->p_id." selected>". $rowPersonne->p_nom .' '.$rowPersonne->p_prenom." </option>";
		 }
	         else {
		        echo "<option value= ".$rowPersonne->p_id.">". $rowPersonne->p_nom .' '.$rowPersonne->p_prenom." </option>";
		 }

	}
	 echo"</select></td>";
	echo '                <td><input type="text" class="date" name="ndateDotation" id="ndateDotation" value='.$this->utilisateurnouveau->dateDotation.'> </td>';
	echo '                <td><input type="text" class="date" name="ndateRetour" id="ndateRetour" value='.$this->utilisateurnouveau->dateRetour.'> </td></tr>';
	$i++;



}


else {
	echo '<tr><td colspan =4 > ';
	echo ' <button type="submit" name="numAction" value="172ADD" class="btn-link">+</button>';
	echo '</td></tr>';
}

echo '<tr><td><label> Equipe: </label></td>';
echo '<td colspan=3><select name="eq_id" id="eq_id" size=1 >';
echo '<option value="" disabled selected >Choisissez</option>';
foreach ( $listeEquipe as $row ) {
        if (isset ( $this->eq_id ) && $this->eq_id == $row->eq_id) {
                echo '<option value="'.$row->eq_id.'" selected>'. $row->eq_nom.'</option>';
        } else {
            echo '<option value="'.$row->eq_id.'">'. $row->eq_nom.'</option>';
         }
}
echo '</select>';
echo '</td></tr>';


echo '                <tr><td><label> Fonction:</label> </td><td colspan=3><input type="text" name="fonction" id="fonction" value="'. $this->fonction.'">  </td></tr>';



echo '                <tr><td><label> Localisation :</label> </td><td colspan=3><input type="text" name="localisation" id="localisation" value="'.  $this->localisation.'">  </td></tr>';
echo '                <tr><td><label> Commentaires :</label> </td><td colspan=3><input type="text" name="commentaires" id="commentaires" value="'.  $this->commentaires.'">  </td></tr>';


echo '</table>';



// Projets
//serge
if (!($this->creation==1)) {
 echo "<h2>Ce cahier est affecté aux projets: </h2>";

echo '<table>        <tr>';



#MEMBRES
echo '                <td>';

echo "<select name='listeCahierlaboProjet[]' id='listeCahierlaboProjet' size='10' multiple='multiple'>";
foreach ($listeCahierlaboProjetListe as $row) 
{
                        echo "<option value= ".$row->projet_id.">".$arrayProjets[$row->projet_id]." </option>";
}
echo"</select>";

echo '                </td>';
echo '                <td>';
echo '                <button type="submit" name="numAction" value="115" class="button" > <- </button>';
echo '                        <br>';
echo '                 <button type="submit" name="numAction" value="116" class="button" > -> </button>';
echo '                </td>';

echo '                <td>';

echo "<select name='listeProjets[]' id='listeProjets' size='10' multiple='multiple'>";
foreach ($arrayProjets as $key => $value) {
                                echo "<option value= ".$key.">".$value." </option>";
                        }
echo"</select>";

echo '                </td>';
echo '        </tr></table>';







}


if ( $this->menuDestination != "chercheurcahier") {

	if ($this->creation ==1 )echo '   <br><button type="submit" name="numAction"  value="171" id="submit_click"> Soumettre </button>';

	else echo '   <br><button type="submit" name="numAction"  value="173" id="submit_click"> Enregistrer </button>';
}

echo '  </form>';





echo '</div>';


	

echo '</body>';
echo '</html>';
}
}
?>
