﻿<?php
class PageResponsableSuiviProjet {
public function __construct()  {
}


public function afficher($nomEquipe,$eq_id,$listeProjet,$listeProjetSoumisPorteur,$listeProjetSoumisPartenaire,$listeProjetAcceptePorteur,$listeProjetAcceptePartenaire,$listeProjetRefusePorteur,$listeProjetRefusePartenaire,$listeFin12mois,$listeProjetParAnnee,$listeProjetSoumis,$listeProjetAccepte,$listeProjetRefuse,$listeProjetTermine) {


	


echo '   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>';
echo '    <script type="text/javascript">';
echo "      google.charts.load('current', {'packages':['corechart']});";
echo "      google.charts.load('current', {'packages':['bar']});";




echo "      google.charts.setOnLoadCallback(soumisPorteur);";
echo "      google.charts.setOnLoadCallback(soumisPartenaire);";
echo "      google.charts.setOnLoadCallback(acceptePorteur);";
echo "      google.charts.setOnLoadCallback(acceptePartenaire);";
echo "      google.charts.setOnLoadCallback(refusePorteur);";
echo "      google.charts.setOnLoadCallback(refusePartenaire);";
echo "      google.charts.setOnLoadCallback(projetDynamique);";



echo "function soumisPorteur() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Financeur', 'NB'],";
$nb=0;
foreach ($listeProjetSoumisPorteur as $row) {
        echo "          ['".$row->financeur_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('soumisporteur'));";
echo "        chart.draw(data, options);";
echo "      }";


echo "function soumisPartenaire() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Financeur', 'NB'],";
$nb=0;
foreach ($listeProjetSoumisPartenaire as $row) {
        echo "          ['".$row->financeur_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('soumispartenaire'));";
echo "        chart.draw(data, options);";
echo "      }";


echo "function acceptePorteur() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Financeur', 'NB'],";
$nb=0;
foreach ($listeProjetAcceptePorteur as $row) {
        echo "          ['".$row->financeur_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('accepteporteur'));";
echo "        chart.draw(data, options);";
echo "      }";


echo "function acceptePartenaire() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Financeur', 'NB'],";
$nb=0;
foreach ($listeProjetAcceptePartenaire as $row) {
        echo "          ['".$row->financeur_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('acceptepartenaire'));";
echo "        chart.draw(data, options);";
echo "      }";


echo "function refusePorteur() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Financeur', 'NB'],";
$nb=0;
foreach ($listeProjetRefusePorteur as $row) {
        echo "          ['".$row->financeur_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('refuseporteur'));";
echo "        chart.draw(data, options);";
echo "      }";



echo "function refusePartenaire() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Financeur', 'NB'],";
$nb=0;
foreach ($listeProjetRefusePartenaire as $row) {
        echo "          ['".$row->financeur_nom."',      $row->nb],\n";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "          title: '".$nb." projets de recherche'";
echo "        };";
echo "        var chart = new google.visualization.PieChart(document.getElementById('refusepartenaire'));";
echo "        chart.draw(data, options);";
echo "      }";


echo "      function projetDynamique() {";
echo "     var data = google.visualization.arrayToDataTable([";
echo "        ['Genre', ";
//foreach ($liste1 as $row) echo "'".$row->ty_nom."',";
echo "'ANR','Entreprise','CARNOT','EUROPE',";
echo "        { role: 'annotation' } ],";

foreach($listeProjetParAnnee as $year=>$liste) {
      $listeEquipeCurrent= array();
      foreach ($liste as $row) {
              $listeEquipeCurrent[$row->financeur_nom]= $row->nb;
              }

        echo "        ['".$year."',";
      echo $listeEquipeCurrent['ANR'].",".$listeEquipeCurrent['Entreprise'].",";
      echo $listeEquipeCurrent['CARNOT'].",".$listeEquipeCurrent['EUROPE'].",";
        echo "         ''],";
}
echo "      ]);";

echo "      var options = {";
echo "        width: 600,";
echo "        height: 400,";
echo "        legend: { position: 'top', maxLines: 3 },";
echo "        bar: { groupWidth: '75%' },";
echo "        isStacked: true,";

echo "series: {";
echo "    0:{color:'#FFBF00'},";
echo "    1:{color:'#2E64FE'},";
echo "    2:{color:'#FA5858'},";
echo "    3:{color:'#01DF3A'},";
echo "  }";

echo "      };";

echo "     var chart = new google.charts.Bar(document.getElementById('projetDynamique'));";
echo "        chart.draw(data, google.charts.Bar.convertOptions(options));";
echo "}";


echo "    </script>";



echo '<div id="FormulairesModificationPersonne">';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
//$listeProjetSoumis,$listeProjetAccepte,$listeProjetRefuse,$listeProjetTermine);
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurProjet.php">';




//echo "<h2>Les projets de l'équipe: </h2>";
//echo '<table>';
//echo '<tr><th>acronyme</th><th>Selection </th> <th>Porteur </th><th>coordinateur</th><th>coord Ou partenaire</th><th>Institut</th><th>Organisme</th><th>Financeur</th></tr>';
//echo '<tr>';
//foreach ( $listeProjet as $row )   echo '<tr><td><a href="ControleurProjet.php?numAction=112&menuDestination=responsableequipe&id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$row->preselection.'</td><td>'.$row->p_nom.'</td><td>'.$row->coordinateurProjet.'</td><td>'.$row->coordOuPartenaire.'</td><td>'.$row->institut.'</td><td>'.$row->organisme.'</td><td>'.$row->financeur_nom.'</td></tr>';
//echo '</tr></table>';


echo "<h2>Les projets soumis de l'équipe: </h2>";
echo '<table>';
echo '<tr><th>acronyme</th><th>Selection </th> <th>Porteur </th><th>coordinateur</th><th>coord Ou partenaire</th><th>Institut</th><th>Organisme</th><th>Financeur</th></tr>';
echo '<tr>';
foreach ( $listeProjetSoumis as $row )   echo '<tr><td><a href="ControleurProjet.php?numAction=112&menuDestination=responsableequipe&id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$row->preselection.'</td><td>'.$row->p_nom.'</td><td>'.$row->coordinateurProjet.'</td><td>'.$row->coordOuPartenaire.'</td><td>'.$row->institut.'</td><td>'.$row->organisme.'</td><td>'.$row->financeur_nom.'</td></tr>';
echo '</tr></table>';


echo "<h2>Les projets acceptés de l'équipe: </h2>";
echo '<table>';
echo '<tr><th>acronyme</th><th>Selection </th> <th>Porteur </th><th>coordinateur</th><th>coord Ou partenaire</th><th>Institut</th><th>Organisme</th><th>Financeur</th></tr>';
echo '<tr>';
foreach ( $listeProjetAccepte as $row )   echo '<tr><td><a href="ControleurProjet.php?numAction=112&menuDestination=responsableequipe&id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$row->preselection.'</td><td>'.$row->p_nom.'</td><td>'.$row->coordinateurProjet.'</td><td>'.$row->coordOuPartenaire.'</td><td>'.$row->institut.'</td><td>'.$row->organisme.'</td><td>'.$row->financeur_nom.'</td></tr>';
echo '</tr></table>';

echo "<h2>Les projets refusés de l'équipe: </h2>";
echo '<table>';
echo '<tr><th>acronyme</th><th>Selection </th> <th>Porteur </th><th>coordinateur</th><th>coord Ou partenaire</th><th>Institut</th><th>Organisme</th><th>Financeur</th></tr>';
echo '<tr>';
foreach ( $listeProjetRefuse as $row )   echo '<tr><td><a href="ControleurProjet.php?numAction=112&menuDestination=responsableequipe&id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$row->preselection.'</td><td>'.$row->p_nom.'</td><td>'.$row->coordinateurProjet.'</td><td>'.$row->coordOuPartenaire.'</td><td>'.$row->institut.'</td><td>'.$row->organisme.'</td><td>'.$row->financeur_nom.'</td></tr>';
echo '</tr></table>';

echo "<h2>Les projets terminés de l'équipe: </h2>";
echo '<table>';
echo '<tr><th>acronyme</th><th>Selection </th> <th>Porteur </th><th>coordinateur</th><th>coord Ou partenaire</th><th>Institut</th><th>Organisme</th><th>Financeur</th></tr>';
echo '<tr>';
foreach ( $listeProjetTermine as $row )   echo '<tr><td><a href="ControleurProjet.php?numAction=112&menuDestination=responsableequipe&id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$row->preselection.'</td><td>'.$row->p_nom.'</td><td>'.$row->coordinateurProjet.'</td><td>'.$row->coordOuPartenaire.'</td><td>'.$row->institut.'</td><td>'.$row->organisme.'</td><td>'.$row->financeur_nom.'</td></tr>';
echo '</tr></table>';
        echo '<input type="hidden" name="eq_id" id="eq_id" value="'.$eq_id.'">';
      echo '<button type="Submit" name="numAction" value="110" id="submit_click"> Ajouter </button>&nbsp;&nbsp;';
      echo '<button type="Submit" name="numAction" value="601" id="submit_click"> Exporter csv </button>';
echo '</form>';


echo '<form id="modifier_equipe" method="post"  action="../control/ControleurPersonne.php">';



echo '<h2> Projets soumis</h2>';
echo '<table>';
echo '<tr><th>HSM Porteur</th><th>Partenaire</th></tr><tr>';
echo '    <td><div id="soumisporteur" style="width: 500px; height: 300px;"></div></td>';
echo '    <td><div id="soumispartenaire" style="width: 500px; height: 300px;"></div></td>';
echo '</tr></table>';
echo '<h2> Projets acceptés</h2>';
echo '<table>';
echo '<tr><th>HSM Porteur</th><th>Partenaire</th></tr><tr>';
echo '    <td><div id="accepteporteur" style="width: 500px; height: 300px;"></div></td>';
echo '    <td><div id="acceptepartenaire" style="width: 500px; height: 300px;"></div></td>';
echo '</tr></table>';
echo '<h2> Projets refusés </h2>';
echo '<table>';
echo '<tr><th>HSM Porteur</th><th>Partenaire</th></tr><tr>';
echo '    <td><div id="refuseporteur" style="width: 500px; height: 300px;"></div></td>';
echo '    <td><div id="refusepartenaire" style="width: 500px; height: 300px;"></div></td>';
echo '</tr></table>';


echo '<h2> Projets acceptés se terminant cette année </h2>';
///////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurProjet.php">';
echo '<table>';
echo '<tr><th>acronyme</th><th>Date FIN</th></tr>';
echo '<tr>';
foreach ( $listeFin12mois as $row )   echo '<tr><td><a href="ControleurProjet.php?numAction=112&menuDestination=responsableequipe&id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$row->dateFin.'</td></tr>';

echo '</tr></table>';

        echo '<input type="hidden" name="eq_id" id="eq_id" value="'.$eq_id.'">';
      echo '<button type="Submit" name="numAction" value="601" id="submit_click"> Exporter csv </button></td>';
echo '</form>';


echo '<form id="modifier_equipe" method="post"  action="../control/ControleurPersonne.php">';


echo "<h2> Dynamique Projets</h2>";
echo '    <div id="projetDynamique" style="width: 500px; height: 300px;"></div>';


echo "FIN";
echo '</div>';

echo '</body>';
echo '</html>';
}
}
?>
