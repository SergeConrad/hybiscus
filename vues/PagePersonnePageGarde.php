﻿<?php
class PagePersonnePageGarde {






//Public function afficher($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$menuDestination,$listeEffectif,$listeEffectifSexe,$listeStagiaire,$listeStagiaireArchive,$listeDoctorant,$listeDoctorantArchive,$listeCdd,$listeCddArchive,$nomEquipe,$cahiersEquipe)
Public function afficher($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$menuDestination,$listeEffectif,$listeEffectifSexe,$listePersPageGarde,$nomEquipe,$cahiersEquipe)
{

echo '   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>';
echo '    <script type="text/javascript">';
echo "      google.charts.load('current', {'packages':['corechart']});";
echo "      google.charts.setOnLoadCallback(effectif);";
echo "      google.charts.setOnLoadCallback(effectifSexe);";

echo "      function effectif() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Statut', 'Nombres'],";
$nb=0;
foreach ($listeEffectif as $row) {
        echo "          ['".$row->st_nom."',      $row->nb],";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "    title:  '".$nb." Effectif '";
echo "  };";
echo "  var chart = new google.visualization.PieChart(document.getElementById('effectif'));";
echo "  chart.draw(data, options);";
echo "}";


echo "      function effectifSexe() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Sexe', 'Nombres'],";
$nb=0;
foreach ($listeEffectifSexe as $row) {
        echo "          ['".$row->sexe."',      $row->nb],";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "    title:  '".$nb." Effectif '";
echo "  };";
echo "  var chart = new google.visualization.PieChart(document.getElementById('effectifSexe'));";
echo "  chart.draw(data, options);";
echo "}";


echo "    </script>";


echo '<div id="FormulairesModificationPersonne">';


echo '<form id="listes" method="post"  action="../control/ControleurExport.php">';

echo '<table><tr>';
echo '    <td><div id="effectif" style="width: 500px; height: 300px;"></div></td>';
echo '    <td><div id="effectifSexe" style="width: 500px; height: 300px;"></div></td>';
echo '</tr></table>';


echo "<h2>Exportation de la liste personnalisée en format csv: </h2>";
//echo '<i> ceux ayant une date de départ renseigné </i>';
//echo "<b> Année fin contrat: </b> <input type='number' name='year' id='year' value=".$this->year." >";
echo '<button type="Submit" name="numAction" value="994" id="submit_click2"> Création </button></td>';

echo "<br><i> Les archivés </i>:";
echo '<button type="Submit" name="numAction" value="995" id="submit_click3"> Archivé </button></td>';

echo '<input type="hidden" name="selectSite" id="selectSite" value="'.$selectSite.'">';
echo '<input type="hidden" name="selectStatut" id="selectStatut" value="'.$selectStatut.'">';
echo '<input type="hidden" name="selectType" id="selectType" value="'.$selectType.'">';
echo '<input type="hidden" name="selectEquipe" id="selectEquipe" value="'.$selectEquipe.'">';
echo '<input type="hidden" name="selectDate" id="selectDate" value="'.$selectDate.'">';





if ($nomEquipe =='') echo "<h2>Les stagiaires :</h2>";
else echo "<h2>Les stagiaires de l'équipe ".$nomEquipe.":</h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Encadrant</th><th>Date Arrivée</th><th>Date départ</th></tr>';
foreach ( $listePersPageGarde as $row ) if (($row->archive==0)&&($row->st_id==3)&&($row->ty_id==2))   echo '<tr><td><a href="ControleurPersonne.php?numAction=303&selectEquipe='.$selectEquipe.'&menuDestination='.$menuDestination.'&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom. '</td><td>'.$row->p_mail.'</td><td>'.$row->encadrant.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->dateFinH.'</td></tr>';
echo '<tr>';

echo '</tr></table>';



if ($nomEquipe =='') echo "<h2>Les doctorants : </h2>";
else echo "<h2>Les doctorants de l'équipe ".$nomEquipe.": </h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Directeur 1</th><th>Directeur 2</th><th>Date début</th><th>intitulé</th></tr>';
echo '<tr>';
foreach ( $listePersPageGarde as $row ) if (($row->archive==0)&&($row->st_id==3)&&($row->ty_id==1))     echo '<tr><td><a href="ControleurPersonne.php?numAction=303&selectEquipe='.$selectEquipe.'&menuDestination='.$menuDestination.'&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom.'</td><td>'.$row->p_mail.'</td><td>'.$row->directeur1.'</td><td>'.$row->directeur2.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->intitule.'</td></tr>';

echo '</tr></table>';

if ($nomEquipe =='') echo "<h2>Les contractuels : </h2>";
else echo "<h2>Les contractuels de l'équipe ".$nomEquipe.": </h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Type</th><th>Encadrant</th><th>Sujet Recherche</th><th>Date Début</th><th>Date Départ</th></tr>';
echo '<tr>';
foreach ( $listePersPageGarde as $row ) if (($row->archive==0)&&($row->st_id==2))  echo '<tr><td><a href="ControleurPersonne.php?numAction=303&selectEquipe='.$selectEquipe.'&menuDestination='.$menuDestination.'&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom.'</td><td>'.$row->p_mail.'</td><td>'.$row->ty_nom.'</td><td>'.$row->superieur.'</td><td>'.$row->contractuel_sujet_recherche.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->dateFinH.'</td></tr>';

echo '</tr></table>';

if ($nomEquipe =='') echo "<h2>Les permanents : </h2>";
else echo "<h2>Les permanents de l'équipe ".$nomEquipe.": </h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Corps</th><th>Tutelle</th></tr>';
echo '<tr>';
foreach ( $listePersPageGarde as $row ) if (($row->archive==0)&&($row->st_id==1))  echo '<tr><td><a href="ControleurPersonne.php?numAction=303&selectEquipe='.$selectEquipe.'&menuDestination='.$menuDestination.'&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom.'</td><td>'.$row->p_mail.'</td><td>'.$row->corps.'</td><td>'.$row->t_nom.'</td></tr>';
echo '</tr></table>';



if (!($nomEquipe =='')) {
echo "<h2>Les cahiers de laboratoires de  l'équipe ".$nomEquipe.": </h2>";

echo '<table>';
echo '<tr><th>Numero </th> <th>Modele </th><th>Attribué à</th><th>type</th><th>Date Attribution</th><th>Fonction</th><th>Equipe</th><th>Date Retour</th><th>commentaire</th></tr>';
foreach ( $cahiersEquipe as $row )   echo '<tr><td><a href="ControleurCahier.php?numAction=172&selectEquipe='.$selectEquipe.'&menuDestination='.$menuDestination.'&cahierNum=' .$row->cahierNum .'">'.$row->cahierNum.' </a></td><td>'.$row->modele.'</td><td>'.$row->appartient.'</td><td>'.$row->type.'</td><td>'.$row->dateDotation.'</td><td>'.$row->fonction.'</td><td>'.$row->eq_nom.'</td><td>'.$row->dateRetour.'</td><td>'.$row->commentaires.'</td></tr>';

echo '</table>';

}

if ($nomEquipe =='') echo "<h2>Les stagiaires archivés :</h2>";
else echo "<h2>Les stagiaires archivés  de l'équipe ".$nomEquipe.":</h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Encadrant</th><th>Date Arrivée</th><th>Date départ</th></tr>';
foreach ( $listePersPageGarde as $row ) if (($row->archive==1)&&($row->st_id==3)&&($row->ty_id==2))   echo '<tr><td><a href="ControleurPersonne.php?numAction=303&selectEquipe='.$selectEquipe.'&menuDestination='.$menuDestination.'&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom. '</td><td>'.$row->p_mail.'</td><td>'.$row->encadrant.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->dateFinH.'</td></tr>';
echo '<tr>';

echo '</tr></table>';




if ($nomEquipe =='') echo "<h2>Les doctorants archivés : </h2>";
else echo "<h2>Les doctorants archivés de  l'équipe ".$nomEquipe.": </h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Directeur 1</th><th>Directeur 2</th><th>Date début</th><th>Date Fin</th><th>intitulé</th></tr>';
echo '<tr>';
foreach ( $listePersPageGarde as $row ) if (($row->archive==1)&&($row->st_id==3)&&($row->ty_id==1))  echo '<tr><td><a href="ControleurPersonne.php?numAction=303&selectEquipe='.$selectEquipe.'&menuDestination='.$menuDestination.'&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom.'</td><td>'.$row->p_mail.'</td><td>'.$row->directeur1.'</td><td>'.$row->directeur2.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->dateFinH.'</td><td>'.$row->intitule.'</td></tr>';
echo '</tr></table>';


if ($nomEquipe =='') echo "<h2>Les contractuels archivés : </h2>";
else echo "<h2>Les contractuels archivés  de l'équipe ".$nomEquipe.": </h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Type</th><th>Encadrant</th><th>Sujet Recherche</th><th>Date Fin</th><th>Date Départ</th></tr>';
echo '<tr>';
foreach ( $listePersPageGarde as $row ) if (($row->archive==1)&&($row->st_id==2))  echo '<tr><td><a href="ControleurPersonne.php?numAction=303&selectEquipe='.$selectEquipe.'&menuDestination='.$menuDestination.'&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom.'</td><td>'.$row->p_mail.'</td><td>'.$row->ty_nom.'</td><td>'.$row->superieur.'</td><td>'.$row->contractuel_sujet_recherche.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->dateFinH.'</td></tr>';

echo '</tr></table>';



if ($nomEquipe =='') echo "<h2>Les permanents archivés : </h2>";
else echo "<h2>Les permanents archivés de l'équipe ".$nomEquipe.": </h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Corps</th><th>Tutelle</th></tr>';
echo '<tr>';
foreach ( $listePersPageGarde as $row ) if (($row->archive==1)&&($row->st_id==1))  echo '<tr><td><a href="ControleurPersonne.php?numAction=303&selectEquipe='.$selectEquipe.'&menuDestination='.$menuDestination.'&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom.'</td><td>'.$row->p_mail.'</td><td>'.$row->corps.'</td><td>'.$row->t_nom.'</td></tr>';
echo '</tr></table>';



echo '</form>';


//if ($menuDestination!='chercheurlabo') {

//echo '<form id="modifier_equipe" method="post"  action="../control/ControleurAdmin.php">';
//echo "<h2>Génération Page Perso (site web spip): </h2>";
//echo '<button type="Submit" name="numAction" value="312" id="submit_click"> Création </button></td>';
//echo '<i> Création des pages perso sur le site hydrosciences </i>';
//echo '</form>';
//}

echo '</div>';




	

echo '</body>';
echo '</html>';
}
}
?>
