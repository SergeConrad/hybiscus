﻿<?php
class PageResponsablePageGarde {
public function __construct()  {
}


public function afficher($listeStagiaire,$listeDoctorant,$nomEquipe,$eq_id,$cahiersEquipe,$listeStagiaireArchive,$listeDoctorantArchive,$listeCdd,$listeCddArchive,$listeEquipeEffectif){
	

//echo  '<script type="text/javascript">';
//echo  'function RefreshDoctorantPG() {';
//echo  'var form = document.createElement("form");';
//echo  'form.action = "../control/ControleurPersonne.php?numAction=260"';
//echo  'form.method = "post";';
//echo  'form.appendChild(year);';
//echo  'document.body.appendChild(form);';
//echo  'form.submit();';
//echo  '}';
//echo  '</script>';

echo '   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>';
echo '    <script type="text/javascript">';
echo "      google.charts.load('current', {'packages':['corechart']});";
echo "      google.charts.setOnLoadCallback(equipeEffectif);";

echo "      function equipeEffectif() {";
echo "        var data = google.visualization.arrayToDataTable([";
echo "          ['Statut', 'Nombres'],";
$nb=0;
foreach ($listeEquipeEffectif as $row) {
        echo "          ['".$row->st_nom."',      $row->nb],";
        $nb=$nb+$row->nb;
}
echo "        ]);";
echo "        var options = {";
echo "    title:  '".$nb." Effectif de équipe'";
echo "  };";
echo "  var chart = new google.visualization.PieChart(document.getElementById('equipeEffectif'));";
echo "  chart.draw(data, options);";
echo "}";


echo "    </script>";



echo '<div id="FormulairesModificationPersonne">';


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurPersonne.php">';


echo '<table><tr>';
echo '    <td><div id="equipeEffectif" style="width: 500px; height: 300px;"></div></td>';
echo '</tr></table>';



echo "<h2>Les stagiaires de l'équipe ".$nomEquipe.":</h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Encadrant</th><th>Date Arrivée</th><th>Date départ</th></tr>';
foreach ( $listeStagiaire as $row )   echo '<tr><td><a href="ControleurPersonne.php?numAction=303&eq_id='.$eq_id.'&menuDestination=responsablestagiaire&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom. '</td><td>'.$row->p_mail.'</td><td>'.$row->encadrant.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->dateFinH.'</td></tr>';
echo '<tr>';

echo '</tr></table>';

echo '</form>';

echo '<form id="modifier_equipe" method="post"  action="../control/ControleurExport.php">';
      echo '<button type="Submit" name="numAction" value="442'.$eq_id.'" id="submit_click"> Exporter csv </button></td>';
echo '</form>';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurChercheur.php">';
echo "<h2>Les doctorants de l'équipe ".$nomEquipe.": </h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Directeur 1</th><th>Directeur 2</th><th>Date début</th><th>intitulé</th></tr>';
echo '<tr>';
foreach ( $listeDoctorant as $row )   echo '<tr><td><a href="ControleurPersonne.php?numAction=303&eq_id='.$eq_id.'&menuDestination=responsabledoctorant&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom.'</td><td>'.$row->p_mail.'</td><td>'.$row->directeur1.'</td><td>'.$row->directeur2.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->intitule.'</td></tr>';

echo '</tr></table>';


echo '</form>';

echo '<form id="modifier_equipe" method="post"  action="../control/ControleurExport.php">';
      echo '<button type="Submit" name="numAction" value="443'.$eq_id.'" id="submit_click"> Exporter csv </button></td>';
echo '</form>';



///////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurChercheur.php">';
echo "<h2>Les contractuels de l'équipe ".$nomEquipe.": </h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Type</th><th>Encadrant</th><th>Sujet Recherche</th><th>Date Début</th><th>Date Départ</th></tr>';
echo '<tr>';
foreach ( $listeCdd as $row )   echo '<tr><td><a href="ControleurPersonne.php?numAction=303&eq_id='.$eq_id.'&menuDestination=responsablecdd&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom.'</td><td>'.$row->p_mail.'</td><td>'.$row->ty_nom.'</td><td>'.$row->superieur.'</td><td>'.$row->contractuel_sujet_recherche.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->dateFinH.'</td></tr>';

echo '</tr></table>';

echo '</form>';

echo '<form id="modifier_equipe" method="post"  action="../control/ControleurExport.php">';
      echo '<button type="Submit" name="numAction" value="451'.$eq_id.'" id="submit_click"> Exporter csv </button></td>';
echo '</form>';



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurChercheur.php">';

echo "<h2>Les cahiers de laboratoires de  l'équipe ".$nomEquipe.": </h2>";

echo '<table>';
echo '<tr><th>Numero </th> <th>Modele </th><th>Attribué à</th><th>type</th><th>Date Attribution</th><th>Fonction</th><th>Equipe</th><th>Date Retour</th><th>commentaire</th></tr>';
foreach ( $cahiersEquipe as $row )   echo '<tr><td><a href="ControleurCahier.php?numAction=172&menuDestination=responsableequipe&cahierNum=' .$row->cahierNum .'">'.$row->cahierNum.' </a></td><td>'.$row->modele.'</td><td>'.$row->appartient.'</td><td>'.$row->type.'</td><td>'.$row->dateDotation.'</td><td>'.$row->fonction.'</td><td>'.$row->eq_nom.'</td><td>'.$row->dateRetour.'</td><td>'.$row->commentaires.'</td></tr>';

echo '</table>';

echo '</form>';


echo '<form id="modifier_equipe" method="post"  action="../control/ControleurExport.php">';
      echo '<input type="hidden" name="eq_id" id="eq_id" value="'.$eq_id.'">';
      echo '<button type="Submit" name="numAction" value="445" id="submit_click"> Exporter csv </button></td>';
echo '</form>';

///////////////////////////////////////////////////////////////////////////////////////////////////
//echo '<form id="modifier_equipe" method="post"  action="../control/ControleurProjet.php">';
//echo "<h2>Les projets de l'équipe: </h2>";
//echo '<table>';
//echo '<tr><th>acronyme</th><th>Equipe </th> <th>Porteur </th><th>coordinateur</th><th>coord Ou partenaire</th><th>Institut</th><th>Organisme</th><th>Financeur</th></tr>';
//echo '<tr>';
//foreach ( $listeProjet as $row )   echo '<tr><td><a href="ControleurProjet.php?numAction=112&menuDestination=responsableequipe&id=' .$row->projet_id .'">'.$row->acronyme .'</a></td><td>'.$row->eq_nom.'</td><td>'.$row->nom_porteur.'</td><td>'.$row->coordinateurProjet.'</td><td>'.$row->coordOuPartenaire.'</td><td>'.$row->institut_rattachement.'</td><td>'.$row->organisme_gestionnaire.'</td><td>'.$row->financeur.'</td></tr>';
//      echo '<td colspan=6><button type="Submit" name="numAction" value="110" id="submit_click"> Ajouter </button>&nbsp;&nbsp;';

//echo '</tr></table>';

//	echo '<input type="hidden" name="eq_id" id="eq_id" value="'.$eq_id.'">';
//      echo '<button type="Submit" name="numAction" value="601" id="submit_click"> Exporter csv </button></td>';
//echo '</form>';


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurChercheur.php">';
echo "<h2>Les stagiaires archivés  de l'équipe ".$nomEquipe.":</h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Encadrant</th><th>Date Arrivée</th><th>Date départ</th></tr>';
foreach ( $listeStagiaireArchive as $row )   echo '<tr><td><a href="ControleurPersonne.php?numAction=303&eq_id='.$eq_id.'&menuDestination=responsablestagiairearchive&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom. '</td><td>'.$row->p_mail.'</td><td>'.$row->encadrant.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->dateFinH.'</td></tr>';
echo '<tr>';

echo '</tr></table>';

echo '</form>';

echo '<form id="modifier_equipe" method="post"  action="../control/ControleurExport.php">';
      echo '<button type="Submit" name="numAction" value="453'.$eq_id.'" id="submit_click"> Exporter csv </button></td>';
echo '</form>';


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurChercheur.php">';
echo "<h2>Les doctorants archivés de  l'équipe ".$nomEquipe.": </h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Directeur 1</th><th>Directeur 2</th><th>Date début</th><th>Date Fin</th><th>intitulé</th></tr>';
echo '<tr>';
foreach ( $listeDoctorantArchive as $row )   echo '<tr><td><a href="ControleurPersonne.php?numAction=303&eq_id='.$eq_id.'&menuDestination=responsabledoctorantarchive&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom.'</td><td>'.$row->p_mail.'</td><td>'.$row->directeur1.'</td><td>'.$row->directeur2.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->dateFinH.'</td><td>'.$row->intitule.'</td></tr>';
echo '</tr></table>';


echo '</form>';

echo '<form id="modifier_equipe" method="post"  action="../control/ControleurExport.php">';
      echo '<button type="Submit" name="numAction" value="454'.$eq_id.'" id="submit_click"> Exporter csv </button></td>';
echo '</form>';

///////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form id="modifier_equipe" method="post"  action="../control/ControleurChercheur.php">';
echo "<h2>Les contractuels archivés  de l'équipe ".$nomEquipe.": </h2>";
echo '<table>';
echo '<tr><th>Nom </th> <th>Prenom </th><th>mail</th><th>Type</th><th>Encadrant</th><th>Sujet Recherche</th><th>Date Fin</th><th>Date Départ</th></tr>';
echo '<tr>';
foreach ( $listeCddArchive as $row )   echo '<tr><td><a href="ControleurPersonne.php?numAction=303&eq_id='.$eq_id.'&menuDestination=responsablecddarchive&id=' .$row->p_id .'">'.$row->p_nom .' '.$row->p_prenom. '</a></td><td>'.$row->p_prenom.'</td><td>'.$row->p_mail.'</td><td>'.$row->ty_nom.'</td><td>'.$row->superieur.'</td><td>'.$row->contractuel_sujet_recherche.'</td><td>'.$row->dateDebutH.'</td><td>'.$row->dateFinH.'</td></tr>';

echo '</tr></table>';

echo '</form>';

echo '<form id="modifier_equipe" method="post"  action="../control/ControleurExport.php">';
      echo '<button type="Submit" name="numAction" value="452'.$eq_id.'" id="submit_click"> Exporter csv </button></td>';
echo '</form>';


echo '</div>';

echo '</body>';
echo '</html>';
}
}
?>
