
////////////////////////////////////////////////////////
// Ajouter un tutelle
//../vues/PageTutelleAjoute.php
// ou on vient de changer le type dans la vue VuePersonneAjoute.php
//////////////////////////////////////////////////////
if ($numAction==140) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageTutelleNavigation.php';
 require_once '../actions/actionTutelleListe.php';


         $action =  new actionTutelleListe;
        $listeTutelle = $action->execute();

   $vue1= new PageTutelleNavigation();
    $vue1->afficher($listeTutelle);


        require_once '../vues/PageTutelleAjoute.php';
        $vue2= new PageTutelleAjoute();
        $vue2->afficher("");
}

/////////////////////////////////////////////////////////
// appuyer sur le bouton submit de la vue vue PageTutelleAjoute.php
//////////////////////////////////////////////////////
if ($numAction==141) {
 require_once '../formulaires/FormTutelleAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionTutelleListe.php';

 require_once '../vues/PageTutelleNavigation.php';

         $action =  new actionTutelleListe;
        $listeTutelle = $action->execute();

   $vue= new PageTutelleNavigation();
    $vue->afficher($listeTutelle);

try {
        $tutelle = new FormTutelleAjoute($_POST);

        require_once '../actions/actionTutelleAjoute.php';
        $action =  new actionTutelleAjoute();
        $action->execute($tutelle);


        $action2 =  new actionTutelleListe;
        $listeTutelle = $action2->execute();

        $vue= new PageTutelleNavigation();
        $vue->afficher($listeTutelle);



} catch (Exception $e) {
        require_once '../vues/PageTutelleAjoute.php';
        $vue= new PageTutelleAjoute($_POST);

        $vue->afficher($e->getMessage());


}


}

////////////////////////////////////////////////////////
// Modifier un tutelle
//////////////////////////////////////////////////////
if ($numAction==142) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageTutelleNavigation.php';
 require_once '../actions/actionTutelleListe.php';

    $action =  new actionTutelleListe;
    $listeTutelle = $action->execute();

   $vue= new PageTutelleNavigation();
    $vue->afficher($listeTutelle);

   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionTutelleGet.php';
        // rend un tableau associatif
         $action1 =  new actionTutelleGet;
        $tabtutelle =  $action1->execute($id);


        require_once '../vues/PageTutelleModifie.php';
        $vue2= new PageTutelleModifie($tabtutelle);
        $vue2->afficher("");


        }
    else {
        // On vient de changer le type dans le formulaire, en mode refresh les infos sont dans le formulaire et non dans la base de données

        require_once '../vues/PageTutelleModifie.php';
        $vue2= new PageTutelleModifie($_POST);
        $vue2->afficher("");

        }
}

/////////////////////////////////////////////////////
// appuyer sur le bouton submit de la vue vueTutelleModifie.php
/////////////////////////////////////////////////////
if ($numAction==143) {
 require_once '../formulaires/FormTutelleModifie.php';
 require_once '../web/MenuHaut.php';

 require_once '../vues/PageTutelleNavigation.php';

require_once '../actions/actionTutelleListe.php';

    $action =  new actionTutelleListe;
    $liste = $action->execute();

   $vue= new PageTutelleNavigation();
    $vue->afficher($liste);

try {

        // cas des stagiaires
        $equipe = new FormTutelleModifie($_POST);


//echo "modification de ".$personne->getNom();
        require_once '../actions/actionTutelleModifie.php';
        $action =  new actionTutelleModifie();
        $action->execute($equipe);

       $action2 =  new actionTutelleListe;
        $liste = $action2->execute();

        $vue= new PageTutelleNavigation();
        $vue->afficher($liste);



} catch (Exception $e) {
        require_once '../vues/PageTutelleModifie.php';
        $vue2= new PageTutelleModifie($_POST);
        $vue2->afficher($e->getMessage());



}
}
/////////////////////////////////////////////////////////
// Navigation Tutelle
///////////////////////////////////////////////////////////

if  ($numAction==144){

 require_once '../web/MenuHaut.php';
 require_once '../vues/PageTutelleNavigation.php';
require_once '../actions/actionTutelleListe.php';

   $action =  new actionTutelleListe;
    $liste = $action->execute();

    $vue= new PageTutelleNavigation($_POST);
    $vue->afficher($liste);

}

