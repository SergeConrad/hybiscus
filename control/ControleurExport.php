<?php
/*********************************************************************************************************************************\
 *  Hybicus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                                                                                *
 *  Copyright (c) 2018 2019                                                                                                   *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

require_once("../web/session_start.php");
	$numAction=$_POST['numAction'];
////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des theses en cours
//////////////////////////////////////////////////////
if ($numAction==270) {


$champd1 = ",t.intitule,CONCAT (p2.p_nom,' ',p2.p_prenom) AS directeur1, CONCAT(p3.p_nom,' ',p3.p_prenom) AS directeur2,t.tfinancement, t.m2obtenu,t.ecoleDoctorale,t.laboaccueil,t.dateDebut,t.dateFin,t.etsInscription,t.etsCoTutelle";

$claused1="
                RIGHT JOIN theses AS t ON historique.theseId = t.theseId
                LEFT JOIN t_personne AS p2 ON t.p_these_directeur1 = p2.p_id
                LEFT JOIN t_personne AS p3 ON t.p_these_directeur2 = p3.p_id
";

$clauseArchive = 
                 " AND t.abandon != 1
                AND t.theseSoutenance = '0000-00-00'";


 require_once '../actions/actionPersonneFiltreComplet.php';
         $action =  new actionPersonneFiltreComplet;
        $liste=  $action->execute($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$champd1,$claused1,$clauseArchive,"");
        $entete = $action->entete(array("INTITULE","DIRECTEUR 1","DIRECTEUR 2","FINANCEMENT","M2 OBTENU","ECOLE DOCTORALE","LABO ACCUEIL","DATE DEBUT THESE","DATE FIN THESE","ETS INSCRIPTION","ETS CO TUTELLE"));


$nomfich='listeThesesEnCours.csv';
        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);



}
if ($numAction==273) {


$champd1 = ",t.intitule,CONCAT (p2.p_nom,' ',p2.p_prenom) AS directeur1, CONCAT(p3.p_nom,' ',p3.p_prenom) AS directeur2,t.tfinancement, t.m2obtenu,t.ecoleDoctorale,t.laboaccueil,t.theseDebut,t.etsInscription,t.etsCoTutelle";

$claused1="
                RIGHT JOIN theses AS t ON historique.theseId = t.theseId
                LEFT JOIN t_personne AS p2 ON t.p_these_directeur1 = p2.p_id
                LEFT JOIN t_personne AS p3 ON t.p_these_directeur2 = p3.p_id
";

$clauseArchive = 
                 " AND t.abandon != 1
                AND t.theseSoutenance = '0000-00-00'";


 require_once '../actions/actionPersonneFiltreComplet.php';
         $action =  new actionPersonneFiltreComplet;
        $liste=  $action->execute($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$champd1,$claused1,$clauseArchive,"");
        $entete = $action->entete(array("INTITULE","DIRECTEUR 1","DIRECTEUR 2","FINANCEMENT","M2 OBTENU","ECOLE DOCTORALE","LABO ACCUEIL","DATE DEBUT","ETS INSCRIPTION","ETS CO TUTELLE"));


$nomfich='listeThesesEnCours.csv';
        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);



}
////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des theses soutenues
//////////////////////////////////////////////////////
if ($numAction==271) {

	$year=$_POST['year'];


        require_once '../actions/actionDoctorantExportTheseSoutenue.php';
        $action =  new actionDoctorantExportTheseSoutenue;
        $liste= $action->execute($year);

$nomfich='listeThesesSoutenues'.$year.'.csv';
$entete=array('NOM', 'PRENOM','DATE NAISSANCE','INTITULE','DIRECTEUR 1','DIRECTEUR 2','EQUIPES','FINANCEMENT','M2 OBTENU','ECOLE DOCTORALE','LABO ACCUEIL','DATE DEBUT','ETS INSCRIPTION','ETS CO TUTELLE','DATE SOUTENANCE','APRES THESE');

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);






}
////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des theses abandonnées
//////////////////////////////////////////////////////
if ($numAction==272) {
        require_once '../actions/actionDoctorantExportTheseAbandon.php';
        $action =  new actionDoctorantExportTheseAbandon;
        $liste= $action->execute();





$nomfich='listeThesesAbandon.csv';
$entete=array('NOM', 'PRENOM','DATE NAISSANCE','INTITULE','DIRECTEUR 1','DIRECTEUR 2','EQUIPES','FINANCEMENT','M2 OBTENU','ECOLE DOCTORALE','LABO ACCUEIL','DATE DEBUT','ETS INSCRIPTION','ETS CO TUTELLE','APRES THESE');


        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);



}








////////////////////////////////////////////////////////
// USE CASE CHERCHEUR cahier de laboratoire
//////////////////////////////////////////////////////
if ($numAction==444) {


	require_once '../actions/actionCahierListe.php';
        $action =  new actionCahierListe;
        $liste =  $action->parUtilisateur($_SESSION['p_id']);
        $listeSD =  $action->mesStagiairesEtDoctorants($_SESSION['p_id']);




        require_once '../actions/actionChercheurExportCahier.php';
        $action =  new actionChercheurExportCahier;
        $action->execute($liste,$listeSD);
}

////////////////////////////////////////////////////////
// USE CASE Responsable cahier de laboratoire
//////////////////////////////////////////////////////
if ($numAction==445) {



        require_once '../actions/actionCahierListe.php';
        $action =  new actionCahierListe;
        $liste =  $action->pourLequipe($_POST['eq_id']);
        $entete = $action->entete();

	$nomfich='cahiersEquipe.csv';

        require_once '../actions/actionExportExcel.php';
        $action2 =  new actionExportExcel;
        $action2->execute($liste,$entete,$nomfich);

}



////////////////////////////////////////////////////////
// USE CASE Chercheur  exporter ses stagiaires
//////////////////////////////////////////////////////
if ($numAction==440) {

   require_once '../actions/actionChercheurStagiaireFiltreComplet.php';
   $action =  new actionChercheurStagiaireFiltreComplet;
   $listeStagiaire =  $action->execute($_SESSION['p_id'],"non");

$nomfich = 'mesStagiaires.csv';

$entete = array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date debut','date fin','encadrant','stage intitule','etablissement','intitule','autreformation');



        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listeStagiaire,$entete,$nomfich);
}


////////////////////////////////////////////////////////
// USE CASE Chercheur  exporter ses stagiaires archivés
//////////////////////////////////////////////////////
if ($numAction==447) {

   require_once '../actions/actionChercheurStagiaireFiltreComplet.php';
   $action =  new actionChercheurStagiaireFiltreComplet;
   $listeStagiaire =  $action->execute($_SESSION['p_id'],"oui");

$nomfich = 'mesStagiairesArchive.csv';

$entete = array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date debut','date fin','encadrant','stage intitule','etablissement','intitule','autreformation');

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listeStagiaire,$entete,$nomfich);
}




////////////////////////////////////////////////////////
// USE CASE RESPONSABLE exporter ses doctorants
//////////////////////////////////////////////////////
if ($numAction==441) {

 require_once '../actions/actionChercheurDoctorantFiltreComplet.php';
        $action =  new actionChercheurDoctorantFiltreComplet;
        $listeDoctorant =  $action->execute($_SESSION['p_id'],"non");


	$nomfich = 'Mesdoctorants.csv';



	$entete=array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date début','date fin','these directeur1','these directeur2','ecole Doctorale','intitule','financement','m2 obtenu','labo accueil','ets Inscription','these Soutenance','abandon','apresThese');

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listeDoctorant,$entete,$nomfich);
}


////////////////////////////////////////////////////////
// USE CASE RESPONSABLE exporter ses doctorants archivés
//////////////////////////////////////////////////////
if ($numAction==448) {

 require_once '../actions/actionChercheurDoctorantFiltreComplet.php';
        $action =  new actionChercheurDoctorantFiltreComplet;
        $listeDoctorant =  $action->execute($_SESSION['p_id'],"oui");


	$nomfich = 'MesdoctorantsArchive.csv';
	$entete=array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date début','date fin','these directeur1','these directeur2','ecole Doctorale','intitule','financement','m2 obtenu','labo accueil','ets Inscription','these Soutenance','abandon','apresThese');

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listeDoctorant,$entete,$nomfich);
}





////////////////////////////////////////////////////////
// USE CASE CHERCHEUR exporter ses contractuels 
//////////////////////////////////////////////////////
if ($numAction==446) {


//require_once("../web/session_start.php");
 require_once '../actions/actionChercheurCddFiltreComplet.php';
        $action =  new actionChercheurCddFiltreComplet;
        $liste =  $action->execute($_SESSION['p_id'],"non");


	$entete=array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date début','date fin','type','sujet recherche','superieur hierarchique');

$nomfich='Mescontractuels.csv';

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);
}



////////////////////////////////////////////////////////
// USE CASE CHERCHEUR exporter ses contractuels 
//////////////////////////////////////////////////////
if ($numAction==455) {


//require_once("../web/session_start.php");
 require_once '../actions/actionChercheurInviteFiltreComplet.php';
        $action =  new actionChercheurInviteFiltreComplet;
        $liste =  $action->execute($_SESSION['p_id'],"non");


	$entete=array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date début','date fin','type','commentaire','invite par ');

$nomfich='MesInvites.csv';

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);
}
////////////////////////////////////////////////////////
// USE CASE CHERCHEUR exporter ses contractuels 
//////////////////////////////////////////////////////
if ($numAction==456) {


//require_once("../web/session_start.php");
 require_once '../actions/actionChercheurInviteFiltreComplet.php';
        $action =  new actionChercheurInviteFiltreComplet;
        $liste =  $action->execute($_SESSION['p_id'],"oui");


	$entete=array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date début','date fin','type','commentaire','invite par ');

$nomfich='MesInvitesArchive.csv';

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);
}

////////////////////////////////////////////////////////
// USE CASE CHERCHEUR exporter ses contractuels archivés
//////////////////////////////////////////////////////
if ($numAction==449) {


//require_once("../web/session_start.php");
 require_once '../actions/actionChercheurCddFiltreComplet.php';
        $action =  new actionChercheurCddFiltreComplet;
        $liste =  $action->execute($_SESSION['p_id'],"oui");



	$entete=array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date début','date fin','type','sujet recherche','superieur hierarchique');

$nomfich='MescontractuelsArchive.csv';

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);
}














////////////////////////////////////////////////////////
// USE CASE Export Gratif 
//////////////////////////////////////////////////////
if ($numAction==450) {



      require_once '../actions/actionAdminStagiaireGratificationFiltre.php';

        $action =  new actionAdminStagiaireGratificationFiltre;
        $listePersFiltre =  $action->execute();

$entete= array('site','nom','prenom','tutelle','gratification','num Missionnaire','montant Gratif','Montant Transport','financement Credit','financementEntite',  'convention Envoyee','convention Revenue','num Bon Commande Sifac','ref fournisseur','date Dossier Gratif','par Dossier Gratif');
$nomfich ='gratificationsStagiaires.csv';

    require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listePersFiltre,$entete,$nomfich);

}


////////////////////////////////////////////////////////
// USE CASE RESPONSABLE exporter ses contractuels
//////////////////////////////////////////////////////
if (substr( $numAction, 0, 3 ) == '451') {

$eq_id= substr($numAction,3);


//require_once("../web/session_start.php");
 require_once '../actions/actionResponsableCddFiltreComplet.php';
        $action =  new actionResponsableCddFiltreComplet;
        $liste =  $action->execute($eq_id,"non");

     $entete=array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date début','date fin','type','sujet recherche','superieur hierarchique');


$nomfich='contractuelsEquipe.csv';

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);
}



////////////////////////////////////////////////////////
// USE CASE RESPONSABLE exporter ses contractuels archivés
//////////////////////////////////////////////////////
if (substr( $numAction, 0, 3 ) == '452') {

$eq_id= substr($numAction,3);


 require_once '../actions/actionResponsableCddFiltreComplet.php';
        $action =  new actionResponsableCddFiltreComplet;
        $liste =  $action->execute($eq_id,"oui");


     $entete=array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date début','date fin','type','sujet recherche','superieur hierarchique');

$nomfich='contractuelsEquipeArchive.csv';

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);
}


////////////////////////////////////////////////////////
// USE CASE RESPONSABLE EQUIPE exporter ses stagiaires
//////////////////////////////////////////////////////
if (substr( $numAction, 0, 3 ) == '442') {




$eq_id= substr($numAction,3);

   require_once '../actions/actionResponsableStagiaireFiltreComplet.php';
   $action =  new actionResponsableStagiaireFiltreComplet;
   $listeStagiaire =  $action->execute($eq_id,"non");

$nomfich = 'stagiairesEquipe.csv';


$entete = array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date debut','date fin','encadrant','stage intitule','etablissement','intitule','autreformation');

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listeStagiaire,$entete,$nomfich);
}


////////////////////////////////////////////////////////
// USE CASE RESPONSABLE EQUIPE exporter ses stagiaires archivés
//////////////////////////////////////////////////////
if (substr( $numAction, 0, 3 ) == '453') {

$eq_id= substr($numAction,3);


   require_once '../actions/actionResponsableStagiaireFiltreComplet.php';
   $action =  new actionResponsableStagiaireFiltreComplet;
   $listeStagiaire =  $action->execute($eq_id,"oui");

$nomfich = 'stagiairesEquipeArchive.csv';

$entete = array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date debut','date fin','encadrant','stage intitule','etablissement','intitule','autreformation');

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listeStagiaire,$entete,$nomfich);
}




////////////////////////////////////////////////////////
// USE CASE RESPONSABLE exporter ses doctorants
//////////////////////////////////////////////////////
if (substr( $numAction, 0, 3 ) == '443') {

$eq_id= substr($numAction,3);

 require_once '../actions/actionResponsableDoctorantFiltreComplet.php';
        $action =  new actionResponsableDoctorantFiltreComplet;
        $listeDoctorant =  $action->execute($eq_id,"non");


$nomfich = 'doctorantsEquipe.csv';
       $entete=array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date début','date fin','these directeur1','these directeur2','ecole Doctorale','intitule','financement','m2 obtenu','labo accueil','ets Inscription','these Soutenance','abandon','apresThese');

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listeDoctorant,$entete,$nomfich);
}


////////////////////////////////////////////////////////
// USE CASE RESPONSABLE exporter ses doctorants archivés
//////////////////////////////////////////////////////
if (substr( $numAction, 0, 3 ) == '454') {

$eq_id= substr($numAction,3);

 require_once '../actions/actionResponsableDoctorantFiltreComplet.php';
        $action =  new actionResponsableDoctorantFiltreComplet;
        $listeDoctorant =  $action->execute($eq_id,"oui");


$nomfich = 'doctorantsEquipeArchive.csv';
    $entete=array('id','nom','prenom','date Naissance','mail','mail perso','tel','tel perso','page perso','commentaire','nationalite','personneaprevenir','horaireacces','site','equipes','date début','date fin','these directeur1','these directeur2','ecole Doctorale','intitule','financement','m2 obtenu','labo accueil','ets Inscription','these Soutenance','abandon','apresThese');


        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listeDoctorant,$entete,$nomfich);
}




////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des stagiaires
//////////////////////////////////////////////////////
if ($numAction==800) {
        $yeardebut=$_POST['yeardebut'];
        $yearfin=$_POST['yearfin'];
$equipe=$_POST['listeEquipe'];
//echo 'eq".$equipe;


        require_once '../actions/actionCahierFiltreComplet.php';
        $action =  new actionCahierFiltreComplet;
        $liste= $action->execute($yeardebut,$yearfin,$equipe);

$nomfich='listeCahiers'.$yeardebut.'_'.$yearfin.'.csv';

$entete=array('cahier Numéro','modele','appartient','dateDotation','dateRetour','fonction','equipe','localisation','commentaires');




        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);






}





////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste custom
//////////////////////////////////////////////////////
if ($numAction==994) {
  require_once '../actions/actionPersonneFiltreComplet.php';
         $action =  new actionPersonneFiltreComplet;

$clauseArchive = " AND p1.archive NOT LIKE 1";


        $listePersonne =  $action->execute($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],"","",$clauseArchive,"");
        $entete = $action->entete("");

$nomfich='listeCustom.csv';

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listePersonne,$entete,$nomfich);

}


////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste custom
//////////////////////////////////////////////////////
if ($numAction==995) {
  require_once '../actions/actionPersonneFiltreComplet.php';
         $action =  new actionPersonneFiltreComplet;

$clauseArchive = " AND p1.archive LIKE 1";


        $listePersonne =  $action->execute($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],"","",$clauseArchive,"");
        $entete = $action->entete("");

$nomfich='listeCustomArchive.csv';

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listePersonne,$entete,$nomfich);

}


////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des archivés 
//////////////////////////////////////////////////////
if ($numAction==996) {
        $year=$_POST['year'];


 require_once '../actions/actionArchiveFiltreComplet.php';
        $action =  new actionArchiveFiltreComplet;
        $liste =  $action->execute($year);


$nomfich='listeArchives'.$year.'.csv';
$entete=array('NOM', 'PRENOM','MAIL','TEL','SITE','TUTELLE','CORPS','EQUIPES');
        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);
}

////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des permanents 
//////////////////////////////////////////////////////
if ($numAction==997) {

$champpermanent = ",t_nom,corps,CONCAT (p2.p_nom,' ',p2.p_prenom) AS eval";

$clausepermanent="
                RIGHT JOIN permanent ON historique.permanent_id = permanent.permanent_id
                LEFT JOIN t_tutelle ON permanent.t_id = t_tutelle.t_id
                LEFT JOIN t_corps ON permanent.permanent_c_id = t_corps.permanent_c_id
                LEFT JOIN t_personne p2 ON permanent.evaluateur = p2.p_id
";

$clauseArchive = "AND p1.archive NOT LIKE 1";


 require_once '../actions/actionPersonneFiltreComplet.php';
         $action =  new actionPersonneFiltreComplet;
        $liste=  $action->execute($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$champpermanent,$clausepermanent,$clauseArchive,"");
        $entete = $action->entete(array("Tutelle","Corps","Evaluateur"));





$nomfich='listePermaments.csv';
        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);
}








?>



