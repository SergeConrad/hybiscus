<?php

/*********************************************************************************************************************************\
 *  Hybicus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                                                                                *
 *  Copyright (c) 2018 2019                                                                                                   *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

require_once("../web/session_start.php");

//header ( 'Content-Type: text/html; charset=utf-8' );

?>


<!DOCTYPE html>

<?php
// require_once '../web/head.php';
require_once '../control/CachedPDOStatement.php';
// require_once '../entites/class.php';
 ?>

<body>
<!--
MENUS
CHERCHEUR                                               RESPONSABLE
* NOUVEAU STAGIAIRE 90                                  * LISTES STAGIAIRES 1001
* LISTE STAGIAIRES 92                                   * LISTES DOCTORANTS 1003
* NOUVEAU DOCTORANT 95
* LISTE DOCTORANTS 97



90 -> PageChercheurDeclareStagiaire -> 91 ajout dans bd
92 -> MenuChercheurConsulteStagiaire -> 93 PageChercheurModifieStagiaire -> 1007 ajout dans bd
95 -> PageChercheurDeclareDoctorant -> 96 ajout dans bd
97 -> MenuChercheurConsulteDoctorant -> 98 PageChercheurModifieDoctorant -> 1008 ajout dans bd


1001 -> MenuResponsableConsulteStagiaire -> 1002 PageResponsableModifieStagiaire -> 1005 ajout dans bd
1003 -> MenuResponsableConsulteDoctorant -> 1004 PageResponsableModifieDoctorant -> 1006 ajout dans bd

GERER EQUIPE 110 à 118

GERER PARAMETRES 120 à 165 
Mail d'avertissement 500 à 501
-->
<?php


// Chargement des tables ...
	require_once '../actions/actionPersonneFiltre.php';
	require_once '../actions/actionTutelleListe.php';
	require_once '../actions/actionSiteListe.php';
 	require_once '../actions/actionStatutListe.php';
 	require_once '../actions/actionTypeListe.php';
 	require_once '../actions/actionFormationListe.php';
    	$actionTutelle =  new actionTutelleListe;
      	$actionSite =  new actionSiteListe;
      	$actionStatut =  new actionStatutListe;
      	$actionType =  new actionTypeListe;
	$actionPersonne =  new actionPersonneFiltre;
	$actionFormation =  new actionFormationListe;
	// cf https://stackoverflow.com/questions/15385965/php-pdo-with-foreach-and-fetch
	$listeTutelle = new CachedPDOStatement($actionTutelle->execute());
        $listeSite = new CachedPDOStatement( $actionSite->execute());
        $listeStatut =  new CachedPDOStatement($actionStatut->execute());
        $listeType =  new CachedPDOStatement($actionType->execute());
	$listePersonne =  new CachedPDOStatement($actionPersonne->execute('T','T','T','T',date('Y-m-d'),"","",0)); // Tout le monde sauf les archivés
	$listeFormation =  new CachedPDOStatement($actionFormation->execute()); // Tout le monde sauf les archivés

     if (isset ( $_GET ['numAction'] )) $numAction=$_GET['numAction'];
     if (isset ( $_POST ['numAction'] )) $numAction=$_POST['numAction'];




////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// Ajouter une équipe
//../vues/PagePersonneAjoute.php
// ou on vient de changer le type dans la vue VuePersonneAjoute.php
//////////////////////////////////////////////////////
if ($numAction==110) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageEquipeNavigation.php';
 require_once '../actions/actionEquipeListe.php';


	 $action =  new actionEquipeListe;
        $listeEquipe = $action->execute();

   $vue1= new PageEquipeNavigation();
    $vue1->afficher($listeEquipe);


        require_once '../vues/PageEquipeAjoute.php';
        $vue2= new PageEquipeAjoute("");
        $vue2->afficher("");
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// appuyer sur le bouton submit de la vue vue PageEquipeAjoute.php
//////////////////////////////////////////////////////
if ($numAction==111) {
 require_once '../formulaires/FormEquipeAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionEquipeListe.php';

 require_once '../vues/PageEquipeNavigation.php';

	 $action =  new actionEquipeListe;
        $listeEquipe = $action->execute();

   $vue= new PageEquipeNavigation();
    $vue->afficher($listeEquipe);

try {

        $equipe = new FormEquipeAjoute($_POST);

        require_once '../actions/actionEquipeAjoute.php';
        $action =  new actionEquipeAjoute();
        $action->execute($equipe);


       	$action2 =  new actionEquipeListe;
        $listeEquipe = $action2->execute();

   	$vue= new PageEquipeNavigation();
    	$vue->afficher($listeEquipe);



} catch (Exception $e) {
        require_once '../vues/PageEquipeAjoute.php';
        $vue= new PageEquipeAjoute($_POST);

        $vue->afficher($e->getMessage());


}


}
////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// Modifier une équipe 
//////////////////////////////////////////////////////
if ($numAction==112) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageEquipeNavigation.php';
 //require_once '../actions/actionEquipeListeResp.php';
 require_once '../actions/actionEquipeListe.php';

    //$action =  new actionEquipeListeResp;
    $action =  new actionEquipeListe;
    echo $_SESSION['p_id'];
    $listeEquipe = $action->execute($_SESSION['p_id']);

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


    if (!(isset($_SESSION['responsableEquipe'])))  {
// require_once '../vues/MenuAdminConsulteListe.php';
//   $vue= new MenuAdminConsulteListe('T','T','T','T');
//    $vue->afficher($listeEquipe,$menuDestination,'','','','',0,0,0,0);

       $vue= new PageEquipeNavigation();
        $vue->afficher($listeEquipe);

}


   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionEquipeGet.php';
        // rend un tableau associatif
         $action1 =  new actionEquipeGet;
        $tabequipe =  $action1->execute($id);
//serge new

	require_once '../actions/actionEquipeMembres.php';
	$action2 = new actionEquipeMembres;
        $listemembres = new CachedPDOStatement ($action2->execute($id));

	require_once '../actions/actionEquipeNonMembres.php';
	$action3 = new actionEquipeNonMembres;
        $listenonmembres = $action3->execute($id);

	require_once '../actions/actionEquipeResponsables.php';
	$action4 = new actionEquipeResponsables;
        $listeresponsables = $action4->execute($id);

        require_once '../vues/PageEquipeModifie.php';
        $vue2= new PageEquipeModifie($tabequipe,$menuDestination);
        $vue2->afficher("",$listemembres,$listenonmembres,$listeresponsables);


        }
    else {
        // On vient de changer le type dans le formulaire, en mode refresh les infos sont dans le formulaire et non dans la base de données

        require_once '../vues/PageEquipeModifie.php';
        $vue2= new PageEquipeModifie($_POST,$menuDestination);
        $vue2->afficher("","","","");

        }
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// appuyer sur le bouton submit de la vue vueEquipeModifie.php
//////////////////////////////////////////////////////
if ($numAction==113) {
 require_once '../formulaires/FormEquipeModifie.php';
 require_once '../web/MenuHaut.php';

 require_once '../vues/PageEquipeNavigation.php';

require_once '../actions/actionEquipeListe.php';

    $action =  new actionEquipeListe;
    $listeEquipe = $action->execute();


   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

    if (!(isset($_SESSION['responsableEquipe'])))  {
// require_once '../vues/MenuAdminConsulteListe.php';
//   $vue= new MenuAdminConsulteListe('T','T','T','T');
//    $vue->afficher($listeEquipe,$menuDestination,'','','','',0,0,0,0);
   $vue= new PageEquipeNavigation();
    $vue->afficher($listeEquipe);
}



try {
        // cas des stagiaires
        $equipe = new FormEquipeModifie($_POST);

        require_once '../actions/actionEquipeModifie.php';
        $action =  new actionEquipeModifie();
        $action->execute($equipe,current($_POST['listeMembres']));

       $action2 =  new actionEquipeListe;
        $listeEquipe = $action2->execute();

      //  $vue= new PageEquipeNavigation();
      //  $vue->afficher($listeEquipe);


        $eq_id=$_POST['eq_id'];
        require_once '../actions/actionEquipeGet.php';
        // rend un tableau associatif
         $action1 =  new actionEquipeGet;
        $tabequipe =  $action1->execute($eq_id);

        require_once '../actions/actionEquipeMembres.php';
        $action2 = new actionEquipeMembres;
        $listemembres = new CachedPDOStatement ($action2->execute($eq_id));

        require_once '../actions/actionEquipeNonMembres.php';
        $action3 = new actionEquipeNonMembres;
        $listenonmembres = $action3->execute($eq_id);


        require_once '../actions/actionEquipeResponsables.php';
        $action4 = new actionEquipeResponsables;
        $listeresponsables = $action4->execute($eq_id);

        require_once '../vues/PageEquipeModifie.php';
        $vue2= new PageEquipeModifie($tabequipe,$menuDestination);
        $vue2->afficher("",$listemembres,$listenonmembres,$listeresponsables);





} catch (Exception $e) {
        require_once '../vues/PageEquipeModifie.php';
        $vue2= new PageEquipeModifie($_POST,$menuDestination);
        $vue2->afficher($e->getMessage(),"","","");



}
}
/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// Navigation Equipe
///////////////////////////////////////////////////////////

if  ($numAction==114){

 require_once '../web/MenuHaut.php';
 require_once '../vues/PageEquipeNavigation.php';
require_once '../actions/actionEquipeListe.php';



   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


   $action =  new actionEquipeListe;
    $listeEquipe = $action->execute();


//serge new
//    if (!(isset($_SESSION['responsableEquipe'])))  {
    $vue= new PageEquipeNavigation($_POST);
    $vue->afficher($listeEquipe);
//}






    $listeEquipe = $action->execute();
    require_once '../vues/PageEquipeAlias.php';
    $vue= new PageEquipeAlias();
    $vue->afficher($listeEquipe,$menuDestination);

}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// Equipe Ajout Membre 
///////////////////////////////////////////////////////////
if  ($numAction==115){
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageEquipeNavigation.php';
 require_once '../actions/actionEquipeListe.php';


    $action =  new actionEquipeListe;
    $listeEquipe = $action->execute();


   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


    if (!(isset($_SESSION['responsableEquipe'])))  {
// require_once '../vues/MenuAdminConsulteListe.php';
//   $vue= new MenuAdminConsulteListe('T','T','T','T');
//    $vue->afficher($listeEquipe,$menuDestination,'','','','',0,0,0,0);
   $vue= new PageEquipeNavigation();
    $vue->afficher($listeEquipe);
}



require_once '../actions/actionEquipeAjouteMembre.php';

   $action =  new actionEquipeAjouteMembre;
    $action->execute($_POST);


  	$eq_id=$_POST['eq_id'];
        require_once '../actions/actionEquipeGet.php';
        // rend un tableau associatif
         $action1 =  new actionEquipeGet;
        $tabequipe =  $action1->execute($eq_id);

        require_once '../actions/actionEquipeMembres.php';
        $action2 = new actionEquipeMembres;
        $listemembres = new CachedPDOStatement ($action2->execute($eq_id));

        require_once '../actions/actionEquipeNonMembres.php';
        $action3 = new actionEquipeNonMembres;
        $listenonmembres = $action3->execute($eq_id);

        require_once '../actions/actionEquipeResponsables.php';
        $action4 = new actionEquipeResponsables;
        $listeresponsables = $action4->execute($eq_id);

        require_once '../vues/PageEquipeModifie.php';
        $vue2= new PageEquipeModifie($tabequipe,$menuDestination);
        $vue2->afficher("",$listemembres,$listenonmembres,$listeresponsables);





}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// Equipe Supp Membre 
///////////////////////////////////////////////////////////
if  ($numAction==116){
require_once '../web/MenuHaut.php';
 require_once '../vues/PageEquipeNavigation.php';
 require_once '../actions/actionEquipeListe.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

    $action =  new actionEquipeListe;
    $listeEquipe = $action->execute();

    if (!(isset($_SESSION['responsableEquipe'])))  {
// require_once '../vues/MenuAdminConsulteListe.php';
//   $vue= new MenuAdminConsulteListe('T','T','T','T');
//    $vue->afficher($listeEquipe,$menuDestination,'','','','',0,0,0,0);
   $vue= new PageEquipeNavigation();
    $vue->afficher($listeEquipe);
}






require_once '../actions/actionEquipeSupprimeMembre.php';

   $action =  new actionEquipeSupprimeMembre;
    $action->execute($_POST);


        $eq_id=$_POST['eq_id'];
        require_once '../actions/actionEquipeGet.php';
        // rend un tableau associatif
         $action1 =  new actionEquipeGet;
        $tabequipe =  $action1->execute($eq_id);

        require_once '../actions/actionEquipeMembres.php';
        $action2 = new actionEquipeMembres;
        $listemembres = new CachedPDOStatement ($action2->execute($eq_id));

        require_once '../actions/actionEquipeNonMembres.php';
        $action3 = new actionEquipeNonMembres;
        $listenonmembres = $action3->execute($eq_id);


        require_once '../actions/actionEquipeResponsables.php';
        $action4 = new actionEquipeResponsables;
        $listeresponsables = $action4->execute($eq_id);

        require_once '../vues/PageEquipeModifie.php';
        $vue2= new PageEquipeModifie($tabequipe,$menuDestination);
        $vue2->afficher("",$listemembres,$listenonmembres,$listeresponsables);




}
/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// Generer Alias
///////////////////////////////////////////////////////////
if  ($numAction=='117'){
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageEquipeNavigation.php';
 require_once '../actions/actionEquipeListe.php';

    $action =  new actionEquipeListe;
    $listeEquipe = $action->execute();

    $menuDestination=$_POST['menuDestination'];

    if (!(isset($_SESSION['responsableEquipe'])))  {
// require_once '../vues/MenuAdminConsulteListe.php';
 //  $vue= new MenuAdminConsulteListe('T','T','T','T');
 //   $vue->afficher($listeEquipe,$menuDestination,'','','','',0,0,0,0);
   $vue= new PageEquipeNavigation();
    $vue->afficher($listeEquipe);
}


     $nomliste= $_POST['eq_nom'];
     require_once '../actions/actionEquipeGenererAlias.php';
     $action1 = new actionEquipeGenererAlias;
     $listemail = $action1->execute($nomliste);

     require_once '../vues/PageEquipeAliasAffiche.php';
     $vue2= new PageEquipeAliasAffiche();
     $vue2->afficher($nomliste,$listemail);

}
// les alias génériques
///////////////////////////////////////////////////////////
if (substr( $numAction, 0, 4 ) == '117_') {

 require_once '../web/MenuHaut.php';
 require_once '../vues/PageEquipeNavigation.php';
 require_once '../actions/actionEquipeListe.php';

    $action =  new actionEquipeListe;
    $listeEquipe = $action->execute();

   $vue= new PageEquipeNavigation();
    $vue->afficher($listeEquipe);

     $nomliste = substr($numAction,4,strlen($numAction));
     require_once '../actions/actionEquipeGenererAlias.php';
     $action1 = new actionEquipeGenererAlias;
     $listemail = $action1->execute($nomliste);

     require_once '../vues/PageEquipeAliasAffiche.php';
     $vue2= new PageEquipeAliasAffiche();
     $vue2->afficher($nomliste,$listemail);

}


/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// Supprimer equipe
///////////////////////////////////////////////////////////
if  ($numAction==118){
        require_once '../actions/actionEquipeSupprime.php';
         $action2 =  new actionEquipeSupprime;
          $action2->execute($_POST['eq_id']);

 require_once '../web/MenuHaut.php';
 require_once '../vues/PageEquipeNavigation.php';
 require_once '../actions/actionEquipeListe.php';

    $action =  new actionEquipeListe;
    $listeEquipe = $action->execute();

   $vue= new PageEquipeNavigation();
    $vue->afficher($listeEquipe);

}




/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// Equipe Ajout Respon
///////////////////////////////////////////////////////////
if  ($numAction==120){
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageEquipeNavigation.php';
 require_once '../actions/actionEquipeListe.php';

    $action =  new actionEquipeListe;
    $listeEquipe = $action->execute();

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

    if (!(isset($_SESSION['responsableEquipe'])))  {
   $vue= new PageEquipeNavigation();
    $vue->afficher($listeEquipe);
}

require_once '../actions/actionEquipeAjouteResponsable.php';

   $action =  new actionEquipeAjouteResponsable;
    $action->execute($_POST);

        $eq_id=$_POST['eq_id'];
        require_once '../actions/actionEquipeGet.php';
        // rend un tableau associatif
         $action1 =  new actionEquipeGet;
        $tabequipe =  $action1->execute($eq_id);

        require_once '../actions/actionEquipeMembres.php';
        $action2 = new actionEquipeMembres;
        $listemembres = new CachedPDOStatement ($action2->execute($eq_id));

        require_once '../actions/actionEquipeNonMembres.php';
        $action3 = new actionEquipeNonMembres;
        $listenonmembres = $action3->execute($eq_id);

        require_once '../actions/actionEquipeResponsables.php';
        $action4 = new actionEquipeResponsables;
        $listeresponsables = $action4->execute($eq_id);

        require_once '../vues/PageEquipeModifie.php';
        $vue2= new PageEquipeModifie($tabequipe,$menuDestination);
        $vue2->afficher("",$listemembres,$listenonmembres,$listeresponsables);
}


/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// Equipe Supp Respo
///////////////////////////////////////////////////////////
if  ($numAction==121){
require_once '../web/MenuHaut.php';
 require_once '../vues/PageEquipeNavigation.php';
 require_once '../actions/actionEquipeListe.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

    $action =  new actionEquipeListe;
    $listeEquipe = $action->execute();

    if (!(isset($_SESSION['responsableEquipe'])))  {
   $vue= new PageEquipeNavigation();
    $vue->afficher($listeEquipe);
}

require_once '../actions/actionEquipeSupprimeResponsable.php';

   $action =  new actionEquipeSupprimeResponsable;
    $action->execute($_POST);

        $eq_id=$_POST['eq_id'];
        require_once '../actions/actionEquipeGet.php';
        // rend un tableau associatif
         $action1 =  new actionEquipeGet;
        $tabequipe =  $action1->execute($eq_id);

        require_once '../actions/actionEquipeMembres.php';
        $action2 = new actionEquipeMembres;
        $listemembres = new CachedPDOStatement ($action2->execute($eq_id));

        require_once '../actions/actionEquipeNonMembres.php';
        $action3 = new actionEquipeNonMembres;
        $listenonmembres = $action3->execute($eq_id);


        require_once '../actions/actionEquipeResponsables.php';
        $action4 = new actionEquipeResponsables;
        $listeresponsables = $action4->execute($eq_id);

        require_once '../vues/PageEquipeModifie.php';
        $vue2= new PageEquipeModifie($tabequipe,$menuDestination);
        $vue2->afficher("",$listemembres,$listenonmembres,$listeresponsables);

}

