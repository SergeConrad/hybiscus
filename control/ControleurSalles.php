<?php

/*********************************************************************************************************************************\
 *  Hybicus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                                                                                *
 *  Copyright (c) 2018 2019                                                                                                   *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

require_once("../web/session_start.php");

//header ( 'Content-Type: text/html; charset=utf-8' );

?>


<!DOCTYPE html>

<?php
// require_once '../web/head.php';
require_once '../control/CachedPDOStatement.php';
// require_once '../entites/class.php';
 ?>

<body>
<!--
MENUS
CHERCHEUR                                               RESPONSABLE
* NOUVEAU STAGIAIRE 90                                  * LISTES STAGIAIRES 1001
* LISTE STAGIAIRES 92                                   * LISTES DOCTORANTS 1003
* NOUVEAU DOCTORANT 95
* LISTE DOCTORANTS 97



90 -> PageChercheurDeclareStagiaire -> 91 ajout dans bd
92 -> MenuChercheurConsulteStagiaire -> 93 PageChercheurModifieStagiaire -> 1007 ajout dans bd
95 -> PageChercheurDeclareDoctorant -> 96 ajout dans bd
97 -> MenuChercheurConsulteDoctorant -> 98 PageChercheurModifieDoctorant -> 1008 ajout dans bd


1001 -> MenuResponsableConsulteStagiaire -> 1002 PageResponsableModifieStagiaire -> 1005 ajout dans bd
1003 -> MenuResponsableConsulteDoctorant -> 1004 PageResponsableModifieDoctorant -> 1006 ajout dans bd

GERER EQUIPE 110 à 118

GERER PARAMETRES 120 à 165 
Mail d'avertissement 500 à 501
-->
<?php


// Chargement des tables ...
	require_once '../actions/actionPersonneFiltre.php';
	require_once '../actions/actionTutelleListe.php';
	require_once '../actions/actionSiteListe.php';
 	require_once '../actions/actionStatutListe.php';
 	require_once '../actions/actionTypeListe.php';
 	require_once '../actions/actionFormationListe.php';
    	$actionTutelle =  new actionTutelleListe;
      	$actionSite =  new actionSiteListe;
      	$actionStatut =  new actionStatutListe;
      	$actionType =  new actionTypeListe;
	$actionPersonne =  new actionPersonneFiltre;
	$actionFormation =  new actionFormationListe;
	// cf https://stackoverflow.com/questions/15385965/php-pdo-with-foreach-and-fetch
	$listeTutelle = new CachedPDOStatement($actionTutelle->execute());
        $listeSite = new CachedPDOStatement( $actionSite->execute());
        $listeStatut =  new CachedPDOStatement($actionStatut->execute());
        $listeType =  new CachedPDOStatement($actionType->execute());
	$listePersonne =  new CachedPDOStatement($actionPersonne->execute('T','T','T','T',date('Y-m-d'),"","",0)); // Tout le monde sauf les archivés
	$listeFormation =  new CachedPDOStatement($actionFormation->execute()); // Tout le monde sauf les archivés

     if (isset ( $_GET ['numAction'] )) $numAction=$_GET['numAction'];
     if (isset ( $_POST ['numAction'] )) $numAction=$_POST['numAction'];



  if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Ajouter un piece
//../vues/PagePieceAjoute.php
// ou on vient de changer le type dans la vue VuePersonneAjoute.php
//////////////////////////////////////////////////////
if ($numAction==160) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePieceNavigation.php';
 require_once '../actions/actionPieceListe.php';


         $action =  new actionPieceListe;
	// le retour dans un tableau associatif
        $listePiece = $action->execute($listeSite);

   $vue1= new PagePieceNavigation();
    $vue1->afficher($listePiece,$menuDestination);


        require_once '../vues/PagePieceAjoute.php';
        $vue2= new PagePieceAjoute("");
        $vue2->afficher("",$listeSite);
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// appuyer sur le bouton submit de la vue vue PagePieceAjoute.php
//////////////////////////////////////////////////////
if ($numAction==161) {
 require_once '../formulaires/FormPieceAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionPieceListe.php';

 require_once '../vues/PagePieceNavigation.php';

// affichage
         $action =  new actionPieceListe;
        $listePiece = $action->execute($listeSite);

   $vue= new PagePieceNavigation();
    $vue->afficher($listePiece,$menuDestination);

try {
	// validation formulaire
        $piece = new FormPieceAjoute($_POST);

	// ajout dans la bd
        require_once '../actions/actionPieceAjoute.php';
        $action =  new actionPieceAjoute();
        $action->execute($piece);


        $action2 =  new actionPieceListe;
        $listePiece = $action2->execute($listeSite);

        $vue= new PagePieceNavigation();
        $vue->afficher($listePiece,$menuDestination);



} catch (Exception $e) {
require_once '../web/MenuHaut.php';
 require_once '../vues/PagePieceNavigation.php';
 require_once '../actions/actionPieceListe.php';


         $action =  new actionPieceListe;
        // le retour dans un tableau associatif
        $listePiece = $action->execute($listeSite);

   $vue1= new PagePieceNavigation();
    $vue1->afficher($listePiece,$menuDestination);

        require_once '../vues/PagePieceAjoute.php';
        $vue= new PagePieceAjoute($_POST);

        $vue->afficher($e->getMessage(),$listeSite);


}


}

////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Modifier un piece
//////////////////////////////////////////////////////
if ($numAction==162) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePieceNavigation.php';
 require_once '../actions/actionPieceListe.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

    $action =  new actionPieceListe;
    $listePiece = $action->execute($listeSite);

   $vue= new PagePieceNavigation();
    $vue->afficher($listePiece,$menuDestination);

   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionPieceGet.php';
        // rend un tableau associatif
         $action1 =  new actionPieceGet;
        $tabpiece =  $action1->execute($id);


	// Recupere  la liste des places de la piece
        require_once '../actions/actionListePlaceGet.php';
         $action2 =  new actionListePlaceGet;
        $tabplace =  $action2->execute($id);
	$nbplaces= $tabplace->rowCount();

	// Recupere les réservation sur la période 
	 if (! empty ( $_POST ['year'] )) $year=$_POST['year'];
	 else $year= date('Y');

	$datedebut = date("$year-01-01 00:00:00");
	$datefin =  date("$year-12-31 00:00:00");
        require_once '../actions/actionReservationGet.php';

	// Retourne un tableau associatif, liste de place, requete de réservations
         $action3 =  new actionReservationGet;
        $tabresa =  $action3->execute($tabplace,$datedebut,$datefin);





        require_once '../vues/PagePieceModifie.php';
        $vue2= new PagePieceModifie($tabpiece,$year,$nbplaces,$menuDestination);
        $vue2->afficher("",$tabresa);


        }
}

/////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// appuyer sur le bouton submit de la vue vuePieceModifie.php
/////////////////////////////////////////////////////
if ($numAction==163) {
 require_once '../formulaires/FormPieceModifie.php';
 require_once '../web/MenuHaut.php';

 require_once '../vues/PagePieceNavigation.php';

require_once '../actions/actionPieceListe.php';

	// afficahge
    $action =  new actionPieceListe;
    $liste = $action->execute($listeSite);

   $vue= new PagePieceNavigation();
    $vue->afficher($liste,$menuDestination);

try {

        // validation formulaire
        $form = new FormPieceModifie($_POST);


//echo "modification de ".$personne->getNom();
        require_once '../actions/actionPieceModifie.php';
        $action =  new actionPieceModifie();
        $action->execute($form);


  $id=$_POST['pi_id'];

        require_once '../actions/actionPieceGet.php';
        // rend un tableau associatif
         $action1 =  new actionPieceGet;
        $tabpiece =  $action1->execute($id);


        // Recupere  la liste des places de la piece
        require_once '../actions/actionListePlaceGet.php';
         $action2 =  new actionListePlaceGet;
        $tabplace =  $action2->execute($id);
        $nbplaces= $tabplace->rowCount();

        // Recupere les réservation sur la période
         if (! empty ( $_POST ['year'] )) $year=$_POST['year'];
         else $year= date('Y');

        $datedebut = date("$year-01-01 00:00:00");
        $datefin =  date("$year-12-31 00:00:00");
        require_once '../actions/actionReservationGet.php';

        // Retourne un tableau associatif, liste de place, requete de réservations
         $action3 =  new actionReservationGet;
        $tabresa =  $action3->execute($tabplace,$datedebut,$datefin);





        require_once '../vues/PagePieceModifie.php';
        $vue2= new PagePieceModifie($tabpiece,$year,$nbplaces);
        $vue2->afficher("",$tabresa);






} catch (Exception $e) {
   $id=$_POST['pi_id'];

        require_once '../actions/actionPieceGet.php';
        // rend un tableau associatif
         $action1 =  new actionPieceGet;
        $tabpiece =  $action1->execute($id);


        // Recupere  la liste des places de la piece
        require_once '../actions/actionListePlaceGet.php';
         $action2 =  new actionListePlaceGet;
        $tabplace =  $action2->execute($id);
        $nbplaces= $tabplace->rowCount();

        // Recupere les réservation sur la période
         if (! empty ( $_POST ['year'] )) $year=$_POST['year'];
         else $year= date('Y');

        $datedebut = date("$year-01-01 00:00:00");
        $datefin =  date("$year-12-31 00:00:00");
        require_once '../actions/actionReservationGet.php';

        // Retourne un tableau associatif, liste de place, requete de réservations
         $action3 =  new actionReservationGet;
        $tabresa =  $action3->execute($tabplace,$datedebut,$datefin);





        require_once '../vues/PagePieceModifie.php';
        $vue2= new PagePieceModifie($tabpiece,$year,$nbplaces);
        $vue2->afficher($e->getMessage(),$tabresa);




}
}
/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Navigation Piece
///////////////////////////////////////////////////////////

if  ($numAction==164){

 require_once '../web/MenuHaut.php';
require_once '../vues/PagePieceNavigation.php';
 require_once '../actions/actionPieceListe.php';


         $action =  new actionPieceListe;
        // le retour dans un tableau associatif
        $listePiece = $action->execute($listeSite);





   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


// require_once '../vues/MenuAdminConsulteListe.php';
//   $vue= new MenuAdminConsulteListe();
//    $vue->afficher($listePiece,$menuDestination);




   $vue1= new PagePieceNavigation();
    $vue1->afficher($listePiece,$menuDestination);



}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Supprime Piece
///////////////////////////////////////////////////////////

if  ($numAction==165){
  	require_once '../actions/actionPieceSupprime.php';
         $action2 =  new actionPieceSupprime;
          $action2->execute($_POST['pi_id']);

 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePieceNavigation.php';
 require_once '../actions/actionPieceListe.php';


         $action =  new actionPieceListe;
        // le retour dans un tableau associatif
        $listePiece = $action->execute($listeSite);

   $vue1= new PagePieceNavigation();
    $vue1->afficher($listePiece,$menuDestination);


}


////////////////////////////////////////////////////////
// FIN
/////////////////////////////////////////////////////
echo "<div id='header'>";
echo '	</div>';
?>
</html>
