<?php
/*********************************************************************************************************************************\
 *  Hybicus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                                                                                *
 *  Copyright (c) 2018 2019                                                                                                   *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

require_once("../web/session_start.php");
//header ( 'Content-Type: text/html; charset=utf-8' );
// PERSONNES
// AJOUTER UNE PERSONNE 200
// GESTION DOCTORANTS   260
// GESTION PERSONNEL    300
// GESTION Archivés     320
// 200 ->  PageAdminPersonneAjoute 			-> 201 	->actionPersonneAjoute.php		-> PageAccueil
//	-> Si Stagiaire TableauStageAjoute		Si Stagiaire actionStageAjoute
//	->si Doctorant TableauTheseAjoute.php		Si Doctorant actionTheseAjoute
// 260 	->  affiche MenuAdminConsulteDoctorant	-> Sélection 307	->PageAdminModifieDoctorant -> Enregistre 308 -> personne = FormDoctorantModifie
//	->  affiche PageDoctorantPageGarde				affiche TableauTheseModifie			 et actionPersonneModifie
// 300 	-> affiche  MenuAdminTriPersonne; 	-> Sélection 301 	-> PageAdminModifiePersonne
//	-> PagePersonnePageGarde.php					-> affiche TableauTheseModifie si doctorant
//									-> affiche TableauStageModifie si stagiaire
require_once '../control/CachedPDOStatement.php';
// Chargement des tables ...
	require_once '../actions/actionPersonneFiltre.php';
	require_once '../actions/actionTutelleListe.php';
	require_once '../actions/actionSiteListe.php';
 	require_once '../actions/actionStatutListe.php';
 	require_once '../actions/actionTypeListe.php';
 	require_once '../actions/actionFormationListe.php';
 	require_once '../actions/actionCorpsListe.php';
 require_once '../actions/actionRessourcesMessageListe.php';
    	$actionTutelle =  new actionTutelleListe;
      	$actionSite =  new actionSiteListe;
      	$actionStatut =  new actionStatutListe;
      	$actionType =  new actionTypeListe;
	$actionPersonne =  new actionPersonneFiltre;
	$actionFormation =  new actionFormationListe;
	 $actionRessMesgL=  new actionRessourcesMessageListe;
	 $actionCorps=  new actionCorpsListe;
	// cf https://stackoverflow.com/questions/15385965/php-pdo-with-foreach-and-fetch
	$listeTutelle = new CachedPDOStatement($actionTutelle->execute());
        $listeSite = new CachedPDOStatement( $actionSite->execute());
        $listeStatut =  new CachedPDOStatement($actionStatut->execute());
        $listeType =  new CachedPDOStatement($actionType->execute());
	$listePersonne =  new CachedPDOStatement($actionPersonne->execute('T','T','T','T',date('Y-m-d'),"","",0)); // Tout le monde sauf les archivés
	$listeFormation =  new CachedPDOStatement($actionFormation->execute()); // Tout le monde sauf les archivés
    $listeRessource =  new CachedPDOStatement($actionRessMesgL->execute());
	$listeCorps =   $actionCorps->execute(); 

     if (isset ( $_GET ['numAction'] )) $numAction=$_GET['numAction'];
     if (isset ( $_POST ['numAction'] )) $numAction=$_POST['numAction'];



         require_once '../actions/actionEquipeRechercheListe.php';
        $action =  new actionEquipeRechercheListe;
        $listeEquipe = new CachedPDOStatement($action->execute());

////////////////////////////////////////////////////////
// USE CASE ADMIN GERER les stagiaires gratifications
//////////////////////////////////////////////////////
if ($numAction==230) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/MenuAdminConsulteStagiaireGratification.php';
        require_once '../actions/actionPersonneEquipe.php';
        require_once '../actions/actionAdminStagiaireGratificationFiltre.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

        $action =  new actionAdminStagiaireGratificationFiltre;
        $listePersFiltre =  $action->execute();

   $vue= new MenuAdminConsulteStagiaireGratification();
    $vue->afficher($listePersFiltre,$menuDestination);
}
////////////////////////////////////////////////////////
// USE CASE ADMIN GERER les stagiaires gratifications
//////////////////////////////////////////////////////
if ($numAction==235) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/MenuAdminConsultePNA.php';


   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


require_once '../actions/actionAdminPNA.php';
         $action =  new actionAdminPNA;
        $listePersFiltre =  $action->execute($_POST);




   $vue= new MenuAdminConsultePNA();
    $vue->afficher($listePersFiltre,$menuDestination);
}





////////////////////////////////////////////////////////
// FIN
/////////////////////////////////////////////////////
//echo "<div id='header'>";
//echo '	</div>';
?>
