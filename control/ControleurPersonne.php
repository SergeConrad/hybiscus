<?php
/*********************************************************************************************************************************\
 *  Hybicus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                                                                                *
 *  Copyright (c) 2018 2019                                                                                                   *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

require_once("../web/session_start.php");
require_once '../control/CachedPDOStatement.php';
// Chargement des tables ...
	require_once '../actions/actionPersonneFiltre.php';
	require_once '../actions/actionTutelleListe.php';
	require_once '../actions/actionSiteListe.php';
 	require_once '../actions/actionStatutListe.php';
 	require_once '../actions/actionTypeListe.php';
 	require_once '../actions/actionFormationListe.php';
 	require_once '../actions/actionCorpsListe.php';
 require_once '../actions/actionRessourcesMessageListe.php';
 require_once '../actions/actionCollegeListe.php';
    	$actionTutelle =  new actionTutelleListe;
      	$actionSite =  new actionSiteListe;
      	$actionStatut =  new actionStatutListe;
      	$actionType =  new actionTypeListe;
	$actionPersonne =  new actionPersonneFiltre;
	$actionFormation =  new actionFormationListe;
	 $actionRessMesgL=  new actionRessourcesMessageListe;
	 $actionCollegeListe=  new actionCollegeListe;
	 $actionCorps=  new actionCorpsListe;
	// cf https://stackoverflow.com/questions/15385965/php-pdo-with-foreach-and-fetch
	$listeTutelle = new CachedPDOStatement($actionTutelle->execute());
        $listeSite = new CachedPDOStatement( $actionSite->execute());
        $listeStatut =  new CachedPDOStatement($actionStatut->execute());
        $listeType =  new CachedPDOStatement($actionType->execute());
	$listePersonne =  new CachedPDOStatement($actionPersonne->execute('T','T','T','T',date('Y-m-d'),"","",0)); // Tout le monde sauf les archivés
	$listeFormation =  new CachedPDOStatement($actionFormation->execute()); // Tout le monde sauf les archivés
    $listeRessource =  new CachedPDOStatement($actionRessMesgL->execute());
    $listeCollege =  new CachedPDOStatement($actionCollegeListe->execute());
	$listeCorps =   $actionCorps->execute(); 

     if (isset ( $_GET ['numAction'] )) $numAction=$_GET['numAction'];
     if (isset ( $_POST ['numAction'] )) $numAction=$_POST['numAction'];


// virer tous les $_GET et $_POST DANS LE CODE!
   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

   if (isset($_POST['selectSite']))  $selectSite = $_POST['selectSite'];
   	elseif (isset($_GET['selectSite']))  $selectSite = $_GET['selectSite'];
        else $selectSite= 'T';
   if (isset($_POST['selectStatut']))  $selectStatut = $_POST['selectStatut'];
   	elseif (isset($_GET['selectStatut']))  $selectStatut = $_GET['selectStatut'];
        else $selectStatut= 'T';
   if (isset($_POST['selectType']))  $selectType = $_POST['selectType'];
   	elseif (isset($_GET['selectType']))  $selectType = $_GET['selectType'];
        else $selectType= 'T';
   if (isset($_POST['selectDate']))  $selectDate = $_POST['selectDate'];
   	elseif (isset($_GET['selectDate']))  $selectDate = $_GET['selectDate'];
        else $selectDate=  date('Y-m-d');
   if (isset($_POST['selectArchive']))  $selectArchive = $_POST['selectArchive'];
   	elseif (isset($_GET['selectArchive']))  $selectArchive = $_GET['selectArchive'];
        else $selectArchive=  0;




if ($menuDestination=="responsableequipe")  {
        if (isset ( $_GET['selectEquipe'] )) $selectEquipe=$_GET['selectEquipe'];
           else  $selectEquipe=$_POST['selectEquipe'];
        }
else            if (isset($_POST['selectEquipe'])) $selectEquipe = $_POST['selectEquipe'];
		elseif (isset($_GET['selectEquipe'])) $selectEquipe = $_GET['selectEquipe'];
                else $selectEquipe = 'T';



         require_once '../actions/actionEquipeRechercheListe.php';
        $action =  new actionEquipeRechercheListe;
        $listeEquipe = new CachedPDOStatement($action->execute());
        $arrayEquipe=array();
        foreach ($listeEquipe as $row) $arrayEquipe[$row->eq_id]= $row->eq_nom;


function actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe)
{
require_once '../web/MenuHaut.php';

        $arrayEquipe=array();
        foreach ($listeEquipe as $row) $arrayEquipe[$row->eq_id]= $row->eq_nom;

	$action =  new actionPersonneFiltre;
       	$liste =  $action->execute($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,"","",$selectArchive);

 	require_once '../vues/PagePersonneNavigationAdmin.php';
    	$vue= new PagePersonneNavigationAdmin($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive);
    	$vue->afficher($liste,$menuDestination,$id,$listeSite,$listeStatut,$listeType,$listeEquipe,0,0,0,0,$arrayEquipe);
}









// affarch permet d'afficher les stagiaires ,cdd et invités archivés dans un contexte admin
function actionMenuGauche($menuDestination,$id,$affarch,$yeardebut,$yearfin)
{
require_once '../web/MenuHaut.php';

switch ($menuDestination) {
    case "chercheurmafiche":
	echo '<div id="NavigationBarLabo">';
	echo '</div>';
	 break;
    case "chercheurstagiaire":
                require_once '../actions/actionChercheurStagiaireFiltre.php';
                $action =  new actionChercheurStagiaireFiltre;
                $liste = $action->execute($_SESSION['p_id'],"non");
		$p="stagiaires";
        break;
    case "chercheurstagiairearchive":
                require_once '../actions/actionChercheurStagiaireFiltre.php';
                $action =  new actionChercheurStagiaireFiltre;
                $liste = $action->execute($_SESSION['p_id'],"oui");
		$p="stagiaires";
        break;
    case "chercheurdoctorant":
                require_once '../actions/actionChercheurDoctorantFiltre.php';
                $action =  new actionChercheurDoctorantFiltre;
                $liste = $action->execute($_SESSION['p_id'],"non");
		$p="doctorants";
        break;
    case "chercheurdoctorantarchive":
                require_once '../actions/actionChercheurDoctorantFiltre.php';
                $action =  new actionChercheurDoctorantFiltre;
                $liste = $action->execute($_SESSION['p_id'],"oui");
		$p="doctorants";
        break;
    case "chercheurcdd":
                require_once '../actions/actionChercheurCddFiltre.php';
                 $action =  new actionChercheurCddFiltre;
                $liste = $action->execute($_SESSION['p_id'],"non");
		$p="contractuels";
        break;
    case "chercheurcddarchive":
                require_once '../actions/actionChercheurCddFiltre.php';
                 $action =  new actionChercheurCddFiltre;
                $liste = $action->execute($_SESSION['p_id'],"oui");
		$p="contractuels";
        break;
    case "chercheurinvite":
                require_once '../actions/actionChercheurInviteFiltre.php';
                $action =  new actionChercheurInviteFiltre;
                $liste = $action->execute($_SESSION['p_id'],"non");
		$p="invites";
        break;
    case "responsablestagiaire":
		  require_once '../actions/actionResponsableStagiaireFiltre.php';
   		$action =  new actionResponsableStagiaireFiltre;
   		$liste =  $action->execute($_SESSION['responsableEquipe'],"non");
		$p="stagiaires";
		
	break;
    case "responsablestagiairearchive":
                  require_once '../actions/actionResponsableStagiaireFiltre.php';
                $action =  new actionResponsableStagiaireFiltre;
                $liste =  $action->execute($_SESSION['responsableEquipe'],"oui");
                $p="stagiaires";

        break;

    case "responsabledoctorant":
   		require_once '../actions/actionResponsableDoctorantFiltre.php';

   		$action =  new actionResponsableDoctorantFiltre;
   		$liste =  $action->execute($_SESSION['responsableEquipe'],"non");
		$p="doctorants";
	break;
    case "responsabledoctorantarchive":
                require_once '../actions/actionResponsableDoctorantFiltre.php';

                $action =  new actionResponsableDoctorantFiltre;
                $liste =  $action->execute($_SESSION['responsableEquipe'],"oui");
                $p="doctorants";
        break;

    case "responsablecdd":
   		require_once '../actions/actionResponsableCddFiltre.php';
		$action =  new actionResponsableCddFiltre;
   		$liste =  $action->execute($_SESSION['responsableEquipe'],"non");
		$p="contractuels";

	break;
    case "responsablecddarchive":
                require_once '../actions/actionResponsableCddFiltre.php';
                $action =  new actionResponsableCddFiltre;
                $liste =  $action->execute($_SESSION['responsableEquipe'],"oui");
                $p="contractuels";
        break;
    case "stagiaire":
       		require_once '../actions/actionAdminStagiaireFiltre.php';
        	$action =  new actionAdminStagiaireFiltre;

		if ($affarch==0) $liste =  $action->execute(0,0);
		else $liste =  $action->execute($yeardebut,$yearfin);
                $p="stagiaires";
        break;
    case "doctorant":
	         require_once '../actions/actionDoctorantFiltre.php';
         	$action =  new actionDoctorantFiltre;
        	$liste =  $action->execute();
                $p="doctorants";
        break;
    case "cdd":
        	require_once '../actions/actionCddFiltre.php';
        	$action =  new actionCddFiltre;
		if ($affarch==0) $liste =  $action->execute(0,0);
		else $liste =  $action->execute($yeardebut,$yearfin);
                $p="contractuels";
        break;
    case "invite":
        	require_once '../actions/actionInviteFiltre.php';
        	$action =  new actionInviteFiltre;
		if ($affarch==0) $liste =  $action->execute(0,0);
		else $liste =  $action->execute($yeardebut,$yearfin);
		$p="invites";
        break;
    case "permanent":
        	require_once '../actions/actionPermanentFiltre.php';
        	$action =  new actionPermanentFiltre;
        	$liste =  $action->execute();
		$p="permanents";
        break;
    case "archive":
         	require_once '../actions/actionArchiveFiltre.php';
         	$action =  new actionArchiveFiltre;
	        $liste =  $action->execute();
		$p="archivés";
	break;
    case "validation":
     		require_once '../actions/actionAdminValidationFiltre.php';
        	$action =  new actionAdminValidationFiltre;
        	$liste =  $action->execute();
		$p="en attente validation";

	break;
}





if (!($menuDestination=="chercheurmafiche")) {
                require_once '../vues/PagePersonneNavigation.php';
                $vue= new PagePersonneNavigation();
                $vue->afficher($liste,$menuDestination,$id,$p);
}

}



/////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////
// USE CASE CHERCEUR DECLARE STAGIAIRE
// Ces stagiaires sont ajoutés par les chercheurs via l'intranet
//////////////////////////////////////////////////////
if ($numAction==90) {

 require_once '../web/MenuHaut.php';

if ($_SESSION['arrivant']==0) $menuDestination="chercheurstagiaire";
else $menuDestination="arrivantstagiaire";
 


if ($menuDestination == "chercheurstagiaire") {

  require_once '../actions/actionChercheurStagiaireFiltre.php';
   $action =  new actionChercheurStagiaireFiltre;
   $listeStagiaire = $action->execute($_SESSION['p_id'],"non");

        require_once '../vues/PagePersonneNavigation.php';
        $vue= new PagePersonneNavigation();
        $vue->afficher($listeStagiaire,$menuDestination,0,"stagiaires");

}
else {

 echo '<div id="NavigationBarLabo">';


echo '</div>';
}




  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(1,0,1,$_SESSION['personnes'],$_SESSION['reservations'],'','','','','','','','','',$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher("",$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,'',$listeRessource,$listeCollege,"info");
}






/////////////////////////////////////////////////////////
// USE CASE CHERCEUR DECLARE DOCTORANT
// Ces stagiaires sont ajoutés par les chercheurs via l'intranet
//////////////////////////////////////////////////////
if ($numAction==95) {



 require_once '../web/MenuHaut.php';

if ($_SESSION['arrivant']==0) $menuDestination="chercheurdoctorant";
else $menuDestination="arrivantdoctorant";



if ($menuDestination == "chercheurdoctorant") {

  require_once '../actions/actionChercheurDoctorantFiltre.php';
   $action =  new actionChercheurDoctorantFiltre;
   $liste = $action->execute($_SESSION['p_id'],"non");


        require_once '../vues/PagePersonneNavigation.php';
        $vue= new PagePersonneNavigation();
        $vue->afficher($liste,$menuDestination,0,"doctorants");

}
else {

 echo '<div id="NavigationBarLabo">';


echo '</div>';
}




  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(1,0,1,$_SESSION['personnes'],$_SESSION['reservations'],'','','','','','','','','',$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher("",$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,'',$listeRessource,$listeCollege,"info");



}







/////////////////////////////////////////////////////////
// USE CASE CHERCEUR DECLARE CDD
// Ces stagiaires sont ajoutés par les chercheurs via l'intranet
//////////////////////////////////////////////////////
if ($numAction==100) {

 require_once '../web/MenuHaut.php';

if ($_SESSION['arrivant']==0) $menuDestination="chercheurcdd";
else $menuDestination="arrivantcdd";


if ($menuDestination == "chercheurcdd") {
  require_once '../actions/actionChercheurCddFiltre.php';
   $action =  new actionChercheurCddFiltre;
   $liste = $action->execute($_SESSION['p_id'],"non");

        require_once '../vues/PagePersonneNavigation.php';
        $vue= new PagePersonneNavigation();
        $vue->afficher($liste,$menuDestination,0,"contractuels");

}
else {

 echo '<div id="NavigationBarLabo">';


echo '</div>';
}




  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(1,0,1,$_SESSION['personnes'],$_SESSION['reservations'],'','','','','','','','','',$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher("",$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,'',$listeRessource,$listeCollege,"info");



}




/////////////////////////////////////////////////////////
// USE CASE CHERCEUR DECLARE INVITE 
// Ces invités sont ajoutés par les chercheurs via l'intranet
//////////////////////////////////////////////////////
if ($numAction==105) {
 require_once '../web/MenuHaut.php';

if ($_SESSION['arrivant']==0) $menuDestination="chercheurinvite";
else $menuDestination="arrivantinvite";



if ($menuDestination == "chercheurinvite") {

 require_once '../actions/actionChercheurInviteFiltre.php';
   $action =  new actionChercheurInviteFiltre;
   $liste = $action->execute($_SESSION['p_id'],"non");


        require_once '../vues/PagePersonneNavigation.php';
        $vue= new PagePersonneNavigation();
        $vue->afficher($liste,$menuDestination,0,"invites");

}
else {

 echo '<div id="NavigationBarLabo">';


echo '</div>';
}




  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(1,0,1,$_SESSION['personnes'],$_SESSION['reservations'],'','','','','','','','','',$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher("",$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,'',$listeRessource,$listeCollege,"info");



}


/////////////////////////////////////////////////////////
// USE CASE ADMIN Ajouter une personne 
//100
//////////////////////////////////////////////////////
if ($numAction==200) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePersonneNavigationAdmin.php';


$menuDestination = 'laboratoire';
   $vue= new PagePersonneNavigationAdmin('T','T','T','T',date('Y-m-d'),$selectArchive);
    $vue->afficher($listePersonne,$menuDestination,0,$listeSite,$listeStatut,$listeType,$listeEquipe,0,0,0,0,$arrayEquipe);


// 	require_once '../actions/actionCorpsListe.php';
//	$actionCorps =  new actionCorpsListe;
//	$listeCorps =  $actionCorps->execute($_POST['ty_id']); // Tout le monde sauf les archivés


  require_once '../vues/PagePersonne.php';

        $vue2= new PagePersonne(1,0,0,$_SESSION['personnes'],$_SESSION['reservations'],'','','','','','','','','',$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);

        $vue2->afficher("",$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,'',$listeRessource,$listeCollege,"info" );

}




////////////////////////////////////////////////////////
// USE CASE ADMIN GERER les stagiaires
//80
//////////////////////////////////////////////////////
if ($numAction==220) {
 require_once '../web/MenuHaut.php';
        require_once '../actions/actionPersonneEquipe.php';
        require_once '../actions/actionAdminStagiaireFiltre.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


   if (isset ( $_POST['yeardebut'] )) $yeardebut=$_POST['yeardebut'];
	else   $yeardebut= date('Y');
   if (isset ( $_POST['yearfin'] )) $yearfin=$_POST['yearfin'];
	else   $yearfin= date('Y');


if (isset($_POST['affarch'])) $affarch=1;
else $affarch=0;



//if ($affarch==0) {
//        $action =  new actionAdminStagiaireFiltre;
//        $listePersFiltre =  $action->execute(0,0);
//}
//else {
//        require_once '../actions/actionAdminStagiaireFiltreComplet.php';
        $action =  new actionAdminStagiaireFiltre;
        $listePersFiltre =  $action->execute($yeardebut,$yearfin);
//}

actionMenuGauche($menuDestination,0,$affarch,$yeardebut,$yearfin);


   require_once '../vues/PageStagiairePageGarde.php';
    $vue2= new PageStagiairePageGarde($yeardebut,$yearfin,$affarch);
    $vue2->afficher($menuDestination);
}


////////////////////////////////////////////////////////
// USE CASE ADMIN GERER les stagiaires gratifications
//////////////////////////////////////////////////////
if ($numAction==230) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/MenuAdminConsulteStagiaireGratification.php';
        require_once '../actions/actionPersonneEquipe.php';
        require_once '../actions/actionAdminStagiaireGratificationFiltre.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

        $action =  new actionAdminStagiaireGratificationFiltre;
        $listePersFiltre =  $action->execute();

   $vue= new MenuAdminConsulteStagiaireGratification();
    $vue->afficher($listePersFiltre,$menuDestination);
}
////////////////////////////////////////////////////////
// USE CASE ADMIN GERER les stagiaires gratifications
//////////////////////////////////////////////////////
if ($numAction==235) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/MenuAdminConsultePNA.php';


   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


require_once '../actions/actionAdminPNA.php';
         $action =  new actionAdminPNA;
        $listePersFiltre =  $action->execute($_POST);




   $vue= new MenuAdminConsultePNA();
    $vue->afficher($listePersFiltre,$menuDestination);
}


////////////////////////////////////////////////////////
// USE CASE : ADMIN GERER les permanents
//81
//////////////////////////////////////////////////////
if ($numAction==240) {
 require_once '../web/MenuHaut.php';
        require_once '../actions/actionPermanentFiltre.php';



   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];



actionMenuGauche($menuDestination,0,0,0,0);



  $year= date('Y');
   require_once '../vues/PagePermanentPageGarde.php';
    $vue2= new PagePermanentPageGarde($year);
    $vue2->afficher();

}


////////////////////////////////////////////////////////
// USE CASE : ADMIN :GERER les doctorants

//82
//////////////////////////////////////////////////////
if ($numAction==260) {
 require_once '../web/MenuHaut.php';
        require_once '../actions/actionDoctorantFiltre.php';


   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

//        $action =  new actionDoctorantFiltre;
//        $listePersFiltre =  $action->execute();

actionMenuGauche($menuDestination,0,0,0,0);



   $year= date('Y');
   require_once '../vues/PageDoctorantPageGarde.php';
    $vue2= new PageDoctorantPageGarde($year);
    $vue2->afficher();

}

////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des theses en cours
//////////////////////////////////////////////////////
if ($numAction==270) {
        require_once '../actions/actionDoctorantExportTheseEnCours.php';
        $action =  new actionDoctorantExportTheseEnCours;
        $action->execute();
}

////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des theses soutenues
//////////////////////////////////////////////////////
if ($numAction==271) {
	$year=$_POST['year'];

        require_once '../actions/actionDoctorantExportTheseSoutenue.php';
        $action =  new actionDoctorantExportTheseSoutenue;
        $action->execute($year);
}

////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des theses abandonnées
//////////////////////////////////////////////////////
if ($numAction==272) {
        require_once '../actions/actionDoctorantExportTheseAbandon.php';
        $action =  new actionDoctorantExportTheseAbandon;
        $action->execute();
}



////////////////////////////////////////////////////////
// USE CASE ADMIN GERER les contractuels
//////////////////////////////////////////////////////
if ($numAction==280) {
 require_once '../web/MenuHaut.php';
        require_once '../actions/actionPersonneEquipe.php';
        require_once '../actions/actionCddFiltre.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


   if (isset ( $_POST['yeardebut'] )) $yeardebut=$_POST['yeardebut'];
        else   $yeardebut= date('Y');
   if (isset ( $_POST['yearfin'] )) $yearfin=$_POST['yearfin'];
        else   $yearfin= date('Y');


if (isset($_POST['affarch'])) $affarch=1;
else $affarch=0;



//if ($affarch==0) {
//        $action =  new actionCddFiltre;
//        $listePersFiltre =  $action->execute(0,0);
//}
//else {
//        $action =  new actionCddFiltre;
//        $listePersFiltre =  $action->execute($yeardebut,$yearfin);
//}



actionMenuGauche($menuDestination,0,$affarch,$yeardebut,$yearfin);

//    $vue->afficher($listePersFiltre,$menuDestination,'','','','');

   require_once '../vues/PageCddPageGarde.php';
    $vue2= new PageCddPageGarde($yeardebut,$yearfin,$affarch);
    $vue2->afficher($menuDestination);
}


// USE CASE: ADMIN GERER les post doc et cdd
// 83
//////////////////////////////////////////////////////
//if ($numAction==280) {
//$_SESSION['mode']="cdd";
// require_once '../web/MenuHaut.php';



//   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
//   else  $menuDestination=$_POST['menuDestination'];


//        require_once '../actions/actionCddFiltre.php';
//        $action =  new actionCddFiltre;
//        $listePersFiltre =  $action->execute();


//   $year= date('Y');
//   require_once '../vues/PageCddPageGarde.php';
//    $vue2= new PageCddPageGarde($year,$year);
//    $vue2->afficher();



//}


/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PERSONNELS
// PERMET DE TRIER SELON TYPE, TUTELLE, STATUT...
// Gestion des appels de ../vues/MenuAdminTriPersonne.php
// 104
//////////////////////////////////////////////////////
if ($numAction==300) {
 require_once '../web/MenuHaut.php';
require_once '../vues/PagePersonneNavigationAdmin.php';
require_once '../actions/actionPersonneFiltre.php';


         $action =  new actionPersonneFiltre;
        $listePersFiltre =  $action->execute($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,"","",$selectArchive);



    $vue= new PagePersonneNavigationAdmin($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive);
    $vue->afficher($listePersFiltre,$menuDestination,0,$listeSite,$listeStatut,$listeType,$listeEquipe,0,0,0,0,$arrayEquipe);

require_once '../actions/actionEffectif.php';
//serge new
$action3 =  new actionEffectif($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive);
$listeEffectif = $action3->execute($_GET['eq_id']);


	require_once '../actions/actionPersonneFiltre3.php';
        $action4 =  new actionPersonneFiltre3;


//        $listeStagiaire =  new CachedPDOStatement($action4->execute($selectSite,3,2,$selectEquipe));
        $listePersPageGarde =  new CachedPDOStatement($action4->execute($selectSite,$selectEquipe));

//        $listeStagiaire =  $action->execute($selectSite,3,2,$selectEquipe,$selectDate,"","",0);
//        $listeStagiaireArchive =  $action->execute($selectSite,3,2,$selectEquipe,$selectDate,"","",1);


//        $listeDoctorant =  $action->execute($selectSite,3,1,$selectEquipe,$selectDate,"","",0);
//        $listeDoctorantArchive =  $action->execute($selectSite,3,1,$selectEquipe,$selectDate,"","",1);



//        $listeCdd =  $action->execute($selectSite,2,"T",$selectEquipe,$selectDate,"","",0);
//        $listeCddArchive =  $action->execute($selectSite,2,"T",$selectEquipe,$selectDate,"","",1);



//fin

$listeEffectifSexe = $action3->sexe();

    require_once '../vues/PagePersonnePageGarde.php';
    $vue2= new PagePersonnePageGarde;
//serge new
$nomEquipe =$arrayEquipe[ $selectEquipe ];
if (!($nomEquipe =='')) {
        require_once '../actions/actionCahierListe.php';
        $action =  new actionCahierListe;
        $cahiersEquipe =  $action->pourLequipe($selectEquipe);
}

//    $vue2->afficher($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$menuDestination,$listeEffectif,$listeEffectifSexe,$listeStagiaire,$listeStagiaireArchive,$listeDoctorant,$listeDoctorantArchive,$listeCdd,$listeCddArchive,$nomEquipe,$cahiersEquipe);
    $vue2->afficher($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$menuDestination,$listeEffectif,$listeEffectifSexe,$listePersPageGarde,$nomEquipe,$cahiersEquipe);


}


/////////////////////////////////////////////////////////
// USE CASE ADMIN MODIFIE PERSONNE
// appuyer sur le bouton submit de la vue PagePersonne.php onglet INFOS ADMIN
//////////////////////////////////////////////////////

if (substr( $numAction, 0, 3 ) == '314') {
    $tab= $_COOKIE["tab"];


 require_once '../formulaires/FormInformationAdmin.php';
         $menuDestination=$_POST['menuDestination'];
// recuperér l'onglet actif de FormAdminModifiePersonne
$tab= $_COOKIE["tab"];


try {

             $formAdmin = new FormInformationAdmin($_POST);

       require_once '../actions/actionInfoAdmin.php';
        $action =  new actionInfoAdmin();
        $action->execute($formAdmin); // 0 pas de validation en attente
       $id=$formAdmin->getId();

        $messageretour = "Enregistré";
        $ajouthistorique=0;
        $tab="admin";


if ($menuDestination=="laboratoire")
	// actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);


    require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);




  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,$ajouthistorique,$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($messageretour,$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);





} catch (Exception $e) {
 require_once '../vues/PagePersonne.php';


        $id=$_POST['p_id'];
        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);

if ($menuDestination=="laboratoire")
// actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);



        $vue2= new PagePersonne($_POST['creation'],$_POST['ajouthistorique'],$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$_POST,$tabhistorique,$listePersonneEquipe,$historiquen,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$_POST['menuDestination'],'',0,$_POST['ajoutfinancementstage'],$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($e->getMessage(),$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);



}
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN MODIFIE PERSONNE
// appuyer sur le bouton submit de la vue PagePersonne.php onglet PERMANENT
//////////////////////////////////////////////////////

if (substr( $numAction, 0, 3 ) == '313') {
    $tab= $_COOKIE["tab"];
        $menuDestination = $_POST['menuDestination'];
  try {
        // On valide le formulaire
        require_once '../formulaires/FormPermanent.php';
        $permanent = new FormPermanent($_POST);

        require_once '../actions/actionPermanent.php';
        $action =  new actionPermanent();
        $action->execute($permanent);

        // ON AFFICHE L'UTILISATEUR
	$messageretour= "Enregistrement sauvegardé";
        //$messageretour= "Permanent enregistré";





        $id = $permanent->getId();
if ($menuDestination=="laboratoire") 
//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);

        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);



        require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,0,$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($messageretour,$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);


       } catch (Exception $e) {



        $id = $_POST['p_id'];
if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);
        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);

        require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne($_POST['creation'],$_POST['ajouthistorique'],$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,$historiquen,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$_POST['menuDestination'],'',0,$_POST['ajoutfinancementstage'],$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($e->getMessage(),$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);
}
}





/////////////////////////////////////////////////////////
// USE CASE ADMIN MODIFIE PERSONNE
// appuyer sur le bouton submit de la vue PagePersonne.php onglet INVITE 
//////////////////////////////////////////////////////

if (substr( $numAction, 0, 3 ) == '309') {
    $tab= $_COOKIE["tab"];
        $menuDestination = $_POST['menuDestination'];

  try {
        // On valide le formulaire
        require_once '../formulaires/FormInvite.php';
        $invite = new FormInvite($_POST);

        require_once '../actions/actionInvite.php';
        $action =  new actionInvite();
        $action->execute($invite);

        // ON AFFICHE L'UTILISATEUR
$messageretour= "Enregistrement sauvegardé";
//        $messageretour= "Invité enregistré";



        $id = $invite->getId();
if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);

        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);



        require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,0,$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($messageretour,$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);


       } catch (Exception $e) {



        $id = $_POST['p_id'];
if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);
        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);

        require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne($_POST['creation'],$_POST['ajouthistorique'],$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,$historiquen,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$_POST['menuDestination'],'',0,$_POST['ajoutfinancementstage'],$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($e->getMessage(),$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);
}
}


/////////////////////////////////////////////////////////
// USE CASE ADMIN MODIFIE PERSONNE
// appuyer sur le bouton submit de la vue PagePersonne.php onglet CONTRAT 
//////////////////////////////////////////////////////

if (substr( $numAction, 0, 3 ) == '308') {
    $tab= $_COOKIE["tab"];
        $menuDestination = $_POST['menuDestination'];
    try {
        // On valide le formulaire
        require_once '../formulaires/FormCdd.php';
        $cdd = new FormCdd($_POST);

        require_once '../actions/actionCdd.php';
        $action =  new actionCdd();
        $action->execute($cdd);

	// ON AFFICHE L'UTILISATEUR
$messageretour= "Enregistrement sauvegardé";


        $id = $cdd->getId();
if ($menuDestination=="laboratoire")
	// actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);

        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);



	require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,0,$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($messageretour,$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);


       } catch (Exception $e) {



        $id = $_POST['p_id'];
if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectDate'],$_POST['selectEquipe'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);
        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);

        require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne($_POST['creation'],$_POST['ajouthistorique'],$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,$historiquen,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$_POST['menuDestination'],'',0,$_POST['ajoutfinancementstage'],$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($e->getMessage(),$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);
}
}
/////////////////////////////////////////////////////////
// USE CASE ADMIN MODIFIE PERSONNE
// appuyer sur le bouton submit de la vue PagePersonne.php onglet THESES
//////////////////////////////////////////////////////

if (substr( $numAction, 0, 3 ) == '307') {
    $tab= $_COOKIE["tab"];
      // le menuDestination peut être chercheurstagiaire lorsq'un chercheur ajoute un stagiaire
        if (isset($_POST['menuDestination'])) $menuDestination = $_POST['menuDestination'];
         else    $menuDestination="doctorant";
    try {

        // On valide le formulaire
         require_once '../formulaires/FormDoctorant.php';
        $these = new FormDoctorant($_POST);

              require_once '../actions/actionThese.php';
                $action =  new actionThese();
                $action->execute($these);

// ON AFFICHE L'UTILISATEUR
$messageretour= "Enregistrement sauvegardé";
//$messageretour= "These enregistrée";





        $id = $these->getId();
if ($menuDestination=="laboratoire")
	// actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);

        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);



  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,0,$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($messageretour,$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);

       } catch (Exception $e) {



        $id = $_POST['p_id'];
if ($menuDestination=="laboratoire")
	// actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);
        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);

	require_once '../vues/PagePersonne.php';
	$vue2= new PagePersonne($_POST['creation'],$_POST['ajouthistorique'],$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,$historiquen,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$_POST['menuDestination'],'',0,$_POST['ajoutfinancementstage'],$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($e->getMessage(),$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);


}

}

/////////////////////////////////////////////////////////
// USE CASE ADMIN MODIFIE PERSONNE
// appuyer sur le bouton submit de la vue PagePersonne.php onglet STAGES
//////////////////////////////////////////////////////

if (substr( $numAction, 0, 3 ) == '306') {
    $tab= $_COOKIE["tab"];
      // le menuDestination peut être chercheurstagiaire lorsq'un chercheur ajoute un stagiaire
        //if (isset($_POST['menuDestination'])) $menuDestination = $_POST['menuDestination'];
        // else    $menuDestination="stagiaire";
        $menuDestination = $_POST['menuDestination'];
    try {

        // On valide le formulaire
         require_once '../formulaires/FormStagiaire.php';
        $stage = new FormStagiaire($_POST);


if  (substr($numAction,3,3) == 'DEL')  {
$liste = $stage->getlistefinancement();
unset($liste[substr($numAction,6)]);
$stage->setlistefinancement($liste);

 }






        // Si le scan de la convention a été uploadée, on la  charge sur le serveur....
        if(strlen($_FILES['scanConvention']['name'])!=0) {
                // liste des réservations
                require_once '../actions/actionUpload.php';
                $action5 =  new actionUpload;
                $action5->execute($stage,"scanConvention");
       }




        	require_once '../actions/actionStage.php';
	        $action =  new actionStage();
	        $action->execute($stage); 

// ON AFFICHE L'UTILISATEUR
$messageretour= "Enregistrement sauvegardé";



        $id = $stage->getId();
if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);
        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);




  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,0,$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($messageretour,$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);

       } catch (Exception $e) {




        $id = $_POST['p_id'];
if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);

        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);




  require_once '../vues/PagePersonne.php';
      $vue2= new PagePersonne($_POST['creation'],$_POST['ajouthistorique'],$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,$historiquen,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$_POST['menuDestination'],'',0,$_POST['ajoutfinancementstage'],$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($e->getMessage(),$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);


}

}

/////////////////////////////////////////////////////////
// USE CASE ADMIN MODIFIE PERSONNE
// appuyer sur le bouton submit de la vue PagePersonne.php onglet historique
//////////////////////////////////////////////////////

if (substr( $numAction, 0, 3 ) == '305') {
         $menuDestination=$_POST['menuDestination'];
    try {
         //$menuDestination="laboratoire";
         $menuDestination=$_POST['menuDestination'];
         require_once '../formulaires/FormHistorique.php';
         $tabhistorique = new FormHistorique($_POST);
	if ( $_POST['ajouthistorique']==1) {
		$messageretour= "Appuyez sur Enregistrer pour finaliser l'inscription";

        	require_once '../actions/actionHistoriqueAjoute.php';
	        $action =  new actionHistoriqueAjoute();
	        $numHistorique=$action->execute($tabhistorique); 

	        // ACTION MESSAGE a destination de l'équipe gestion et  des gestionnaires des ressources
	       require_once '../actions/actionMessageAvertissement.php';
	       $action =  new actionMessageAvertissement();
	       $action->execute($tabhistorique->getHistoriquen()->ressourcen,$_SESSION['user'],$tabhistorique,$listeRessource,$listeEquipe);

	       // ACTION MESSAGE a destination du nouvel arrivant
	       require_once '../actions/actionMessageBienvenue.php';
	       $action =  new actionMessageBienvenue();
	       $action->execute($tabhistorique->getId(),"",$tabhistorique->getHistoriquen()->ressourcen,$listeRessource);

		$messageretour= "Enregistrement non terminé, veuillez enregistrer";
        	//$messageretour= "Message avertissement envoyé aux gestionnaires des ressources";
	}
	else {

	     // Le numéro du premier historique
	        $key = key($tabhistorique->getHistorique());

	        // ON CHARGE LES RESSOURCES INITIALES POUR LE PREMIER HISTORIQUE TELS QUELS SONT DANS LA BASE DE DONNEES
	        require_once '../actions/actionPersonneRessourceListe.php';
	        $action =  new actionPersonneRessourceListe();
        	$listeRessourcesIni =  $action->execute($key);

		// MODIFICATION BD
        	require_once '../actions/actionHistoriqueModifie.php';
	        $action =  new actionHistoriqueModifie();
	        $action->execute($tabhistorique); 
		$messageretour= "Enregistré";

		/////////////////////////////////////////////////////////////////
		///////////// ENVOI MESSAGES ... en cas de modification
		// les ressources actuellement sélectionnées dans le premier historique:
		$lrHistorique = array_pop(array_reverse($tabhistorique->getHistorique()))->ressource;


		// Si Ajout ressource dans le premier historique on envoit un message avertissement au gestionnaire de la ressource
		$send=0;
		foreach	($lrHistorique as $r)  if (!(in_array($r, $listeRessourcesIni))) $send=1 ;
//echo "KEY".$key;
//var_dump($listeRessourcesIni);
//var_dump($lrHistorique);
//echo "SEND".$send;
//die;

		// Si Ajout ressource dans le premier historique on envoit un message avertissement au gestionnaire de la ressource
		if ($send ==1) {
		        // ACTION MESSAGE a destination de l'équipe gestion et  des gestionnaires des ressources
		        require_once '../actions/actionMessageAvertissementModification.php';
		        $action =  new actionMessageAvertissementModification();
		        $action->execute($listeRessourcesIni,$lrHistorique,$_SESSION['user'],$tabhistorique,$listeRessource,$listeEquipe);

		$messageretour= "Message avertissement envoyé au gestionnaire de la ressource";
		}

	}







	$id = $tabhistorique->getId();
//	actionMenuGauche($menuDestination,$id,0,0,0);

if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);



	// On affichera l'onglet de l'historique que l'on vient de créér
	$tab="historique".$numHistorique;
        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);



  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,0,$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($messageretour,$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);


       } catch (Exception $e) {
  require_once '../vues/PagePersonne.php';




	$id = $_POST['p_id'];
if ($menuDestination=="laboratoire")
	// actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);
        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);




require_once '../objets/Historiquen.php';
$ressourcen=array();
foreach ($listeRessource as $ressource) if ($_POST[$ressource->rid.'n']) array_push ($ressourcen,$ressource->rid);

$historiquen = new Historiquen ($_POST['sit_idn'],$_POST['st_idn'],$_POST['ty_idn'],$_POST['dateDebutHn'],$_POST['dateFinHn'],$_POST['eq_idn'],$ressourcen,$_POST["stageIdn"],$_POST["theseIdn"],$_POST["contractuel_idn"],$_POST["permanent_idn"],$_POST["invite_idn"]);








        $vue2= new PagePersonne($_POST['creation'],$_POST['ajouthistorique'],$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,$historiquen,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$_POST['menuDestination'],'',0,$_POST['ajoutfinancementstage'],$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($e->getMessage(),$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,"historique");


}

}

/////////////////////////////////////////////////////////
// USE CASE ADMIN MODIFIE PERSONNE
// appuyer sur le bouton submit de la vue PagePersonne.php onglet info personnelles
//////////////////////////////////////////////////////

if (substr( $numAction, 0, 3 ) == '304') {
 require_once '../formulaires/FormInformationsPersonnelles.php';
         $menuDestination=$_POST['menuDestination'];
// recuperér l'onglet actif de FormAdminModifiePersonne
$tab= $_COOKIE["tab"];


try {
//////////////////////////////////
///////////////////////// CREATION
if ( $_POST['creation']==1) {
        $personne = new FormInformationsPersonnelles($_POST);
	if ( $_SESSION['arrivant']==0) {
		// Pour les arrivants, on désactive le système de recherche de doublon
        	require_once '../actions/actionRechercheDoublon.php';
	        $action4 =  new actionRechercheDoublon();
	        $doublon = $action4->execute($personne); // Validation demandée

		//////////////////////////////////////////////:
		////////////////////// CREATION DOUBLON DEMANDE CONFIRMATION
	        if ((substr($numAction,3,5)!="FORCE")&&(substr($numAction,3,5)!="RECUP")&&($doublon!=false)) {
	                throw new Exception('Un homonyme (d\'adresse email '.$doublon['p_mail'].'et de date de Naissance '.$doublon['dateNaissance'].') est déjà présent dans la base de données".  Voulez vous récupérez les données ? ');
        }
	}




	/////////////////////////////////////////////
	//////////////////// ON NE CREE PLUS , ON CHARGE L'ORIGINAL
        if (substr($numAction,3,5)=="RECUP") {
		// On récupère le doublon et on l'affiche
        $id = $doublon['p_id'];

	$messageretour= "Chargement, il faut ajouter un historique";
	$ajouthistorique=1;
	$tab="historique";

        }
	//////////////////////////////////////////////////////
	/////////////////// ON PROCEDE A LA CREATION
	else {

        require_once '../actions/actionPersonneAjoute.php';
        $action =  new actionPersonneAjoute();
        // Si c'est un chercheur qui ajoute un non permanent, il doit être validé par l'accueil
        $id = $action->execute($personne,$_POST['chercheur']); // 0 pas de validation en attente
if ($_SESSION['p_id']=="NEW") $_SESSION['p_id']=$id;

	$messageretour= "Enregistrement non terminé, veuillez enregistrer";
	$ajouthistorique=1;
	$tab="historique";
	}	
}
else {
/////////////////////////////////////////////////////////
///////////////////////MODIFICATION
              $personne = new FormInformationsPersonnelles($_POST);



if  (substr($numAction,3,3) == 'VAL') {
// validation !!
        echo 'VALIDATION'.$personne->getDemandeIntranet();
        $personne->setDemandeIntranet(0);
        echo 'FINVALIDATION'.$personne->getDemandeIntranet();
        //$_POST['demandeIntranet']=0;

      // ACTION MESSAGE


	   require_once '../actions/actionMailBienvenueCC.php';
           $actionCC =  new actionMailBienvenueCC();
           $cc =  $actionCC->execute($personne->getId());


//     public function execute($id,$cc,$lrid,$listeRessource){
//               $action->execute($tabhistorique->getId(),"",$tabhistorique->getHistoriquen()->ressourcen,$listeRessource);


        // ACTION MESSAGE a destination du nouvel arrivant
 //       require_once '../actions/actionMessageBienvenue.php';
 //       $action =  new actionMessageBienvenue();
 //       $action->execute($personne->getId(),$cc);


// message aux gestionnaires de ressources ?

}

       require_once '../actions/actionPersonneModifie.php';
        $action =  new actionPersonneModifie();
        $action->execute($personne); // 0 pas de validation en attente
       $id=$personne->getId();



	$messageretour = "Enregistré";
	$ajouthistorique=0;
	$tab="info";

}



if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);




    require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);




  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,$ajouthistorique,$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($messageretour,$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);





} catch (Exception $e) {
  require_once '../vues/PagePersonne.php';


	$id=$_POST['p_id'];
        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);


if ($menuDestination=="laboratoire")
	// actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);




        $vue2= new PagePersonne($_POST['creation'],$_POST['ajouthistorique'],$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$_POST,$tabhistorique,$listePersonneEquipe,$historiquen,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$_POST['menuDestination'],'',0,$_POST['ajoutfinancementstage'],$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
        $vue2->afficher($e->getMessage(),$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);


}

}
/////////////////////////////////////////////////////////
// USE CASE ADMIN MODIFIE PERSONNE
// appuyer sur le bouton submit de la vue PagePersonne.php VALIDATION
//////////////////////////////////////////////////////
if (substr( $numAction, 0, 3 ) == '302') {


}


////////////////////////////////////////////////////////
// Modifier une personne 
//////////////////////////////////////////////////////
if (substr( $numAction, 0, 3 ) == '303') {
## TRAD
require_once '../web/MenuHaut.php';

// permet de récupérer l'onglet actif de la page personne
$tab= $_COOKIE["tab"];
if ($tab=="") $tab="historique";
 //require_once '../vues/MenuAdminTriPersonne.php';
        require_once '../actions/actionPersonneEquipe.php';
        require_once '../actions/actionPersonneFiltre.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

if (($menuDestination=="chercheurdoctorantarchive")||($menuDestination=="chercheurstagiaire") || ($menuDestination=="chercheurcdd") || ($menuDestination=="chercheurdoctorant") || ($menuDestination=="chercheurinvite") || ($menuDestination=="chercheursalle") || ($menuDestination=="chercheurmafiche")) $chercheur=1;
else $chercheur=0;

// surement inutile: a virer
if (($menuDestination=="responsablestagiaire") || ($menuDestination=="responsablecdd") || ($menuDestination=="responsabledoctorant") || ($menuDestination=="responsablestagiairearchive") || ($menuDestination=="responsablecddarchive") || ($menuDestination=="responsabledoctorantarchive")||($menuDestination=="responsableequipe")) {
		$responsable=1;
		$eq_id=$_GET['eq_id'];
		}
else $responsable=0;




if  ((substr($numAction,3,3) == 'ADH') ||(substr($numAction,3,3)=='DUP')||($_POST['ajouthistorique']==1)) $ajouthistorique=1;
else $ajouthistorique=0;

///////////////////////////////////////////////////////////////////////////////////////////////////
        // premier appel, on charge depuis la base de données
   if ((isset ( $_GET['id'] )&&($_GET['id']!='NEW'))||(substr($numAction,3,3) == 'REC')) {
	if (substr($numAction,3,3) == 'REC') $id=$_POST['p_id'];

	else        $id=$_GET['id'];

if (($menuDestination=="laboratoire")||($menuDestination=="responsableequipe"))
	// actionMenuGaucheLaboratoire($menuDestination,$id,$_GET['selectSite'],$_GET['selectStatut'],$_GET['selectType'],$_GET['selectEquipe'],$_GET['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);


        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);



  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,$ajouthistorique,$chercheur,$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_GET['selectSite'],$_GET['selectStatut'],$_GET['selectType'],$_GET['selectEquipe'],$_GET['selectDate']);



        $vue2->afficher("",$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);



        }

///////////////////////////////////////////////////////////////////////////////////////////////////
        // Ce  n'est pas un premier appel
        // on récupére les données dans le formulaire
///////////////////////////////////////////////////////////////////////////////////////////////////
else {

if (($menuDestination=="laboratoire")||($menuDestination=="responsableequipe"))
	// actionMenuGaucheLaboratoire($menuDestination,0,$_GET['selectSite'],$_GET['selectStatut'],$_GET['selectType'],$_GET['selectEquipe'],$_GET['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,0,0,0,0);

$creation=$_POST['creation'];
if ($_SESSION['p_id']=="NEW") $creation=1;

//  on récupère les données dans  le $_POST directement !!!!



if  (substr($numAction,3,3) == 'DEH') {
	 require_once '../actions/actionHistoriqueSupprime.php';
         $actionhs =  new actionHistoriqueSupprime;
        $tabhistorique =  $actionhs->execute($_POST['numHistorique'.substr($numAction,6,1)]);
}

if ($_POST['p_id']!="") {

	 require_once '../actions/actionPersonneHistoriqueGet.php';
        // charge depuis la base de données, rend un tableau associatif
         $action6 =  new actionPersonneHistoriqueGet;
 	$tabhistorique = new CachedPDOStatement($action6->execute($_POST['p_id']));

}







// le nouvel historique
         require_once '../objets/Historiquen.php';


$ressourcen=array();
foreach ($listeRessource as $ressource) if ($_POST[$ressource->rid.'n']) array_push ($ressourcen,$ressource->rid); 




if  (substr($numAction,3,3) == 'DUP') {
$s1="sit_id".substr($numAction,6,1);
$s2="st_id".substr($numAction,6,1);
$s3="ty_id".substr($numAction,6,1);
$s4="dateDebutH".substr($numAction,6,1);
$s5="dateFinH".substr($numAction,6,1);
$s6="eq_id".substr($numAction,6,1);
$s7= "listeEquipePersonne".substr($numAction,6,1);
$stageId="stageId".substr($numAction,6,1);
$theseId="theseId".substr($numAction,6,1);
$contractuel_id="contractuel_id".substr($numAction,6,1);
$permanent_id="permanent_id".substr($numAction,6,1);
$invite_id="invite_id".substr($numAction,6,1);

$historiquen = new Historiquen ($_POST["$s1"],$_POST["$s2"],$_POST["$s3"],$_POST["$s4"],$_POST["$s5"],$_POST["$s6"],$ressourcen,$_POST["$stageId"],$_POST["$theseId"],$_POST["$contractuel_id"],$_POST["$permanent_id"],$_POST["$invite_id"]);
}

//else $historiquen = new Historiquen ($_POST['sit_idn'],$_POST['st_idn'],$_POST['ty_idn'],$_POST['dateDebutHn'],$_POST['dateFinHn'],$_POST['eq_idn'],$ressourcen,0,0,0,0,0);
else $historiquen = new Historiquen ($_POST['sit_idn'],$_POST['st_idn'],$_POST['ty_idn'],$_POST['dateDebutHn'],$_POST['dateFinHn'],$_POST['eq_idn'],$ressourcen,$_POST["stageIdn"],$_POST["theseIdn"],$_POST["contractuel_idn"],$_POST["permanent_idn"],$_POST["invite_idn"]);



if  (substr($numAction,3,3) == 'AEQ') {
// On ajoute une équipe pour la personne
         require_once '../actions/actionPersonneAjouteEquipe.php';
         $action5 =  new actionPersonneAjouteEquipe;
         $var='numHistorique'.substr($numAction,6,1);
         if (substr($numAction,6,1)!='n')$action5->execute($_POST[$var],$_POST["listeEquipe".substr($numAction,6,1)]);
	//else $historiquen->eq_idn = $_POST["listeEquipe".substr($numAction,6,1)];
	else $historiquen->eq_idn = $_POST["listeEquipen"];
}
elseif  (substr($numAction,3,3) == 'SEQ') {
//on supprime une équipe 
         require_once '../actions/actionPersonneSupprimeEquipe.php';
         $action5 =  new actionPersonneSupprimeEquipe;
         $var='numHistorique'.substr($numAction,6,1);
         $action5->execute($_POST[$var],$_POST['listeEquipePersonne'.substr($numAction,6,1)]);
}

 //les équipes sont liées à 'historique
         $action2 =  new actionPersonneEquipe;
        foreach ($tabhistorique as $row) {
            
                $num = $row->numHistorique;
                $listeEq =  $action2->execute($num);
                $arrayListeEq =array();
                //foreach ($listeEq as $r) array_push($arrayListeEq,$r->eq_id);
                foreach ($listeEq as $r) $arrayListeEq[$r->eq_id]=$r->eq_nom;

                $listePersonneEquipe[$num] =  $arrayListeEq;
        }
///////////////
// Les stagiaires
// AFF correspond au bouton VOIR de la partie historique
// on verifie alors que c'est bien d' un stagiaire qu'on parle
// ADD correspond au + de la partie credit eotp du stage




if  (substr($numAction,3,3) == 'ADD') $ajoutfinancementstage=1;
//elseif ((($_POST['creation']=="1")||($_POST['ajouthistorique']==1))&&($_POST['st_idn']=="3")&&($_POST['ty_idn']=="2")) $ajoutfinancementstage=1;
//else $ajoutfinancementstage=0;

                require_once '../actions/actionStageListe.php';
                $action4 =  new actionStageListe;
                $listestage = $action4->execute( $_POST['p_id']);

if  (substr($numAction,3,3) == 'DEL')  {
	//var_dump ($listestage[$_POST['numHistorique']]['listeFinancement']);
	unset ($listestage[$_POST['numHistorique']]['listeFinancement'][substr($numAction,6)]);
	//var_dump ($listestage[$_POST['numHistorique']]['listeFinancement']);
}


        $id= $_POST['p_id'];

        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);



  require_once '../vues/PagePersonne.php';
        // $listeFinancement intégrée à $listestage
        $vue2= new PagePersonne($creation,$ajouthistorique,$_POST['chercheur'],$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,$historiquen,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'','',$ajoutfinancementstage,$listeselect[1],$listeselect[2],$listeselect[3],$listeselect[4],$listeselect[5]);

        $vue2->afficher("",$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);

}
}













/////////////////////////////////////////////////////////
//  ARCHIVAGE
//////////////////////////////////////////////////////
if ($numAction==310) {

 require_once '../formulaires/FormInformationsPersonnelles.php';

 //require_once '../formulaires/FormPersonne.php';
 require_once '../formulaires/FormStagiaire.php';
 require_once '../formulaires/FormPermanent.php';
 require_once '../formulaires/FormDoctorant.php';
 require_once '../formulaires/FormCdd.php';



$menuDestination="archive";
// Quand on archive, on "valide" également la demande intranet
$_POST['demandeIntranet']=0;

         $personne = new FormInformationsPersonnelles($_POST);

//                $personne = new FormPersonne($_POST);



require_once '../actions/actionPersonneArchive.php';
$action3 =  new actionPersonneArchive;
$action3->execute($personne);


//unction actionMenuGauche($menuDestination,$id,$affarch,$yeardebut,$yearfin)

actionMenuGauche($menuDestination,$personne->getId(),0,0,0);

//    require_once '../actions/actionAffichage.php';
//        $action =  new actionAffichage;
//        $action->execute($menuDestination,$personne->getId(),310,0,0,$_SESSION['personnes'],$_SESSION['reservations'],0,'');


}


/////////////////////////////////////////////////////////
//  ARCHIVAGE
//////////////////////////////////////////////////////
if ($numAction==311) {

 require_once '../formulaires/FormInformationsPersonnelles.php';

 //require_once '../formulaires/FormPersonne.php';
 require_once '../formulaires/FormStagiaire.php';
 require_once '../formulaires/FormPermanent.php';
 require_once '../formulaires/FormDoctorant.php';
 require_once '../formulaires/FormCdd.php';




$menuDestination="archive";
// Quand on archive, on "valide" également la demande intranet
$_POST['demandeIntranet']=0;
         //       $personne = new FormPersonne($_POST);
         $personne = new FormInformationsPersonnelles($_POST);




require_once '../actions/actionPersonneDesarchive.php';
$action3 =  new actionPersonneDesarchive;
$action3->execute($personne);

actionMenuGauche($menuDestination,$personne->getId(),0,0,0);


//    require_once '../actions/actionAffichage.php';
//        $action =  new actionAffichage;
//        $action->execute($menuDestination,$personne->getId(),311,0,0,$_SESSION['personnes'],$_SESSION['reservations'],0,'');


}


///////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PAGE PERSO SPIP
// Bouton submitCréation page perso
//////////////////////////////////////////////////////
if ($numAction==312) {
require_once '../web/MenuHaut.php';



require_once '../actions/actionPersonneFiltre.php';


         $action =  new actionPersonneFiltre;
       // $listePersFiltre =  $action->execute($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],"","");
        $listePersFiltre =  $action->execute($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,"","",$selectArchive);






    require_once '../vues/PagePersonnePageGarde.php';
    $vue2= new PagePersonnePageGarde;
//    $vue2->afficher($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate']);
    //$vue2->afficher($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$menuDestination,'','','','','','','','','','');
    $vue2->afficher($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$menuDestination,'','','','','');


    require_once '../actions/actionPersonnePagePerso.php';
    $action2= new actionPersonnePagePerso;
    $action2->execute();

}


/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PERSONNELS
//suprssion
//////////////////////////////////////////////////////
if ($numAction==315) {
 require_once '../web/MenuHaut.php';
require_once '../vues/PagePersonneNavigationAdmin.php';
require_once '../actions/actionPersonneFiltre.php';
$id= $_POST['p_id'];

       require_once '../actions/actionPersonneSupprime.php';
        $action =  new actionPersonneSupprime();
        $action->execute($id);

         $action2 =  new actionPersonneFiltre;
        $listePersFiltre =  $action2->execute($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,"","",$selectArchive);



    $vue= new PagePersonneNavigationAdmin($selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive);
    $vue->afficher($listePersFiltre,$menuDestination,0,$listeSite,$listeStatut,$listeType,$listeEquipe,0,0,0,0,$arrayEquipe);


}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER ARCHIVE 
// PERMET DE TRIER SELON TYPE, TUTELLE, STATUT...
// Gestion des appels de ../vues/MenuAdminTriPersonne.php
// 104
//////////////////////////////////////////////////////
if ($numAction==320) {


 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePersonneNavigationAdmin.php';
        require_once '../actions/actionArchiveFiltre.php';


   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


        $action =  new actionArchiveFiltre;
        $listePersFiltre =  $action->execute();

   $vue= new PagePersonneNavigationAdmin('T','T','T','T',date('Y-m-d'),$selectArchive);
    $vue->afficher($listePersFiltre,$menuDestination,0,'','','','',0,0,0,0,$arrayEquipe);

   $year= date('Y');
   require_once '../vues/PageArchivePageGarde.php';
    $vue2= new PageArchivePageGarde($year);
    $vue2->afficher();
}














////////////////////////////////////////////////////////
// USE CASE ADMIN GERER les invités
//////////////////////////////////////////////////////
if ($numAction==330) {
 require_once '../web/MenuHaut.php';
        require_once '../actions/actionPersonneEquipe.php';
        require_once '../actions/actionInviteFiltre.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


   if (isset ( $_POST['yeardebut'] )) $yeardebut=$_POST['yeardebut'];
        else   $yeardebut= date('Y');
   if (isset ( $_POST['yearfin'] )) $yearfin=$_POST['yearfin'];
        else   $yearfin= date('Y');


if (isset($_POST['affarch'])) $affarch=1;
else $affarch=0;



//if ($affarch==0) {
//        $action =  new actionInviteFiltre;
//        $listePersFiltre =  $action->execute(0,0);
//}
//else {
//        $action =  new actionInviteFiltre;
//        $listePersFiltre =  $action->execute($yeardebut,$yearfin);
//}


actionMenuGauche($menuDestination,0,$affarch,$yeardebut,$yearfin);


   require_once '../vues/PageInvitePageGarde.php';
    $vue2= new PageInvitePageGarde($yeardebut,$yearfin,$affarch);
    $vue2->afficher($menuDestination);
}

























////////////////////////////////////////////////////////
// USE CASE ADMIN VALIDATION
//////////////////////////////////////////////////////
if ($numAction==340) {
// require_once '../vues/PagePersonneNavigationAdmin.php';
// require_once '../web/MenuHaut.php';
//        require_once '../actions/actionAdminValidationFiltre.php';
//        $action =  new actionAdminValidationFiltre;
//        $listePersFiltre =  $action->execute();

actionMenuGauche("validation",0,0,0,0);
}



/////////////////////////////////////////////////////////
// USE CASE ADMIN DONNEES ANONYMES
if ($numAction==350) {

require_once '../web/MenuHaut.php';
 require_once '../vues/PagePersonneNavigationAdmin.php';
        require_once '../actions/actionAnonymeFiltre.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

        $action =  new actionAnonymeFiltre;
        $listePersFiltre =  $action->execute();

   $vue= new PagePersonneNavigationAdmin('T','T','T','T',date('Y-m-d'),$selectArchive);
    $vue->afficher($listePersFiltre,$menuDestination,0,'','','','',0,0,0,0,$arrayEquipe);

   require_once '../vues/PageAnonymePageGarde.php';
    $vue2= new PageAnonymePageGarde();
    $vue2->afficher($menuDestination);

}

/////////////////////////////////////////////////////////
// USE CASE ADMIN ANONYMISER
if ($numAction==351) {

require_once '../web/MenuHaut.php';
 require_once '../vues/PagePersonneNavigationAdmin.php';
        require_once '../actions/actionAnonymeFiltre.php';

        $action =  new actionAnonymeFiltre;
        $listePersFiltre =  $action->execute();


        require_once '../actions/actionAnonymiser.php';
        $action =  new actionAnonymiser;
        $action->execute($listePersFiltre);

   $vue= new PagePersonneNavigationAdmin('T','T','T','T',date('Y-m-d'),$selectArchive);
    $vue->afficher($listePersFiltre,"anonyme",0,'','','','',0,0,0,0,$arrayEquipe);

   require_once '../vues/PageAnonymePageGarde.php';
    $vue2= new PageAnonymePageGarde();
    $vue2->afficher();

}

/////////////////////////////////////////////////////////
// USE CASE ADMIN FAIRE RESERVATIONS SALLES 
//////////////////////////////////////////////////////
if ($numAction==400) {



require_once '../web/MenuHaut.php';
 //require_once '../vues/MenuAdminTriPersonne.php';
        require_once '../actions/actionPersonneEquipe.php';
        require_once '../actions/actionPersonneFiltre.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


 $id= $_POST['p_id'];

if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);



//serge




 // LES DATES ISSUES DE PagePersonnePlaceReserver
        if (isset ( $_POST ["date_debut"] ) ) $dateDebut=$_POST["date_debut"];
// la première date de l'historique
        else  $dateDebut= $_POST['dateDebutH0'];


        if (isset ( $_POST ["date_fin"] ) && (!($_POST["date_fin"] == "")) ) {
		$dateFin=$_POST["date_fin"];
//		error_log("=======AAAAAAAAAAAAA======================================");
		}
		
	elseif (isset ( $_POST['dateFinH0']) && ($_POST['dateFinH0'] != '0000-00-00'))  {
		$dateFin= $_POST['dateFinH0'];
//		error_log("=======BBBBBBBBBBBBBB======================================");
	}
	else {
//		error_log("=======CCCCCCCCCCCCCCC======================================");
		$dateFin= "9999-12-31";
	}

//dateDebutH0 sit_id0

        require_once '../actions/actionPlaceLibreListe.php';
        $action2 =  new actionPlaceLibreListe;
        $listepl = $action2->execute($_POST['sit_id0'],$dateDebut,$dateFin);

        require_once '../vues/PagePersonnePlaceReserver.php';
        $vue2= new PagePersonnePlaceReserver($_POST,$dateDebut,$dateFin);
        $vue2->afficher("",$listepl,$listeSite);



}

/////////////////////////////////////////////////////////
// USE CASE ADMIN FAIRE RESERVATIONS SALLES 
// Submit de la réservation 
//106
//////////////////////////////////////////////////////
if ($numAction==401) {
try {
	//eValidation du  formulaire
        require_once '../formulaires/FormPersonnePlaceReserver.php';
        $reservation= new FormPersonnePlaceReserver($_POST);
	
	// reservation
        require_once '../actions/actionPlaceReserve.php';
        $action =  new actionPlaceReserve();
        $action->execute($reservation);




   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


 $id= $_POST['p_id'];

if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);


//         require_once '../actions/actionAffichage.php';
//         $action1 =  new actionAffichage;
//        $tabpersonne =  $action1->execute($_POST['menuDestination'],$_POST['p_id'],401,0,0,$_SESSION['personnes'],$_SESSION['reservations'],0,"") ;



        require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);

// permet de récupérer l'onglet actif de la page personne
$tab= $_COOKIE["tab"];

  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,$ajouthistorique,$chercheur,$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_GET['selectSite'],$_GET['selectStatut'],$_GET['selectType'],$_GET['selectEquipe'],$_GET['selectDate']);
        $vue2->afficher("",$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);




} catch (Exception $e) {
require_once '../web/MenuHaut.php';



   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


 $id= $_POST['p_id'];

if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);



        require_once '../vues/PagePersonnePlaceReserver.php';


	 // LES DATES ISSUES DE PagePersonnePlaceReserver
        if (isset ( $_POST ["date_debut"] ) ) $date_debut=$_POST["date_debut"];
        else  $date_debut= date("Y-m-d");
        if ( $_POST ["date_fin"] =='' ) $date_fin= "9999-12-31";
        else $date_fin = $_POST['date_fin'];
	
        require_once '../actions/actionPlaceLibreListe.php';
        $action2 =  new actionPlaceLibreListe;
        $listepl = $action2->execute($_POST['sit_id'],$date_debut,$date_fin);


        require_once '../vues/PagePersonnePlaceReserver.php';
        $vue2= new PagePersonnePlaceReserver($_POST,$date_debut,$date_fin);
        $vue2->afficher($e->getMessage(),$listepl,$listeSite);





}

}
//////////////////////////////////////////////////////
// USE CASE ADMIN FAIRE RESERVATIONS SALLES 
// Suppression de la réservation 
//////////////////////////////////////////////////////
//107DEL
if (substr( $numAction, 0, 3 ) == "402") {
$r_id = substr($numAction,3,strlen($numAction));
   require_once '../actions/actionReservationSupprime.php';
        $action1 =  new actionReservationSupprime;
        $action1->execute($r_id);

//        $requeteDeleteReservation = Model::$pdo->prepare("DELETE FROM t_reservation2 WHERE r_id = :r_id ");
//        $requeteDeleteReservation->bindParam(':r_id', $idReservation);

//        $statut = $requeteDeleteReservation->execute(); // returns true on success

        // Affichage
  //       require_once '../actions/actionAffichage.php';
  //       $action1 =  new actionAffichage;
  //      $tabpersonne =  $action1->execute($_POST['menuDestination'],$_POST['p_id'],402,0,0,$_SESSION['personnes'],$_SESSION['reservations'],0,"") ;


   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


 $id= $_POST['p_id'];

if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);


       require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);

// permet de récupérer l'onglet actif de la page personne
$tab= $_COOKIE["tab"];

  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,$ajouthistorique,$chercheur,$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_GET['selectSite'],$_GET['selectStatut'],$_GET['selectType'],$_GET['selectEquipe'],$_GET['selectDate']);
        $vue2->afficher("",$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);



}
//////////////////////////////////////////////////////
// USE CASE ADMIN FAIRE RESERVATIONS SALLES STAGIAIRES
// Ajout d'une date de fin
//////////////////////////////////////////////////////
//108ADD
if (substr( $numAction, 0, 3 ) == "403") {



try {

	$r_id = substr($numAction,3,strlen($numAction));
	$nom= 'dateFin'.$r_id;
	$datefin= $_POST[$nom];
	$nom= 'dateDebut'.$r_id;
	$datedebut= $_POST[$nom];

        require_once '../formulaires/FormReservationModifier.php';
        $modifresa = new FormReservationModifier($r_id,$datedebut,$datefin);


    // Modifi 
        require_once '../actions/actionReservationModification.php';
        $action =  new actionReservationModification();
        $action->execute($modifresa);



  if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


 $id= $_POST['p_id'];

if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);


       require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);

// permet de récupérer l'onglet actif de la page personne
$tab= $_COOKIE["tab"];

  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,$ajouthistorique,$chercheur,$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_GET['selectSite'],$_GET['selectStatut'],$_GET['selectType'],$_GET['selectEquipe'],$_GET['selectDate']);
        $vue2->afficher("",$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);






} catch (Exception $e) {
require_once '../web/MenuHaut.php';
        require_once '../vues/PagePersonnePlaceReserver.php';
        require_once '../actions/actionPersonneFiltre.php';



  if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


 $id= $_POST['p_id'];

if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);


       require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);

// permet de récupérer l'onglet actif de la page personne
$tab= $_COOKIE["tab"];

  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,$ajouthistorique,$chercheur,$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_GET['selectSite'],$_GET['selectStatut'],$_GET['selectType'],$_GET['selectEquipe'],$_GET['selectDate']);
        $vue2->afficher($e->getMessage(),$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);





//        require_once '../vues/PageEquipeAjoute.php';
  //      $vue= new PageEquipeAjoute($_POST);
    //    $vue->afficher($e->getMessage());
   // echo $e->getMessage();
echo '</div>';


}

}
//////////////////////////////////////////////////////
// USE CASE ADMIN FAIRE RESERVATIONS SALLES STAGIAIRES
// MODIFIER DATE FIN
//////////////////////////////////////////////////////
//108MOD
if (substr( $numAction, 0, 3 ) == "404") {
try {

        $r_id = substr($numAction,3,strlen($numAction));
        $nom= 'dateFinOri'.$r_id;
        $datefinori= $_POST[$nom];
        $nom= 'dateFin'.$r_id;
        $datefin= $_POST[$nom];

        require_once '../formulaires/FormReservationModifDateFin.php';
        $modifresa = new FormReservationModifDateFin($r_id,$datefinori,$datefin);


    // Moeifi
        require_once '../actions/actionReservationModification.php';
        $action =  new actionReservationModification();
        $action->execute($modifresa);


     // Affichage



  if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


 $id= $_POST['p_id'];

if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);


       require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);

// permet de récupérer l'onglet actif de la page personne
$tab= $_COOKIE["tab"];

  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,$ajouthistorique,$chercheur,$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_GET['selectSite'],$_GET['selectStatut'],$_GET['selectType'],$_GET['selectEquipe'],$_GET['selectDate']);
        $vue2->afficher("",$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);

} catch (Exception $e) {


  if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


 $id= $_POST['p_id'];

if ($menuDestination=="laboratoire") 
	//actionMenuGaucheLaboratoire($menuDestination,$id,$_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$listeSite,$listeStatut,$listeType,$listeEquipe);
	 actionMenuGaucheLaboratoire($menuDestination,$id,$selectSite,$selectStatut,$selectType,$selectEquipe,$selectDate,$selectArchive,$listeSite,$listeStatut,$listeType,$listeEquipe);
else actionMenuGauche($menuDestination,$id,0,0,0);


       require_once '../actions/actionGetRessources.php';
        $action3 =  new actionGetRessources;
        list ($listeReservation,$listeCahier,$tabpersonne,$tabhistorique,$listePersonneEquipe,$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent) = $action3->execute($id);

// permet de récupérer l'onglet actif de la page personne
$tab= $_COOKIE["tab"];

  require_once '../vues/PagePersonne.php';
        $vue2= new PagePersonne(0,$ajouthistorique,$chercheur,$_SESSION['personnes'],$_SESSION['reservations'],$tabpersonne,$tabhistorique,$listePersonneEquipe,'',$listestage,$tabthese,$listeContrat,$listeInvite,$listePermanent,$menuDestination,'',0,0,$_GET['selectSite'],$_GET['selectStatut'],$_GET['selectType'],$_GET['selectEquipe'],$_GET['selectDate']);
        $vue2->afficher($e->getMessage(),$listeSite,$listeTutelle,$listeType,$listeStatut,$listePersonne,$listeReservation,$listeCorps,$listeFormation,$listeEquipe,$listeCahier,$listeRessource,$listeCollege,$tab);



}





}



////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des archivés
//////////////////////////////////////////////////////
if ($numAction==995) {

        $yeardebut=$_POST['yeardebut'];
        $yearfin=$_POST['yearfin'];
$nomfich='listeInvites'.$yeardebut.'_'.$yearfin.'.csv';


$champinvite = ",invite_commentaire,CONCAT (p2.p_nom,' ',p2.p_prenom) AS hote";

$clauseinvite="
                RIGHT JOIN invite ON historique.invite_id = invite.invite_id
                LEFT JOIN t_personne p2 ON invite.invite_par_id = p2.p_id
";

$clauseArchive = "";
//        AND p1.archive NOT LIKE 1        
  
$clauseHistorique ="        
AND    (( year(historique.dateDebutH) >= $yeardebut AND    year(historique.dateDebutH) <= $yearfin) OR (year(historique.dateFinH) >= $yeardebut AND    year(historique.dateFinH)<=$yearfin ) OR (year(historique.dateDebutH) < $yeardebut AND    year(historique.dateFinH)>$yearfin))
";


require_once '../actions/actionPersonneFiltreComplet.php';
         $action =  new actionPersonneFiltreComplet;
        $liste=  $action->execute($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$champinvite,$clauseinvite,$clauseArchive,$clauseHistorique);
        $entete = $action->entete(array("Commentaire","Hote"));


        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);
}


////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des cdd
//////////////////////////////////////////////////////
if ($numAction==998) {
       $yeardebut=$_POST['yeardebut'];
        $yearfin=$_POST['yearfin'];
$nomfich='listeContractuels'.$yeardebut.'_'.$yearfin.'.csv';

$champcdd = ",t_nom,contractuel_sujet_recherche,CONCAT (p2.p_nom,' ',p2.p_prenom) AS superieur";

$clausecdd="
                RIGHT JOIN contractuel ON historique.contractuel_id = contractuel.contractuel_id
                LEFT JOIN t_personne p2 ON contractuel.contractuel_sup_id = p2.p_id
                LEFT JOIN t_tutelle ON contractuel.contractuel_t_id = t_tutelle.t_id
";

$clauseArchive = "";
//        AND p1.archive NOT LIKE 1
 
$clauseHistorique ="
AND    (( year(historique.dateDebutH) >= $yeardebut AND    year(historique.dateDebutH) <= $yearfin) OR (year(historique.dateFinH) >= $yeardebut AND    year(historique.dateFinH)<=$yearfin ) OR (year(historique.dateDebutH) < $yeardebut AND    year(historique.dateFinH)>$yearfin))
";


require_once '../actions/actionPersonneFiltreComplet.php';
         $action =  new actionPersonneFiltreComplet;
        $liste=  $action->execute($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$champcdd,$clausecdd,$clauseArchive,$clauseHistorique);
        $entete = $action->entete(array("Tutelle","Sujet Recherche","Supérieur"));


        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);
}


////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste des stagiaires
//////////////////////////////////////////////////////
if ($numAction==999) {
        $yeardebut=$_POST['yeardebut'];
        $yearfin=$_POST['yearfin'];
	$nomfich='listeStagiaires'.$yeardebut.'_'.$yearfin.'.csv';


$champstage = ",CONCAT (p2.p_nom,' ',p2.p_prenom) AS encadrant,stag_intitule,etablissement,t_formation_stagiaire.intitule,autreformation,t_nom";

$clausestage="
                RIGHT JOIN stage ON historique.stageId = stage.stageId
                LEFT JOIN t_personne p2 ON stage.p_stag_encadrant = p2.p_id
                LEFT JOIN t_tutelle ON stage.t_conv_id = t_tutelle.t_id
                LEFT JOIN t_formation_stagiaire ON stage.f_id = t_formation_stagiaire.f_id
";

$clauseArchive = "";
//        AND p1.archive NOT LIKE 1
 
$clauseHistorique ="
AND    (( year(historique.dateDebutH) >= $yeardebut AND    year(historique.dateDebutH) <= $yearfin) OR (year(historique.dateFinH) >= $yeardebut AND    year(historique.dateFinH)<=$yearfin ) OR (year(historique.dateDebutH) < $yeardebut AND    year(historique.dateFinH)>$yearfin))
";


require_once '../actions/actionPersonneFiltreComplet.php';
         $action =  new actionPersonneFiltreComplet;
        $liste=  $action->execute($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$_POST['selectDate'],$champstage,$clausestage,$clauseArchive,$clauseHistorique);
        $entete = $action->entete(array("Encadrant","Intitule","Etablissement","Formation","Autre Formation","Tutelle"));




        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste,$entete,$nomfich);



}


/////////////////////////////////////////////////////////
// USE CASE ADMIN IMPORT HCERES
//////////////////////////////////////////////////////
if ($numAction==1200) {

 require_once '../web/MenuHaut.php';
// require_once '../vues/PagePersonneNavigationAdmin.php';

   require_once '../vues/PageImportPageGarde.php';
    $vue2= new PageImportPageGarde();
    $vue2->afficher('');
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN IMPORT HCERES
//////////////////////////////////////////////////////
if ($numAction==1201) {

 require_once '../web/MenuHaut.php';
// require_once '../vues/PagePersonneNavigationAdmin.php';

        var_dump($_FILES);
  // Si le fichier de la convention a été uploadée, on la  charge sur le serveur....
        if(strlen($_FILES['importdoctorant']['name'])!=0) {

	    $csv_string = file_get_contents($_FILES['importdoctorant']['tmp_name']);
//	    $lines = explode(";", $csv_string);
       }


   require_once '../vues/PageImportPageGarde.php';
    $vue2= new PageImportPageGarde();
    //$vue2->afficher($csv_string);
    $vue2->afficher($_FILES['importdoctorant']['tmp_name']);
}












////////////////////////////////////////////////////////
// FIN
/////////////////////////////////////////////////////
//echo "<div id='header'>";
//echo '	</div>';
?>
