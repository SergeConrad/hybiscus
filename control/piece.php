
////////////////////////////////////////////////////////
// Ajouter un piece
//../vues/PagePieceAjoute.php
// ou on vient de changer le type dans la vue VuePersonneAjoute.php
//////////////////////////////////////////////////////
if ($numAction==160) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePieceNavigation.php';
 require_once '../actions/actionPieceListe.php';


         $action =  new actionPieceListe;
        $listePiece = $action->execute();

   $vue1= new PagePieceNavigation();
    $vue1->afficher($listePiece);


        require_once '../vues/PagePieceAjoute.php';
        $vue2= new PagePieceAjoute();
        $vue2->afficher("");
}

/////////////////////////////////////////////////////////
// appuyer sur le bouton submit de la vue vue PagePieceAjoute.php
//////////////////////////////////////////////////////
if ($numAction==161) {
 require_once '../formulaires/FormPieceAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionPieceListe.php';

 require_once '../vues/PagePieceNavigation.php';

         $action =  new actionPieceListe;
        $listePiece = $action->execute();

   $vue= new PagePieceNavigation();
    $vue->afficher($listePiece);

try {
        $piece = new FormPieceAjoute($_POST);

        require_once '../actions/actionPieceAjoute.php';
        $action =  new actionPieceAjoute();
        $action->execute($piece);


        $action2 =  new actionPieceListe;
        $listePiece = $action2->execute();

        $vue= new PagePieceNavigation();
        $vue->afficher($listePiece);



} catch (Exception $e) {
        require_once '../vues/PagePieceAjoute.php';
        $vue= new PagePieceAjoute($_POST);

        $vue->afficher($e->getMessage());


}


}

////////////////////////////////////////////////////////
// Modifier un piece
//////////////////////////////////////////////////////
if ($numAction==162) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePieceNavigation.php';
 require_once '../actions/actionPieceListe.php';

    $action =  new actionPieceListe;
    $listePiece = $action->execute();

   $vue= new PagePieceNavigation();
    $vue->afficher($listePiece);

   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionPieceGet.php';
        // rend un tableau associatif
         $action1 =  new actionPieceGet;
        $tabpiece =  $action1->execute($id);


        require_once '../vues/PagePieceModifie.php';
        $vue2= new PagePieceModifie($tabpiece);
        $vue2->afficher("");


        }
    else {
        // On vient de changer le type dans le formulaire, en mode refresh les infos sont dans le formulaire et non dans la base de données

        require_once '../vues/PagePieceModifie.php';
        $vue2= new PagePieceModifie($_POST);
        $vue2->afficher("");

        }
}

/////////////////////////////////////////////////////
// appuyer sur le bouton submit de la vue vuePieceModifie.php
/////////////////////////////////////////////////////
if ($numAction==163) {
 require_once '../formulaires/FormPieceModifie.php';
 require_once '../web/MenuHaut.php';

 require_once '../vues/PagePieceNavigation.php';

require_once '../actions/actionPieceListe.php';

    $action =  new actionPieceListe;
    $liste = $action->execute();

   $vue= new PagePieceNavigation();
    $vue->afficher($liste);

try {

        // cas des stagiaires
        $equipe = new FormPieceModifie($_POST);


//echo "modification de ".$personne->getNom();
        require_once '../actions/actionPieceModifie.php';
        $action =  new actionPieceModifie();
        $action->execute($equipe);

       $action2 =  new actionPieceListe;
        $liste = $action2->execute();

        $vue= new PagePieceNavigation();
        $vue->afficher($liste);



} catch (Exception $e) {
        require_once '../vues/PagePieceModifie.php';
        $vue2= new PagePieceModifie($_POST);
        $vue2->afficher($e->getMessage());



}
}
/////////////////////////////////////////////////////////
// Navigation Piece
///////////////////////////////////////////////////////////

if  ($numAction==164){

 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePieceNavigation.php';
require_once '../actions/actionPieceListe.php';

   $action =  new actionPieceListe;
    $liste = $action->execute();

    $vue= new PagePieceNavigation($_POST);
    $vue->afficher($liste);

}

