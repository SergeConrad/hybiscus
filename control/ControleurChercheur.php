<?php

/*********************************************************************************************************************************\
 *  Hybicus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                                                                                *
 *  Copyright (c) 2018 2019                                                                                                   *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

require_once("../web/session_start.php");

//header ( 'Content-Type: text/html; charset=utf-8' );

?>


<!DOCTYPE html>

<?php
// require_once '../web/head.php';
require_once '../control/CachedPDOStatement.php';
// require_once '../entites/class.php';
 ?>

<body>
<!--
MENUS
CHERCHEUR                                               RESPONSABLE
* NOUVEAU STAGIAIRE 90                                  * LISTES STAGIAIRES 1001
* LISTE STAGIAIRES 92                                   * LISTES DOCTORANTS 1003
* NOUVEAU DOCTORANT 95
* LISTE DOCTORANTS 97



90 -> PageChercheurDeclareStagiaire -> 91 ajout dans bd
92 -> MenuChercheurConsulteStagiaire -> 93 PageChercheurModifieStagiaire -> 1007 ajout dans bd
95 -> PageChercheurDeclareDoctorant -> 96 ajout dans bd
97 -> MenuChercheurConsulteDoctorant -> 98 PageChercheurModifieDoctorant -> 1008 ajout dans bd


1001 -> MenuResponsableConsulteStagiaire -> 1002 PageResponsableModifieStagiaire -> 1005 ajout dans bd
1003 -> MenuResponsableConsulteDoctorant -> 1004 PageResponsableModifieDoctorant -> 1006 ajout dans bd

GERER EQUIPE 110 à 118

GERER PARAMETRES 120 à 165
Mail d'avertissement 500 à 501
-->


<?php


// Chargement des tables ...
        require_once '../actions/actionPersonneFiltre.php';
        require_once '../actions/actionTutelleListe.php';
        require_once '../actions/actionSiteListe.php';
        require_once '../actions/actionStatutListe.php';
        require_once '../actions/actionTypeListe.php';
        require_once '../actions/actionFormationListe.php';
        $actionTutelle =  new actionTutelleListe;
        $actionSite =  new actionSiteListe;
        $actionStatut =  new actionStatutListe;
        $actionType =  new actionTypeListe;
        $actionPersonne =  new actionPersonneFiltre;
        $actionFormation =  new actionFormationListe;
        // cf https://stackoverflow.com/questions/15385965/php-pdo-with-foreach-and-fetch
        $listeTutelle = new CachedPDOStatement($actionTutelle->execute());
        $listeSite = new CachedPDOStatement( $actionSite->execute());
        $listeStatut =  new CachedPDOStatement($actionStatut->execute());
        $listeType =  new CachedPDOStatement($actionType->execute());
        $listePersonne =  new CachedPDOStatement($actionPersonne->execute('T','T','T','T',date('Y-m-d'),"","",0)); // Tout le monde sauf les archivés
        $listeFormation =  new CachedPDOStatement($actionFormation->execute()); // Tout le monde sauf les archivés

     if (isset ( $_GET ['numAction'] )) $numAction=$_GET['numAction'];
     if (isset ( $_POST ['numAction'] )) $numAction=$_POST['numAction'];





/////////////////////////////////////////////////////////
// Page Par défaut
///////////////////////////////////////////////////////////
//foreach ($_SESSION as $key => $value) {echo $key.'-'.$value;}


if (!isset($numAction)) {

if ($_SESSION['arrivant']==1) $numAction=4;
elseif (($_SESSION['admin']==0)&&( $_SESSION['personnes']==0)) $numAction=5;
else $numAction=6;

}


/////////////////////////////////////////////////////////
// Deconnexion
///////////////////////////////////////////////////////////
if ($numAction==2) {
                session_start();
                unset($_SESSION['user']);
                session_destroy();
                header("Location: ControleurChercheur.php");
        }




/////////////////////////////////////////////////////////
// Page Nouvel Arrivant
///////////////////////////////////////////////////////////
if ($numAction==4) {
 require_once '../web/MenuHaut.php';
}



/////////////////////////////////////////////////////////
// Page Garde Enseignant Chercheur
///////////////////////////////////////////////////////////
if ($numAction==5) {

 require_once '../web/MenuHaut.php';
   require_once '../actions/actionChercheurStagiaireFiltreComplet.php';
   //require_once '../vues/MenuAdminConsulteListe.php';
   $action =  new actionChercheurStagiaireFiltreComplet;
   $listeStagiaire = new CachedPDOStatement( $action->execute($_SESSION['p_id'],"non"));
   $listeStagiaireArchive =  $action->execute($_SESSION['p_id'],"oui");





//   $vue= new MenuAdminConsulteListe();
//    $vue->afficher($listeStagiaire,"chercheurstagiaire",'','','','');






        require_once '../actions/actionChercheurDoctorantFiltreComplet.php';
        $action =  new actionChercheurDoctorantFiltreComplet;
        $listeDoctorant =  $action->execute($_SESSION['p_id'],"non");
        $listeDoctorantArchive =  $action->execute($_SESSION['p_id'],"oui");

        require_once '../actions/actionChercheurCddFiltreComplet.php';
        $action =  new actionChercheurCddFiltreComplet;
        $listeCdd =  $action->execute($_SESSION['p_id'],"non");
        $listeCddArchive =  $action->execute($_SESSION['p_id'],"oui");

        require_once '../actions/actionChercheurInviteFiltreComplet.php';
        $action =  new actionChercheurInviteFiltreComplet;
        $listeInvite =  $action->execute($_SESSION['p_id'],"non");
        $listeInviteArchive =  $action->execute($_SESSION['p_id'],"oui");

        require_once '../actions/actionCahierListe.php';
        $action =  new actionCahierListe;
//        $mesCahiers =  $action->parUtilisateur($_SESSION['p_id']);
// ATTENTION CECI rend les cahiers du chercheur, plus ceux de ses stagiaires doctorants et contractuels
        $cahiersStagiaires =  $action->mesStagiairesEtDoctorants($_SESSION['p_id']);

//var_dump($listeInvite);
//die;
        require_once '../actions/actionProjetListeParChercheur.php';
        $action2 =  new actionProjetListeParChercheur;
        $listeProjet = $action2->execute($_SESSION['p_id']);


    require_once '../vues/PageChercheurPageGarde.php';
    $vue2= new PageChercheurPageGarde();
    //$vue2->afficher($listeStagiaire,$listeDoctorant,$mesCahiers,$cahiersStagiaires,$listeStagiaireArchive,$listeDoctorantArchive,$listeCdd,$listeCddArchive);
    $vue2->afficher($listeStagiaire,$listeDoctorant,$cahiersStagiaires,$listeStagiaireArchive,$listeDoctorantArchive,$listeCdd,$listeCddArchive,$listeInvite,$listeInviteArchive,$listeProjet);

}

/////////////////////////////////////////////////////////
// Page Garde Admin
///////////////////////////////////////////////////////////
if ($numAction==6) {

 require_once '../web/MenuHaut.php';
    require_once '../vues/PageAdminPageGarde.php';
    $vue2= new PageAdminPageGarde();
    $vue2->afficher();
}

