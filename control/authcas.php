<?php
/***************************************************************************\
 *  GES UMR : gestion rh pour les umr                                      *
 *                                                                         *
 *  Copyright (c) 2018                                                     *
 *  Serge Conrad                                                           *
 *  Elise Deme								   *
 *  	                                                                   *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/
//session_start();

require_once '../entites/Model.php';

require_once '../phpCAS/CAS.php';

$ini = parse_ini_file("../entites/hybiscus.ini", true );
$cas_host=$ini['cas']['host'];
$cas_port=(int)$ini['cas']['port'];
$cas_context=$ini['cas']['context'];
$cas_version=$ini['cas']['version'];
$supannAffectation=$ini['cas']['supannAffectation'];




phpCAS::client($cas_version, $cas_host, $cas_port, $cas_context);
phpCAS::setNoCasServerValidation();
phpCAS::setDebug();
phpCAS::setVerbose(true);
phpCAS::forceAuthentication();

// at this step, the user has been authenticated by the CAS server
// and the user's login name can be read with phpCAS::getUser().
// logout if desired




// echo "   <p>the user's login is <b>".phpCAS::getUser()."</b>.</p>";
// echo "   <p>phpCAS Version  is <b>".phpCAS::getVersion()."</b>.</p>";
// echo "   <p>mail  is <b>".phpCAS::getAttribute("mail")."</b>.</p>";

 $array = phpCAS::getAttributes();
//var_dump($array);
//echo "==";

$mail = $array['mail'];
$givenName = $array['givenName'];
$sn  = $array['sn'];
$supannEntiteAffectation  = $array['supannEntiteAffectation'];
#echo "supannEntiteAffectation $supannEntiteAffectation ";
#  echo gettype($supannEntiteAffectation), "\n";

$ok=0;
if ((is_string($supannEntiteAffectation)) && ($supannEntiteAffectation == $supannAffectation)) $ok=1;
else foreach ($supannEntiteAffectation as $valeur) 	if ($valeur == $supannAffectation) $ok=1;

//if ($supannEntiteAffectation != $supannAffectation) {
if ($ok==0){
	echo "Vous êtes affecté dans l'entité supann $supannEntiteAffectation qui n'est pas autorisée à visualiser cette page";
	die;
}
$user= strtolower(substr($givenName,0,1).$sn);



// On met le nom dans la session php
$_SESSION['user'] = $mail;

// On met le p_id dans la session php
$rq =  Model::daoAuthIdCas($mail);
$row = $rq -> fetch();
$_SESSION['p_id'] = $row->p_id;


// On regarde dans la table droitsspip si l'utilisateur a des droits spécifiques 
$rq =  Model::daoAuthDroits($mail);

// L'utilisateur est-il  déclaré dans la table droits ?
if ($rq->rowCount()==1) {
        $row = $rq -> fetch();

          $_SESSION['admin'] = $row->admin;
          $_SESSION['equipe'] = $row->equipe;
          $_SESSION['personnes'] = $row->personnes;
          $_SESSION['ordi'] = $row->ordi;
          $_SESSION['cahiersLabo'] = $row->cahiersLabo;
          $_SESSION['reservations'] = $row->reservations;
          $_SESSION['demandeintranet'] = $row->demandeIntranet;
}

//echo " $user ADMIN".$row->admin;die;
// On regarde dans la table equipe si l'utilisateur est responsable d'équipe
$rq =  Model::daoAuthRespEquip($_SESSION['p_id']);
$arrayResp=array();
foreach ($rq as $row) $arrayResp[$row->eq_id]= $row->eq_nom;
$_SESSION['responsableEquipe'] = $arrayResp;

//if ($rq->rowCount()==1) {
//        $row = $rq -> fetch();
//          $_SESSION['responsableEquipe'] = $row->eq_id;
//          $_SESSION['responsableEquipeNom'] = $row->eq_nom;
//}


     
     
//             require_once '../actions/actionPersonneFiltre.php';
//     $action =  new actionPersonneFiltre;
//       $listePersFiltre =  $action->execute('T','T','T','T',date('Y-m-d'),"","",0);
//$test = array ();
//foreach ($listePersFiltre as $row) {
//var_dump($row);
//$test[$row->sit_id][$row->p_id]=$row;

//jet.php.ori:	foreach ($listeFinanceur as $row) $arrayFinanceur[$row->financeur_id]= $row->financeur_nom;


//echo "=============\n<br>";
//}
//foreach ($test as $key => $value) { echo "key $key value $value";}
//die;

        //echo "bienvenue ".$user;
        header( "Location: ControleurChercheur.php" );
?>
