<?php

/*********************************************************************************************************************************\
 *  Hybicus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                                                                                *
 *  Copyright (c) 2018 2019                                                                                                   *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

require_once("../web/session_start.php");

//header ( 'Content-Type: text/html; charset=utf-8' );

?>


<!DOCTYPE html>

<?php
// require_once '../web/head.php';
require_once '../control/CachedPDOStatement.php';
// require_once '../entites/class.php';
 ?>

<body>
<!--
MENUS
CHERCHEUR                                               RESPONSABLE
* NOUVEAU STAGIAIRE 90                                  * LISTES STAGIAIRES 1001
* LISTE STAGIAIRES 92                                   * LISTES DOCTORANTS 1003
* NOUVEAU DOCTORANT 95
* LISTE DOCTORANTS 97



90 -> PageChercheurDeclareStagiaire -> 91 ajout dans bd
92 -> MenuChercheurConsulteStagiaire -> 93 PageChercheurModifieStagiaire -> 1007 ajout dans bd
95 -> PageChercheurDeclareDoctorant -> 96 ajout dans bd
97 -> MenuChercheurConsulteDoctorant -> 98 PageChercheurModifieDoctorant -> 1008 ajout dans bd


1001 -> MenuResponsableConsulteStagiaire -> 1002 PageResponsableModifieStagiaire -> 1005 ajout dans bd
1003 -> MenuResponsableConsulteDoctorant -> 1004 PageResponsableModifieDoctorant -> 1006 ajout dans bd

GERER EQUIPE 110 à 118

GERER PARAMETRES 120 à 165
Mail d'avertissement 500 à 501
-->


<?php


// Chargement des tables ...
        require_once '../actions/actionPersonneFiltre.php';
        require_once '../actions/actionTutelleListe.php';
        require_once '../actions/actionSiteListe.php';
        require_once '../actions/actionStatutListe.php';
        require_once '../actions/actionTypeListe.php';
        require_once '../actions/actionFormationListe.php';
        $actionTutelle =  new actionTutelleListe;
        $actionSite =  new actionSiteListe;
        $actionStatut =  new actionStatutListe;
        $actionType =  new actionTypeListe;
        $actionPersonne =  new actionPersonneFiltre;
        $actionFormation =  new actionFormationListe;
        // cf https://stackoverflow.com/questions/15385965/php-pdo-with-foreach-and-fetch
        $listeTutelle = new CachedPDOStatement($actionTutelle->execute());
        $listeSite = new CachedPDOStatement( $actionSite->execute());
        $listeStatut =  new CachedPDOStatement($actionStatut->execute());
        $listeType =  new CachedPDOStatement($actionType->execute());
        $listePersonne =  new CachedPDOStatement($actionPersonne->execute('T','T','T','T',date('Y-m-d'),"","",0)); // Tout le monde sauf les archivés
        $listeFormation =  new CachedPDOStatement($actionFormation->execute()); // Tout le monde sauf les archivés

     if (isset ( $_GET ['numAction'] )) $numAction=$_GET['numAction'];
     if (isset ( $_POST ['numAction'] )) $numAction=$_POST['numAction'];


        require_once '../actions/actionEquipeRechercheListe.php';
        $action =  new actionEquipeRechercheListe;
        $listeEquipe = new CachedPDOStatement($action->execute());


   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


/////////////////////////////////////////////////////////
// Page Garde Responsable
///////////////////////////////////////////////////////////
if ($numAction==10) {

//serge
 require_once '../web/MenuHaut.php';

   require_once '../actions/actionResponsableStagiaireFiltreComplet.php';
   $action =  new actionResponsableStagiaireFiltreComplet;
   $listeStagiaire =  $action->execute($_GET['eq_id'],"non");
   $listeStagiaireArchive =  $action->execute($_GET['eq_id'],"oui");


        require_once '../actions/actionResponsableDoctorantFiltreComplet.php';
        $action =  new actionResponsableDoctorantFiltreComplet;
        $listeDoctorant =  $action->execute($_GET['eq_id'],"non");
        $listeDoctorantArchive =  $action->execute($_GET['eq_id'],"oui");



       require_once '../actions/actionResponsableCddFiltreComplet.php';
        $action =  new actionResponsableCddFiltreComplet;
        $listeCdd =  $action->execute($_GET['eq_id'],"non");
        $listeCddArchive =  $action->execute($_GET['eq_id'],"oui");


        require_once '../actions/actionCahierListe.php';
        $action =  new actionCahierListe;
        $cahiersEquipe =  $action->pourLequipe($_GET['eq_id']);
//        $listeM =  $action->cahier_equipe_membre($_SESSION['p_id']);
  //      $listeS =  $action->cahier_equipe_stagiaire($_SESSION['p_id']);
    //    $listeD =  $action->cahier_equipe_doctorant($_SESSION['p_id']);




//        require_once '../actions/actionProjetListeParEquipe.php';
//        $action2 =  new actionProjetListeParEquipe;
//        $listeProjet = $action2->execute($_GET['eq_id']);


require_once '../actions/actionEquipeEffectif.php';
$action3 =  new actionEquipeEffectif;
$listeEquipeEffectif = $action3->execute($_GET['eq_id']);



    require_once '../vues/PageResponsablePageGarde.php';
    $vue2= new PageResponsablePageGarde();
    $vue2->afficher($listeStagiaire,$listeDoctorant,$_GET['nomEquipe'],$_GET['eq_id'],$cahiersEquipe,$listeStagiaireArchive,$listeDoctorantArchive,$listeCdd,$listeCddArchive,$listeEquipeEffectif);

}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PERSONNELS
// PERMET DE TRIER SELON TYPE, TUTELLE, STATUT...
// Gestion des appels de ../vues/MenuAdminTriPersonne.php
// 104
//////////////////////////////////////////////////////
if ($numAction==300) {
//serge
 require_once '../web/MenuHaut.php';
require_once '../vues/PagePersonneNavigationAdmin.php';
require_once '../actions/actionPersonneFiltre.php';

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

         $action =  new actionPersonneFiltre;
        $listePersFiltre =  $action->execute($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_GET['eq_id'],"","");



    $vue= new PagePersonneNavigationAdmin($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_GET['eq_id']);
    $vue->afficher($listePersFiltre,$menuDestination,0,$listeSite,$listeStatut,$listeType,$listeEquipe,0,0,0,0);

require_once '../actions/actionEffectif.php';
$action3 =  new actionEffectif;
$listeEffectif = $action3->execute($_GET['eq_id']);


    require_once '../vues/PagePersonnePageGarde.php';
    $vue2= new PagePersonnePageGarde;
    $vue2->afficher($_POST['selectSite'],$_POST['selectStatut'],$_POST['selectType'],$_POST['selectEquipe'],$menuDestination,$listeEffectif);


}


/////////////////////////////////////////////////////////
// Page Garde Responsable SUIVI RH
///////////////////////////////////////////////////////////
if ($numAction==11) {

 
/**
* Charger la page index du site Fobec.com
*/
file_get_contents('http://www.fobec.com');
 
//Fin du code PHP
 
//Afficher le temps de chargement
echo "Debut du chargement: ".date("H:i:s", $timestart);
echo "<br>Fin de reception: ".date("H:i:s", $timeend);
echo "<br>Page chargee en " . $page_load_time . " sec";



//serge
 require_once '../web/MenuHaut.php';
require_once '../actions/actionEquipeEffectif.php';
$action3 =  new actionEquipeEffectif;

$timestart=microtime(true);
$listeEquipeEffectif = $action3->execute($_GET['eq_id']);
$timeend=microtime(true);
$time=$timeend-$timestart;
$message = " fonction execute ".number_format($time, 3);



$timestart=microtime(true);
$listeEquipePermanent = $action3->permanent($_GET['eq_id']);
$timeend=microtime(true);
$time=$timeend-$timestart;
$message .= " <br>fonction permanent ".number_format($time, 3);

$timestart=microtime(true);
$listeEquipeNonPermanent = new CachedPDOStatement($action3->nonpermanent($_GET['eq_id']));
$timeend=microtime(true);
$time=$timeend-$timestart;
$message .= " <br>fonction non permanent ".number_format($time, 3);


$timestart=microtime(true);
$listeEquipeEtudiant = new CachedPDOStatement($action3->etudiant($_GET['eq_id']));
$timeend=microtime(true);
$time=$timeend-$timestart;
$message .= " <br>fonction etudiant ".number_format($time, 3);



$year = date("Y");
$listeEquipeTypeParAnnee= array();

$timestart=microtime(true);
for ($i = 2015; $i <= $year; $i++) 	$listeEquipeTypeParAnnee[$i]= new CachedPDOStatement($action3->typeParAnnee($_GET['eq_id'],$i));
$timeend=microtime(true);
$time=$timeend-$timestart;
$message .= " <br>fonction typeParAnnee ".number_format($time, 3);




//$timestart=microtime(true);
//for ($i = $year-10; $i <= $year; $i++) $listeEquipeNonPermanentParAnnee[$i]= new CachedPDOStatement($action3->nonpermanentParAnnee($_GET['eq_id'],$i));
//$timeend=microtime(true);
//$time=$timeend-$timestart;
//$message .= " <br>fonction nonpermanentParAnnee ".number_format($time, 3);


//$timestart=microtime(true);
//for ($i = $year-10; $i <= $year; $i++) $listeEquipePermanentParAnnee[$i]= new CachedPDOStatement($action3->permanentParAnnee($_GET['eq_id'],$i));
//$timeend=microtime(true);
//$time=$timeend-$timestart;
//$message .= " <br>fonction permanentParAnnee ".number_format($time, 3);





$today=date("Y-m-d");
//$today=date("2019-07-01");
$time = strtotime("+3 month", time());
$dans3mois = date("Y-m-d", $time);



$timestart=microtime(true);
$listeDepart3mois = $action3->depart3mois($_GET['eq_id'],$today,$dans3mois);
$timeend=microtime(true);
$time=$timeend-$timestart;
$message .= " <br>fonction depart3mois ".number_format($time, 3);




$timestart=microtime(true);
$listeArrivee3mois = $action3->arrivee3mois($_GET['eq_id'],$today,$dans3mois);
$timeend=microtime(true);
$time=$timeend-$timestart;
$message .= " <br>fonction arrivee3mois ".number_format($time, 3);





    require_once '../vues/PageResponsableSuiviRh.php';
    $vue2= new PageResponsableSuiviRh();
    $vue2->afficher($_GET['nomEquipe'],$_GET['eq_id'],$listeEquipeEffectif,$listeEquipePermanent,$listeEquipeNonPermanent,$listeEquipeEtudiant,$listeDepart3mois,$listeArrivee3mois,$listeEquipeTypeParAnnee,$message);

}
/////////////////////////////////////////////////////////
// Page Garde Responsable SUIVI PROJET
///////////////////////////////////////////////////////////
if ($numAction==12) {

//serge
 require_once '../web/MenuHaut.php';

        require_once '../actions/actionProjetListeParEquipe.php';
        $action2 =  new actionProjetListeParEquipe;
        $listeProjet = $action2->execute($_GET['eq_id']);

       require_once '../actions/actionProjetListeParEquipe.php';
        $action2 =  new actionProjetListeParEquipe;
        $listeProjetSoumis = new CachedPDOStatement($action2->soumis($_GET['eq_id']));
        $listeProjetAccepte = new CachedPDOStatement($action2->accepte($_GET['eq_id']));
        $listeProjetRefuse = new CachedPDOStatement($action2->refuse($_GET['eq_id']));
        $listeProjetTermine = new CachedPDOStatement($action2->termine($_GET['eq_id']));

        $listeProjetSoumisPorteur = new CachedPDOStatement($action2->soumisporteur($_GET['eq_id']));
        $listeProjetSoumisPartenaire = new CachedPDOStatement($action2->soumispartenaire($_GET['eq_id']));
        $listeProjetAcceptePorteur = new CachedPDOStatement($action2->accepteporteur($_GET['eq_id']));
        $listeProjetAcceptePartenaire = new CachedPDOStatement($action2->acceptepartenaire($_GET['eq_id']));
        $listeProjetRefusePorteur = new CachedPDOStatement($action2->refuseporteur($_GET['eq_id']));
        $listeProjetRefusePartenaire = new CachedPDOStatement($action2->refusepartenaire($_GET['eq_id']));


$today=date("Y-m-d");
$time = strtotime("+12 month", time());
$dans12mois = date("Y-m-d", $time);
$listeFin12mois = $action2->fin12mois($_GET['eq_id'],$today,$dans12mois);

$year = date("Y");
$listeEquipeProjetParAnnee= array();

//    $listeEquipeProjetParAnnee[$$year]= new CachedPDOStatement($action2->projetParAnnee($_GET['eq_id'],$year));
for ($i = $year-10; $i <= $year; $i++) {
        $listeEquipeProjetParAnnee[$i]= new CachedPDOStatement($action2->projetParAnnee($_GET['eq_id'],$i));
        // 3mn 30s
        }


    require_once '../vues/PageResponsableSuiviProjet.php';
    $vue2= new PageResponsableSuiviProjet();
    $vue2->afficher($_GET['nomEquipe'],$_GET['eq_id'],$listeProjet,$listeProjetSoumisPorteur,$listeProjetSoumisPartenaire,$listeProjetAcceptePorteur,$listeProjetAcceptePartenaire,$listeProjetRefusePorteur,$listeProjetRefusePartenaire,$listeFin12mois,$listeEquipeProjetParAnnee,$listeProjetSoumis,$listeProjetAccepte,$listeProjetRefuse,$listeProjetTermine); 
}
