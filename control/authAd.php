<?php
/***************************************************************************\
 *  GES UMR : gestion rh pour les umr                                      *
 *                                                                         *
 *  Copyright (c) 2018                                                     *
 *  Serge Conrad                                                           *
 *  Elise Deme								   *
 *  	                                                                   *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/
session_start();

require_once '../entites/Model.php';


$ini = parse_ini_file("../entites/hybiscus.ini", true );


$ad_host=$ini['ad']['host'];
$ad_domain=$ini['ad']['domain'];
$ad_basedn=$ini['ad']['basedn'];
$ad_group=$ini['ad']['group'];
//$user='sconrad';
//$password='ltr$1ctef';
  $user = $_POST["username"];
 $password= $_POST["password"];


$ad = ldap_connect("ldap://{$ad_host}") or die('Could not connect to LDAP server.');

ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
echo "{$user}@{$ad_domain}";
@ldap_bind($ad, "{$user}@{$ad_domain}", $password) or die('Could not bind to AD.');



$userdn = getDN($ad, $user, $ad_basedn);
if (checkGroupEx($ad, $userdn, getDN($ad, $ad_group, $ad_basedn))) {
//if (checkGroup($ad, $userdn, getDN($ad, $ad_group, $basedn))) {
    echo "You're authorized as ".getCN($userdn);
} else {
    echo 'Authorization failed';
}


   $attributes = array("sn","mail","givenname");
    $result = ldap_search($ad, $ad_basedn,"(samaccountname={$user})", $attributes);





    if ($result === FALSE) { return ''; }
    $entries = ldap_get_entries($ad, $result);
    $mail = $entries[0]['mail'][0]; 
    $givenname = $entries[0]['givenname'][0]; 
    $sn = $entries[0]['sn'][0]; 


ldap_unbind($ad);


//$supannEntiteAffectation  = $array['supannEntiteAffectation'];

//if ($supannEntiteAffectation != $supannAffectation) {
//	echo "Vous êtes affecté dans l'entité supann $supannEntiteAffectation qui n'est pas autorisée à visualiser cette page";
//	die;
//}
$user= strtolower(substr($givenName,0,1).$sn);



// On met le nom dans la session php
$_SESSION['user'] = $mail;

// On met le p_id dans la session php
$rq =  Model::daoAuthIdCas($mail);
$row = $rq -> fetch();
if (isset ($row->p_id)) $_SESSION['p_id'] = $row->p_id;
else {
$_SESSION['p_id']="NEW";
}



// On regarde dans la table droitsspip si l'utilisateur a des droits spécifiques 
$rq =  Model::daoAuthDroits($mail);

// L'utilisateur est-il  déclaré dans la table droits ?
if ($rq->rowCount()==1) {
        $row = $rq -> fetch();

          $_SESSION['admin'] = $row->admin;
          $_SESSION['equipe'] = $row->equipe;
          $_SESSION['personnes'] = $row->personnes;
          $_SESSION['ordi'] = $row->ordi;
          $_SESSION['cahiersLabo'] = $row->cahiersLabo;
          $_SESSION['reservations'] = $row->reservations;
          $_SESSION['demandeintranet'] = $row->demandeIntranet;
                       $_SESSION['arrivant'] = $row->arrivant;

}

//echo " $user ADMIN".$row->admin;die;
// On regarde dans la table equipe si l'utilisateur est responsable d'équipe
$rq =  Model::daoAuthRespEquip($_SESSION['p_id']);
//var_dump($rq);
//foreach ($rq as $row) echo $row;

//OLD
//if ($rq->rowCount()==1) {
//        $row = $rq -> fetch();
//          $_SESSION['responsableEquipe'] = $row->eq_id;
//          $_SESSION['responsableEquipeNom'] = $row->eq_nom;
//}


$arrayResp=array();
foreach ($rq as $row) $arrayResp[$row->eq_id]= $row->eq_nom;
$_SESSION['responsableEquipe'] = $arrayResp;




        //echo "bienvenue ".$user;
        header( "Location: ControleurChercheur.php" );
/*
* This function searchs in LDAP tree ($ad -LDAP link identifier)
* entry specified by samaccountname and returns its DN or epmty
* string on failure.
*/
function getDN($ad, $samaccountname, $basedn) {
    $attributes = array('dn');
    $result = ldap_search($ad, $basedn,
        "(samaccountname={$samaccountname})", $attributes);
    if ($result === FALSE) { return ''; }
    $entries = ldap_get_entries($ad, $result);
    if ($entries['count']>0) { return $entries[0]['dn']; }
    else { return ''; };
}

/*
* This function retrieves and returns CN from given DN
*/
function getCN($dn) {
    preg_match('/[^,]*/', $dn, $matchs, PREG_OFFSET_CAPTURE, 3);
    return $matchs[0][0];
}


/*
* This function checks group membership of the user, searching
* in specified group and groups which is its members (recursively).
*/
function checkGroupEx($ad, $userdn, $groupdn) {
        var_dump($userdn);
        var_dump($groupdn);
        echo "==";
    $attributes = array('memberof');
    $result = ldap_read($ad, $userdn, '(objectclass=*)', $attributes);
    if ($result === FALSE) { return FALSE; };
    $entries = ldap_get_entries($ad, $result);
    if ($entries['count'] <= 0) { return FALSE; };
    if (empty($entries[0]['memberof'])) { return FALSE; } else {
        for ($i = 0; $i < $entries[0]['memberof']['count']; $i++) {
            if ($entries[0]['memberof'][$i] == $groupdn) { return TRUE; }
            elseif (checkGroupEx($ad, $entries[0]['memberof'][$i], $groupdn)) { return TRUE; };
        };
    };
    return FALSE;
}


?>
