<?php
/*********************************************************************************************************************************\
 *  Hybicus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                                                                                *
 *  Copyright (c) 2018 2019                                                                                                   *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

require_once("../web/session_start.php");
require_once '../control/CachedPDOStatement.php';
// Chargement des tables ...
	require_once '../actions/actionPersonneFiltre.php';
	require_once '../actions/actionTutelleListe.php';
	require_once '../actions/actionSiteListe.php';
 	require_once '../actions/actionStatutListe.php';
 	require_once '../actions/actionTypeListe.php';
 	require_once '../actions/actionFormationListe.php';
 	require_once '../actions/actionCorpsListe.php';
       require_once '../actions/actionPRFinanceurListe.php';
       require_once '../actions/actionPRTypeListe.php';
  require_once '../actions/actionCahierListe.php';
 require_once '../actions/actionRessourcesMessageListe.php';
    	$actionTutelle =  new actionTutelleListe;
      	$actionSite =  new actionSiteListe;
      	$actionStatut =  new actionStatutListe;
      	$actionType =  new actionTypeListe;
	$actionPersonne =  new actionPersonneFiltre;
	$actionFormation =  new actionFormationListe;
	 $actionRessMesgL=  new actionRessourcesMessageListe;
	 $actionCorps=  new actionCorpsListe;
        $actionPRFinanceur =  new actionPRFinanceurListe;
        $actionPRType =  new actionPRTypeListe;
	$actionCahier =  new actionCahierListe;
	// cf https://stackoverflow.com/questions/15385965/php-pdo-with-foreach-and-fetch
	$listeTutelle = new CachedPDOStatement($actionTutelle->execute());
	$arrayTutelle=array();
	foreach ($listeTutelle as $row) $arrayTutelle[$row->t_id]= $row->t_nom;
        $listeSite = new CachedPDOStatement( $actionSite->execute());
        $listeStatut =  new CachedPDOStatement($actionStatut->execute());
        $listeType =  new CachedPDOStatement($actionType->execute());
	$listePersonne =  new CachedPDOStatement($actionPersonne->execute('T','T','T','T',date('Y-m-d'),'','',0)); // Tout le monde sauf les archivés
	$arrayPersonne=array();
	foreach ($listePersonne as $row) $arrayPersonne[$row->p_id]= $row->p_nom.' '.$row->p_prenom;

	$listeFormation =  new CachedPDOStatement($actionFormation->execute()); // Tout le monde sauf les archivés
    $listeRessource =  new CachedPDOStatement($actionRessMesgL->execute());
	$listeCorps =   $actionCorps->execute(); 

	$listeCahier =  new CachedPDOStatement($actionCahier->execute('T','T','T','T'));



        $listeFinanceur =  new CachedPDOStatement($actionPRFinanceur->execute());
	$arrayFinanceur=array();
	foreach ($listeFinanceur as $row) $arrayFinanceur[$row->financeur_id]= $row->financeur_nom;
        $listeType =  new CachedPDOStatement($actionPRType->execute());
	$arrayType=array();
	foreach ($listeType as $row) $arrayType[$row->type_id]= $row->type_nom;




     if (isset ( $_GET ['numAction'] )) $numAction=$_GET['numAction'];
     else if (isset ( $_POST ['numAction'] )) $numAction=$_POST['numAction'];



         require_once '../actions/actionEquipeRechercheListe.php';
        $action =  new actionEquipeRechercheListe;
        $listeEquipe = new CachedPDOStatement($action->execute());
	$arrayEquipe=array();
	foreach ($listeEquipe as $row) $arrayEquipe[$row->eq_id]= $row->eq_nom;





   if (isset($_POST['selecteq_id']))  $selecteq_id = $_POST['selecteq_id'];
        elseif (isset($_GET['selecteq_id']))  $selecteq_id = $_GET['selecteq_id'];
        else $selecteq_id=  "T";
   if (isset($_POST['selectpreselection']))  $selectpreselection = $_POST['selectpreselection'];
        elseif (isset($_GET['selectpreselection']))  $selectpreselection = $_GET['selectpreselection'];
        else $selectpreselection=  "T";
   if (isset($_POST['selectDate']))  $selectDate = $_POST['selectDate'];
        elseif (isset($_GET['selectDate']))  $selectDate = $_GET['selectDate'];
//        else $selectDate=  ('Y-m-d');
   if (isset($_POST['selectDateDebut']))  $selectDateDebut = $_POST['selectDateDebut'];
        elseif (isset($_GET['selectDateDebut']))  $selectDateDebut = $_GET['selectDateDebut'];
//        else $selectDateDebut=  date('Y-m-d');
   if (isset($_POST['selectPersonnel']))  $selectPersonnel = $_POST['selectPersonnel'];
        elseif (isset($_GET['selectPersonnel']))  $selectPersonnel = $_GET['selectPersonnel'];
        else $selectPersonnel =   "T";
   if (isset($_POST['selectAssocie']))  $selectAssocie = $_POST['selectAssocie'];
        elseif (isset($_GET['selectAssocie']))  $selectAssocie = $_GET['selectAssocie'];
        else $selectAssocie =   "T";



// virer tous les $_GET et $_POST DANS LE CODE!
   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

//if ($menuDestination=="responsableequipe")  {
//   	if (isset ( $_GET['selecteq_id'] )) $selecteq_id=$_GET['selecteq_id'];
//	   else  $selecteq_id=$_POST['selecteq_id'];
//	}
//else     	if (isset($_POST['selecteq_id'])) $selecteq_id = $_POST['selecteq_id'];
//        	else $selecteq_id = 'T';


///////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// Projet Ajout Cahier
///////////////////////////////////////////////////////////
if  ($numAction==115){

require_once '../web/MenuHaut.php';

// id du projet
$id= $_POST['projet_id'];
$menuDestination= $_POST['menuDestination'];

// qui appele admin , chercheur ou responsableequipe
//selecteq_id est le selecteur d'équipe dans la page projetnavigationadmin
if (($menuDestination == "admin")||($menuDestination == "responsableequipe")) {
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
//        $liste = $action2->execute($_POST['selecteq_id'],$_POST['selectpreselection'],$_POST['selectDate'],$_POST['selectDateDebut']);
        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);

        require_once '../vues/PageProjetNavigationAdmin.php';
//        $vue= new PageProjetNavigationAdmin($_POST['selecteq_id'],$_POST['selectpreselection'],$_POST['selectDate'],$_POST['selectDateDebut']);
//        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe);
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);
}

else {

        require_once '../actions/actionProjetListeParChercheur.php';
        $action2 =  new actionProjetListeParChercheur;
        $liste = $action2->execute($_SESSION['p_id']);

        require_once '../vues/PageProjetNavigation.php';
        $vue= new PageProjetNavigation();
        $vue->afficher($liste,$menuDestination);
}


        require_once '../actions/actionProjetGet.php';
        // rend un tableau associatif
         $action1 =  new actionProjetGet;
        $tabprojet =  $action1->execute($id);

//        $listeCahierlaboProjet = $action3->cahierlaboProjetListe($cahierNum);
require_once '../actions/actionProjetAjouteCahier.php';

   $action =  new actionProjetAjouteCahier;
    $action->execute($_POST);



        require_once '../actions/actionProjetCahier.php';
             $action3 =  new actionProjetCahier;
            $listeProjetCahier = $action3->execute($id);


  require_once '../vues/PageProjet.php';

      require_once '../actions/actionProjetAssocieGet.php';
        $action4 =  new actionProjetAssocieGet;
            $listeassocie = $action4->execute($_POST['projet_id']);


       require_once '../actions/actionProjetEquipeGet.php';
       $action5 =  new actionProjetEquipeGet;
       $listeequipe = $action5->execute($_POST['projet_id']);



        $vue2= new PageProjet(0,$tabprojet,$menuDestination,0,0,$listeassocie,$listeequipe,"","");


        $vue2->afficher("",$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType,$_POST['selecteq_id'],$_POST['selectpreselection'],$_POST['selectDate'],$_POST['selectDateDebut'],$listeCahier,$listeProjetCahier,$selectPersonnel,$selectAssocie);

}


///////////////////////////////////////////////////////
// Projet Supp Cahier
///////////////////////////////////////////////////////////
if  ($numAction==116){

require_once '../web/MenuHaut.php';

// id du projet
$id= $_POST['projet_id'];
$menuDestination= $_POST['menuDestination'];

// qui appele admin , chercheur ou responsableequipe
//selecteq_id est le selecteur d'équipe dans la page projetnavigationadmin
if (($menuDestination == "admin")||($menuDestination == "responsableequipe")) {
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
        //$liste = $action2->execute($_POST['selecteq_id'],$_POST['selectpreselection'],$_POST['selectDate'],$_POST['selectDateDebut']);
        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);

        require_once '../vues/PageProjetNavigationAdmin.php';
//        $vue= new PageProjetNavigationAdmin($_POST['selecteq_id'],$_POST['selectpreselection'],$_POST['selectDate'],$_POST['selectDateDebut']);
//        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe);
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);

}

else {

        require_once '../actions/actionProjetListeParChercheur.php';
        $action2 =  new actionProjetListeParChercheur;
        $liste = $action2->execute($_SESSION['p_id']);

        require_once '../vues/PageProjetNavigation.php';
        $vue= new PageProjetNavigation();
        $vue->afficher($liste,$menuDestination);
}


        require_once '../actions/actionProjetGet.php';
        // rend un tableau associatif
         $action1 =  new actionProjetGet;
        $tabprojet =  $action1->execute($id);

//        $listeCahierlaboProjet = $action3->cahierlaboProjetListe($cahierNum);
require_once '../actions/actionProjetSupprimeCahier.php';

   $action =  new actionProjetSupprimeCahier;
    $action->execute($_POST);



        require_once '../actions/actionProjetCahier.php';
             $action3 =  new actionProjetCahier;
            $listeProjetCahier = $action3->execute($id);


  require_once '../vues/PageProjet.php';
      require_once '../actions/actionProjetAssocieGet.php';
        $action4 =  new actionProjetAssocieGet;
            $listeassocie = $action4->execute($_POST['projet_id']);


       require_once '../actions/actionProjetEquipeGet.php';
       $action5 =  new actionProjetEquipeGet;
       $listeequipe = $action5->execute($_POST['projet_id']);


        $vue2= new PageProjet(0,$tabprojet,$menuDestination,0,0,$listeassocie,$listeequipe,"","");

        $vue2->afficher("",$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType,$_POST['selecteq_id'],$_POST['selectpreselection'],$_POST['selectDate'],$_POST['selectDateDebut'],$listeCahier,$listeProjetCahier,$selectPersonnel,$selectAssocie);

}



/////////////////////////////////////////////////////////
// USE CASE ADMIN LISTE PROJET
// Ces projets sont ajoutés par les chercheurs via l'intranet
/////////////s/////////////////////////////////////////
if ($numAction==100) {
//$_POST['selecteq_id'] est le selecteur d'équipe dans pageprojetnavigationadmin
     	if (isset($_POST['selectpreselection']))  $selectpreselection = $_POST['selectpreselection'];
        else $selectpreselection= 'T';
     	if (isset($_POST['selectDate']) AND ($_POST['selectDate'] != ""))  $selectDate = $_POST['selectDate'];
        else $selectDate= 'T';
     	if (isset($_POST['selectDateDebut']) AND ($_POST['selectDateDebut'] != ""))  $selectDateDebut = $_POST['selectDateDebut'];
        else $selectDateDebut= 'T';
require_once '../web/MenuHaut.php';
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);


        require_once '../vues/PageProjetNavigationAdmin.php';
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);

//Premier Appel, $_POST['selecteq_id'] n'est pas positionné par pageprojetnavigationadmi
        require_once '../vues/PageProjetPageGarde.php';
        $vue2= new PageProjetPageGarde();

//abc
$listeProjetSoumis = $action2->soumis($selecteq_id,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
$listeProjetAccepte = $action2->accepte($selecteq_id,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
$listeProjetRefuse = $action2->refuse($selecteq_id,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
$listeProjetTermine = $action2->termine($selecteq_id,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);

//echo "\n============";
//echo " selecteq_id".$selecteq_id;
//foreach ($listeProjetSoumis as $row) echo $row;



        $listeProjetSoumisPorteur = new CachedPDOStatement($action2->soumisporteur($selecteq_id,$selectDate,$selectDateDebut));
        $listeProjetSoumisPartenaire = new CachedPDOStatement($action2->soumispartenaire($selecteq_id,$selectDate,$selectDateDebut));
        $listeProjetAcceptePorteur = new CachedPDOStatement($action2->accepteporteur($selecteq_id,$selectDate,$selectDateDebut));
        $listeProjetAcceptePartenaire = new CachedPDOStatement($action2->acceptepartenaire($selecteq_id,$selectDate,$selectDateDebut));
        $listeProjetRefusePorteur = new CachedPDOStatement($action2->refuseporteur($selecteq_id,$selectDate,$selectDateDebut));
        $listeProjetRefusePartenaire = new CachedPDOStatement($action2->refusepartenaire($selecteq_id,$selectDate,$selectDateDebut));


$today=date("Y-m-d");
$time = strtotime("+12 month", time());
$dans12mois = date("Y-m-d", $time);
$listeFin12mois = $action2->fin12mois("%",$today,$dans12mois);

//$year = 2025;
//$listeProjetParAnneeQuinquennal2= array();
//for ($i = $year-5; $i <= $year; $i++) $listeProjetParAnneeQuinquennal2[$i]= new CachedPDOStatement($action2->projetParAnnee("%",$i));
$listeProjetParAnneeQuinquennal2= new CachedPDOStatement($action2->projetParAnnee("%",'2020','2025'));

//$year = 2019;
//$listeProjetParAnneeQuinquennal1= array();
//for ($i = $year-5; $i <= $year; $i++) $listeProjetParAnneeQuinquennal1[$i]= new CachedPDOStatement($action2->projetParAnnee("%",$i));
$listeProjetParAnneeQuinquennal1= new CachedPDOStatement($action2->projetParAnnee("%",'2014','2019'));

//$year = 2025;
 $listeProjetParAnneeMontantQuinquennal1= new CachedPDOStatement($action2->projetMontantParAnnee("%",'2014','2019'));
 $listeProjetParAnneeMontantQuinquennal2= new CachedPDOStatement($action2->projetMontantParAnnee("%",'2020','2025'));



 $listeProjetParAnneeMontantParTutelleQuinquennal1= new CachedPDOStatement($action2->projetMontantParTutelleParAnnee("%",'2014','2019'));
 $listeProjetParAnneeMontantParTutelleQuinquennal2= new CachedPDOStatement($action2->projetMontantParTutelleParAnnee("%",'2020','2025'));


$listeProjetParAnneeFinanceur= array();
//for ($i = $year-10; $i <= $year; $i++) $listeProjetParAnneeFinanceur[$i]= new CachedPDOStatement($action2->projetParAnneeFinanceur("%",$i));

$listeProjetParAnneeType= array();
//for ($i = $year-10; $i <= $year; $i++) $listeProjetParAnneeType[$i]= new CachedPDOStatement($action2->projetParAnneeType("%",$i));



$vue2->afficher($menuDestination,$selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$listeProjet,$listeProjetSoumisPorteur,$listeProjetSoumisPartenaire,$listeProjetAcceptePorteur,$listeProjetAcceptePartenaire,$listeProjetRefusePorteur,$listeProjetRefusePartenaire,$listeFin12mois,$listeProjetParAnneeQuinquennal1,$listeProjetParAnneeQuinquennal2,$listeProjetParAnneeMontantQuinquennal1,$listeProjetParAnneeMontantQuinquennal2,$listeProjetParAnneeMontantParTutelleQuinquennal1,$listeProjetParAnneeMontantParTutelleQuinquennal2,$listeProjetParAnneeFinanceur,$listeProjetSoumis,$listeProjetAccepte,$listeProjetRefuse,$listeProjetTermine,$listeTutelle,$listeFinanceur,$listeProjetParAnneeType,$listeType,$arrayEquipe,$arrayPersonne,$arrayFinanceur,$arrayType,$arrayTutelle,$selectPersonnel,$selectAssocie); 


}


/////////////////////////////////////////////////////////
// USE CASE CHERCHEUR DECLARE PROJET
// Ces projets sont ajoutés par les chercheurs via l'intranet
/////////////s/////////////////////////////////////////
if ($numAction==110) {
require_once '../web/MenuHaut.php';


if ($menuDestination == "admin") {
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
	//$liste = $action2->execute($_GET['selecteq_id'],$_GET['selectpreselection'],$_GET['selectDate'],$_GET['selectDateDebut']);
        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);


        require_once '../vues///PageProjetNavigationAdmin.php';
//        $vue= new PageProjetNavigationAdmin('T','T','T');
//        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe);
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);

}
else {
       require_once '../actions/actionProjetListeParChercheur.php';
        $action2 =  new actionProjetListeParChercheur;
        $liste = $action2->execute($_SESSION['p_id']);

        require_once '../vues/PageProjetNavigation.php';
        $vue= new PageProjetNavigation();
        $vue->afficher($liste,$menuDestination);
}




  //     require_once '../actions/actionPRFinanceurListe.php';
  //      $action3 =  new actionPRFinanceurListe;
  //      $listeFinanceur = $action3->execute();


  require_once '../vues/PageProjet.php';
      require_once '../actions/actionProjetAssocieGet.php';
        $action4 =  new actionProjetAssocieGet;
            $listeassocie = $action4->execute($_POST['projet_id']);

       require_once '../actions/actionProjetEquipeGet.php';
       $action5 =  new actionProjetEquipeGet;
       $listeequipe = $action5->execute($_POST['projet_id']);



        $vue2= new PageProjet(1,$_POST,$menuDestination,0,0,$listeassocie,$listeequipe,"","");

//        $vue2->afficher("",$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType);
        $vue2->afficher("",$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType,$selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$listeCahier,$listeProjetCahier,$selectPersonnel,$selectAssocie);
}


////////////////////////////////////////////////////////
// USE CASE ADMIN  SUPPRESSION PROJET
if ($numAction==113) {
require_once '../web/MenuHaut.php';


 require_once '../formulaires/FormProjet.php';
 require_once '../web/MenuHaut.php';

try {

        $projet = new FormProjet($_POST);


        require_once '../actions/actionProjetSupprime.php';
        $action =  new actionProjetSupprime();
        $action->execute($projet);

if (($menuDestination == "admin")||($menuDestination =="responsableequipe")) {
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
        //$liste = $action2->execute($selecteq_id,$_POST['selectpreselection'],$_POST['selectDate'],$_POST['selectDateDebut']);
        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);

        require_once '../vues/PageProjetNavigationAdmin.php';
//        $vue= new PageProjetNavigationAdmin($selecteq_id,$_POST['selectpreselection'],$_POST['selectDate'],$_POST['selectDateDebut']);
//        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe);
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);

}

else {

        require_once '../actions/actionProjetListeParChercheur.php';
        $action2 =  new actionProjetListeParChercheur;
        $liste = $action2->execute($_SESSION['p_id']);

        require_once '../vues/PageProjetNavigation.php';
        $vue= new PageProjetNavigation();
        $vue->afficher($liste,$menuDestination);
}



} catch (Exception $e) {
        require_once '../vues/PageProjet.php';
      require_once '../actions/actionProjetAssocieGet.php';
        $action4 =  new actionProjetAssocieGet;
            $listeassocie = $action4->execute($_POST['projet_id']);


       require_once '../actions/actionProjetEquipeGet.php';
       $action5 =  new actionProjetEquipeGet;
       $listeequipe = $action5->execute($_POST['projet_id']);



        $vue= new PageProjet($_POST['iscreation'],$_POST,$menuDestination,0,0,$listeassocie,$listeequipe,"","");

//        $vue->afficher($e->getMessage(),$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType);
        $vue->afficher("",$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType,$_GET['selecteq_id'],$_GET['selectpreselection'],$_GET['selectDate'],$_GET['selectDateDebut'],$listeCahier,$listeProjetCahier,$selectPersonnel,$selectAssocie);

}
}


////////////////////////////////////////////////////////
// USE CASE ADMIN  AJOUT PROJET
if (substr( $numAction, 0, 3 ) == '117') {
require_once '../web/MenuHaut.php';

        require_once '../actions/actionProjetEquipeModifie.php';
        $action =  new actionProjetEquipeModifie();
        $action->execute($_POST);

if (isset ( $_GET ['projet_id'] )) $projet_id = $_GET['projet_id'] ;
else $projet_id = $_POST['projet_id'];
// id du projet

if (($menuDestination == "admin") || ($menuDestination == "responsableequipe")) {
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);

        require_once '../vues/PageProjetNavigationAdmin.php';
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);

}
else {
        require_once '../actions/actionProjetListeParChercheur.php';
        $action2 =  new actionProjetListeParChercheur;
        $liste = $action2->execute($_SESSION['p_id']);

        require_once '../vues/PageProjetNavigation.php';
        $vue= new PageProjetNavigation();
        $vue->afficher($liste,$menuDestination);
}

        require_once '../actions/actionProjetGet.php';
        // rend un tableau associatif
         $action1 =  new actionProjetGet;
        $tabprojet =  $action1->execute($projet_id);

        require_once '../actions/actionProjetCahier.php';
             $action3 =  new actionProjetCahier;
            $listeProjetCahier = $action3->execute($projet_id);

        require_once '../actions/actionProjetAssocieGet.php';
             $action4 =  new actionProjetAssocieGet;
            $listeassocie = $action4->execute($projet_id);

        require_once '../actions/actionProjetEquipeGet.php';
             $action5 =  new actionProjetEquipeGet;
            $listeequipe = $action5->execute($projet_id);

$ajoutassocie=0;
  require_once '../vues/PageProjet.php';
        $vue2= new PageProjet(0,$tabprojet,$menuDestination,$ajoutassocie,0,$listeassocie,$listeequipe,"","");
        $vue2->afficher("",$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType,$selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$listeCahier,$listeProjetCahier,$selectPersonnel,$selectAssocie);
}


////////////////////////////////////////////////////////
// USE CASE ADMIN  AJOUT PROJET
if (substr( $numAction, 0, 3 ) == '114') {
require_once '../web/MenuHaut.php';

        require_once '../actions/actionProjetAssocieModifie.php';
        $action =  new actionProjetAssocieModifie();
        $action->execute($_POST);

if (isset ( $_GET ['projet_id'] )) $projet_id = $_GET['projet_id'] ;
else $projet_id = $_POST['projet_id'];
// id du projet

if (($menuDestination == "admin") || ($menuDestination == "responsableequipe")) {
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);

        require_once '../vues/PageProjetNavigationAdmin.php';
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);

}
else {
        require_once '../actions/actionProjetListeParChercheur.php';
        $action2 =  new actionProjetListeParChercheur;
        $liste = $action2->execute($_SESSION['p_id']);

        require_once '../vues/PageProjetNavigation.php';
        $vue= new PageProjetNavigation();
        $vue->afficher($liste,$menuDestination);
}

        require_once '../actions/actionProjetGet.php';
        // rend un tableau associatif
         $action1 =  new actionProjetGet;
        $tabprojet =  $action1->execute($projet_id);

        require_once '../actions/actionProjetCahier.php';
             $action3 =  new actionProjetCahier;
            $listeProjetCahier = $action3->execute($projet_id);

        require_once '../actions/actionProjetAssocieGet.php';
             $action4 =  new actionProjetAssocieGet;
            $listeassocie = $action4->execute($projet_id);

$ajoutassocie=0;
  require_once '../vues/PageProjet.php';
        $vue2= new PageProjet(0,$tabprojet,$menuDestination,$ajoutassocie,0,$listeassocie,$listeequipe,"","");
        $vue2->afficher("",$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType,$selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$listeCahier,$listeProjetCahier,$selectPersonnel,$selectAssocie);
}




////////////////////////////////////////////////////////
// USE CASE ADMIN  AJOUT PROJET
if (substr( $numAction, 0, 3 ) == '111') {
require_once '../web/MenuHaut.php';


 require_once '../formulaires/FormProjet.php';
 require_once '../web/MenuHaut.php';



try {

        $projet = new FormProjet($_POST);


// email à envoyer lors soumission projet ?
       require_once '../actions/actionProjetSoumis.php';
         $actionProjetSoumis =  new actionProjetSoumis;
         $projetSoumis =  $actionProjetSoumis->execute();
if (($projet->getpreselection()=="soumis") &&(!($projetSoumis==""))) {
               // ACTION MESSAGE a destination du nouvel arrivant
               require_once '../actions/actionMessageProjet.php';
               $action =  new actionMessageProjet();
               $action->execute($projetSoumis,$projet,"soumis");

	// envoyer mail
}

// email à envoyer lors  projet accepté ?
       require_once '../actions/actionProjetAccepte.php';
         $actionProjetAccepte =  new actionProjetAccepte;
         $projetAccepte =  $actionProjetAccepte->execute();
if (($projet->getpreselection()=="accepté") &&(!($projetAccepte==""))) {
               require_once '../actions/actionMessageProjet.php';
               $action =  new actionMessageProjet();
               $action->execute($projetAccepte,$projet,"accepté");
        // envoyer mail
}


if ($projet->getiscreation() == 1) {

        require_once '../actions/actionProjetAjoute.php';
        $action =  new actionProjetAjoute();
        $action->execute($projet);
	//serge 
}
else
{
        require_once '../actions/actionProjetModifie.php';
        $action =  new actionProjetModifie();
        $action->execute($projet);
}


//ICI
if ($menuDestination == "admin") {
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;

        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        require_once '../vues/PageProjetNavigationAdmin.php';
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);
}
elseif ($menuDestination == "responsableequipe") {
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
        //$liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut);
        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);

        require_once '../vues/PageProjetNavigationAdmin.php';
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);
}

else {
        require_once '../actions/actionProjetListeParChercheur.php';
        $action2 =  new actionProjetListeParChercheur;
        $liste = $action2->execute($_SESSION['p_id']);

        require_once '../vues/PageProjetNavigation.php';
        $vue= new PageProjetNavigation();
        $vue->afficher($liste,$menuDestination);
}

if  ((substr($numAction,3,3) == 'ADD') ||($_POST['ajoutassocie']==1)) $ajoutassocie=1;
else $ajoutassocie=0;
if  ((substr($numAction,3,3) == 'ADE') ||($_POST['ajoutequipe']==1)) $ajoutequipe=1;
else $ajoutequipe=0;

        require_once '../actions/actionProjetCahier.php';
             $action3 =  new actionProjetCahier;
            $listeProjetCahier = $action3->execute($_POST['projet_id']);

        require_once '../actions/actionProjetAssocieGet.php';
        $action4 =  new actionProjetAssocieGet;
            $listeassocie = $action4->execute($_POST['projet_id']);

       require_once '../actions/actionProjetEquipeGet.php';
       $action5 =  new actionProjetEquipeGet;
       $listeequipe = $action5->execute($_POST['projet_id']);

  require_once '../vues/PageProjet.php';
        $vue2= new PageProjet(0,$_POST,$menuDestination,$ajoutassocie,$ajoutequipe,$listeassocie,$listeequipe,"","");
//        $vue2->afficher("Enregistré",$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType);
        $vue2->afficher("Enregistré",$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType,$selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$listeCahier,$listeProjetCahier,$selectPersonnel,$selectAssocie);


} catch (Exception $e) {
if ($menuDestination == "admin") {
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;

        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        require_once '../vues/PageProjetNavigationAdmin.php';
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);
}
elseif ($menuDestination == "responsableequipe") {
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
        //$liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut);
        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);

        require_once '../vues/PageProjetNavigationAdmin.php';
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);
}

else {
        require_once '../actions/actionProjetListeParChercheur.php';
        $action2 =  new actionProjetListeParChercheur;
        $liste = $action2->execute($_SESSION['p_id']);

        require_once '../vues/PageProjetNavigation.php';
        $vue= new PageProjetNavigation();
        $vue->afficher($liste,$menuDestination);
}
        require_once '../vues/PageProjet.php';
      require_once '../actions/actionProjetAssocieGet.php';
        $action4 =  new actionProjetAssocieGet;
            $listeassocie = $action4->execute($_POST['projet_id']);



       require_once '../actions/actionProjetEquipeGet.php';
       $action5 =  new actionProjetEquipeGet;
       $listeequipe = $action5->execute($_POST['projet_id']);


        $vue= new PageProjet($_POST['iscreation'],$_POST,$menuDestination,0,0,$listeassocie,$listeequipe,"","");

        //$vue->afficher($e->getMessage(),$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType);
        $vue->afficher($e->getMessage(),$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType,$selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$listeCahier,$listeProjetCahier,$selectPersonnel,$selectAssocie);

}
}

////////////////////////////////////////////////////////
// USE CASE ADMIN  AJOUT PROJET
if (substr( $numAction, 0, 3 ) == '112') {
require_once '../web/MenuHaut.php';

  if (isset ( $_GET ['projet_id'] )) $projet_id = $_GET['projet_id'] ;
else $projet_id = $_POST['projet_id'];
// id du projet

// qui appele admin , chercheur ou responsableequipe
//selecteq_id est le selecteur d'équipe dans la page projetnavigationadmin
if (($menuDestination == "admin") || ($menuDestination == "responsableequipe")) {
        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
        //$liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut);
        $liste = $action2->execute($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);

        require_once '../vues/PageProjetNavigationAdmin.php';
//        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut);
//        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe);
        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);
        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe,$listePersonne);

}

else {

        require_once '../actions/actionProjetListeParChercheur.php';
        $action2 =  new actionProjetListeParChercheur;
        $liste = $action2->execute($_SESSION['p_id']);

        require_once '../vues/PageProjetNavigation.php';
        $vue= new PageProjetNavigation();
        $vue->afficher($liste,$menuDestination);
}

        require_once '../actions/actionProjetGet.php';
        // rend un tableau associatif
         $action1 =  new actionProjetGet;
        $tabprojet =  $action1->execute($projet_id);

        require_once '../actions/actionProjetCahier.php';
    	     $action3 =  new actionProjetCahier;
            $listeProjetCahier = $action3->execute($projet_id);

        require_once '../actions/actionProjetAssocieGet.php';
    	     $action4 =  new actionProjetAssocieGet;
            $listeassocie = $action4->execute($projet_id);
//sergeequipe
        require_once '../actions/actionProjetEquipeGet.php';
    	     $action5 =  new actionProjetEquipeGet;
            $listeequipe = $action5->execute($projet_id);



if  ((substr($numAction,3,3) == 'ADD') ||($_POST['ajoutassocie']==1)) $ajoutassocie=1;
else $ajoutassocie=0;
if  ((substr($numAction,3,3) == 'ADE') ||($_POST['ajoutequipe']==1)) $ajoutequipe=1;
else $ajoutequipe=0;

  require_once '../vues/PageProjet.php';
        $vue2= new PageProjet(0,$tabprojet,$menuDestination,$ajoutassocie,$ajoutequipe,$listeassocie,$listeequipe,"","");
        $vue2->afficher("",$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType,$selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$listeCahier,$listeProjetCahier,$selectPersonnel,$selectAssocie);


}



////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste custom
//////////////////////////////////////////////////////
//if ($numAction==300) {

//        require_once '../actions/actionProjetListeAdmin.php';
//        $action =  new actionProjetListeAdmin;

//        $liste2=  $action->export($_POST['selecteq_id'],$_POST['selectpreselection'],$_POST['selectDate'],$_POST['selectDateDebut']);
//        	//$liste2=  $action->export($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);

//        $entete = $action->entete();






//if  ((substr($numAction,3,3) == 'ADD') ||($_POST['ajoutassocie']==1)) $ajoutassocie=1;
//else $ajoutassocie=0;
//if  ((substr($numAction,3,3) == 'ADE') ||($_POST['ajoutequipe']==1)) $ajoutequipe=1;
//else $ajoutequipe=0;

//  require_once '../vues/PageProjet.php';
//        $vue2= new PageProjet(0,$tabprojet,$menuDestination,$ajoutassocie,$ajoutequipe,$listeassocie,$listeequipe,"","");
//        $vue2->afficher("",$listeEquipe,$listePersonne,$listeTutelle,$listeFinanceur,$listeType,$selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$listeCahier,$listeProjetCahier,$selectPersonnel,$selectAssocie);


//}



////////////////////////////////////////////////////////
// USE CASE ADMIN Exporter la liste custom
//////////////////////////////////////////////////////
if ($numAction==300) {

        require_once '../actions/actionProjetListeAdmin.php';
        $action =  new actionProjetListeAdmin;

        //$liste2=  $action->export($_POST['selecteq_id'],$_POST['selectpreselection'],$_POST['selectDate'],$_POST['selectDateDebut']);
       	$liste2=  $action->export($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie);

        $entete = $action->entete();




$nomfich='listeProjets.csv';
        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($liste2,$entete,$nomfich);





}

////////////////////////////////////////////////////////
// USE CASE CHERCHEUR Exporter la liste des projets
//////////////////////////////////////////////////////
if ($numAction==600) {

        require_once '../actions/actionProjetListeParChercheur.php';
        $action2 =  new actionProjetListeParChercheur;
        $listeProjet = $action2->export($_SESSION['p_id']);
        $entete = $action2->entete();

        $nomfich='projetschercheur.csv';

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listeProjet,$entete,$nomfich);
}


////////////////////////////////////////////////////////
// USE CASE RESPONSABLE Exporter la liste des projets
//////////////////////////////////////////////////////
if ($numAction==601) {

        require_once '../actions/actionProjetListeParEquipe.php';
        $action2 =  new actionProjetListeParEquipe;
        $listeProjet = $action2->export($_POST['eq_id']);
        $entete = $action2->entete();

        $nomfich='projetsresponsable.csv';

        require_once '../actions/actionExportExcel.php';
        $action =  new actionExportExcel;
        $action->execute($listeProjet,$entete,$nomfich);
}


////////////////////////////////////////////////////////
// FIN
/////////////////////////////////////////////////////
//echo "<div id='header'>";
//echo '	</div>';
?>
