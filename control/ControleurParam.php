<?php

/*********************************************************************************************************************************\
 *  Hybicus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                                                                                *
 *  Copyright (c) 2018 2019                                                                                                   *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

require_once("../web/session_start.php");

//header ( 'Content-Type: text/html; charset=utf-8' );

?>


<!DOCTYPE html>

<?php
// require_once '../web/head.php';
require_once '../control/CachedPDOStatement.php';
// require_once '../entites/class.php';
 ?>

<body>
<!--
MENUS
CHERCHEUR                                               RESPONSABLE
* NOUVEAU STAGIAIRE 90                                  * LISTES STAGIAIRES 1001
* LISTE STAGIAIRES 92                                   * LISTES DOCTORANTS 1003
* NOUVEAU DOCTORANT 95
* LISTE DOCTORANTS 97



90 -> PageChercheurDeclareStagiaire -> 91 ajout dans bd
92 -> MenuChercheurConsulteStagiaire -> 93 PageChercheurModifieStagiaire -> 1007 ajout dans bd
95 -> PageChercheurDeclareDoctorant -> 96 ajout dans bd
97 -> MenuChercheurConsulteDoctorant -> 98 PageChercheurModifieDoctorant -> 1008 ajout dans bd


1001 -> MenuResponsableConsulteStagiaire -> 1002 PageResponsableModifieStagiaire -> 1005 ajout dans bd
1003 -> MenuResponsableConsulteDoctorant -> 1004 PageResponsableModifieDoctorant -> 1006 ajout dans bd

GERER EQUIPE 110 à 118

GERER PARAMETRES 120 à 165 
Mail d'avertissement 500 à 501
-->
<?php


// Chargement des tables ...
	require_once '../actions/actionPersonneFiltre.php';
	require_once '../actions/actionTutelleListe.php';
	require_once '../actions/actionSiteListe.php';
 	require_once '../actions/actionStatutListe.php';
 	require_once '../actions/actionTypeListe.php';
 	require_once '../actions/actionFormationListe.php';
    	$actionTutelle =  new actionTutelleListe;
      	$actionSite =  new actionSiteListe;
      	$actionStatut =  new actionStatutListe;
      	$actionType =  new actionTypeListe;
	$actionPersonne =  new actionPersonneFiltre;
	$actionFormation =  new actionFormationListe;
	// cf https://stackoverflow.com/questions/15385965/php-pdo-with-foreach-and-fetch
	$listeTutelle = new CachedPDOStatement($actionTutelle->execute());
        $listeSite = new CachedPDOStatement( $actionSite->execute());
        $listeStatut =  new CachedPDOStatement($actionStatut->execute());
        $listeType =  new CachedPDOStatement($actionType->execute());
	$listePersonne =  new CachedPDOStatement($actionPersonne->execute('T','T','T','T',date('Y-m-d'),"","",0)); // Tout le monde sauf les archivés
	$listeFormation =  new CachedPDOStatement($actionFormation->execute()); // Tout le monde sauf les archivés

     if (isset ( $_GET ['numAction'] )) $numAction=$_GET['numAction'];
     if (isset ( $_POST ['numAction'] )) $numAction=$_POST['numAction'];


  if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];


////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Ajouter un site
//../vues/PageSiteAjoute.php
// ou on vient de changer le type dans la vue VuePersonneAjoute.php
//////////////////////////////////////////////////////
if ($numAction==120) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageSiteNavigation.php';
 require_once '../actions/actionSiteListe.php';


         $action =  new actionSiteListe;
        $listeSite = $action->execute();

   $vue1= new PageSiteNavigation();
    $vue1->afficher($listeSite);


        require_once '../vues/PageSiteAjoute.php';
        $vue2= new PageSiteAjoute("");
        $vue2->afficher("");
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// appuyer sur le bouton submit de la vue vue PageSiteAjoute.php
//////////////////////////////////////////////////////
if ($numAction==121) {
 require_once '../formulaires/FormSiteAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionSiteListe.php';

 require_once '../vues/PageSiteNavigation.php';

         $action =  new actionSiteListe;
        $listeSite = $action->execute();

   $vue= new PageSiteNavigation();
    $vue->afficher($listeSite);

try {
        $site = new FormSiteAjoute($_POST);

        require_once '../actions/actionSiteAjoute.php';
        $action =  new actionSiteAjoute();
        $action->execute($site);


        $action2 =  new actionSiteListe;
        $listeSite = $action2->execute();

        $vue= new PageSiteNavigation();
        $vue->afficher($listeSite);



} catch (Exception $e) {
        require_once '../vues/PageSiteAjoute.php';
        $vue= new PageSiteAjoute($_POST);

        $vue->afficher($e->getMessage());


}


}

////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Modifier un site
//////////////////////////////////////////////////////
if ($numAction==122) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageSiteNavigation.php';
 require_once '../actions/actionSiteListe.php';

    $action =  new actionSiteListe;
    $listeSite = $action->execute();

   $vue= new PageSiteNavigation();
    $vue->afficher($listeSite);

   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionSiteGet.php';
        // rend un tableau associatif
         $action1 =  new actionSiteGet;
        $tabsite =  $action1->execute($id);


        require_once '../vues/PageSiteModifie.php';
        $vue2= new PageSiteModifie($tabsite);
        $vue2->afficher("");


        }
    else {
        // On vient de changer le type dans le formulaire, en mode refresh les infos sont dans le formulaire et non dans la base de données

        require_once '../vues/PageSiteModifie.php';
        $vue2= new PageSiteModifie($_POST);
        $vue2->afficher("");

        }
}

/////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// appuyer sur le bouton submit de la vue vueSiteModifie.php
/////////////////////////////////////////////////////
if ($numAction==123) {
 require_once '../formulaires/FormSiteModifie.php';
 require_once '../web/MenuHaut.php';

 require_once '../vues/PageSiteNavigation.php';

require_once '../actions/actionSiteListe.php';

    $action =  new actionSiteListe;
    $liste = $action->execute();

   $vue= new PageSiteNavigation();
    $vue->afficher($liste);

try {

        // cas des stagiaires
        $equipe = new FormSiteModifie($_POST);


//echo "modification de ".$personne->getNom();
        require_once '../actions/actionSiteModifie.php';
        $action =  new actionSiteModifie();
        $action->execute($equipe);

       $action2 =  new actionSiteListe;
        $liste = $action2->execute();

        $vue= new PageSiteNavigation();
        $vue->afficher($liste);



} catch (Exception $e) {
        require_once '../vues/PageSiteModifie.php';
        $vue2= new PageSiteModifie($_POST);
        $vue2->afficher($e->getMessage());



}
}
/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Navigation Site
///////////////////////////////////////////////////////////

if  ($numAction==124){

 require_once '../web/MenuHaut.php';
 require_once '../vues/PageSiteNavigation.php';
require_once '../actions/actionSiteListe.php';

   $action =  new actionSiteListe;
    $liste = $action->execute();

    $vue= new PageSiteNavigation($_POST);
    $vue->afficher($liste);

}
////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Ajouter un type
//../vues/PageTypeAjoute.php
// ou on vient de changer le type dans la vue VuePersonneAjoute.php
//////////////////////////////////////////////////////
if ($numAction==130) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageTypeNavigation.php';
 require_once '../actions/actionTypeListe.php';


         $action =  new actionTypeListe;
        $listeType = $action->execute();

   $vue1= new PageTypeNavigation();
    $vue1->afficher($listeStatut,$listeType);


        require_once '../vues/PageTypeAjoute.php';
        $vue2= new PageTypeAjoute("");
        $vue2->afficher("",$listeStatut);
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// appuyer sur le bouton submit de la vue vue PageTypeAjoute.php
//////////////////////////////////////////////////////
if ($numAction==131) {
 require_once '../formulaires/FormTypeAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionTypeListe.php';

 require_once '../vues/PageTypeNavigation.php';

         $action =  new actionTypeListe;
        $listeType = $action->execute();

   $vue= new PageTypeNavigation();
    $vue->afficher($listeStatut,$listeType);

try {
        $type = new FormTypeAjoute($_POST);

        require_once '../actions/actionTypeAjoute.php';
        $action =  new actionTypeAjoute();
        $action->execute($type);


        $action2 =  new actionTypeListe;
        $listeType = $action2->execute();

        $vue= new PageTypeNavigation();
        $vue->afficher($listeStatut,$listeType);



} catch (Exception $e) {
        require_once '../vues/PageTypeAjoute.php';
        $vue= new PageTypeAjoute($_POST);

        $vue->afficher($e->getMessage(),$listeStatut);


}


}

////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Modifier un type
//////////////////////////////////////////////////////
if ($numAction==132) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageTypeNavigation.php';
 require_once '../actions/actionTypeListe.php';

    $action =  new actionTypeListe;
    $listeType = $action->execute();

   $vue= new PageTypeNavigation();
    $vue->afficher($listeStatut,$listeType);

   if (isset ( $_GET ['st_id'] )&& isset($_GET['ty_id'])) {
        // premier appel, on charge depuis la base de données
        $st_id=$_GET['st_id'];
        $ty_id=$_GET['ty_id'];

        require_once '../actions/actionTypeGet.php';
        // rend un tableau associatif
         $action1 =  new actionTypeGet;
        $tabtype =  $action1->execute($st_id,$ty_id);


        require_once '../vues/PageTypeModifie.php';
        $vue2= new PageTypeModifie($tabtype);
        $vue2->afficher("",$listeStatut);


        }
    else {
        // On vient de changer le type dans le formulaire, en mode refresh les infos sont dans le formulaire et non dans la base de données

        require_once '../vues/PageTypeModifie.php';
        $vue2= new PageTypeModifie($_POST);
        $vue2->afficher("",$listeStatut);

        }
}

/////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// appuyer sur le bouton submit de la vue vueTypeModifie.php
/////////////////////////////////////////////////////
if ($numAction==133) {
 require_once '../formulaires/FormTypeModifie.php';
 require_once '../web/MenuHaut.php';

 require_once '../vues/PageTypeNavigation.php';

require_once '../actions/actionTypeListe.php';

    $action =  new actionTypeListe;
    $liste = $action->execute();

   $vue= new PageTypeNavigation();
    $vue->afficher($listeStatut,$liste);

try {

        $equipe = new FormTypeModifie($_POST);


//echo "modification de ".$personne->getNom();
        require_once '../actions/actionTypeModifie.php';
        $action =  new actionTypeModifie();
        $action->execute($equipe);

       $action2 =  new actionTypeListe;
        $liste = $action2->execute();

        $vue= new PageTypeNavigation();
        $vue->afficher($listeStatut,$liste);



} catch (Exception $e) {
        require_once '../vues/PageTypeModifie.php';
        $vue2= new PageTypeModifie($_POST);
        $vue2->afficher($e->getMessage(),$listeStatut);



}
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Navigation Type
///////////////////////////////////////////////////////////

if  ($numAction==134){

 require_once '../web/MenuHaut.php';
 require_once '../vues/PageTypeNavigation.php';
require_once '../actions/actionTypeListe.php';

   $action =  new actionTypeListe;
    $liste = $action->execute();

    $vue= new PageTypeNavigation($_POST);
    $vue->afficher($listeStatut,$liste);

}




////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Ajouter un tutelle
//../vues/PageTutelleAjoute.php
// ou on vient de changer le type dans la vue VuePersonneAjoute.php
//////////////////////////////////////////////////////
if ($numAction==140) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageTutelleNavigation.php';
 require_once '../actions/actionTutelleListe.php';


         $action =  new actionTutelleListe;
        $listeTutelle = $action->execute();

   $vue1= new PageTutelleNavigation();
    $vue1->afficher($listeTutelle);


        require_once '../vues/PageTutelleAjoute.php';
        $vue2= new PageTutelleAjoute("");
        $vue2->afficher("");
}


////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// Ajouter un financeur projets recherche 
//////////////////////////////////////////////////////
if ($numAction==145) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePRFinanceurNavigation.php';
 require_once '../actions/actionPRFinanceurListe.php';


         $action =  new actionPRFinanceurListe;
        $liste = $action->execute();

   $vue1= new PagePRFinanceurNavigation();
    $vue1->afficher($liste);


        require_once '../vues/PagePRFinanceurAjoute.php';
        $vue2= new PagePRFinanceurAjoute("");
        $vue2->afficher("");
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// appuyer sur le bouton submit de la vue vue PagePRFinanceurAjoute.php
//////////////////////////////////////////////////////
if ($numAction==146) {
 require_once '../formulaires/FormPRFinanceurAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionPRFinanceurListe.php';

 require_once '../vues/PagePRFinanceurNavigation.php';


try {
        $financeur = new FormPRFinanceurAjoute($_POST);

        require_once '../actions/actionPRFinanceurAjoute.php';
        $action =  new actionPRFinanceurAjoute();
        $action->execute($financeur);


        $action2 =  new actionPRFinanceurListe;
        $liste = $action2->execute();

        $vue= new PagePRFinanceurNavigation();
        $vue->afficher($liste);

} catch (Exception $e) {
        require_once '../vues/PagePRFinanceurAjoute.php';
        $vue= new PagePRFinanceurAjoute($_POST);
        $vue->afficher($e->getMessage());

}

}


////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// Modifier un financeur 
//////////////////////////////////////////////////////
if ($numAction==147) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePRFinanceurNavigation.php';
 require_once '../actions/actionPRFinanceurListe.php';
    $action =  new actionPRFinanceurListe;
    $liste = $action->execute();
   $vue= new PagePRFinanceurNavigation();
    $vue->afficher($liste);
   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionPRFinanceurGet.php';
        // rend un tableau associatif
         $action1 =  new actionPRFinanceurGet;
        $tab =  $action1->execute($id);

        require_once '../vues/PagePRFinanceurModifie.php';
        $vue2= new PagePRFinanceurModifie($tab);
        $vue2->afficher("");
        }
    else {
        require_once '../vues/PagePRFinanceurModifie.php';
        $vue2= new PagePRFinanceurModifie($_POST);
        $vue2->afficher("");
        }
}

/////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// appuyer sur le bouton submit de la vue vuePRFinanceurModifie.php
/////////////////////////////////////////////////////
if ($numAction==148) {
 require_once '../formulaires/FormPRFinanceurModifie.php';
 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePRFinanceurNavigation.php';
require_once '../actions/actionPRFinanceurListe.php';

try {
        // cas des stagiaires
        $financeur = new FormPRFinanceurModifie($_POST);
        require_once '../actions/actionPRFinanceurModifie.php';
        $action =  new actionPRFinanceurModifie();
        $action->execute($financeur);

       $action2 =  new actionPRFinanceurListe;
        $liste = $action2->execute();

        $vue= new PagePRFinanceurNavigation();
        $vue->afficher($liste);
} catch (Exception $e) {
        require_once '../vues/PagePRFinanceurModifie.php';
        $vue2= new PageFinanceurModifie($_POST);
        $vue2->afficher($e->getMessage());



}
}


/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// Navigation FINANCEUR 
///////////////////////////////////////////////////////////

if  ($numAction==149){

 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePRFinanceurNavigation.php';
require_once '../actions/actionPRFinanceurListe.php';

   $action =  new actionPRFinanceurListe;
    $liste = $action->execute();

    $vue= new PagePRFinanceurNavigation($_POST);
    $vue->afficher($liste);

}



////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// Ajouter un type projets recherche
//////////////////////////////////////////////////////
if ($numAction==160) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePRTypeNavigation.php';
 require_once '../actions/actionPRTypeListe.php';


         $action =  new actionPRTypeListe;
        $liste = $action->execute();

   $vue1= new PagePRTypeNavigation();
    $vue1->afficher($liste);


        require_once '../vues/PagePRTypeAjoute.php';
        $vue2= new PagePRTypeAjoute("");
        $vue2->afficher("");
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// appuyer sur le bouton submit de la vue vue PagePRFinanceurAjoute.php
//////////////////////////////////////////////////////
if ($numAction==161) {
 require_once '../formulaires/FormPRTypeAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionPRTypeListe.php';

 require_once '../vues/PagePRTypeNavigation.php';


try {
        $type = new FormPRTypeAjoute($_POST);

        require_once '../actions/actionPRTypeAjoute.php';
        $action =  new actionPRTypeAjoute();
        $action->execute($type);


        $action2 =  new actionPRTypeListe;
        $liste = $action2->execute();

        $vue= new PagePRTypeNavigation();
        $vue->afficher($liste);

} catch (Exception $e) {
        require_once '../vues/PagePRTypeAjoute.php';
        $vue= new PagePRTypeAjoute($_POST);
        $vue->afficher($e->getMessage());

}

}

////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// Modifier un financeur
//////////////////////////////////////////////////////
if ($numAction==162) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePRTypeNavigation.php';
 require_once '../actions/actionPRTypeListe.php';
    $action =  new actionPRTypeListe;
    $liste = $action->execute();
   $vue= new PagePRTypeNavigation();
    $vue->afficher($liste);
   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionPRTypeGet.php';
        // rend un tableau associatif
         $action1 =  new actionPRTypeGet;
        $tab =  $action1->execute($id);

        require_once '../vues/PagePRTypeModifie.php';
        $vue2= new PagePRTypeModifie($tab);
        $vue2->afficher("");
        }
    else {
        require_once '../vues/PagePRTypeModifie.php';
        $vue2= new PagePRTypeModifie($_POST);
        $vue2->afficher("");
        }
}


/////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// appuyer sur le bouton submit de la vue vuePRFinanceurModifie.php
/////////////////////////////////////////////////////
if ($numAction==163) {
 require_once '../formulaires/FormPRTypeModifie.php';
 require_once '../web/MenuHaut.php';
 require_once '../vues/PagePRTypeNavigation.php';
require_once '../actions/actionPRTypeListe.php';

try {
        // cas des stagiaires
        $type = new FormPRTypeModifie($_POST);
        require_once '../actions/actionPRTypeModifie.php';
        $action =  new actionPRTypeModifie();
        $action->execute($type);

       $action2 =  new actionPRTypeListe;
        $liste = $action2->execute();

        $vue= new PagePRTypeNavigation();
        $vue->afficher($liste);
} catch (Exception $e) {
        require_once '../vues/PagePRTypeModifie.php';
        $vue2= new PageTypeModifie($_POST);
        $vue2->afficher($e->getMessage());



}
}





/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// appuyer sur le bouton submit de la vue vue PageTutelleAjoute.php
//////////////////////////////////////////////////////
if ($numAction==141) {
 require_once '../formulaires/FormTutelleAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionTutelleListe.php';

 require_once '../vues/PageTutelleNavigation.php';

         $action =  new actionTutelleListe;
        $listeTutelle = $action->execute();

   $vue= new PageTutelleNavigation();
    $vue->afficher($listeTutelle);

try {
        $tutelle = new FormTutelleAjoute($_POST);

        require_once '../actions/actionTutelleAjoute.php';
        $action =  new actionTutelleAjoute();
        $action->execute($tutelle);


        $action2 =  new actionTutelleListe;
        $listeTutelle = $action2->execute();

        $vue= new PageTutelleNavigation();
        $vue->afficher($listeTutelle);



} catch (Exception $e) {
        require_once '../vues/PageTutelleAjoute.php';
        $vue= new PageTutelleAjoute($_POST);

        $vue->afficher($e->getMessage());


}


}

////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Modifier un tutelle
//////////////////////////////////////////////////////
if ($numAction==142) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageTutelleNavigation.php';
 require_once '../actions/actionTutelleListe.php';

    $action =  new actionTutelleListe;
    $listeTutelle = $action->execute();

   $vue= new PageTutelleNavigation();
    $vue->afficher($listeTutelle);

   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionTutelleGet.php';
        // rend un tableau associatif
         $action1 =  new actionTutelleGet;
        $tabtutelle =  $action1->execute($id);


        require_once '../vues/PageTutelleModifie.php';
        $vue2= new PageTutelleModifie($tabtutelle);
        $vue2->afficher("");


        }
    else {
        // On vient de changer le type dans le formulaire, en mode refresh les infos sont dans le formulaire et non dans la base de données

        require_once '../vues/PageTutelleModifie.php';
        $vue2= new PageTutelleModifie($_POST);
        $vue2->afficher("");

        }
}

/////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// appuyer sur le bouton submit de la vue vueTutelleModifie.php
/////////////////////////////////////////////////////
if ($numAction==143) {
 require_once '../formulaires/FormTutelleModifie.php';
 require_once '../web/MenuHaut.php';

 require_once '../vues/PageTutelleNavigation.php';

require_once '../actions/actionTutelleListe.php';

    $action =  new actionTutelleListe;
    $liste = $action->execute();

   $vue= new PageTutelleNavigation();
    $vue->afficher($liste);

try {

        // cas des stagiaires
        $equipe = new FormTutelleModifie($_POST);


//echo "modification de ".$personne->getNom();
        require_once '../actions/actionTutelleModifie.php';
        $action =  new actionTutelleModifie();
        $action->execute($equipe);

       $action2 =  new actionTutelleListe;
        $liste = $action2->execute();

        $vue= new PageTutelleNavigation();
        $vue->afficher($liste);



} catch (Exception $e) {
        require_once '../vues/PageTutelleModifie.php';
        $vue2= new PageTutelleModifie($_POST);
        $vue2->afficher($e->getMessage());



}
}
/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Navigation Tutelle
///////////////////////////////////////////////////////////

if  ($numAction==144){

 require_once '../web/MenuHaut.php';
 require_once '../vues/PageTutelleNavigation.php';
require_once '../actions/actionTutelleListe.php';

   $action =  new actionTutelleListe;
    $liste = $action->execute();

    $vue= new PageTutelleNavigation($_POST);
    $vue->afficher($liste);

}


////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Ajouter un statut
//../vues/PageStatutAjoute.php
// ou on vient de changer le type dans la vue VuePersonneAjoute.php
//////////////////////////////////////////////////////
if ($numAction==150) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageStatutNavigation.php';
 require_once '../actions/actionStatutListe.php';


         $action =  new actionStatutListe;
        $listeStatut = $action->execute();

   $vue1= new PageStatutNavigation();
    $vue1->afficher($listeStatut);


        require_once '../vues/PageStatutAjoute.php';
        $vue2= new PageStatutAjoute("");
        $vue2->afficher("");
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// appuyer sur le bouton submit de la vue vue PageStatutAjoute.php
//////////////////////////////////////////////////////
if ($numAction==151) {
 require_once '../formulaires/FormStatutAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionStatutListe.php';

 require_once '../vues/PageStatutNavigation.php';

         $action =  new actionStatutListe;
        $listeStatut = $action->execute();

   $vue= new PageStatutNavigation();
    $vue->afficher($listeStatut);

try {
        $statut = new FormStatutAjoute($_POST);

        require_once '../actions/actionStatutAjoute.php';
        $action =  new actionStatutAjoute();
        $action->execute($statut);

        $action2 =  new actionStatutListe;
        $listeStatut = $action2->execute();

        $vue= new PageStatutNavigation();
        $vue->afficher($listeStatut);

} catch (Exception $e) {
        require_once '../vues/PageStatutAjoute.php';
        $vue= new PageStatutAjoute($_POST);

        $vue->afficher($e->getMessage());
}
}

////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Modifier un statut
//////////////////////////////////////////////////////
if ($numAction==152) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageStatutNavigation.php';
 require_once '../actions/actionStatutListe.php';

    $action =  new actionStatutListe;
    $listeStatut = $action->execute();

   $vue= new PageStatutNavigation();
    $vue->afficher($listeStatut);

   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionStatutGet.php';
        // rend un tableau associatif
         $action1 =  new actionStatutGet;
        $tabstatut =  $action1->execute($id);


        require_once '../vues/PageStatutModifie.php';
        $vue2= new PageStatutModifie($tabstatut);
        $vue2->afficher("");
        }
    else {
        // On vient de changer le type dans le formulaire, en mode refresh les infos sont dans le formulaire et non dans la base de données
        require_once '../vues/PageStatutModifie.php';
        $vue2= new PageStatutModifie($_POST);
        $vue2->afficher("");

        }
}

/////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// appuyer sur le bouton submit de la vue vueStatutModifie.php
/////////////////////////////////////////////////////
if ($numAction==153) {
 require_once '../formulaires/FormStatutModifie.php';
 require_once '../web/MenuHaut.php';

 require_once '../vues/PageStatutNavigation.php';

require_once '../actions/actionStatutListe.php';

    $action =  new actionStatutListe;
    $liste = $action->execute();

   $vue= new PageStatutNavigation();
    $vue->afficher($liste);

try {

        // cas des stagiaires
        $equipe = new FormStatutModifie($_POST);


//echo "modification de ".$personne->getNom();
        require_once '../actions/actionStatutModifie.php';
        $action =  new actionStatutModifie();
        $action->execute($equipe);

       $action2 =  new actionStatutListe;
        $liste = $action2->execute();

        $vue= new PageStatutNavigation();
        $vue->afficher($liste);



} catch (Exception $e) {
        require_once '../vues/PageStatutModifie.php';
        $vue2= new PageStatutModifie($_POST);
        $vue2->afficher($e->getMessage());



}
}
/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// Navigation Statut
///////////////////////////////////////////////////////////

if  ($numAction==154){

 require_once '../web/MenuHaut.php';
 require_once '../vues/PageStatutNavigation.php';
require_once '../actions/actionStatutListe.php';

   $action =  new actionStatutListe;
    $liste = $action->execute();

    $vue= new PageStatutNavigation($_POST);
    $vue->afficher($liste);

}

////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// Ajouter un college électoral 
//////////////////////////////////////////////////////
if ($numAction==200) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageCollegeNavigation.php';
 require_once '../actions/actionCollegeListe.php';


         $action =  new actionCollegeListe;
        $liste = $action->execute();

   $vue1= new PageCollegeNavigation();
    $vue1->afficher($liste);


        require_once '../vues/PageCollege.php';
        $vue2= new PageCollege(1,"");
        $vue2->afficher("");
}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// appuyer sur le bouton submit de la vue vue PageCollege.php
//////////////////////////////////////////////////////
if ($numAction==201) {
 require_once '../formulaires/FormCollege.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionCollegeListe.php';

 require_once '../vues/PageCollegeNavigation.php';


try {
        $college = new FormCollege($_POST);

if ($college->getCreation()==1) {

        require_once '../actions/actionCollegeAjoute.php';
        $action =  new actionCollegeAjoute();
        $action->execute($college);

}
else {

	// modification
        require_once '../actions/actionCollegeModifie.php';
        $action =  new actionCollegeModifie();
        $action->execute($college);

}
        $action =  new actionCollegeListe;
        $liste = $action->execute();

   	$vue= new PageCollegeNavigation();
    	$vue->afficher($liste);



} catch (Exception $e) {
        require_once '../vues/PageCollege.php';
        $vue= new PageCollege($_POST);

        $vue->afficher($e->getMessage());
}
}

////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// Modifier un statut
//////////////////////////////////////////////////////
if ($numAction==202) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageCollegeNavigation.php';
 require_once '../actions/actionCollegeListe.php';

    $action =  new actionCollegeListe;
    $liste = $action->execute();

   $vue= new PageCollegeNavigation();
    $vue->afficher($liste);

   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionCollegeGet.php';
        // rend un tableau associatif
         $action1 =  new actionCollegeGet;
        $tab =  $action1->execute($id);


        require_once '../vues/PageCollege.php';
        $vue2= new PageCollege(0,$tab);
        $vue2->afficher("");
        }
    else {
        // On vient de changer le type dans le formulaire, en mode refresh les infos sont dans le formulaire et non dans la base de données
        require_once '../vues/PageCollege.php';
        $vue2= new PageCollege(0,$_POST);
        $vue2->afficher("");

        }
}


/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// iGenerer le lien loginspip forme sconrad table t_personne
///////////////////////////////////////////////////////////

if  ($numAction==500){
        //require_once '../actions/actionLienSpipPersonne.php';
        // $action =  new actionLienSpipPersonne;
        require_once '../actions/actionDroitsadmin.php';
         $action =  new actionDroitsadmin;

          $action->execute($_POST['droit'],$_POST['checked']);

 require_once '../web/MenuHaut.php';


}


/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// 
// iGenerer le lien loginspip forme sconrad table t_personne
///////////////////////////////////////////////////////////
if (substr( $numAction, 0, 3 ) == "511") {
        require_once '../actions/actionLienSpipPersonne2.php';
         $action =  new actionLienSpipPersonne2;

          $action->execute();

 require_once '../web/MenuHaut.php';
}





/////////////////////////////////////////////////////////
//numAction=501 Gerer les destinataires des mails d'avertissement
//  if (isset ($_POST['arch'])) Archiver automatiquement à la date de départ ?
//  if (isset ($_POST['aver'])) Envoyer un message d'avertissement 15 jours et 8 jours avant l'archivage ?
//  if (isset ($_POST['ano'])) Anonymiser les utilisateurs deux mois après le départ ?
//  if (isset ($_POST['neo'])) Faire des emails d'avertissement aux responsables si la formation sécurité n'est pas faite?
//  if(strlen($_FILES['mailPj']['name'])!=0) { ajout  piece jointe au message de bienvenue du stagiaire
//  if(strlen($_FILES['mailPjChimie']['name'])!=0) { ajout  piece jointe au message de bienvenue du stagiaire
//  if(strlen($_FILES['mailPjBlanche']['name'])!=0) { ajout  piece jointe au message de bienvenue du stagiaire


//numAction=502  les destinataires du mail d'avertissement lors d'ajout de déclaration stagiaires. (hsm-gestion)
//numAction=503  les destinataires du mail d'avertissement Responsables Securités si la formation sécurité n'est pas faite
//numAction=504  actionMetaPjSupprime Suppression piece jointe au message de bienvenue du stagiaire
//numAction=505  actionMessageBienvenueModifie Message de bienvenue au stagiaire modifie
//numAction=506  les destinataires du mail d'avertissement lors d'ajout de déclaration stagiaire utilisant le LABO CHIMIE 
//numAction=507 les destinataires du mail d'avertissementlors d'ajout de déclaration stagiaire utilisant la  Salle Blanche (hsm-aete-iso)
//numAction=508  actionMetaPjSupprime Chimie Suppression piece jointe au message de bienvenue de utilisateur salle de chimie
//numAction=509 actionMessageChimieModifie Message de bienvenue aux utilisateurs de la salle de chimie modifie

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// PREMIER APPELles destinataires du mail d'avertissement
///////////////////////////////////////////////////////////

if (substr( $numAction, 0, 3 ) == '507') {

 require_once '../web/MenuHaut.php';



	 // PREMIER APPEL, ON CHERCHE
        // rechercher la valeur dans /var/spool/cron/apache
        $searcString = 'actionArchivePersonne.php';
        $output = shell_exec('crontab -l');
        if (strpos($output, $searcString) !== false) $arch=1;
        else $arch=0;

	 // PREMIER APPEL, ON CHERCHE
        // rechercher la valeur dans /var/spool/cron/apache
        $searcString = 'actionArchiveMessage.php';
        $output = shell_exec('crontab -l');
        if (strpos($output, $searcString) !== false) $aver=1;
        else $aver=0;

 // PREMIER APPEL, ON CHERCHE
        // rechercher la valeur dans /var/spool/cron/apache

        $searcString = 'actionMailFormationSecurite.php';
        $output = shell_exec('crontab -l');
        if (strpos($output, $searcString) !== false) $neo=1;
        else $neo=0;



//        require_once '../actions/actionMessageDestinataire.php';
//         $action =  new actionMessageDestinataire;
//         $dest =  $action->execute();

       require_once '../actions/actionRespSec.php';
         $actionRespSec =  new actionRespSec;
         $respSec =  $actionRespSec->execute();

       require_once '../actions/actionProjetSoumis.php';
         $actionProjetSoumis =  new actionProjetSoumis;
         $projetSoumis =  $actionProjetSoumis->execute();

       require_once '../actions/actionProjetAccepte.php';
         $actionProjetAccepte =  new actionProjetAccepte;
         $projetAccepte =  $actionProjetAccepte->execute();


//       require_once '../actions/actionListePJ.php';
//         $actionListeC =  new actionListePJ;
// La ressource "bienvenue à l'utilisateur à comme id 1 et n'est pas supprimable
  //       $listeC =  $actionListeC->execute(1);

//       require_once '../actions/actionMessageBienvenueGet.php';
//         $actionMB =  new actionMessageBienvenueGet;
  //       $messageBienvenue =  $actionMB->execute("messageBienvenue");


        require_once '../actions/actionRessourcesMessageListe.php';
         $action6 =  new actionRessourcesMessageListe;
         $listeR =  $action6->execute();

        require_once '../actions/actionRessourcesMessageListePJ.php';
         $action7 =  new actionRessourcesMessageListePJ;
         $listeRPJ =  $action7->execute();


 require_once '../vues/PageMessageDestinataire.php';

   $vue= new PageMessageDestinataire(0,$arch,$aver,$neo,$respSec,$projetSoumis,$projetAccepte,$listeR,$listeRPJ);
    $vue->afficher("");


}



/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// les destinataires du mail d'avertissement
// traitement du formulaire
///////////////////////////////////////////////////////////

if (substr( $numAction, 0, 3 ) == '501') {
	  $param = explode(";", $numAction);

	if ($param[1]== 'ADH') $ajoutRessource=1;
	else $ajoutRessource=0;




	if ($param[1]== 'RESPSECU') {
        require_once '../formulaires/FormRespSec.php';
        $meta = new FormRespSec($_POST);


        require_once '../actions/actionRespSecModifie.php';
        $action =  new actionRespSecModifie();
        $action->execute($meta);
	}

       require_once '../actions/actionRespSec.php';
         $actionRespSec =  new actionRespSec;
         $respSec =  $actionRespSec->execute();

//serge
      if ($param[1]== 'PROJETSOUMIS') {
        require_once '../formulaires/FormProjetSoumis.php';
        $meta = new FormProjetSoumis($_POST);


        require_once '../actions/actionProjetSoumisModifie.php';
        $action =  new actionProjetSoumisModifie();
        $action->execute($meta);
        }

       require_once '../actions/actionProjetSoumis.php';
         $actionProjetSoumis =  new actionProjetSoumis;
         $projetSoumis =  $actionProjetSoumis->execute();

      if ($param[1]== 'PROJETACCEPTE') {
        require_once '../formulaires/FormProjetAccepte.php';
        $meta = new FormProjetAccepte($_POST);


        require_once '../actions/actionProjetAccepteModifie.php';
        $action =  new actionProjetAccepteModifie();
        $action->execute($meta);
        }

       require_once '../actions/actionProjetAccepte.php';
         $actionProjetAccepte =  new actionProjetAccepte;
         $projetAccepte =  $actionProjetAccepte->execute();

////////////////////////////////////////////////////////////////////////////
// LES ARCHIVAGE

if ($param[1]=="ARCH") {
// modifier l'archivage. Si activée on désactive, si désactivée on active

        if ($_POST['arch']==0) {
		// On ajoute archivage dans la crontab
		$output = shell_exec('crontab -l');
		//file_put_contents('/tmp/crontab.txt', $output.'0 0 * * * php '. dirname(getcwd()) . '/actionscron/actionArchivePersonne.php'.PHP_EOL);
		file_put_contents('/tmp/crontab.txt', $output.'0 0 * * * cd '. dirname(getcwd()) . '/actionscron/ && php ./actionArchivePersonne.php'.PHP_EOL);
		echo exec('crontab /tmp/crontab.txt');
		$arch=1;
		}
	else {
		// on enleve archivage dans crontab
		echo exec('crontab  -l | grep -v "actionArchivePersonne"  | crontab -');
		$arch=0;
	}

}
else $arch= $_POST['arch'];






////////////////////////////////////////////////////////////////////////////
// LES AVERTISSEMENTS
if ($param[1]=="AVER") {
// modifier l'archivage. Si activée on désactive, si désactivée on active

        if ($_POST['aver']==0) {
		$output = shell_exec('crontab -l');
                file_put_contents('/tmp/crontab.txt', $output.'0 0 * * * cd '. dirname(getcwd()) . '/actionscron/ && php ./actionArchiveMessage.php'.PHP_EOL);
                echo exec('crontab /tmp/crontab.txt');
                $aver=1;
                }
        else {
		echo exec('crontab  -l | grep -v "actionArchiveMessage"  | crontab -');
                $aver=0;
        }

}
else $aver= $_POST['aver'];





////////////////////////////////////////////////////////////////////////////
// mails formation securite

if ($param[1]=="NEO") {
// modifier l'archivage. Si activée on désactive, si désactivée on active

        if ($_POST['neo']==0) {
                $output = shell_exec('crontab -l');
                file_put_contents('/tmp/crontab.txt', $output.'0 0 * * * cd '. dirname(getcwd()) . '/actionscron/ && php ./actionMailFormationSecurite.php'.PHP_EOL);
                echo exec('crontab /tmp/crontab.txt');
                $neo=1;
                }
        else {
		echo exec('crontab  -l | grep -v "actionMailFormationSecurite"  | crontab -');
                $neo=0;
        }

}
else $neo= $_POST['neo'];




////////////////////////////////////////////////////////////////////////////
// AFFICHAGE
 require_once '../web/MenuHaut.php';


////////////////////////////////////////////////////////////////////////////
// Ajout pièce jointe
//if ($param[1]=="MSGPJ") {
//        if(strlen($_FILES['mailPj']['name'])!=0) {
//                require_once '../actions/actionUploadPJ.php';
//                $action5 =  new actionUploadPJ;
//                $action5->execute("mailPj","mailPj");
//      }


//}


//if ($param[1]=="SUPPJ") {
//	require_once '../actions/actionMetaPjSupprime.php';
//        $action1 =  new actionMetaPjSupprime;
//        $action1->execute('mailPj',$param[2]);

//}


//       require_once '../actions/actionListePJ.php';
//         $actionListeC =  new actionListePJ;
//         $listeC =  $actionListeC->execute(1);
//         $listePjChimie =  $actionListeC->execute("mailPjChimie");
//         $listePjBlanche =  $actionListeC->execute("mailPjBlanche");




//if ($param[1]=="MSGBIENVENUE") {
//   require_once '../actions/actionMessageBienvenueModifie.php';
//        $action =  new actionMessageBienvenueModifie();
//        $action->execute($_POST['messageBienvenue'],"messageBienvenue");
//}

//       require_once '../actions/actionMessageBienvenueGet.php';
//         $actionMB =  new actionMessageBienvenueGet;
//         $messageBienvenue =  $actionMB->execute("messageBienvenue");



// LES ressources
if ($param[1]=="ADDRESS") {
	 require_once '../actions/actionRessourceAjoute.php';
        $action =  new actionRessourceAjoute();
        $action->execute($_POST['nomn']);
}
if ($param[1]=="SUPRESS") {
	 require_once '../actions/actionRessourceSupprime.php';
        $action =  new actionRessourceSupprime();
        $action->execute($param[2]);

	// également les pièces jointes associées
	// BUG ne fonctionne pas
//	require_once '../actions/actionMetaPjSupprime.php';
//        $action1 =  new actionMetaPjSupprime;
//        $action1->execute($param[2],'*');
}

if ($param[1]=="RESSMOD") {
	 require_once '../actions/actionMessageDestinataireRessourceModifie.php';
        $action =  new actionMessageDestinataireRessourceModifie();
        $action->execute($param[2],$_POST['mail'.$param[2]],$_POST['message'.$param[2]]);
}

if ($param[1]=="SUPRESPJ") {
//value="501;SUPPJ;'.$row->nom.';'.$value.'"
	require_once '../actions/actionMetaPjSupprime.php';
        $action1 =  new actionMetaPjSupprime;
        $action1->execute($param[2],$param[3]);
}

if ($param[1]=="RESPJ") {
	        require_once '../actions/actionUploadPJ.php';
                $action5 =  new actionUploadPJ;
                $action5->execute($param[2],"pj".$param[2]);

}


        require_once '../actions/actionRessourcesMessageListe.php';
         $action6 =  new actionRessourcesMessageListe;
         $listeR =  $action6->execute();

        require_once '../actions/actionRessourcesMessageListePJ.php';
         $action7 =  new actionRessourcesMessageListePJ;
         $listeRPJ =  $action7->execute();


 require_once '../vues/PageMessageDestinataire.php';

   $vue= new PageMessageDestinataire($ajoutRessource,$arch,$aver,$neo,$respSec,$projetSoumis,$projetAccepte,$listeR,$listeRPJ);
    $vue->afficher("");


}



/////////////////////////////////////////////////////////
/// USE CASE ADMIN GERER PARAMETRES
// Ajouter administrateur bd
///////////////////////////////////////////////////////////

if  ($numAction==512){
try {


 require_once '../web/MenuHaut.php';




        require_once '../actions/actionDroitsadminAjoute.php';
         $action =  new actionDroitsadminAjoute;
          $action->execute($_POST['adminbd']);


        //require_once '../actions/actionLienSpipPersonne.php';
         //$action =  new actionLienSpipPersonne;
        require_once '../actions/actionDroitsadmin.php';
         $action =  new actionDroitsadmin;
          $action->execute('','');


} catch (Exception $e) {

}
}


/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// Enregistrer supann affectation
///////////////////////////////////////////////////////////

if  ($numAction==513){
try {


 require_once '../web/MenuHaut.php';




        require_once '../actions/actionSupannAffectationEnregistre.php';
         $action =  new actionSupannAffectationEnregistre;
          $action->execute($_POST['supannAffectation']);

        require_once '../actions/actionDroitsadmin.php';
         $action =  new actionDroitsadmin;

          $action->execute('','');


} catch (Exception $e) {

}
}


/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// les destinataires du mail d'avertissement Labo Chimie
///////////////////////////////////////////////////////////

//if (substr( $numAction, 0, 3 ) == '506') {

//try {

//$ressource= substr( $numAction, 3 );



//        require_once '../actions/actionMessageDestinataireRessourceModifie.php';
//        $action =  new actionMessageDestinataireRessourceModifie();
//        $action->execute($ressource,$_POST['mail'.$ressource],$_POST['message'.$ressource]);


// require_once '../web/MenuHaut.php';



//} catch (Exception $e) {
// require_once '../web/MenuHaut.php';
//        require_once '../vues/PageMessageDestinataire.php';
//        $vue2= new PageMessageDestinataire($_POST['messageToLaboChimie']);
//        $vue2->afficher($e->getMessage());

//}
//}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES 
// les destinataires du mail d'avertissement Salle Blanche
///////////////////////////////////////////////////////////

//if  ($numAction==507){
//try {
// 	require_once '../formulaires/FormMessageDestinataireSalleBlanche.php';
//        $meta = new FormMessageDestinataireSalleBlanche($_POST);


//        require_once '../actions/actionMessageDestinataireSalleBlancheModifie.php';
//        $action =  new actionMessageDestinataireSalleBlancheModifie();
//        $action->execute($meta);


// require_once '../web/MenuHaut.php';



//} catch (Exception $e) {
// require_once '../web/MenuHaut.php';
//        require_once '../vues/PageMessageDestinataire.php';
//        $vue2= new PageMessageDestinataire($_POST['messageToSalleBlanche']);
//        $vue2->afficher($e->getMessage());

//}
//}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// les destinataires du mail d'avertissement
///////////////////////////////////////////////////////////
//if (substr( $numAction, 0, 3 ) == "504") {
//echo '<button type="submit" name="numAction" value="504;'.$row->nom.';'.$value.'" id="submit_click"> Supprimer </button>';
//	$valeur=  split(";", $numAction);
//	require_once '../actions/actionMetaPjSupprime.php';
  //      $action1 =  new actionMetaPjSupprime;
//        $action1->execute($valeur[1],$valeur[2]);

 //	require_once '../web/MenuHaut.php';
//	echo "<br><br><br>Piece jointe $valeur[2] supprimée";
//}


/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// les destinataires du mail d'avertissement
///////////////////////////////////////////////////////////
//if (substr( $numAction, 0, 3 ) == "508") {
//echo ' <br> Charger une pièce jointe : <input type="file" name="pj'.$row->nom.'"  >';
//echo '<br><button type="submit" name="numAction" value="508_'.$row->nom.'" id="submit_click"> Charger la pièce jointe </button>';

//        $valeur=  split(";", $numAction);


//        if(strlen($_FILES['pj'.$valeur[1]]['name'])!=0) {
//                require_once '../actions/actionUploadCharte.php';
//                $action5 =  new actionUploadCharte;
//                $action5->execute($valeur[1],"pj".$valeur[1]);
  //     }




//        require_once '../web/MenuHaut.php';
//        echo "<br><br><br>Piece jointe ajoutée";
//}




/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// les destinataires du mail d'avertissement CHIMIE PJ
///////////////////////////////////////////////////////////
//if (substr( $numAction, 0, 3 ) == "508") {
//	$valeur= substr($numAction,3,strlen($numAction));
//	require_once '../actions/actionMetaPjSupprime.php';
//        $action1 =  new actionMetaPjSupprime;
//        $action1->execute($valeur);
//
// 	require_once '../web/MenuHaut.php';
//	echo "<br><br><br>Piece jointe $valeur supprimée";
//}

/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// les destinataires du mail d'avertissement CHIMIE PJ
///////////////////////////////////////////////////////////
//if (substr( $numAction, 0, 3 ) == "511") {
//	$valeur= substr($numAction,3,strlen($numAction));
//	require_once '../actions/actionMetaPjSupprime.php';
 //       $action1 =  new actionMetaPjSupprime;
//        $action1->execute($valeur);

// 	require_once '../web/MenuHaut.php';
//	echo "<br><br><br>Piece jointe $valeur supprimée";
//}


/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// Message Bienvenye CHIMIE
///////////////////////////////////////////////////////////
//if  ($numAction==509){
//try {
//        require_once '../actions/actionMessageBienvenueModifie.php';
//        $action =  new actionMessageBienvenueModifie();
//        $action->execute($_POST['messageChimie'],"messageChimie");

// require_once '../web/MenuHaut.php';

//} catch (Exception $e) {
// require_once '../web/MenuHaut.php';
//        require_once '../vues/PageMessageDestinataire.php';
//        $vue2= new PageMessageDestinataire($_POST['messageTo']);
//        $vue2->afficher($e->getMessage());
//}

//}
/////////////////////////////////////////////////////////
// USE CASE ADMIN GERER PARAMETRES
// Message Bienvenue Blanche
///////////////////////////////////////////////////////////
//if  ($numAction==510){
//try {
//        require_once '../actions/actionMessageBienvenueModifie.php';
//        $action =  new actionMessageBienvenueModifie();
//        $action->execute($_POST['messageBlanche'],"messageBlanche");

// require_once '../web/MenuHaut.php';

//} catch (Exception $e) {
// require_once '../web/MenuHaut.php';
//        require_once '../vues/PageMessageDestinataire.php';
//        $vue2= new PageMessageDestinataire($_POST['messageTo']);
//        $vue2->afficher($e->getMessage());
//}

//}
////////////////////////////////////////////////////////
// FIN
/////////////////////////////////////////////////////
echo "<div id='header'>";
echo '	</div>';
?>
</html>
