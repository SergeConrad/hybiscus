
////////////////////////////////////////////////////////
// Ajouter un statut
//../vues/PageStatutAjoute.php
// ou on vient de changer le type dans la vue VuePersonneAjoute.php
//////////////////////////////////////////////////////
if ($numAction==150) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageStatutNavigation.php';
 require_once '../actions/actionStatutListe.php';


         $action =  new actionStatutListe;
        $listeStatut = $action->execute();

   $vue1= new PageStatutNavigation();
    $vue1->afficher($listeStatut);


        require_once '../vues/PageStatutAjoute.php';
        $vue2= new PageStatutAjoute();
        $vue2->afficher("");
}

/////////////////////////////////////////////////////////
// appuyer sur le bouton submit de la vue vue PageStatutAjoute.php
//////////////////////////////////////////////////////
if ($numAction==151) {
 require_once '../formulaires/FormStatutAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionStatutListe.php';

 require_once '../vues/PageStatutNavigation.php';

         $action =  new actionStatutListe;
        $listeStatut = $action->execute();

   $vue= new PageStatutNavigation();
    $vue->afficher($listeStatut);

try {
        $statut = new FormStatutAjoute($_POST);

        require_once '../actions/actionStatutAjoute.php';
        $action =  new actionStatutAjoute();
        $action->execute($statut);


        $action2 =  new actionStatutListe;
        $listeStatut = $action2->execute();

        $vue= new PageStatutNavigation();
        $vue->afficher($listeStatut);



} catch (Exception $e) {
        require_once '../vues/PageStatutAjoute.php';
        $vue= new PageStatutAjoute($_POST);

        $vue->afficher($e->getMessage());


}


}

////////////////////////////////////////////////////////
// Modifier un statut
//////////////////////////////////////////////////////
if ($numAction==152) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageStatutNavigation.php';
 require_once '../actions/actionStatutListe.php';

    $action =  new actionStatutListe;
    $listeStatut = $action->execute();

   $vue= new PageStatutNavigation();
    $vue->afficher($listeStatut);

   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionStatutGet.php';
        // rend un tableau associatif
         $action1 =  new actionStatutGet;
        $tabstatut =  $action1->execute($id);


        require_once '../vues/PageStatutModifie.php';
        $vue2= new PageStatutModifie($tabstatut);
        $vue2->afficher("");


        }
    else {
        // On vient de changer le type dans le formulaire, en mode refresh les infos sont dans le formulaire et non dans la base de données

        require_once '../vues/PageStatutModifie.php';
        $vue2= new PageStatutModifie($_POST);
        $vue2->afficher("");

        }
}

/////////////////////////////////////////////////////
// appuyer sur le bouton submit de la vue vueStatutModifie.php
/////////////////////////////////////////////////////
if ($numAction==153) {
 require_once '../formulaires/FormStatutModifie.php';
 require_once '../web/MenuHaut.php';

 require_once '../vues/PageStatutNavigation.php';

require_once '../actions/actionStatutListe.php';

    $action =  new actionStatutListe;
    $liste = $action->execute();

   $vue= new PageStatutNavigation();
    $vue->afficher($liste);

try {

        // cas des stagiaires
        $equipe = new FormStatutModifie($_POST);


//echo "modification de ".$personne->getNom();
        require_once '../actions/actionStatutModifie.php';
        $action =  new actionStatutModifie();
        $action->execute($equipe);

       $action2 =  new actionStatutListe;
        $liste = $action2->execute();

        $vue= new PageStatutNavigation();
        $vue->afficher($liste);



} catch (Exception $e) {
        require_once '../vues/PageStatutModifie.php';
        $vue2= new PageStatutModifie($_POST);
        $vue2->afficher($e->getMessage());



}
}
/////////////////////////////////////////////////////////
// Navigation Statut
///////////////////////////////////////////////////////////

if  ($numAction==154){

 require_once '../web/MenuHaut.php';
 require_once '../vues/PageStatutNavigation.php';
require_once '../actions/actionStatutListe.php';

   $action =  new actionStatutListe;
    $liste = $action->execute();

    $vue= new PageStatutNavigation($_POST);
    $vue->afficher($liste);

}

