<?php
/*********************************************************************************************************************************\
 *  Hybicus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                                                                                *
 *  Copyright (c) 2018 2019                                                                                                   *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

header ( 'Content-Type: text/html; charset=utf-8' );
require_once("../web/session_start.php");


?>


<!DOCTYPE html>

<?php
 require_once '../web/head.php';

require_once '../control/CachedPDOStatement.php';
// require_once '../entites/class.php';
 ?>

<body>

<?php
// Chargement des tables ...
        require_once '../actions/actionPersonneFiltre.php';
        $actionPersonne =  new actionPersonneFiltre;
        $listePersonne =  new CachedPDOStatement($actionPersonne->execute('T','T','T','T',date('Y-m-d'),"","",0)); // Tout le monde sauf les archivés

        require_once '../actions/actionPersonneTousFiltre.php';
        $actionPersonneTous =  new actionPersonneTousFiltre;
        $listePersonneTous =  new CachedPDOStatement($actionPersonneTous->execute('T','T','T','T')); // Tout le monde avec les archivés


         require_once '../actions/actionEquipeRechercheListe.php';
        $action =  new actionEquipeRechercheListe;
        $listeEquipe = new CachedPDOStatement($action->execute());

 if (isset ( $_GET ['numAction'] )) $numAction=$_GET['numAction'];
 if (isset ( $_POST ['numAction'] )) $numAction=$_POST['numAction'];

   if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

        if (isset($_POST['selectdateDotation']) AND ($_POST['selectdateDotation'] != ""))  $selectdateDotation = $_POST['selectdateDotation'];
        elseif (isset($_GET['selectdateDotation']))  $selectdateDotation = $_GET['selectdateDotation'];
        else $selectdateDotation= 'T';
        if (isset($_POST['selectdateRetour']) AND ($_POST['selectdateRetour'] != ""))  $selectdateRetour = $_POST['selectdateRetour'];
        elseif (isset($_GET['selectdateRetour']))  $selectdateRetour = $_GET['selectdateRetour'];
        else $selectdateRetour= 'T';
        if (isset($_POST['selecteq_id']) AND ($_POST['selecteq_id'] != ""))  $selecteq_id = $_POST['selecteq_id'];
        elseif (isset($_GET['selecteq_id']))  $selecteq_id = $_GET['selecteq_id'];
        else $selecteq_id= 'T';
        if (isset($_POST['selectp_id']) AND ($_POST['selectp_id'] != ""))  $selectp_id = $_POST['selectp_id'];
        elseif (isset($_GET['selectp_id']))  $selectp_id = $_GET['selectp_id'];
        else $selectp_id= 'T';

////////////////////////////////////////////////////////
// USE CASE ADMIN GERER EQUIPE
// Cahier Ajout Projet
///////////////////////////////////////////////////////////
if  ($numAction==115){
 require_once '../web/MenuHaut.php';

if ($menuDestination == "responsableequipe") {
         $action =  new actionCahierListe;
        //$listeCahier =  $action->mesStagiairesEtDoctorants($_SESSION['p_id']);
        $listeCahier = $action->pourLequipe($_SESSION['responsableEquipe']);


}

else {

 require_once '../actions/actionCahierListe.php';

         $action =  new actionCahierListe;
        $listeCahier = $action->execute($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);

}


 require_once '../vues/PageCahierNavigation.php';
   $vue1= new PageCahierNavigation($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);
    $vue1->afficher($listeCahier,$menuDestination,$listeEquipe,$listePersonne);



//serge



require_once '../actions/actionCahierAjouteProjet.php';

   $action =  new actionCahierAjouteProjet;
    $action->execute($_POST);

 $cahierNum=$_POST['cahierNum'];

        require_once '../actions/actionCahierGet.php';
        $action1 =  new actionCahierGet;
        $tabcahier =  $action1->execute($cahierNum);

        // les utilisateurs affectés aux cahiers
        require_once '../actions/actionCahierAppartientGet.php';
        $action3 =  new actionCahierAppartientGet;
        $tabappartient =  $action3->execute($cahierNum);

        // la liste des équipes
        require_once '../actions/actionEquipeListe.php';
        $action2 =  new actionEquipeListe;
        $listeEquipes =  $action2->execute();



if  ((substr($numAction,3,3) == 'ADD') ||($_POST['ajoututil']==1)) $ajoututil=1;
else $ajoututil=0;



        require_once '../vues/PageCahierModifie.php';
        $vue2= new PageCahierModifie(0,$ajoututil,$tabcahier,$tabappartient,'',$menuDestination);

        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
        $listeProjet = $action2->execute('T','T','T','T','','');

        $action3 =  new actionCahierListe;
        $listeCahierlaboProjet = $action3->cahierlaboProjetListe($cahierNum);

        $vue2->afficher("",$listePersonneTous,$listeEquipes,$listeProjet,$listeCahierlaboProjet);





}

///////////////////////////////////////////////////////////
// Cahier supprimer Projet
///////////////////////////////////////////////////////////
if  ($numAction==116){
 require_once '../web/MenuHaut.php';

if ($menuDestination == "responsableequipe") {
         $action =  new actionCahierListe;
        //$listeCahier =  $action->mesStagiairesEtDoctorants($_SESSION['p_id']);
        $listeCahier = $action->pourLequipe($_SESSION['responsableEquipe']);
}
else {
 require_once '../actions/actionCahierListe.php';
         $action =  new actionCahierListe;
        $listeCahier = $action->execute($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id,$selecteq_id,$selectp_id);
}

 require_once '../vues/PageCahierNavigation.php';
   $vue1= new PageCahierNavigation($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);
    $vue1->afficher($listeCahier,$menuDestination,$listeEquipe,$listePersonne);


require_once '../actions/actionCahierSupprimeProjet.php';

   $action =  new actionCahierSupprimeProjet;
    $action->execute($_POST);

 $cahierNum=$_POST['cahierNum'];

        require_once '../actions/actionCahierGet.php';
        $action1 =  new actionCahierGet;
        $tabcahier =  $action1->execute($cahierNum);

        // les utilisateurs affectés aux cahiers
        require_once '../actions/actionCahierAppartientGet.php';
        $action3 =  new actionCahierAppartientGet;
        $tabappartient =  $action3->execute($cahierNum);

        // la liste des équipes
        require_once '../actions/actionEquipeListe.php';
        $action2 =  new actionEquipeListe;
        $listeEquipes =  $action2->execute();

if  ((substr($numAction,3,3) == 'ADD') ||($_POST['ajoututil']==1)) $ajoututil=1;
else $ajoututil=0;


    require_once '../vues/PageCahierModifie.php';
        $vue2= new PageCahierModifie(0,$ajoututil,$tabcahier,$tabappartient,'',$menuDestination);

        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
        $listeProjet = $action2->execute('T','T','T','T');

        $action3 =  new actionCahierListe;
        $listeCahierlaboProjet = $action3->cahierlaboProjetListe($cahierNum);

        $vue2->afficher("",$listePersonneTous,$listeEquipes,$listeProjet,$listeCahierlaboProjet);
}


////////////////////////////////////////////////////////
// Ajouter un cahier
//../vues/PageOrdiAjoute.php
// ou on vient de changer le type dans la vue VuePersonneAjoute.php
//////////////////////////////////////////////////////
if ($numAction==170) {





 require_once '../web/MenuHaut.php';


 require_once '../actions/actionCahierListe.php';
         $action =  new actionCahierListe;
        $listeCahier = $action->execute($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);

 require_once '../vues/PageCahierNavigation.php';
   $vue1= new PageCahierNavigation($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);
    $vue1->afficher($listeCahier,$menuDestination,$listeEquipe,$listePersonne);




// require_once '../vues/MenuAdminConsulteListe.php';
//  $vue= new MenuAdminConsulteListe('T','T','T','T');
//    $vue->afficher($listeCahier,"admincahier",'','','','',0,0,0,0);

$menuDestination = 'admincahier';


        // la liste des équipes
        require_once '../actions/actionEquipeListe.php';
        $action2 =  new actionEquipeListe;
        $listeEquipes =  $action2->execute();




// $tab : Les informations du cahier
// $listeappartient cahierLaboAppartient
// $nblisteappartient
// $nb nombre de nouveau

        require_once '../vues/PageCahierModifie.php';
        $vue2= new PageCahierModifie(1,0,'','','',$menuDestination);

        $vue2->afficher("",$listePersonne,$listeEquipes);



}





/////////////////////////////////////////////////////////
// appuyer sur le bouton submit de la vue vue PageCahierAjoute.php
//////////////////////////////////////////////////////
if ($numAction==171) {
## TRAD
 require_once '../formulaires/FormCahier.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionCahierListe.php';




try {
        $cahier = new FormCahier($_POST);

        require_once '../actions/actionCahierAjoute.php';
        $action =  new actionCahierAjoute();
        $action->execute($cahier);


         $action =  new actionCahierListe;
        $listeCahier = $action->execute($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);

// require_once '../vues/MenuAdminConsulteListe.php';
//   $vue= new MenuAdminConsulteListe('T','T','T','T');
//    $vue->afficher($listeCahier,"admincahier",'','','','',0,0,0,0);

 require_once '../vues/PageCahierNavigation.php';
   $vue1= new PageCahierNavigation($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);
    $vue1->afficher($listeCahier,$menuDestination,$listeEquipe,$listePersonne);


        $cahierNum=$_POST['cahierNum'];

        require_once '../actions/actionCahierGet.php';
        $action1 =  new actionCahierGet;
        $tabcahier =  $action1->execute($cahierNum);

        // les utilisateurs affectés aux cahiers
        require_once '../actions/actionCahierAppartientGet.php';
        $action3 =  new actionCahierAppartientGet;
        $tabappartient =  $action3->execute($cahierNum);

        // la liste des équipes
        require_once '../actions/actionEquipeListe.php';
        $action2 =  new actionEquipeListe;
        $listeEquipes =  $action2->execute();






        require_once '../vues/PageCahierModifie.php';
        $vue2= new PageCahierModifie(0,0,$tabcahier,$tabappartient,'',"admincahier");

        $vue2->afficher("",$listePersonne,$listeEquipes);








} catch (Exception $e) {
         $action =  new actionCahierListe;
        $listeCahier = $action->execute($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);

// require_once '../vues/MenuAdminConsulteListe.php';
//   $vue= new MenuAdminConsulteListe('T','T','T','T');
//    $vue->afficher($listeCahier,"admincahier",'','','','',0,0,0,0);

 require_once '../vues/PageCahierNavigation.php';
   $vue1= new PageCahierNavigation($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);
    $vue1->afficher($listeCahier,$menuDestination,$listeEquipe,$listePersonne);


   require_once '../actions/actionEquipeRechercheListe.php';
        $action =  new actionEquipeRechercheListe;
        $listeEquipe = $action->execute();


require_once '../objets/UtilCahier.php';
$utilCahier = new UtilCahier ($_POST['np_id'],$_POST['ndateDotation'],$_POST['ndateRetour']);



  require_once '../vues/PageCahierModifie.php';
        $vue= new PageCahierModifie(1,0,$_POST,'',$utilCahier,"admincahier");





        $vue->afficher($e->getMessage(),$listePersonne,$listeEquipe);


## FIN
}


}

////////////////////////////////////////////////////////
// Modifier un cahier
//////////////////////////////////////////////////////
if (substr( $numAction, 0, 3 ) == '172') {
## TRAD
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionCahierListe.php';




if ($menuDestination == "responsableequipe") {
         $action =  new actionCahierListe;
	//$listeCahier =  $action->mesStagiairesEtDoctorants($_SESSION['p_id']);
        $listeCahier = $action->pourLequipe($_SESSION['responsableEquipe']);


}

else {

         $action =  new actionCahierListe;
        $listeCahier = $action->execute($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);

}


 require_once '../vues/PageCahierNavigation.php';
   $vue1= new PageCahierNavigation($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);
    $vue1->afficher($listeCahier,$menuDestination,$listeEquipe,$listePersonne);






        // premier appel, on charge depuis la base de données
   if (isset ( $_GET['cahierNum'] )) {
        $cahierNum=$_GET['cahierNum'];

        require_once '../actions/actionCahierGet.php';
        $action1 =  new actionCahierGet;
        $tabcahier =  $action1->execute($cahierNum);

	// les utilisateurs affectés aux cahiers
        require_once '../actions/actionCahierAppartientGet.php';
        $action3 =  new actionCahierAppartientGet;
        $tabappartient =  $action3->execute($cahierNum);

	// la liste des équipes
  	require_once '../actions/actionEquipeListe.php';
        $action2 =  new actionEquipeListe;
        $listeEquipes =  $action2->execute();



if  ((substr($numAction,3,3) == 'ADD') ||($_POST['ajoututil']==1)) $ajoututil=1;
else $ajoututil=0;



        require_once '../vues/PageCahierModifie.php';
        $vue2= new PageCahierModifie(0,$ajoututil,$tabcahier,$tabappartient,'',$menuDestination);

        require_once '../actions/actionProjetListeAdmin.php';
        $action2 =  new actionProjetListeAdmin;
        $listeProjet = $action2->execute('T','T','T','T');

	$listeCahierlaboProjet = $action->cahierlaboProjetListe($cahierNum);

        $vue2->afficher("",$listePersonneTous,$listeEquipes,$listeProjet,$listeCahierlaboProjet);




        }
else {
	// Ce  n'est pas un premier appel 
        // on récupére les données dans le formulaire

  	require_once '../actions/actionEquipeListe.php';
        $action2 =  new actionEquipeListe;
        $listeEquipes =  $action2->execute();


       // on transforme le formulaire $_POST en objet CahierLabo;
//	 require_once '../objets/CahierLabo.php';
  //       $cahierlabo = new CahierLabo($_POST['cahierNum'],$_POST['modele'],$_POST['fonction'],$_POST['eq_id'],$_POST['localisation'],$_POST['commentaires']);

	// la liste des utilisateurs enregistrés dans la bases de données

	 require_once '../objets/CahierLaboUtilisateur.php';

// Traitement de la suppression
	$listeUtil = array();
	for ($i = 0; $i < $_POST['nblisteutilisateur']; $i++) {
		if ((substr( $numAction, 3, 3 ) == 'DEL') && (substr($numAction,6) == $i)) {}
		else $listeUtil[$i]= new CahierLaboUtilisateur ($_POST['p_id'.$i],$_POST['cahierNum'],$_POST['dateDotation'.$i],$_POST['dateRetour'.$i]);

	}

	if (substr( $numAction, 3, 3 ) == 'ADD') $ajoututil=1 ;
	else $ajoututil=0;
	

	// la liste des nouveaux utilisateurs non enregistrés
	$listeUtilNouveau = array();
	for ($i = 0; $i < $nblisteutilisateurnouveau; $i++) {
		$listeUtilNouveau[$i]= new CahierLaboUtilisateur ($_POST['np_id'.$i],$_POST['cahierNum'],$_POST['ndateDotation'.$i],$_POST['ndateRetour'.$i]);
	}



        require_once '../vues/PageCahierModifie.php';
        $vue2= new PageCahierModifie(0,$ajoututil,$_POST,$listeUtil,'',$menuDestination);

        $vue2->afficher("",$listePersonne,$listeEquipes);


}
}

/////////////////////////////////////////////////////
// appuyer sur le bouton submit de la vue vueOrdiModifie.php
/////////////////////////////////////////////////////
if ($numAction==173) {
## TRAD
 require_once '../formulaires/FormCahier.php';
 require_once '../web/MenuHaut.php';


require_once '../actions/actionCahierListe.php';


try {

        $cahier = new FormCahier($_POST);



        require_once '../actions/actionCahierModifie.php';
        $action =  new actionCahierModifie();
        $action->execute($cahier);



  if (isset ( $_GET['menuDestination'] )) $menuDestination=$_GET['menuDestination'];
   else  $menuDestination=$_POST['menuDestination'];

if ($menuDestination=="chercheurcahier") {


        $action =  new actionCahierListe;
//        $mesCahiers =  $action->parUtilisateur($_SESSION['p_id']);
//        $cahiersStagiaires =  $action->mesStagiairesEtDoctorants($_SESSION['p_id']);
        $listeCahier =  $action->mesStagiairesEtDoctorants($_SESSION['p_id']);


//        require_once '../vues/MenuAdminConsulteListe.php';
//        $vue= new MenuAdminConsulteListe('T','T','T','T');
//        $vue->afficher($cahiersStagiaires,$menuDestination,'','','','',0,0,0,0);
}

else {
         $action =  new actionCahierListe;
        $listeCahier = $action->execute($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);

}


 require_once '../vues/PageCahierNavigation.php';
   $vue1= new PageCahierNavigation($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);
    $vue1->afficher($listeCahier,$menuDestination,$listeEquipe,$listePersonne);





   $cahierNum=$_POST['cahierNum'];

//        require_once '../actions/actionCahierGet.php';
//        $action1 =  new actionCahierGet;
//        $tabcahier =  $action1->execute($cahierNum);

        // les utilisateurs affectés aux cahiers
        require_once '../actions/actionCahierAppartientGet.php';
        $action3 =  new actionCahierAppartientGet;
        $tabappartient =  $action3->execute($cahierNum);

        // la liste des équipes
        require_once '../actions/actionEquipeListe.php';
        $action2 =  new actionEquipeListe;
        $listeEquipes =  $action2->execute();





        require_once '../vues/PageCahierModifie.php';
        $vue2= new PageCahierModifie(0,0,$_POST,$tabappartient,'',$menuDestination);


        $vue2->afficher("enregistré",$listePersonne,$listeEquipes);


} catch (Exception $e) {
      $action2 =  new actionCahierListe;
        $listeCahier = $action2->execute($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);


 require_once '../vues/PageCahierNavigation.php';
   $vue1= new PageCahierNavigation($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);
    $vue1->afficher($listeCahier,$menuDestination,$listeEquipe,$listePersonne);


// require_once '../vues/MenuAdminConsulteListe.php';
//   $vue= new MenuAdminConsulteListe('T','T','T','T');
//    $vue->afficher($liste,"admincahier",'','','','',0,0,0,0);


	require_once '../actions/actionEquipeRechercheListe.php';
        $action =  new actionEquipeRechercheListe;
        $listeEquipe = $action->execute();

        // les utilisateurs affectés aux cahiers
        require_once '../actions/actionCahierAppartientGet.php';
        $action3 =  new actionCahierAppartientGet;
        $tabappartient =  $action3->execute($_POST['cahierNum']);

        require_once '../vues/PageCahierModifie.php';
        $vue2= new PageCahierModifie(0,0,$_POST,$tabappartient,'',$menuDestination);
        $vue2->afficher($e->getMessage(),$listePersonne,$listeEquipe);

## FIN


}
}
/////////////////////////////////////////////////////////
// Navigation CVahier 
///////////////////////////////////////////////////////////

if  ($numAction==174){
## TRAD

 require_once '../web/MenuHaut.php';
require_once '../actions/actionCahierListe.php';
   $action =  new actionCahierListe;
    $listeCahier = $action->execute($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);


  //      require_once '../vues/PageProjetNavigationAdmin.php';
//        $vue= new PageProjetNavigationAdmin($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut);
//        $vue->afficher($liste,$menuDestination,$listeEquipe,$arrayEquipe);


 require_once '../vues/PageCahierNavigation.php';
   $vue1= new PageCahierNavigation($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);
    $vue1->afficher($listeCahier,$menuDestination,$listeEquipe,$listePersonne);




        // la liste des équipes
        require_once '../actions/actionEquipeRechercheListe.php';
        $action2 =  new actionEquipeRechercheListe;
        $listeEquipes =  $action2->execute();


   $year1= date('Y');
   $year2= date('Y');
   require_once '../vues/PageCahierPageGarde.php';
    $vue2= new PageCahierPageGarde($year1,$year2,$listeEquipes);
    $vue2->afficher();


##
}
?>
