<?php
/*********************************************************************************************************************************\
 *  Hybicus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                                                                                *
 *  Copyright (c) 2018 2019                                                                                                   *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

header ( 'Content-Type: text/html; charset=utf-8' );
require_once("../web/session_start.php");


?>


<!DOCTYPE html>

<?php
 require_once '../web/head.php';

require_once '../control/CachedPDOStatement.php';
// require_once '../entites/class.php';
 ?>

<body>

<?php
// Chargement des tables ...
        require_once '../actions/actionPersonneFiltre.php';
        $actionPersonne =  new actionPersonneFiltre;
        $listePersonne =  new CachedPDOStatement($actionPersonne->execute('T','T','T','T',date('Y-m-d'),"","",0)); // Tout le monde sauf les archivés

 if (isset ( $_GET ['numAction'] )) $numAction=$_GET['numAction'];
 if (isset ( $_POST ['numAction'] )) $numAction=$_POST['numAction'];


////////////////////////////////////////////////////////
// Ajouter un ordi
//../vues/PageOrdiAjoute.php
// ou on vient de changer le type dans la vue VuePersonneAjoute.php
//////////////////////////////////////////////////////
if ($numAction==170) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageOrdiNavigation.php';
 require_once '../actions/actionOrdiListe.php';


         $action =  new actionOrdiListe;
        $listeOrdi = $action->execute();

   $vue1= new PageOrdiNavigation();
    $vue1->afficher($listeOrdi);


        require_once '../vues/PageOrdiAjoute.php';
        $vue2= new PageOrdiAjoute("");
        $vue2->afficher("",$listePersonne);
}

/////////////////////////////////////////////////////////
// appuyer sur le bouton submit de la vue vue PageOrdiAjoute.php
//////////////////////////////////////////////////////
if ($numAction==171) {
 require_once '../formulaires/FormOrdiAjoute.php';
 require_once '../web/MenuHaut.php';
 require_once '../actions/actionOrdiListe.php';

 require_once '../vues/PageOrdiNavigation.php';

         $action =  new actionOrdiListe;
        $listeOrdi = $action->execute();

   $vue= new PageOrdiNavigation();
    $vue->afficher($listeOrdi);

try {
        $ordi = new FormOrdiAjoute($_POST);

        require_once '../actions/actionOrdiAjoute.php';
        $action =  new actionOrdiAjoute();
        $action->execute($ordi);


        $action2 =  new actionOrdiListe;
        $listeOrdi = $action2->execute();

        $vue= new PageOrdiNavigation();
        $vue->afficher($listeOrdi);



} catch (Exception $e) {
        require_once '../vues/PageOrdiAjoute.php';
        $vue= new PageOrdiAjoute($_POST);

        $vue->afficher($e->getMessage(),$listePersonne);


}


}

////////////////////////////////////////////////////////
// Modifier un ordi
//////////////////////////////////////////////////////
if ($numAction==172) {
 require_once '../web/MenuHaut.php';
 require_once '../vues/PageOrdiNavigation.php';
 require_once '../actions/actionOrdiListe.php';

    $action =  new actionOrdiListe;
    $listeOrdi = $action->execute();

   $vue= new PageOrdiNavigation();
    $vue->afficher($listeOrdi);

   if (isset ( $_GET ['id'] )) {
        // premier appel, on charge depuis la base de données
        $id=$_GET['id'];

        require_once '../actions/actionOrdiGet.php';
        // rend un tableau associatif
         $action1 =  new actionOrdiGet;
        $tabordi =  $action1->execute($id);


        require_once '../vues/PageOrdiModifie.php';
        $vue2= new PageOrdiModifie($tabordi);
        $vue2->afficher("",$listePersonne);


        }
}

/////////////////////////////////////////////////////
// appuyer sur le bouton submit de la vue vueOrdiModifie.php
/////////////////////////////////////////////////////
if ($numAction==173) {
 require_once '../formulaires/FormOrdiModifie.php';
 require_once '../web/MenuHaut.php';

 require_once '../vues/PageOrdiNavigation.php';

require_once '../actions/actionOrdiListe.php';


try {

        $equipe = new FormOrdiModifie($_POST);



        require_once '../actions/actionOrdiModifie.php';
        $action =  new actionOrdiModifie();
        $action->execute($equipe);

       $action2 =  new actionOrdiListe;
        $liste = $action2->execute();

        $vue= new PageOrdiNavigation();
        $vue->afficher($liste);



} catch (Exception $e) {
      $action2 =  new actionOrdiListe;
        $liste = $action2->execute();

        $vue= new PageOrdiNavigation();
        $vue->afficher($liste);

        require_once '../vues/PageOrdiModifie.php';
        $vue2= new PageOrdiModifie($_POST);
        $vue2->afficher($e->getMessage(),$listePersonne);



}
}
/////////////////////////////////////////////////////////
// Navigation Ordi
///////////////////////////////////////////////////////////

if  ($numAction==174){

 require_once '../web/MenuHaut.php';
 require_once '../vues/PageOrdiNavigation.php';
require_once '../actions/actionOrdiListe.php';

   $action =  new actionOrdiListe;
    $liste = $action->execute();

    $vue= new PageOrdiNavigation($_POST);
    $vue->afficher($liste);

}
?>
