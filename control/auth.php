<?php
/***************************************************************************\
 *  GES UMR : gestion rh pour les umr                                      *
 *                                                                         *
 *  Copyright (c) 2018                                                     *
 *  Serge Conrad                                                           *
 *  Elise Deme								   *
 *  	                                                                   *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/
//session_start();
//
//
session_start();


require_once '../entites/Model.php';




////////////////////////////////////////////////////////////////////////////////


if (file_exists('../web/install.php')) {
echo 'Supprimer le fichier web/install.php avant la première utilisation';
die;
}



// PAS AUTHENTIFIE SUR HYBISCUS ? 
if(!isset($_SESSION['user']))   {
	// il y a une SESSION SPIP ??


$ini = parse_ini_file("../entites/hybiscus.ini", true );


		// AUTHENTIFICATION CAS
		echo '<h2> Bienvenue sur la base Hybiscus de l\'umr hydrosciences</h2>';
		echo '<form action="authcas.php" method="post">';
		    echo '<input type="submit" value="Authentification UM" />';
		echo '</form>';

		// AUTHENTIFICATION AD
		echo '<h2> Authentification HSM</h2>';
		echo '<form action="authAd.php" method="post">';
    			echo '<label for="username">Username:</label><input type="text" value="'. $_POST["username"].'" id="username" name="username" />';
    			echo '<label for="password">Password:</label><input type="password" value="" id="password" name="password" />';
    			echo '<div class="error">'. $errorMsg .'</div>';
		    echo '<input type="submit" value="Authentification HSM" />';
		echo '</form>';

}
else {
// DEJA UNE SESSION
// FAIRE   header( "Location: Controleur.php" );

foreach($_SESSION as $key => $value){
		echo $key.','.$value;
}
}





?>
