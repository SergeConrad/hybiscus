<?php
require_once 'config.php';
class ModelResponsable {
	public static $pdo;
	public static function set_static() {
 		$conf=new Conf;
                $host =$conf->getHostname ();
                $dbname = $conf->getDatabase ();
                $login = $conf->getLogin ();
                $pass = $conf->getPassword ();
		
		try {
			// Connexion à la base de données
			// Le dernier argument sert à ce que toutes les chaines de charactères
			// en entrée et sortie de MySql soit dans le codage UTF-8
			self::$pdo = new PDO ( "mysql:host=$host;dbname=$dbname", $login, $pass, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8" 
			) );
			
			// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
			self::$pdo->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $ex ) {
			echo $ex->getMessage ();
			die ( 'Problème lors de la connexion à la base de données' );
		}
	}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////
static function responsableStagiaireFiltre($eq_id,$archive) {

try {
	self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";


        $requete = self::$pdo->prepare ( "

SELECT DISTINCT p1.p_id,p1.p_nom,p1.p_prenom
 FROM t_personne p1
 LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
 RIGHT JOIN stage ON historique.stageId = stage.stageId
 WHERE  t_membres_equipe2.eq_id  like :eq_id AND p1.archive ".$sql1." '1'
AND dateDebutH = (SELECT MAX(dateDebutH) FROM historique WHERE historique.p_id=p1.p_id)
 ORDER BY dateDebutH

");

//SELECT p1.p_id,p1.p_nom,p1.p_prenom
// FROM t_personne p1
// LEFT JOIN historique ON historique.p_id = p1.p_id
// LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
// RIGHT JOIN stage ON historique.stageId = stage.stageId
// WHERE t_membres_equipe2.eq_id  like :eq_id  AND p1.archive ".$sql1." '1'
// GROUP BY historique.numHistorique
// ORDER BY dateDebutH





         $requete->bindParam(':eq_id', $eq_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
        }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////
static function responsableStagiaireFiltreComplet($eq_id,$archive) {

try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";


        $requete = self::$pdo->prepare ( "

SELECT p1.p_id,p1.p_nom,p1.p_prenom,p1.dateNaissance,p1.p_mail,p1.p_mail_perso,p1.p_tel,p1.p_tel_perso,p1.p_page_perso,p1.p_commentaire,p1.nationalite,
p1.personneaprevenir,p1.horaireacces,
sit_nom,GROUP_CONCAT(eq_nom SEPARATOR '/'), dateDebutH,dateFinH,
CONCAT (p2.p_nom,' ',p2.p_prenom) AS encadrant,stag_intitule,etablissement,intitule,autreformation
 FROM t_personne p1
 LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN t_site ON historique.sit_id = t_site.sit_id
 RIGHT JOIN stage ON historique.stageId = stage.stageId
 LEFT JOIN t_personne p2 ON stage.p_stag_encadrant = p2.p_id
 LEFT JOIN t_formation_stagiaire ON stage.f_id = t_formation_stagiaire.f_id
 LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
 LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id
 WHERE  t_membres_equipe2.eq_id  like :eq_id AND p1.archive ".$sql1." '1'
AND dateDebutH = (SELECT MAX(dateDebutH) FROM historique WHERE historique.p_id=p1.p_id)
 GROUP BY historique.numHistorique
 ORDER BY dateDebutH

");

//SELECT p1.p_id,p1.p_nom,p1.p_prenom,p1.dateNaissance,p1.p_mail,p1.p_tel,p1.p_tel_perso,p1.p_page_perso,p1.p_commentaire,p1.nationalite,
//p1.personneaprevenir,p1.horaireacces,
//sit_nom,GROUP_CONCAT(eq_nom SEPARATOR '/'), dateDebutH,dateFinH,
//CONCAT (p2.p_nom,' ',p2.p_prenom) AS encadrant,stag_intitule,etablissement,intitule,autreformation
// FROM t_personne p1
// LEFT JOIN historique ON historique.p_id = p1.p_id
// LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
// LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id
// LEFT JOIN t_site ON historique.sit_id = t_site.sit_id
// RIGHT JOIN stage ON historique.stageId = stage.stageId
// LEFT JOIN t_personne p2 ON stage.p_stag_encadrant = p2.p_id
// LEFT JOIN t_formation_stagiaire ON stage.f_id = t_formation_stagiaire.f_id
// WHERE t_membres_equipe2.eq_id  like :eq_id  AND p1.archive ".$sql1." '1'
// GROUP BY historique.numHistorique
// ORDER BY dateDebutH

//");

         $requete->bindParam(':eq_id', $eq_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
        }


static function responsableDoctorantFiltreComplet($eq_id,$archive) {

try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";


        $requete = self::$pdo->prepare ( "




SELECT p1.p_id,p1.p_nom,p1.p_prenom,p1.dateNaissance,p1.p_mail,p1.p_mail_perso,p1.p_tel,p1.p_tel_perso,p1.p_page_perso,p1.p_commentaire,p1.nationalite,p1.personneaprevenir,p1.horaireacces,
sit_nom,GROUP_CONCAT(eq_nom SEPARATOR '/'), dateDebutH,dateFinH,
CONCAT (p2.p_nom,' ',p2.p_prenom) AS directeur1,CONCAT (p3.p_nom,' ',p3.p_prenom) AS directeur2 ,ecoleDoctorale,intitule,tfinancement,m2obtenu,laboaccueil,etsInscription,etsCoTutelle,theseSoutenance,abandon,apresThese FROM t_personne p1

 LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN t_site ON historique.sit_id = t_site.sit_id
 LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
 LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id

 RIGHT JOIN theses ON historique.theseId = theses.theseId
 LEFT JOIN t_personne p2 ON theses.p_these_directeur1 = p2.p_id
 LEFT JOIN t_personne p3 ON theses.p_these_directeur2 = p3.p_id
WHERE t_membres_equipe2.eq_id LIKE :eq_id AND p1.archive ".$sql1." '1'
AND dateDebutH = (SELECT MAX(dateDebutH) FROM historique WHERE historique.p_id=p1.p_id)
 GROUP BY historique.numHistorique
 ORDER BY dateDebutH


");

         $requete->bindParam(':eq_id', $eq_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

        }



static function responsableDoctorantFiltre($eq_id,$archive) {

try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";

        $requete = self::$pdo->prepare ( "


SELECT DISTINCT p1.p_id,p1.p_nom,p1.p_prenom
 FROM t_personne p1

 LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique

 RIGHT JOIN theses ON historique.theseId = theses.theseId
 LEFT JOIN t_personne p2 ON theses.p_these_directeur1 = p2.p_id
 LEFT JOIN t_personne p3 ON theses.p_these_directeur2 = p3.p_id
 WHERE t_membres_equipe2.eq_id LIKE :eq_id AND p1.archive ".$sql1." '1'
AND dateDebutH = (SELECT MAX(dateDebutH) FROM historique WHERE historique.p_id=p1.p_id)
 ORDER BY dateDebutH


");

         $requete->bindParam(':eq_id', $eq_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

        }




static function ResponsableCddFiltre($eq_id,$archive) {


try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";

        $requete = self::$pdo->prepare ( "


SELECT DISTINCT p1.p_id,p1.p_nom,p1.p_prenom
 FROM t_personne p1
LEFT JOIN historique ON historique.p_id = p1.p_id
LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
 RIGHT JOIN contractuel ON historique.contractuel_id = contractuel.contractuel_id
 
 WHERE t_membres_equipe2.eq_id  like :eq_id AND p1.archive ".$sql1." '1'
AND dateDebutH = (SELECT MAX(dateDebutH) FROM historique WHERE historique.p_id=p1.p_id)

");

 //LEFT JOIN t_type_personnel ON p1.ty_id = t_type_personnel.ty_id
         $requete->bindParam(':eq_id', $eq_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

        }



static function ResponsableCddFiltreComplet($eq_id,$archive) {


try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";

        $requete = self::$pdo->prepare ( "

SELECT p1.p_id,p1.p_nom,p1.p_prenom,p1.dateNaissance,p1.p_mail,p1.p_mail_perso,p1.p_tel,p1.p_tel_perso,p1.p_page_perso,p1.p_commentaire,p1.nationalite,p1.personneaprevenir,p1.horaireacces,
sit_nom,GROUP_CONCAT(eq_nom SEPARATOR '/'), dateDebutH,dateFinH,ty_nom,
contractuel.contractuel_sujet_recherche, CONCAT (p2.p_nom,' ',p2.p_prenom) AS superieur FROM t_personne p1
LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN t_site ON historique.sit_id = t_site.sit_id
 LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
 LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id
 RIGHT JOIN contractuel ON historique.contractuel_id = contractuel.contractuel_id
 LEFT JOIN t_personne p2 ON contractuel.contractuel_sup_id = p2.p_id
LEFT JOIN t_type_personnel2 ON historique.ty_id = t_type_personnel2.ty_id AND historique.st_id = t_type_personnel2.st_id
 WHERE t_membres_equipe2.eq_id  like :eq_id  AND p1.archive ".$sql1." '1'
AND dateDebutH = (SELECT MAX(dateDebutH) FROM historique WHERE historique.p_id=p1.p_id)

 GROUP BY historique.numHistorique

");

 //LEFT JOIN t_type_personnel ON p1.ty_id = t_type_personnel.ty_id
         $requete->bindParam(':eq_id', $eq_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

        }



 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_equipe_effectif($eq_id) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                 $requeteSelect = self::$pdo->prepare ( "SELECT st_nom,count(*) AS nb FROM t_membres_equipe2,historique,t_statut2,t_personne WHERE eq_id like :eq_id AND t_membres_equipe2.numHistorique like historique.numHistorique AND historique.st_id like t_statut2.st_id AND historique.p_id like t_personne.p_id AND    dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )
					AND t_personne.archive like '0' GROUP BY st_nom");
         $requeteSelect->bindParam(':eq_id', $eq_id);

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_depart3mois($eq_id,$today,$dans3mois) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                 $requeteSelect = self::$pdo->prepare ( "
select t_personne.p_nom,t_personne.p_prenom,t_personne.p_id,dateDebutH,dateFinH from t_personne,historique,t_membres_equipe2
WHERE
	t_personne.p_id like historique.p_id
    AND   dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )
     AND historique.numHistorique like t_membres_equipe2.numHistorique
     AND historique.dateFinH > :today AND historique.dateFinH < :dans3mois
     AND t_membres_equipe2.eq_id = :eq_id
");

         $requeteSelect->bindParam(':eq_id', $eq_id);
         $requeteSelect->bindParam(':today', $today);
         $requeteSelect->bindParam(':dans3mois', $dans3mois);

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }
 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_arrivee3mois($eq_id,$today,$dans3mois) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                 $requeteSelect = self::$pdo->prepare ( "
select t_personne.p_nom,t_personne.p_prenom,t_personne.p_id,dateDebutH,dateFinH from t_personne,historique,t_membres_equipe2
WHERE
        t_personne.p_id like historique.p_id
    AND   dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )
     AND historique.numHistorique like t_membres_equipe2.numHistorique
     AND historique.dateDebutH > :today AND historique.dateDebutH < :dans3mois
     AND t_membres_equipe2.eq_id = :eq_id
");

         $requeteSelect->bindParam(':eq_id', $eq_id);
         $requeteSelect->bindParam(':today', $today);
         $requeteSelect->bindParam(':dans3mois', $dans3mois);

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_equipe_type($eq_id,$st_id) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                 $requeteSelect = self::$pdo->prepare ( "
SELECT ty_nom,count(*) AS nb FROM t_membres_equipe2,historique,t_personne,t_type_personnel2
WHERE eq_id like :eq_id 
AND t_membres_equipe2.numHistorique like historique.numHistorique
AND historique.st_id like t_type_personnel2.st_id
AND historique.ty_id like t_type_personnel2.ty_id
AND historique.p_id like t_personne.p_id AND    dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )
AND historique.st_id like :st_id
AND t_personne.archive like '0' GROUP BY ty_nom");


         $requeteSelect->bindParam(':eq_id', $eq_id);
         $requeteSelect->bindParam(':st_id', $st_id);



                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }


// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_equipe_type_annee($eq_id,$annee) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                 $requeteSelect = self::$pdo->prepare ( "
SELECT ty_nom,count(*) AS nb,GROUP_CONCAT(t_personne.p_nom) AS debug,historique.st_id  FROM t_membres_equipe2,historique,t_personne,t_type_personnel2
WHERE eq_id like :eq_id
AND t_membres_equipe2.numHistorique like historique.numHistorique
AND historique.st_id like t_type_personnel2.st_id
AND historique.ty_id like t_type_personnel2.ty_id
AND historique.p_id like t_personne.p_id AND    dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )
AND (historique.st_id like '1'
 OR historique.st_id like '2'
 OR historique.st_id like '3')
AND ( 
	 (historique.dateDebutH <:date1 AND (historique.dateFinH  >:date2 OR historique.dateFinH = :datevide))
	OR (historique.dateDebutH >:date1 AND historique.dateDebutH  <:date2 )
	OR (historique.dateFinH >:date1 AND historique.dateFinH  <:date2 )
)	
GROUP BY ty_nom ORDER BY historique.st_id ASC");

$date1=date("$annee-01-01");
$date2=date("$annee-12-31");

$datevide=date("0000-00-00");
         $requeteSelect->bindParam(':date1', $date1);
         $requeteSelect->bindParam(':date2', $date2);
         $requeteSelect->bindParam(':datevide', $datevide);
         $requeteSelect->bindParam(':eq_id', $eq_id);
//         $requeteSelect->bindParam(':st_id', $st_id);




                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }





}
ModelResponsable::set_static ();
?>

