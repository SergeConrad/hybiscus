<?php
/***************************************************************************\
 *  GES UMR : gestion rh pour les umr                                      *
 *                                                                         *
 *  Copyright (c) 2018                                                     *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/
require_once 'config.php';
class ModelSpip {

  public static $pdo;
        public static function set_static() {

                $conf=new Conf;
                $host =$conf->getHostname ();
                $dbname = $conf->getDatabase ();
                $login = $conf->getLogin ();
                $pass = $conf->getPassword ();

                try {
                        // Connexion à la base de données
                        // Le dernier argument sert à ce que toutes les chaines de charactères
                        // en entrée et sortie de MySql soit dans le codage UTF-8
                        self::$pdo = new PDO ( "mysql:host=$host;dbname=$dbname", $login, $pass, array (
                                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8"
                        ) );

                        // On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
                        self::$pdo->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                } catch ( PDOException $ex ) {
                        echo $ex->getMessage ();
                        die ( 'Problème lors de la connexion à la base de données' );
                }
        }



	public static $pdospip;
	public static function set_static_spip() {
		$confSpip=new ConfSpip;
		$host = $confSpip->getHostname ();
		$dbname = $confSpip->getDatabase ();
		$login = $confSpip->getLogin ();
		$pass = $confSpip->getPassword ();
		
		try {
			// Connexion à la base de données
			// Le dernier argument sert à ce que toutes les chaines de charactères
			// en entrée et sortie de MySql soit dans le codage UTF-8
			self::$pdospip = new PDO ( "mysql:host=$host;dbname=$dbname", $login, $pass, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8" 
			) );
			

			// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
			self::$pdospip->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $ex ) {
			echo $ex->getMessage ();
			die ( 'Problème lors de la connexion à la base de données Spip' );
		}
	}
	



//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  daoAuthAlea() {
              self::$pdospip->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = ModelSpip::$pdospip->prepare (
        		 "SELECT `valeur` FROM `hsm_meta` WHERE nom LIKE 'alea_ephemere'"
                );


                        $rq->execute ();
          $f = $rq->fetch();
          $alea = $f->valeur;
                        return $alea;

}
	

	

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static function generer_articles_perso() {
		// utilisé dans  Home.php
      try {

			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                        self::$pdospip->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

			// que les permanents et les non permanents
//                        $requeteSelectPersonne = self::$pdo->prepare ( "
//				SELECT *  FROM t_personne WHERE st_id LIKE '1' OR ((st_id LIKE '3') AND (ty_id NOT LIKE '7'))
//			");

                        $requeteSelectPersonne = self::$pdo->prepare ( "
		               SELECT  p1.p_id,p1.p_nom, p1.p_prenom,p1.p_article_spip
		                FROM t_personne AS p1
		                LEFT JOIN historique ON p1.p_id = historique.p_id
		
		                WHERE p1.archive NOT LIKE '1'
				AND ((historique.st_id LIKE '1') OR (historique.st_id LIKE '2') OR (historique.st_id LIKE '3' AND historique.ty_id LIKE '1'))
		                AND
		                        dateDebutH = (
		                                                SELECT MAX(dateDebutH)
		                                                FROM historique
		                                                WHERE p1.p_id LIKE historique.p_id
		                                        )
		                GROUP BY p_id ORDER BY p_nom
			");


//permanents
//st_id LIKE '1'
// cdd
//st_id LIKE '2'
// doctorants
//st_id LIKE '3' AND ty_id LIKE '1'


                        $requeteSelectPersonne->execute ();
                       	foreach ( $requeteSelectPersonne as $row ) {
				if ($row->p_article_spip==0) {

				        // CREATION RUBRIQUE
                                        $montitre= $row->p_nom.' '.$row->p_prenom;
                                        echo "Creation rubrique $montitre <br>";
                                        $requeteInsertionRubrique = ModelSpip::$pdospip->prepare("INSERT INTO hsm_rubriques (id_rubrique,id_parent, titre, descriptif, texte, id_secteur,maj,statut, date,lang,langue_choisie,statut_tmp,date_tmp,profondeur,id_trad,agenda) VALUES ('',31, :montitre,'','',31,NULL,'publie','2016-11-16 20:11:31','fr','non','publie','2016-11-16 20:11:31',0,0,0)");
                                        $requeteInsertionRubrique->bindParam(':montitre', $montitre);
                                        $requeteInsertionRubrique->execute();

                                         //recuperation de l'id rubrique
                                        $requeteSelectSpip = self::$pdospip->prepare ( "SELECT id_rubrique  FROM hsm_rubriques WHERE titre LIKE :titre AND statut LIKE :statut");
                                        $requeteSelectSpip->bindParam(':titre', $montitre);
					$statut= "publie";
                                        $requeteSelectSpip->bindParam(':statut', $statut);
                                        $requeteSelectSpip->execute ();
                                         $id_rubrique=$requeteSelectSpip->fetchColumn();




					// creation de l'article
				        $requeteInsertionArticle = ModelSpip::$pdospip->prepare("INSERT INTO hsm_articles (id_article,surtitre, titre, soustitre, id_rubrique, descriptif, chapo, texte, ps, date, statut, id_secteur, maj, export, date_redac, visites, referers, popularite, accepter_forum, date_modif, lang, langue_choisie, id_trad, nom_site, url_site, virtuel) VALUES ('','', :titre, '', :rubrique, '', '', '', '', '2016-11-16 20:11:37', 'publie', 31, NULL, 'oui', '0000-00-00 00:00:00', 0, 0, 0, 'pos', '2016-11-16 20:11:31', 'fr', 'non', 0, '', '', '')" );
				        $requeteInsertionArticle->bindParam(':titre', $montitre);
				        $requeteInsertionArticle->bindParam(':rubrique', $id_rubrique);
				        $requeteInsertionArticle->execute();


					  //recuperation de l'id article
                                        $requeteSelectSpip = self::$pdospip->prepare ( "SELECT id_article  FROM hsm_articles WHERE titre LIKE :titre AND statut LIKE :statut");
                                        $requeteSelectSpip->bindParam(':titre', $montitre);
                                        $requeteSelectSpip->bindParam(':statut', $statut);
                                        $requeteSelectSpip->execute ();
                                         $id_article=$requeteSelectSpip->fetchColumn();





echo " creation page perso $id_article $montitre dans rubrique $id_rubrique";
 

					//copie de l'id dans la base annuaire
					$requeteUpdate = self::$pdo->prepare("UPDATE t_personne SET p_article_spip =  :p_article_spip WHERE p_id = :p_id");
			                $requeteUpdate->bindParam(':p_id', $row->p_id);
			                $requeteUpdate->bindParam(':p_article_spip', $id_article);
                        		$requeteUpdate->execute();

				}
                        }







                        //return $objet;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
			die;
                }

echo "fin procédure";die;

	}
	
	
	


//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  daoChercheLogin($nom) {
              self::$pdospip->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

$nom= "%$nom%";
                $rq = ModelSpip::$pdospip->prepare (
                         "SELECT login FROM `hsm_auteurs` WHERE `nom` LIKE :user"
                );

                $rq->bindParam ( ':user', $nom );
                $rq->execute ();

                        return $rq;

}
















}

ModelSpip::set_static ();
ModelSpip::set_static_spip ();
?>
