<?php
require_once 'config.php';
class ModelChercheur {
	public static $pdo;
	public static function set_static() {
		 $conf=new Conf;
                $host =$conf->getHostname ();
                $dbname = $conf->getDatabase ();
                $login = $conf->getLogin ();
                $pass = $conf->getPassword ();
		
		try {
			// Connexion à la base de données
			// Le dernier argument sert à ce que toutes les chaines de charactères
			// en entrée et sortie de MySql soit dans le codage UTF-8
			self::$pdo = new PDO ( "mysql:host=$host;dbname=$dbname", $login, $pass, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8" 
			) );
			
			// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
			self::$pdo->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $ex ) {
			echo $ex->getMessage ();
			die ( 'Problème lors de la connexion à la base de données' );
		}
	}

	

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////
static function chercheurStagiaireFiltre($p_id,$archive) {

try {
	self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";

        $requete = self::$pdo->prepare ( "
SELECT DISTINCT p1.p_id,p1.p_nom,p1.p_prenom
 FROM t_personne p1
 LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN stage ON historique.stageId = stage.stageId
 LEFT JOIN t_personne p2 ON stage.p_stag_encadrant = p2.p_id 
 WHERE p2.p_id like :p_id AND p1.archive ".$sql1." '1'
 ORDER BY dateDebutH 
");

 //WHERE stage.p_stag_encadrant like :p_id AND p1.st_id NOT LIKE '2'

         $requete->bindParam(':p_id', $p_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

        }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////
static function chercheurStagiaireFiltreComplet($p_id,$archive) {

try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";

        $requete = self::$pdo->prepare ( "
SELECT p1.p_id,p1.p_nom,p1.p_prenom,p1.dateNaissance,p1.p_mail,p1.p_mail_perso,p1.p_tel,p1.p_tel_perso,p1.p_page_perso,p1.p_commentaire,p1.nationalite,
p1.personneaprevenir,p1.horaireacces,
sit_nom,GROUP_CONCAT(eq_nom SEPARATOR '/'), dateDebutH,dateFinH,
CONCAT (p2.p_nom,' ',p2.p_prenom) AS encadrant,stag_intitule,etablissement,intitule,autreformation
 FROM t_personne p1
 LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN t_site ON historique.sit_id = t_site.sit_id
 LEFT JOIN stage ON historique.stageId = stage.stageId
 LEFT JOIN t_personne p2 ON stage.p_stag_encadrant = p2.p_id
 LEFT JOIN t_formation_stagiaire ON stage.f_id = t_formation_stagiaire.f_id
 LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
 LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id
 WHERE p2.p_id like :p_id AND p1.archive ".$sql1." '1'
 GROUP BY historique.numHistorique
 ORDER BY dateDebutH
");






 //WHERE stage.p_stag_encadrant like :p_id AND p1.st_id NOT LIKE '2'

         $requete->bindParam(':p_id', $p_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

        }




static function chercheurDoctorantFiltre($p_id,$archive) {

try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";

        $requete = self::$pdo->prepare ( "
SELECT DISTINCT p1.p_id,p1.p_nom,p1.p_prenom
 FROM t_personne p1

 LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN theses ON historique.theseId = theses.theseId
 LEFT JOIN t_personne p2 ON theses.p_these_directeur1 = p2.p_id 
 LEFT JOIN t_personne p3 ON theses.p_these_directeur2 = p3.p_id 
 WHERE (theses.p_these_directeur1 like :p_id OR theses.p_these_directeur2 like :p_id) AND p1.archive ".$sql1." '1'
 ORDER BY dateDebutH 

");

         $requete->bindParam(':p_id', $p_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

        }


static function chercheurDoctorantFiltreComplet($p_id,$archive) {

try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";

        $requete = self::$pdo->prepare ( "
SELECT p1.p_id,p1.p_nom,p1.p_prenom,p1.dateNaissance,p1.p_mail,p1.p_mail_perso,p1.p_tel,p1.p_tel_perso,p1.p_page_perso,p1.p_commentaire,p1.nationalite,p1.personneaprevenir,p1.horaireacces,
sit_nom,GROUP_CONCAT(eq_nom SEPARATOR '/'), dateDebutH,dateFinH,
CONCAT (p2.p_nom,' ',p2.p_prenom) AS directeur1,CONCAT (p3.p_nom,' ',p3.p_prenom) AS directeur2 ,ecoleDoctorale,intitule,tfinancement,m2obtenu,laboaccueil,etsInscription,etsCoTutelle,theseSoutenance,abandon,apresThese FROM t_personne p1

 LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN t_site ON historique.sit_id = t_site.sit_id
 LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
 LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id

 LEFT JOIN theses ON historique.theseId = theses.theseId
 LEFT JOIN t_personne p2 ON theses.p_these_directeur1 = p2.p_id
 LEFT JOIN t_personne p3 ON theses.p_these_directeur2 = p3.p_id
 WHERE (theses.p_these_directeur1 like :p_id OR theses.p_these_directeur2 like :p_id) AND p1.archive ".$sql1." '1'
 GROUP BY historique.numHistorique
 ORDER BY dateDebutH

");

         $requete->bindParam(':p_id', $p_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

        }



static function chercheurInviteFiltre($p_id,$archive) {


try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";

        $requete = self::$pdo->prepare ( "
SELECT DISTINCT p1.p_id,p1.p_nom,p1.p_prenom
 FROM t_personne p1
LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN invite ON historique.invite_id = invite.invite_id
 LEFT JOIN t_personne p2 ON invite.invite_par_id = p2.p_id
LEFT JOIN t_type_personnel2 ON historique.ty_id = t_type_personnel2.ty_id AND historique.st_id = t_type_personnel2.st_id
 WHERE invite.invite_par_id like :p_id  AND p1.archive ".$sql1." '1'

");

         $requete->bindParam(':p_id', $p_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

        }




static function chercheurCddFiltre($p_id,$archive) {


try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";

        $requete = self::$pdo->prepare ( "
SELECT DISTINCT p1.p_id,p1.p_nom,p1.p_prenom
 FROM t_personne p1
LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN contractuel ON historique.contractuel_id = contractuel.contractuel_id
 LEFT JOIN t_personne p2 ON contractuel.contractuel_sup_id = p2.p_id 
LEFT JOIN t_type_personnel2 ON historique.ty_id = t_type_personnel2.ty_id AND historique.st_id = t_type_personnel2.st_id
 WHERE contractuel.contractuel_sup_id like :p_id  AND p1.archive ".$sql1." '1'

");

         $requete->bindParam(':p_id', $p_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

        }

static function chercheurCddFiltreComplet($p_id,$archive) {
try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";

        $requete = self::$pdo->prepare ( "
SELECT p1.p_id,p1.p_nom,p1.p_prenom,p1.dateNaissance,p1.p_mail,p1.p_mail_perso,p1.p_tel,p1.p_tel_perso,p1.p_page_perso,p1.p_commentaire,p1.nationalite,p1.personneaprevenir,p1.horaireacces,
sit_nom,GROUP_CONCAT(eq_nom SEPARATOR '/'), dateDebutH,dateFinH,ty_nom,
contractuel.contractuel_sujet_recherche, CONCAT (p2.p_nom,' ',p2.p_prenom) AS superieur FROM t_personne p1
LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN t_site ON historique.sit_id = t_site.sit_id
 LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
 LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id
 LEFT JOIN contractuel ON historique.contractuel_id = contractuel.contractuel_id
 LEFT JOIN t_personne p2 ON contractuel.contractuel_sup_id = p2.p_id
LEFT JOIN t_type_personnel2 ON historique.ty_id = t_type_personnel2.ty_id AND historique.st_id = t_type_personnel2.st_id
 WHERE contractuel.contractuel_sup_id like :p_id  AND p1.archive ".$sql1." '1'
 GROUP BY historique.numHistorique

");

         $requete->bindParam(':p_id', $p_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
        }


static function chercheurInviteFiltreComplet($p_id,$archive) {
try {
        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

if ($archive=='oui') $sql1= " LIKE ";
else $sql1 = " NOT LIKE ";

        $requete = self::$pdo->prepare ( "
SELECT p1.p_id,p1.p_nom,p1.p_prenom,p1.dateNaissance,p1.p_mail,p1.p_mail_perso,p1.p_tel,p1.p_tel_perso,p1.p_page_perso,p1.p_commentaire,p1.nationalite,p1.personneaprevenir,p1.horaireacces,
sit_nom,GROUP_CONCAT(eq_nom SEPARATOR '/'), dateDebutH,dateFinH,ty_nom,
invite.invite_commentaire, CONCAT (p2.p_nom,' ',p2.p_prenom) AS superieur FROM t_personne p1
LEFT JOIN historique ON historique.p_id = p1.p_id
 LEFT JOIN t_site ON historique.sit_id = t_site.sit_id
 LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
 LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id
 LEFT JOIN invite ON historique.invite_id = invite.invite_id
 LEFT JOIN t_personne p2 ON invite.invite_par_id = p2.p_id
LEFT JOIN t_type_personnel2 ON historique.ty_id = t_type_personnel2.ty_id AND historique.st_id = t_type_personnel2.st_id
 WHERE invite.invite_par_id like :p_id  AND p1.archive ".$sql1." '1'
 GROUP BY historique.numHistorique

");

         $requete->bindParam(':p_id', $p_id);
         $requete->execute ();
         return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
        }

}


ModelChercheur::set_static ();
?>

