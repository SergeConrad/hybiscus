<?php
/***************************************************************************\
 *  GES UMR : gestion rh pour les umr                                      *
 *                                                                         *
 *  Copyright (c) 2018                                                     *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/
require_once 'config.php';
class Model {
	public static $pdo;
	public static function set_static() {

		$conf=new Conf;
		$host =$conf->getHostname ();
		$dbname = $conf->getDatabase ();
		$login = $conf->getLogin ();
		$pass = $conf->getPassword ();
		
		try {
			// Connexion à la base de données
			// Le dernier argument sert à ce que toutes les chaines de charactères
			// en entrée et sortie de MySql soit dans le codage UTF-8
			self::$pdo = new PDO ( "mysql:host=$host;dbname=$dbname", $login, $pass, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8" 
			) );
			
			// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
			self::$pdo->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $ex ) {
			echo $ex->getMessage ();
			die ( 'Problème lors de la connexion à la base de données' );
		}
	}


// fonction passées en mode objet
 // static function  meta_messageDestinataire() {
//meta_messageDestinataireLaboChimie() {
//  meta_messageDestinataireSalleBlanche() {
// meta_respSec() {
// static function  meta_messageBienvenue($entite) {
        



//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function recuperation_ordis() {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT  o_id,hostname,adresse_ipv4,t_personne.p_nom FROM t_ordinateur LEFT JOIN t_personne ON t_ordinateur.p_id  LIKE t_personne.p_id   ORDER BY INET_ATON(adresse_ipv4)"
                );


                        $rq->execute ();
                        return $rq;

}






//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  ressourcesMessageListe() {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT * FROM `ressourcesMessage`"
                );


                        $rq->execute ();
                        return $rq;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

  static function   ressourcesMessageListeProjetSoumis() {
//serge
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT * FROM `ressourcesMessage`"
                );


                        $rq->execute ();
                        return $rq;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  ressourcesMessageListePJ() {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT * FROM `ressourcesMessagePJ`"
                );


                        $rq->execute ();
                        return $rq;

}

	
//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  messageDestinataire($rid) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT mail FROM `ressourcesMessage` WHERE rid LIKE :rid"
                );
 		$rq->bindParam ( ':rid', $rid );


               $rq->execute ();
		$f = $rq ->fetchObject();
	          $mail = $f->mail;
                        return $mail;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//  static function  meta_messageDestinataire() {
//              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

//                $rq = Model::$pdo->prepare (
//                         "SELECT `valeur` FROM `meta` WHERE nom LIKE 'messageTo'"
//                );


//                        $rq->execute ();
//$f = $rq ->fetchObject();
//          $alea = $f->valeur;
//                        return $alea;

//}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//  static function  meta_messageDestinataireLaboChimie() {
//              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

//                $rq = Model::$pdo->prepare (
//                         "SELECT `valeur` FROM `meta` WHERE nom LIKE 'messageToLaboChimie'"
//                );


//                        $rq->execute ();
//$f = $rq ->fetchObject();
//          $alea = $f->valeur;
//                        return $alea;

//}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//  static function  meta_messageDestinataireSalleBlanche() {
//              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

//                $rq = Model::$pdo->prepare (
//                         "SELECT `valeur` FROM `meta` WHERE nom LIKE 'messageToSalleBlanche'"
//                );


//                        $rq->execute ();
//$f = $rq ->fetchObject();
//          $alea = $f->valeur;
//                        return $alea;

//}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  meta_respSec() {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT `valeur` FROM `meta` WHERE nom LIKE 'respSec'"
                );


                        $rq->execute ();
$f = $rq ->fetchObject();
          $alea = $f->valeur;
                        return $alea;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  meta_projetSoumis() {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT `valeur` FROM `meta` WHERE nom LIKE 'projetSoumis'"
                );


                        $rq->execute ();
$f = $rq ->fetchObject();
          $alea = $f->valeur;
                        return $alea;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  meta_projetAccepte() {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT `valeur` FROM `meta` WHERE nom LIKE 'projetAccepte'"
                );


                        $rq->execute ();
$f = $rq ->fetchObject();
          $alea = $f->valeur;
                        return $alea;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  meta_listePJ($rid) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT * FROM ressourcesMessagePJ WHERE rid LIKE :rid"
                );
 		$rq->bindParam ( ':rid', $rid );


                        $rq->execute ();
                        return $rq;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  meta_messageBienvenue($rid) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT `message` FROM `ressourcesMessage` WHERE rid LIKE :rid"
                );
                $rq->bindParam ( ':rid', $rid );


                        $rq->execute ();
			$message = $rq ->fetchObject();
                        return $message->message;

}

   // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function daoMessageToModifie($messageTo) {
                // utilisé dans modification personne
                try {

// $ajout_photo = self::$pdo->prepare ("INSERT INTO meta(nom,valeur) VALUES('messageTo', :valeur) ON DUPLICATE KEY UPDATE valeur=:valeur");
$nom ='messageTo';
 $del = self::$pdo->prepare ("DELETE FROM meta  WHERE nom LIKE :nom ");
 $del->bindParam ( ':nom', $nom );
 $del->execute ();

 $ajout_photo = self::$pdo->prepare ("INSERT INTO meta(nom,valeur) VALUES(:nom, :valeur) ");


                        $ajout_photo->bindParam ( ':nom', $nom );
                        $ajout_photo->bindParam ( ':valeur', $messageTo );
                        $ajout_photo->execute ();
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }
        }

  // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function daoMessageToRessourceModifie($rid,$mail,$message) {
                // utilisé dans modification personne
                try {

//echo 't'.$nom.' ' .$mail. ' ' .$message;die;
 $del = self::$pdo->prepare ("
	UPDATE ressourcesMessage
	SET mail=:mail, message=:message
	WHERE rid=:rid  
");
//INSERT INTO ressourcesMessage(nom,mail,message) VALUES(:nom, :mail, :message) ON DUPLICATE KEY UPDATE mail= :mail, message = :message");
 $del->bindParam ( ':rid', $rid );
 $del->bindParam ( ':mail', $mail );
 $del->bindParam ( ':message', $message );
 $del->execute ();

                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }
        }



   // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        static function daoMessageToLaboChimieModifie($messageTo) {
//                // utilisé dans modification personne
//                try {

//$nom ='messageToLaboChimie';
// $del = self::$pdo->prepare ("DELETE FROM meta  WHERE nom LIKE :nom ");
// $del->bindParam ( ':nom', $nom );
// $del->execute ();

// $ajout_photo = self::$pdo->prepare ("INSERT INTO meta(nom,valeur) VALUES(:nom, :valeur) ");


//                        $ajout_photo->bindParam ( ':nom', $nom );
//                        $ajout_photo->bindParam ( ':valeur', $messageTo );
//                        $ajout_photo->execute ();
//                } catch ( PDOException $e ) {
//                        echo 'Error ' . $e->getMessage ();
//                }
//        }

   // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        static function daoMessageToSalleBlancheModifie($messageTo) {
//                // utilisé dans modification personne
//                try {

//$nom ='messageToSalleBlanche';
// $del = self::$pdo->prepare ("DELETE FROM meta  WHERE nom LIKE :nom ");
// $del->bindParam ( ':nom', $nom );
// $del->execute ();

// $ajout_photo = self::$pdo->prepare ("INSERT INTO meta(nom,valeur) VALUES(:nom, :valeur) ");


  //                      $ajout_photo->bindParam ( ':nom', $nom );
//                        $ajout_photo->bindParam ( ':valeur', $messageTo );
//                       $ajout_photo->execute ();
//                } catch ( PDOException $e ) {
//                        echo 'Error ' . $e->getMessage ();
//                }
//        }

   // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function daoMessageRespSec($respSec) {
                // utilisé dans modification personne
                try {
 $nom ='respSec';
 $del = self::$pdo->prepare ("DELETE FROM meta  WHERE nom LIKE :nom ");
 $del->bindParam ( ':nom', $nom );
 $del->execute ();

 $ajout_photo = self::$pdo->prepare ("INSERT INTO meta(nom,valeur) VALUES('respSec', :valeur) ");

                        $ajout_photo->bindParam ( ':valeur', $respSec );
                        $ajout_photo->execute ();
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }
        }

   // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function daoMessageProjetSoumis($projetSoumis) {
                // utilisé dans modification personne
                try {
 $nom ='projetSoumis';
 $del = self::$pdo->prepare ("DELETE FROM meta  WHERE nom LIKE :nom ");
 $del->bindParam ( ':nom', $nom );
 $del->execute ();

 $ajout_photo = self::$pdo->prepare ("INSERT INTO meta(nom,valeur) VALUES('projetSoumis', :valeur) ");

                        $ajout_photo->bindParam ( ':valeur', $projetSoumis );
                        $ajout_photo->execute ();
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }
        }

   // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function daoMessageProjetAccepte($projetAccepte) {
                // utilisé dans modification personne
                try {
 $nom ='projetAccepte';
 $del = self::$pdo->prepare ("DELETE FROM meta  WHERE nom LIKE :nom ");
 $del->bindParam ( ':nom', $nom );
 $del->execute ();

 $ajout_photo = self::$pdo->prepare ("INSERT INTO meta(nom,valeur) VALUES('projetAccepte', :valeur) ");

                        $ajout_photo->bindParam ( ':valeur', $projetAccepte );
                        $ajout_photo->execute ();
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }
        }


   // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function daoMessageBienvenueModifie($mess,$entite) {
                try {
 $del = self::$pdo->prepare ("DELETE FROM meta  WHERE nom LIKE :nom ");
 $del->bindParam ( ':nom', $entite );
 $del->execute ();

 $ajout_photo = self::$pdo->prepare ("INSERT INTO meta(nom,valeur) VALUES(:nom, :valeur) ");

                        $ajout_photo->bindParam ( ':nom', $entite);
                        $ajout_photo->bindParam ( ':valeur', $mess);
                        $ajout_photo->execute ();
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }
        }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  daoAuthDroits($user) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT * FROM droitsadmin WHERE user LIKE :user"
                );
		$rq->bindParam ( ':user', $user );
                $rq->execute ();

                        return $rq;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  daoDroitsadminListe() {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "SELECT * FROM droitsadmin "
                );
                $rq->execute ();

                        return $rq;

}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  daoAuthRespEquip($resp_id) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                         "
			SELECT eq_id,eq_nom FROM t_equipe,historique,t_equipe_responsable
			WHERE t_equipe.eq_id like t_equipe_responsable.equipe_id
			AND t_equipe_responsable.numHistorique like historique.numHistorique
			AND historique.p_id like :resp_id
				
			"
                );
                $rq->bindParam ( ':resp_id', $resp_id );
                $rq->execute ();

                        return $rq;

}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  daoAuthId($user) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

		#if (str_contains($user, '@umontpellier.fr')) {
		if (strpos($user, '@umontpellier.fr') !== false) {
			$rq = Model::$pdo->prepare ("SELECT p_id FROM t_personne WHERE p_mail LIKE :user");
		}
                else $rq = Model::$pdo->prepare ("SELECT p_id FROM t_personne WHERE loginspip LIKE :user");
                $rq->bindParam ( ':user', $user );
                $rq->execute ();

		$f = $rq ->fetchObject();
                  $p_id = $f->p_id;
                        return $p_id;


}





//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  daoAuthIdCas($mail) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare (
                        "SELECT p_id FROM t_personne WHERE p_mail LIKE :mail"
                );
                $rq->bindParam ( ':mail', $mail );
                $rq->execute ();

                        return $rq;

}




	
	

	// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	static function recuperation_piece_noms_et_ids($id_site) {
//		try {
		// utilisé dans modification personne
			
//			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
// V4 avec le site
//			$requeteSelectpiece = self::$pdo->prepare ( "SELECT t_piece.pi_id,t_piece.pi_numero,t_piece.pi_nb_places_dispo,count(t_personne.pi_id) AS occupe FROM t_piece LEFT JOIN t_personne ON t_piece.pi_id = t_personne.pi_id WHERE t_piece.sit_id = :sit_id GROUP BY t_piece.pi_id ORDER BY t_piece.pi_numero" );
//			$requeteSelectpiece->bindParam ( ':sit_id', $id_site );
//			$requeteSelectpiece->execute ();
			
//			return $requeteSelectpiece;
//		} catch ( PDOException $e ) {
//			echo 'Error :' . $e->getMessage ();
//			die;
//		}
//	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static function recuperation_site_placedispo_pour_piece($id_piece) {
		// utilisé dans modification personne
		try {
			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
//			$requeteSelectChampsPers = self::$pdo->prepare ( "SELECT * FROM t_piece WHERE pi_id = :pi_id" );

			$requeteSelectChampsPers = self::$pdo->prepare ( "SELECT t_piece.pi_numero,t_piece.pi_nb_places_dispo, count(*) AS occupe, t_piece.sit_id
                  FROM t_piece,t_personne WHERE t_piece.pi_id like t_personne.pi_id AND t_piece.pi_id = :pi_id");



			$requeteSelectChampsPers->bindParam(':pi_id', $id_piece);
			$requeteSelectChampsPers->execute ();
			$objetPiece = $requeteSelectChampsPers->fetchObject ();
			return $objetPiece;
		} catch ( PDOException $e ) {
			echo 'Error' . $e->getMessage ();
		}

	}
 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function req_sql($req) {
                try {

        // personne non archivée
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                        $requeteSelectsite = self::$pdo->prepare ( $req);

                        $requeteSelectsite->execute ();

                        return $requeteSelectsite;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_equipe() {
                try {

        // personne non archivée
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


	         $requeteSelect = self::$pdo->prepare ( "SELECT  eq_id,eq_nom,eq_nom_long FROM t_equipe ORDER BY eq_verticale DESC, eq_nom" );


                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_equipe_resp($p_id) {
                try {

        // personne non archivée
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                $requeteSelect = self::$pdo->prepare ( "SELECT  eq_id,eq_nom,eq_nom_long FROM t_equipe
							WHERE historique.p_id  LIKE :p_id
							AND historique.numHistorique LIKE t_equipe.resp_id

" );
 		$requeteSelect->bindParam(':p_id', $p_id);

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }


 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_equipe_recherche() {
                try {

        // personne non archivée
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                $requeteSelect = self::$pdo->prepare ( "SELECT  eq_id,eq_nom,eq_nom_long FROM t_equipe WHERE eq_verticale LIKE '1'" );

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

    // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_personne() {
                try {

	// personne non archivée
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                        $requeteSelect = self::$pdo->prepare ( "
                                SELECT *
                       FROM t_personne
			WHERE archive NOT  LIKE 1
			ORDER BY p_nom" );
	
                        $requeteSelect->execute ();
//			WHERE st_id NOT LIKE 2
//			AND ty_id NOT LIKE 7

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

	// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static function recuperation_ecole_doctorale() {
		try {
			
			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
			
			$requeteSelectsite = self::$pdo->prepare ( "
				SELECT ed_id, ed_nom 
			FROM t_ecole_doctorale" );
			$requeteSelectsite->execute ();
			
			return $requeteSelectsite;
		} catch ( PDOException $e ) {
			echo 'Error :' . $e->getMessage ();
		}
	}
	// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static function recuperation_site_noms_et_ids() {
		// utilisé dans modification personne
		// utilisé dans ajoute personne
		try {
			
			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
			
			$requeteSelectsite = self::$pdo->prepare ( "
				SELECT sit_id, sit_nom 
			FROM t_site" );
			$requeteSelectsite->execute ();
			
			return $requeteSelectsite;
		} catch ( PDOException $e ) {
			echo 'Error :' . $e->getMessage ();
		}
	}
	


	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static function generer_aliasv2 ($liste) {
//serge

		try {

# t_id tutelle (ex 1 IRD 2 cnrs  4 UM...)
# ty_id type personnel (ex 1 chercheur,  3 ita )
# st_id  statut ( 1 permament 2 archivé ...}


// On cherche si il existe une équipe correspondant au nom de la liste
 $reqsql="SELECT * FROM t_equipe WHERE eq_nom LIKE :eq_nom";
 $requeteSelect = self::$pdo->prepare ( $reqsql );
 $requeteSelect->bindParam(':eq_nom', $liste);
 $succes = $requeteSelect->execute (); // returns true on succes


if ($requeteSelect->rowCount() > 0) {
  // Il y a une équipe du nom passé en paramètre de la fonction
	$row = $requeteSelect->fetch ( PDO::FETCH_ASSOC );
	$reqsql = 
         " SELECT p_mail,p_nom, p_prenom
                                FROM t_personne,historique,t_membres_equipe2
                                 WHERE t_personne.p_id LIKE historique.p_id AND historique.numHistorique LIKE t_membres_equipe2.numHistorique  AND t_membres_equipe2.eq_id LIKE '".$row['eq_id']."'
				AND (NOT(historique.st_id = 3 AND historique.ty_id= 2))
                                 AND dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )
				AND archive NOT LIKE '1'
				AND demandeIntranet NOT LIKE '1'

                                 ORDER BY p_nom";
//	$reqsql= "SELECT p_mail,p_nom,p_prenom FROM t_personne,t_membres_equipe WHERE t_personne.p_id LIKE t_membres_equipe.p_id AND eq_id LIKE '".$row['eq_id']."'";
} else {


if (($liste=="d") || ($liste=="d-archive") ||($liste=="s") || ($liste=="a")) {
	switch ($liste) {
	    case "d": $where=" AND ty_id LIKE '1' AND  st_id  LIKE '3' AND archive NOT LIKE '1'";break;
	    case "d-archive": $where=" AND ty_id LIKE '1' AND  st_id  LIKE '3' AND archive LIKE '1'";break;
	    case "s": $where=" AND ty_id LIKE '2' AND  st_id  LIKE '3' AND ARCHIVE NOT LIKE '1'";break;
	    case "a": $where=" AND   st_id  LIKE '2' AND ty_id NOT LIKE '2' AND ARCHIVE NOT LIKE '1'";break;
	 } 
			 $reqsql =
	        	 " SELECT p_mail,p_nom, p_prenom
                                FROM t_personne,historique
                                 WHERE t_personne.p_id LIKE historique.p_id 
                                 AND dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )
				".$where." 

                                 ORDER BY p_nom";

}
else if (($liste=="um-i") || ($liste=="cnrs-i") || ($liste=="ird-i") || ($liste== "imt-i")) {
	switch ($liste) {
//	    case "um-i": $where=" AND t_id LIKE '4' AND ((ty_id LIKE '4' AND  st_id LIKE '1') OR (ty_id LIKE '2' AND st_id LIKE '2'))";break;
	    case "um-i": $tutelle ="4";break;

//	    case "cnrs-i": $where=" AND t_id LIKE '2' AND ((ty_id LIKE '4' AND  st_id LIKE '1') OR (ty_id LIKE '2' AND st_id LIKE '2'))";break;
	    case "cnrs-i": $tutelle ="2";break;

//	    case "ird-i": $where=" AND t_id LIKE '1' AND ((ty_id LIKE '4' AND  st_id LIKE '1') OR (ty_id LIKE '2' AND st_id LIKE '2'))";break;
	    case "ird-i": $tutelle ="1";break;

//	    case "imt-i": $where=" AND t_id LIKE '11' AND ((ty_id LIKE '4' AND  st_id LIKE '1') OR (ty_id LIKE '2' AND st_id LIKE '2'))";break;
	    case "imt-i": $tutelle ="11";break;
	 } 
//			 $reqsql =
//	        	 " SELECT p_mail,p_nom, p_prenom
//                                FROM t_personne,historique,permanent
//                                 WHERE t_personne.p_id LIKE historique.p_id 
//				 AND historique.permanent_id LIKE permanent.permanent_id
//                                 AND dateDebutH = (
//                                                SELECT MAX(dateDebutH)
//                                                FROM historique
//                                                WHERE t_personne.p_id LIKE historique.p_id
//                                        )
//                                AND archive NOT LIKE '1'
//				".$where." 
//                                 ORDER BY p_nom";

			 $reqsql ="
SELECT archive,t.p_id,p_mail,p_nom, p_prenom  FROM t_personne t
RIGHT JOIN (
	SELECT h2.p_id 
	from historique h2, contractuel c 
	WHERE h2.contractuel_id like c.contractuel_id
	 AND c.contractuel_t_id like '".$tutelle."'
	 AND h2.ty_id like '2'
	 AND h2.st_id LIKE '2'
	     AND dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE h2.p_id LIKE historique.p_id
                                        )
	 ) y ON t.p_id like y.p_id
WHERE t.archive not like '1'
UNION
SELECT archive,t.p_id,p_mail,p_nom, p_prenom  FROM t_personne t
RIGHT JOIN (SELECT h.p_id from historique h ,permanent p WHERE h.permanent_id like p.permanent_id  AND p.t_id like '".$tutelle."' AND h.ty_id LIKE '4' AND  h.st_id LIKE '1') x ON t.p_id like x.p_id
WHERE t.archive not like '1'
";

}
else {
	switch ($liste) {
	    case "um-c": $where=" AND t_id LIKE '4' AND (ty_id LIKE '1' OR ty_id LIKE '2' OR ty_id LIKE '3' OR ty_id LIKE '5') AND  st_id LIKE '1'";break;
	    case "cnrs-c": $where=" AND t_id LIKE '2' AND (ty_id LIKE '1' OR ty_id LIKE '2' OR ty_id LIKE '3' OR ty_id LIKE '5') AND  st_id LIKE '1'";break;
	    case "ird-c": $where=" AND t_id LIKE '1' AND (ty_id LIKE '1' OR ty_id LIKE '2' OR ty_id LIKE '3' OR ty_id LIKE '5') AND  st_id LIKE '1'";break;
	    case "imt-c": $where=" AND t_id LIKE '11' AND (ty_id LIKE '1' OR ty_id LIKE '2' OR ty_id LIKE '3' OR ty_id LIKE '5') AND  st_id LIKE '1'";break;
	}
			 $reqsql =
	        	 " SELECT p_mail,p_nom, p_prenom
                                FROM t_personne,historique,permanent
                                 WHERE t_personne.p_id LIKE historique.p_id 
				 AND historique.permanent_id LIKE permanent.permanent_id
                                 AND dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )
                                AND archive NOT LIKE '1'
				".$where." 
                                 ORDER BY p_nom";
}
}


			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
				
			$requeteDoctorants = self::$pdo->prepare ($reqsql );
			$requeteDoctorants->execute ();
			$retour="";	
  			foreach ( $requeteDoctorants as $row ) {
			  $retour=$retour.$row->p_mail.' '.$row->p_nom.' '.$row->p_prenom.'<br>';
	              	}


			return $retour;
		} catch ( PDOException $e ) {
			echo 'Error :' . $e->getMessage ();
			die;
		}
	}


	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static function recuperation_site_noms_ids_sans_etranger_hebergement() {
		// utilisé dans modification personne
		// utilisé dans ModificationPiece.php
		try {
				
			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
				
			$requeteSelectsite = self::$pdo->prepare ( "
				SELECT sit_id, sit_nom
			FROM t_site WHERE sit_id NOT LIKE '1'" );
			//FROM t_site WHERE sit_nom  = 'Faculté de pharmacie' OR sit_nom = 'Triolet'" );
			$requeteSelectsite->execute ();
				
			return $requeteSelectsite;
		} catch ( PDOException $e ) {
			echo 'Error :' . $e->getMessage ();
		}
	}
	
	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static function recuperation_tutelles_noms_et_ids() {
		// utilisé dans modification personne
		// utilisé dans ajoute personne
		try {
			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
			
			$requeteSelectTutelle = self::$pdo->prepare ( "
   			SELECT t_nom, t_id
   			FROM t_tutelle
   			" );
			$requeteSelectTutelle->execute ();
			
			return $requeteSelectTutelle;
		} catch ( PDOException $e ) {
			echo 'Error ' . $e->getMessage ();
		}
	}
	
	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static function recuperation_types_personnel_noms_et_ids() {
		// utilisé dans modification personne
		// utilisé dans ajoute personne
		try {
			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
			
			$requeteSelectTypePersonnel = self::$pdo->prepare ( "
				SELECT st_id,ty_id, ty_nom
				FROM t_type_personnel2
				ORDER by ty_nom" );
			
			$requeteSelectTypePersonnel->execute ();
			
			return $requeteSelectTypePersonnel;
		} catch ( PDOException $e ) {
			echo 'Error ' . $e->getMessage ();
		}
	}

       // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_formation() {
                try {
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                        $requeteSelectTypePersonnel = self::$pdo->prepare ( "
                                SELECT * 
                                FROM t_formation_stagiaire
                                ORDER BY intitule" );

                        $requeteSelectTypePersonnel->execute ();

                        return $requeteSelectTypePersonnel;
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }
        }

	
  // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_corps() {
                try {
                       // self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );

                        $requeteSelect = self::$pdo->prepare ( "
                                SELECT *
                                FROM t_corps
                                " );
				//WHERE ty_id = :ty_id
                	//$requeteSelect->bindParam(':ty_id', $ty_id);

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }
        }



//////////////////////////////////////////////////////////////////////////////////
       static function daoPersonneAlerteArchive($jour) {

                try {
                //self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
$date = date("Y-m-d");
$date = date('Y-m-d', strtotime($date. ' + '.$jour.' days'));


$requeteSelect = self::$pdo->prepare("
		SELECT  p1.p_id,p1.p_nom, p1.p_prenom,p1.numeroCle,historique.st_id,historique.ty_id,eq_nom,p3.p_mail AS mailEncadrant,p4.p_mail AS mailSuperieur, p5.p_mail AS mailInvitant,p6.p_mail AS mailDirecteur1, p7.p_mail AS mailDirecteur2 
                FROM t_personne AS p1
                LEFT JOIN historique ON p1.p_id = historique.p_id
		LEFT JOIN t_membres_equipe2 ON historique.numHistorique = t_membres_equipe2.numHistorique
		LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id

                LEFT JOIN stage ON p1.p_id LIKE stage.p_id
                LEFT JOIN t_personne AS p3 ON stage.p_stag_encadrant LIKE p3.p_id
		
		LEFT JOIN contractuel ON p1.p_id LIKE contractuel.contractuel_id
                LEFT JOIN t_personne AS p4 ON contractuel.contractuel_sup_id LIKE p4.p_id

		LEFT JOIN invite ON p1.p_id LIKE invite.invite_id
                LEFT JOIN t_personne AS p5 ON invite.invite_par_id LIKE p5.p_id

		LEFT JOIN theses ON p1.p_id LIKE theses.p_id
                LEFT JOIN t_personne AS p6 ON theses.p_these_directeur1 LIKE p6.p_id
                LEFT JOIN t_personne AS p7 ON theses.p_these_directeur2 LIKE p7.p_id

                WHERE p1.archive NOT LIKE '1' 
                AND
                        dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE p1.p_id LIKE historique.p_id
                                        )
		AND historique.dateFinH = :date
                GROUP BY p_id ORDER BY p_nom");

		//AND historique.dateFinH NOT LIKE '0000-00-00' 


               // $requeteSelect = self::$pdo->prepare("SELECT *  FROM t_personne WHERE p_date_depart = :date");
                $requeteSelect->bindParam(':date', $date);
                $requeteSelect->execute();
return $requeteSelect;

                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

//////////////////////////////////////////////////////////////////////////////////
       static function daoStagiaireEncadrant($id) {

                try {
                self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                $requeteSelect = self::$pdo->prepare("
						SELECT p3.p_nom,p3.p_prenom, p3.p_mail  FROM t_personne AS p1
						LEFT JOIN historique ON p1.p_id LIKE historique.p_id
						LEFT JOIN stage ON historique.stageId LIKE stage.stageId
						LEFT JOIN t_personne AS p3 ON stage.p_stag_encadrant LIKE p3.p_id
						WHERE p1.archive NOT LIKE '1'
  						AND dateDebutH = (
	                                                SELECT MAX(dateDebutH)
	                                                FROM historique
	                                                WHERE p1.p_id LIKE historique.p_id
        	                                )
						AND p1.p_id LIKE :id
						 ");
                $requeteSelect->bindParam(':id', $id);
                $requeteSelect->execute();




return $requeteSelect;

                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}


//////////////////////////////////////////////////////////////////////////////////
       static function daoDoctorantDirecteur($id) {

                try {
                self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                $requeteSelect = self::$pdo->prepare("
                                                SELECT  p2.p_mail , p3.p_mail AS p_mail2 
						FROM t_personne AS p1
						LEFT JOIN historique ON p1.p_id LIKE historique.p_id
                                                LEFT JOIN theses ON historique.theseId LIKE theses.theseId
                                                LEFT JOIN t_personne AS p2 ON theses.p_these_directeur1 LIKE p2.p_id
                                                LEFT JOIN t_personne AS p3 ON theses.p_these_directeur2 LIKE p3.p_id
                                                WHERE p1.archive NOT LIKE '1'
  						AND dateDebutH = (
	                                                SELECT MAX(dateDebutH)
	                                                FROM historique
	                                                WHERE p1.p_id LIKE historique.p_id
        	                                )
                                                AND p1.p_id LIKE :id
                                                 ");
                $requeteSelect->bindParam(':id', $id);
                $requeteSelect->execute();
return $requeteSelect;

                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

//////////////////////////////////////////////////////////////////////////////////
       static function daoStagiaireFormationSecuriteNonFaite() {

                try {
                self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                $requeteSelect = self::$pdo->prepare("
						SELECT p1.p_nom,p1.p_prenom, p3.p_mail AS mailEncadrant FROM t_personne AS p1
						LEFT JOIN stage ON p1.p_id LIKE stage.p_id
						LEFT JOIN t_personne AS p3 ON stage.p_stag_encadrant LIKE p3.p_id
						WHERE p1.st_id NOT LIKE '2'
						AND p1.ty_id LIKE '7'
						AND p1.formationSecurite LIKE '0'
						 ");
                $requeteSelect->execute();
return $requeteSelect;

                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

//////////////////////////////////////////////////////////////////////////////////
       static function daoDoctorantFormationSecuriteNonFaite() {

                try {
                self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                $requeteSelect = self::$pdo->prepare("
                                                SELECT p1.p_nom,p1.p_prenom, p2.p_mail AS mailDirecteur1, p3.p_mail AS mailDirecteur2 
						FROM t_personne AS p1
                                                LEFT JOIN theses ON p1.p_id LIKE theses.p_id
                                                LEFT JOIN t_personne AS p2 ON theses.p_these_directeur1 LIKE p2.p_id
                                                LEFT JOIN t_personne AS p3 ON theses.p_these_directeur2 LIKE p3.p_id
                                                WHERE p1.st_id NOT LIKE '2'
                                                AND p1.ty_id LIKE '5'
                                                AND p1.formationSecurite LIKE '0'
                                                 ");
                $requeteSelect->execute();
return $requeteSelect;

                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

//////////////////////////////////////////////////////////////////////////////////
       static function daoPersonnelFormationSecuriteNonFaite() {

                try {
                self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                $requeteSelect = self::$pdo->prepare("
                                                SELECT p1.p_nom,p1.p_prenom 
						FROM t_personne AS p1
                                                WHERE p1.st_id NOT LIKE '2'
                                                AND p1.ty_id NOT LIKE '5'
                                                AND p1.ty_id NOT LIKE '7'
                                                AND p1.formationSecurite LIKE '0'
                                                 ");
                $requeteSelect->execute();
return $requeteSelect;

                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

//////////////////////////////////////////////////////////////////////////////////
       static function daoPersonneAnonymisation($mois) {

                try {
                self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
$date = date("Y-m-d");
$date = date('Y-m-d', strtotime($date. ' - '.$mois.' months'));
                $requeteSelect = self::$pdo->prepare("SELECT *  FROM t_personne WHERE p_date_depart <= :date AND p_date_depart NOT LIKE '0000-00-00' ");
                $requeteSelect->bindParam(':date', $date);
                $requeteSelect->execute();
return $requeteSelect;

                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

//////////////////////////////////////////////////////////////////////////////////
       static function daoEquipeTableau($id) {
  // utilisé dans modification personne

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
	        $requeteSelect = self::$pdo->prepare("SELECT *  FROM t_equipe WHERE t_equipe.eq_id = :eq_id");
                $requeteSelect->bindParam(':eq_id', $id);
                $requeteSelect->execute();
                $tableau = $requeteSelect->fetch();
                return $tableau;

                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}
	


	static function daoPersonneType($id_personne) {
                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                       $requete = self::$pdo->prepare ( "
				SELECT ty_id FROM `t_personne`,historique
				WHERE t_personne.p_id like :p_id
				AND  historique.p_id like t_personne.p_id
				AND dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )");
                        $requete->bindParam(':p_id', $id_personne);
                        $requete->execute ();
                        $objetPersonne = $requete->fetch ();
                        return $objetPersonne['ty_id'];
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
	}




	static function daoPersonneTableau($id_personne) {
  // utilisé dans modification personne

                try {
          //             self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelectChampsPers = self::$pdo->prepare ( "SELECT * FROM t_personne WHERE  t_personne.p_id = :p_id" );
                        $requeteSelectChampsPers->bindParam(':p_id', $id_personne);
                        $requeteSelectChampsPers->execute ();
                        $objetPersonne = $requeteSelectChampsPers->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

     static function daoInviteListe($p_id) {
                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $rq = self::$pdo->prepare ( "SELECT * FROM `historique` 
				RIGHT JOIN invite ON historique.invite_id = invite.invite_id
				WHERE historique.p_id = :p_id");
                        $rq->bindParam(':p_id', $p_id);
                        $rq->execute ();
                        return $rq;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
}

     static function daoPermanentListe($p_id) {
                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $rq = self::$pdo->prepare ( "SELECT * FROM `historique` 
				RIGHT JOIN permanent ON historique.permanent_id = permanent.permanent_id
				WHERE historique.p_id = :p_id");
                        $rq->bindParam(':p_id', $p_id);
                        $rq->execute ();
                        return $rq;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
}
     static function daoContratListe($p_id) {
                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $rq = self::$pdo->prepare ( "SELECT * FROM `historique` 
				RIGHT JOIN contractuel ON historique.contractuel_id = contractuel.contractuel_id
				WHERE historique.p_id = :p_id");
                        $rq->bindParam(':p_id', $p_id);
                        $rq->execute ();
                        return $rq;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
}

     static function daoStageListe($p_id) {
                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        //$rq = self::$pdo->prepare ( "select * from stage,historique where stage.stageId like historique.stageId and historique.p_id LIKE :p_id");
                        $rq = self::$pdo->prepare ( "SELECT * FROM `historique` 
				RIGHT JOIN stage ON historique.stageId = stage.stageId
				WHERE historique.p_id = :p_id");
                        $rq->bindParam(':p_id', $p_id);
                        $rq->execute ();
                        return $rq;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
}


     static function daoStageTableau($id) {
                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelectChampsPers = self::$pdo->prepare ( "SELECT *  FROM stage WHERE stageId= :stageId " );
                        $requeteSelectChampsPers->bindParam(':stageId', $id);
                        $requeteSelectChampsPers->execute ();
                        $objetPersonne = $requeteSelectChampsPers->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
}

     static function daoContractuelTableau($id) {
                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelectChampsPers = self::$pdo->prepare ( "SELECT *  FROM contractuel WHERE contractuel_id= :id " );
                        $requeteSelectChampsPers->bindParam(':id', $id);
                        $requeteSelectChampsPers->execute ();
                        $objetPersonne = $requeteSelectChampsPers->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
}
     static function daoInviteTableau($id) {
                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelectChampsPers = self::$pdo->prepare ( "SELECT *  FROM invite WHERE invite_id= :id " );
                        $requeteSelectChampsPers->bindParam(':id', $id);
                        $requeteSelectChampsPers->execute ();
                        $objetPersonne = $requeteSelectChampsPers->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
}

     static function daoPermanentTableau($id) {
                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelectChampsPers = self::$pdo->prepare ( "SELECT *  FROM permanent WHERE permanent_id= :id " );
                        $requeteSelectChampsPers->bindParam(':id', $id);
                        $requeteSelectChampsPers->execute ();
                        $objetPersonne = $requeteSelectChampsPers->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function daoStageFinancement($stageId) {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
#              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = self::$pdo->prepare ("
                         SELECT  * FROM stageFinancement 
                         WHERE stageId = :stageId
                ");
                         $rq->bindParam(':stageId', $stageId);
                        $rq->execute ();

//                         "SELECT  * FROM stageFinancement 
//                         WHERE stageId = :stageId


			//SELECT historique.stageId,financementId,financementCredit,dateDebut,dateFin  FROM `historique`
                      
                          //       RIGHT JOIN stageFinancement ON historique.stageId = stageFinancement.stageId
                          //      WHERE historique.p_id = :p_id
                         // "
                        return $rq;
}






   static function daoTheseTableau($p_id) {

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelectChampsPers = self::$pdo->prepare ( "SELECT *  FROM theses,historique WHERE theses.theseId = historique.theseId AND historique.p_id like :p_id" );
                        $requeteSelectChampsPers->bindParam(':p_id', $p_id);
                        $requeteSelectChampsPers->execute ();
                        $objetPersonne = $requeteSelectChampsPers->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function daoPieceListe($sit_id) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = Model::$pdo->prepare ( 
			"SELECT pi_id, pi_numero FROM t_piece WHERE sit_id = :sit_id ORDER BY t_piece.pi_numero ASC"
                );
               $rq->bindParam ( ':sit_id', $sit_id );




                        $rq->execute ();
                        return $rq;

}




//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function daoPersonneReservation($id_personne) {
          try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
          //             self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );

    		$rq = Model::$pdo->prepare ( "SELECT r_id, r_date_debut, r_date_fin, t_piece.pi_id,pi_numero,sit_nom FROM `t_reservation2`,t_place,t_piece,t_site
                        WHERE p_id LIKE :p_id
                        AND t_reservation2.pl_id LIKE t_place.pl_id
                        AND t_place.pi_id LIKE  t_piece.pi_id
                        AND t_piece.sit_id LIKE t_site.sit_id
                        ORDER BY r_date_debut
		");
               $rq->bindParam ( ':p_id', $id_personne );




                        $rq->execute ();
                        return $rq;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function daoReservationSupprime($id) {
     $rq = Model::$pdo->prepare ( "
			DELETE FROM t_reservation2 WHERE r_id = :r_id 
                ");
               $rq->bindParam ( ':r_id', $id );




                        $rq->execute ();
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function daoPlaceLibreListe($sit_id,$r_date_debut,$r_date_fin) {
          try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

       $reqsql="SELECT count(pl_id) as dispo,t_piece.pi_id, pi_numero FROM t_place, t_piece WHERE t_place.pi_id LIKE t_piece.pi_id AND t_piece.sit_id LIKE :site
                               AND pl_id NOT IN (
                                   SELECT t_reservation2.pl_id
                                     FROM t_reservation2
                                     WHERE ( t_reservation2.r_date_debut < :dateFin AND t_reservation2.r_date_fin > :dateDebut)
                             )
                             GROUP BY pi_numero";
                         $rq= Model::$pdo->prepare ( $reqsql );
                         $rq->bindParam(':site', $sit_id);
                            if ($r_date_fin == null) $varfin= '9999-12-31';
                             else $varfin= $r_date_fin;
                             $rq->bindParam(':dateFin', $varfin);
    
                            $rq->bindParam(':dateDebut', $r_date_debut);
    

                        $rq->execute ();
                        return $rq;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
       static function daoTypeGetTableau($st_id,$ty_id) {

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelect = self::$pdo->prepare ( "SELECT st_id,ty_id ,ty_nom FROM t_type_personnel2 WHERE st_id = :st_id AND ty_id = :ty_id" );
                        $requeteSelect->bindParam(':st_id', $st_id);
                        $requeteSelect->bindParam(':ty_id', $ty_id);
                        $requeteSelect->execute ();
                        $objetPersonne = $requeteSelect->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}
       static function daoSiteGetTableau($id) {

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelect = self::$pdo->prepare ( "SELECT sit_id ,sit_nom FROM t_site WHERE sit_id = :sit_id" );
                        $requeteSelect->bindParam(':sit_id', $id);
                        $requeteSelect->execute ();
                        $objetPersonne = $requeteSelect->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}
 static function daoTutelleGetTableau($id) {

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelect = self::$pdo->prepare ( "SELECT t_id ,t_nom FROM t_tutelle WHERE t_id = :t_id" );
                        $requeteSelect->bindParam(':t_id', $id);
                        $requeteSelect->execute ();
                        $objetPersonne = $requeteSelect->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}
static function daoStatutGetTableau($id) {

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelect = self::$pdo->prepare ( "SELECT st_id ,st_nom FROM t_statut2 WHERE st_id = :st_id" );
                        $requeteSelect->bindParam(':st_id', $id);
                        $requeteSelect->execute ();
                        $objetPersonne = $requeteSelect->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}
static function daoCollegeGetTableau($id) {

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelect = self::$pdo->prepare ( "SELECT collegeId ,collegeNom FROM collegeElection WHERE collegeId = :collegeId" );
                        $requeteSelect->bindParam(':collegeId', $id);
                        $requeteSelect->execute ();
                        $objetPersonne = $requeteSelect->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

static function daoListePlaceGet($idpiece) {

	 self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
	 $rq = self::$pdo->prepare ( "SELECT pl_id FROM t_piece,t_place WHERE t_piece.pi_id LIKE t_place.pi_id AND t_piece.pi_id LIKE :pi_id");
         $rq->bindParam ( ':pi_id', $idpiece );
         $rq->execute ();
	return $rq;

}


static function daoPieceGetTableau($id) {

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelect = self::$pdo->prepare ( "SELECT pi_id ,pi_numero,sit_id FROM t_piece WHERE pi_id = :pi_id" );
                        $requeteSelect->bindParam(':pi_id', $id);
                        $requeteSelect->execute ();
                        $objetPersonne = $requeteSelect->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}



	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static function RecupererTTChampsPersonnes($id_personne) {
		// utilisé dans modification personne
		try {
			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
			$requeteSelectChampsPers = self::$pdo->prepare ( "SELECT * FROM t_personne WHERE p_id = :p_id" );
			$requeteSelectChampsPers->bindParam(':p_id', $id_personne);
			$requeteSelectChampsPers->execute ();
			$objetPersonne = $requeteSelectChampsPers->fetchObject ();
			return $objetPersonne;
		} catch ( PDOException $e ) {
			echo 'Error' . $e->getMessage ();
		}
	}
	

	
	


	
   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        static function daoEquipeResponsables($id) {
                try {
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                        $requeteSelect = self::$pdo->prepare ( "
                                SELECT t_personne.p_id,p_nom, p_prenom,historique.numHistorique
                                FROM t_personne,historique,t_equipe_responsable
                                 WHERE t_personne.p_id LIKE historique.p_id AND historique.numHistorique LIKE t_equipe_responsable.numHistorique  AND t_equipe_responsable.equipe_id LIKE :id
                                 AND dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )
                                AND archive NOT LIKE '1'

                                 ORDER BY p_nom");

                $requeteSelect->bindParam ( ':id', $id );

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }

        }


	
	
	
   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        static function daoEquipeMembres($id) {
                try {
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                        $requeteSelect = self::$pdo->prepare ( "
				SELECT t_personne.p_id,p_nom, p_prenom,historique.numHistorique 
				FROM t_personne,historique,t_membres_equipe2
				 WHERE t_personne.p_id LIKE historique.p_id AND historique.numHistorique LIKE t_membres_equipe2.numHistorique  AND t_membres_equipe2.eq_id LIKE :id 
				 AND dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )
				AND archive NOT LIKE '1'

				 ORDER BY p_nom");

		$requeteSelect->bindParam ( ':id', $id );

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }

        }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        static function daoEquipeNonMembres($id) {
                try {
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
			$today=date("Y-m-d");

                        $requeteSelect = self::$pdo->prepare ( "
  				  SELECT t_personne.p_id,p_nom, p_prenom ,historique.numHistorique FROM t_personne,historique
                                        WHERE t_personne.p_id LIKE historique.p_id
					AND dateDebutH = (
						SELECT MAX(dateDebutH)
						FROM historique
						WHERE t_personne.p_id LIKE historique.p_id
					)
                                   	AND NOT EXISTS (
	                                        SELECT null
	                                        FROM t_membres_equipe2
						WHERE historique.numHistorique LIKE t_membres_equipe2.numHistorique
	                                        AND t_membres_equipe2.eq_id LIKE :id
		                                  )
                                  AND archive NOT LIKE '1'
                                 ORDER BY p_nom");

                $requeteSelect->bindParam ( ':id', $id );

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }

        }



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	static function recuperation_statuts_noms_et_ids() {
		// utilisé dans modification personne
		try {
			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
				
			$requeteSelectStatuts = self::$pdo->prepare ( "
				SELECT st_id, st_nom
				FROM t_statut2
				ORDER by st_nom DESC" );
				
			$requeteSelectStatuts->execute ();
				
			return $requeteSelectStatuts;
		} catch ( PDOException $e ) {
			echo 'Error ' . $e->getMessage ();
		}
		
	}
	
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        static function recuperation_college() {
                // utilisé dans modification personne
                try {
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                        $requeteSelectStatuts = self::$pdo->prepare ( "
                                SELECT collegeId, collegeNom
                                FROM collegeElection
                                ORDER by collegeNom DESC" );

                        $requeteSelectStatuts->execute ();

                        return $requeteSelectStatuts;
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }

        }

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	static function recuperation_statut_dossiers_noms_et_ids() {
		// utilisé dans modification personne
		try {
			self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
		
			$requeteSelectStatuts = self::$pdo->prepare ( "
				SELECT do_id, do_statut
				FROM t_dossier
				ORDER by do_statut" );
		
			$requeteSelectStatuts->execute ();
		
			return $requeteSelectStatuts;
		} catch ( PDOException $e ) {
			echo 'Error ' . $e->getMessage ();
		}
	}
	

	
	// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function update_personne_lienspip($p_id,$loginspip) {
   try {
                $update_personne = self::$pdo->prepare ( "UPDATE t_personne SET loginspip = :loginspip
                                                WHERE p_id = :p_id" );

                $update_personne->bindParam ( ':p_id', $p_id );
                $update_personne->bindParam ( ':loginspip', $loginspip );
          $update_personne->execute ();
        } catch ( PDOException $e ) {
                echo "Error: " . $e->getMessage ();
                die;
        }


}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function update_droits_spip($loginspip,$droit,$checked) {
   try {

 $update_personne = self::$pdo->prepare ("INSERT INTO droitsadmin (user, ".$droit.") VALUES(:user, :checked) ON DUPLICATE KEY UPDATE ".$droit."=:checked");
                $update_personne->bindParam ( ':user', $loginspip );
                $update_personne->bindParam ( ':checked', $checked );
          $update_personne->execute ();
        } catch ( PDOException $e ) {
                echo "Error: " . $e->getMessage ();
                die;
        }


}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function insert_droits_spip($user) {
   try {

 $update_personne = self::$pdo->prepare ("INSERT INTO droitsadmin (user) VALUES(:user)");
                $update_personne->bindParam ( ':user', $user );
          $update_personne->execute ();
        } catch ( PDOException $e ) {
                echo "Error: " . $e->getMessage ();
                die;
        }


}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function archive_cron_personne () {
//serge
   try {

 $update_personne = self::$pdo->prepare ("

   UPDATE t_personne,historique SET t_personne.archive = '1' WHERE
                t_personne.p_id = historique.p_id

                AND
                        dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE t_personne.p_id LIKE historique.p_id
                                        )
                AND historique.dateFinH < CURDATE()
                AND historique.dateFinH not like '0000-00-00'
		AND archive = '0'
		AND ((st_id like '2')OR(st_id like '4')OR(st_id like '3' AND ty_id ='2'))


                ");

//st_id 2 non permanent
//st_id 3 etudiant
    //ty_id 2 stagiaire
//st_id 4 invité


	// Les ((nonpermanent) ou (invités) ou (stagiaires))

          $update_personne->execute ();
        } catch ( PDOException $e ) {
                echo "Error: " . $e->getMessage ();
                die;
        }


}


	
	// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static function update_personne ( $idpers, $prenom, $nom, $dateNaissance,$email, $id_site, $id_type_personnel, $id_tutelle, $tel, $telperso, $website, $commentaire,$nationalite,$personneaprevenir,$horaireacces,  $id_statut , $dateDepart,$id_corps,$demandeIntranet,$chartesSignees,$formationSecurite,$numeroCle,$ficheNouveauArrivant,$sessionInformatique,$contractuel_sup_id,$contractuel_sujet_recherche) {

	// utilisé dans modification personne
	// Si le statut est archivé, on enleve la pièece
	try {
		$myNull = null;	
		$update_personne = self::$pdo->prepare ( "UPDATE t_personne SET p_nom = :p_nom, p_prenom = :p_prenom, dateNaissance = :dateNaissance,
						p_mail = :p_mail, p_tel = :p_tel,p_tel_perso = :p_tel_perso, p_page_perso = :p_page_perso, p_commentaire = :p_commentaire,
						nationalite = :nationalite,
						personneaprevenir = :personneaprevenir,
						horaireacces = :horaireacces,
						sit_id = :sit_id, ty_id = :ty_id, t_id = :t_id, st_id = :st_id,
						p_date_depart = :p_date_depart,
						 permanent_c_id = :permanent_c_id,
						demandeIntranet = :demandeIntranet,
						chartesSignees = :chartesSignees,
						formationSecurite = :formationSecurite,
						numeroCle = :numeroCle,
						ficheNouveauArrivant = :ficheNouveauArrivant ,
						sessionInformatique = :sessionInformatique,
						contractuel_sup_id = :contractuel_sup_id,
						contractuel_sujet_recherche = :contractuel_sujet_recherche
						WHERE p_id = :p_id" );

		$update_personne->bindParam ( ':p_id', $idpers );
		$update_personne->bindParam ( ':p_nom', $nom );
		$update_personne->bindParam ( ':p_prenom', $prenom );


   		if ($dateNaissance != null || $dateNaissance != '') {
				// MODIF DU FORMAT FRANCAIS JJ/MM/AAAA en format mysql AAAA-MM-JJ
				$chdate = explode('/',$dateNaissance);
				$dateNaissance = $chdate[2].'-'.$chdate[1].'-'.$chdate[0];

				$update_personne->bindParam ( ':dateNaissance', $dateNaissance );
		}
                else    $update_personne->bindParam ( ':dateNaissance', $myNull, PDO::PARAM_NULL);

		$update_personne->bindParam ( ':p_mail', $email );

		if ($tel != null || $tel != '')	$update_personne->bindParam ( ':p_tel', $tel );
		else 	$update_personne->bindParam ( ':p_tel', $myNull, PDO::PARAM_NULL);

		if ($telperso != null || $telperso != '')	$update_personne->bindParam ( ':p_tel_perso', $telperso );
		else 	$update_personne->bindParam ( ':p_tel_perso', $myNull, PDO::PARAM_NULL);
		
		if($website != null || $website != '') 	$update_personne->bindParam ( ':p_page_perso', $website );
		else 	$update_personne->bindParam ( ':p_page_perso', $myNull, PDO::PARAM_NULL);

		if($commentaire != null || $commentaire != '') 	$update_personne->bindParam ( ':p_commentaire', $commentaire );
		else 	$update_personne->bindParam ( ':p_commentaire', $myNull, PDO::PARAM_NULL);

		if($nationalite != null || $nationalite != '') 	$update_personne->bindParam ( ':nationalite', $nationalite );
		else 	$update_personne->bindParam ( ':nationalite', $myNull, PDO::PARAM_NULL);


		if($personneaprevenir != null || $personneaprevenir != '') 	$update_personne->bindParam ( ':personneaprevenir', $personneaprevenir );
		else 	$update_personne->bindParam ( ':personneaprevenir', $myNull, PDO::PARAM_NULL);


		if($horaireacces != null || $horaireacces != '') 	$update_personne->bindParam ( ':horaireacces', $horaireacces );
		else 	$update_personne->bindParam ( ':horaireacces', $myNull, PDO::PARAM_NULL);

		$update_personne->bindParam ( ':sit_id', $id_site );
		$update_personne->bindParam ( ':ty_id', $id_type_personnel );
		$update_personne->bindParam ( ':st_id', $id_statut);
		if($id_tutelle != null || $id_tutelle != '') 	$update_personne->bindParam ( ':t_id', $id_tutelle );
		else 	$update_personne->bindParam ( ':t_id', $myNull, PDO::PARAM_NULL);
		

		
		if($dateDepart != null || $dateDepart != '')$update_personne->bindParam(':p_date_depart', $dateDepart);
		else $update_personne->bindParam(':p_date_depart', $myNull, PDO::PARAM_NULL);

		if($id_corps != null || $id_corps != '')$update_personne->bindParam(':permanent_c_id', $id_corps);
		else $update_personne->bindParam(':permanent_c_id', $myNull, PDO::PARAM_NULL);
		$update_personne->bindParam ( ':demandeIntranet', $demandeIntranet);


		$update_personne->bindParam ( ':chartesSignees', $chartesSignees);
		$update_personne->bindParam ( ':formationSecurite', $formationSecurite);
		if($numeroCle != null || $numeroCle != '')$update_personne->bindParam(':numeroCle', $numeroCle);
		else $update_personne->bindParam(':numeroCle', $myNull, PDO::PARAM_NULL);
		$update_personne->bindParam ( ':ficheNouveauArrivant', $ficheNouveauArrivant);
		$update_personne->bindParam ( ':sessionInformatique', $sessionInformatique);


		if($contractuel_sup_id != null || $contractuel_sup_id != '')$update_personne->bindParam(':contractuel_sup_id', $contractuel_sup_id);
		else $update_personne->bindParam(':contractuel_sup_id', $myNull, PDO::PARAM_NULL);

		if($contractuel_sujet_recherche != null || $contractuel_sujet_recherche != '')$update_personne->bindParam(':contractuel_sujet_recherche', $contractuel_sujet_recherche);
		else $update_personne->bindParam(':contractuel_sujet_recherche', $myNull, PDO::PARAM_NULL);
		
		$update_personne->execute ();
	} catch ( PDOException $e ) {
		echo "Error: " . $e->getMessage ();
		die;
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static function ModifReservation($id_resa,  $dateFin) {
try {
 $requeteUpdate = self::$pdo->prepare("UPDATE t_reservation2 SET r_date_fin = :r_date_fin  WHERE r_id = :r_id");
 $requeteUpdate->bindParam(':r_id', $id_resa);
 $requeteUpdate->bindParam(':r_date_fin', $dateFin);
 $requeteUpdate->execute();
} catch ( PDOException $e ) {
                echo "Error: " . $e->getMessage ();
                die;
  }

}


//$statutReservation = Model::insertionReservation2($id_personne, $reservationPlace, $dateDebutResa, $dateFinResa);

static function insertionReservation2($id_personne, $reservationSalle, $dateDebut, $dateFin) {
	// utilisé dans modification personne
try {
		// Les réservations sans date de fin 
                if($dateFin == null) $dateFin='9999-12-31';
		// une salle en paramètre, on réserve la première place dispo
	  	$requeteRecuperationPlace = self::$pdo->prepare ( "SELECT pl_id FROM t_place WHERE pi_id = :pi_id
			 AND pl_id NOT IN (
                              SELECT t_reservation2.pl_id
                                FROM t_reservation2,t_place
                                WHERE t_place.pl_id LIKE t_reservation2.pl_id
				AND t_place.pi_id = :pi_id2
                                AND ( t_reservation2.r_date_debut < :dateFin AND t_reservation2.r_date_fin > :dateDebut)
                        )
		");






                $requeteRecuperationPlace->bindParam(':pi_id', $reservationSalle);
                $requeteRecuperationPlace->bindParam(':pi_id2', $reservationSalle);
                $requeteRecuperationPlace->bindParam(':dateFin', $dateFin);
                $requeteRecuperationPlace->bindParam(':dateDebut', $dateDebut);
                $requeteRecuperationPlace->execute ();




                $place = $requeteRecuperationPlace->fetchColumn ();

		// Reservation de la place 

	$requeteInsert = self::$pdo->prepare ( "INSERT INTO t_reservation2 (r_date_debut, r_date_fin, p_id, pl_id) VALUES (:r_date_debut, :r_date_fin, :p_id, :pl_id) " );
	$requeteInsert->bindParam ( ':r_date_debut', $dateDebut );
	$requeteInsert->bindParam ( ':r_date_fin', $dateFin );
	$requeteInsert->bindParam ( ':p_id', $id_personne );
        $requeteInsert->bindParam ( ':pl_id', $place );
        $requeteInsert->execute ();

} catch ( PDOException $e ) {
                echo "Error: " . $e->getMessage ();
                die;
        }


}

static function insertionReservation($id_personne, $salle_reservation, $dateDebut, $dateFin) {
	// utilisé dans modification personne
	try {
		
		$requeteRecuperationIDPiece = self::$pdo->prepare ( "SELECT pi_id FROM t_piece WHERE pi_numero = :pi_numero" );
		$requeteRecuperationIDPiece->bindParam(':pi_numero', $salle_reservation);
		$requeteRecuperationIDPiece->execute ();
			
		$f = $requeteRecuperationIDPiece->fetchColumn ();
		$pieceID = $f;
		
		
		
		$requetePreventionChevauchement = self::$pdo->prepare("SELECT *
    FROM t_reservation
    WHERE t_reservation.r_date_debut <= :r_date_fin
    AND t_reservation.r_date_fin >= :r_date_debut
    AND t_reservation.p_id = :p_id
    AND t_reservation.pi_id = :pi_id");
		$requetePreventionChevauchement->bindParam(':r_date_fin', $dateFin);
		$requetePreventionChevauchement->bindParam(':r_date_debut', $dateDebut);
		$requetePreventionChevauchement->bindParam(':p_id', $id_personne);
		$requetePreventionChevauchement->bindParam(':pi_id', $pieceID);
		
		$requetePreventionChevauchement->execute();

		
		if ($requetePreventionChevauchement->rowCount()) {
			return '*****Interdiction de chevauchement des réservations 
    		(Le système vérifie que pour une vérification donnée, il n\' y a pas de chevauchement de réservation 
    		pour une même pièce et une même personne données).*****';
		}
		
		
		
		
		
		$decreasePlaceDispo;
	
			
		$requeteSiDejaExistante = self::$pdo->prepare ( "SELECT r_date_debut, r_date_fin, p_id, pi_id FROM t_reservation WHERE r_date_debut = :r_date_debut AND r_date_fin = :r_date_fin AND p_id = :p_id AND pi_id = :pi_id" );
		$requeteSiDejaExistante->bindParam ( ':r_date_debut', $dateDebut );
		$requeteSiDejaExistante->bindParam ( ':r_date_fin', $dateFin );
		$requeteSiDejaExistante->bindParam ( ':p_id', $id_personne );
		$requeteSiDejaExistante->bindParam ( ':pi_id', $pieceID );
		$requeteSiDejaExistante->execute ();
		$resultat_reservation_id = $requeteSiDejaExistante->fetchObject ();
		$reservation_id = $resultat_reservation_id;
			
		if (! empty ( $reservation_id )) {
			return;
		}
			
		$requeteInsertReservation = self::$pdo->prepare ( "INSERT INTO t_reservation (r_date_debut, r_date_fin, p_id, pi_id, r_statut) VALUES (:r_date_debut, :r_date_fin, :p_id, :pi_id, :r_statut) " );
	
		$requeteInsertReservation->bindParam ( ':r_date_debut', $dateDebut );
		if($dateFin != '' | $dateFin != null) {
			
			$requeteInsertReservation->bindParam ( ':r_date_fin', $dateFin );
		} 
		else {
			$dateFinUnlimited = '9999-12-31';
			$requeteInsertReservation->bindParam(':r_date_fin', $dateFinUnlimited);
		}
		
		$today = new DateTime(); // This object represents current date/time
		$today->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
		if ($dateFin != '' | $dateFin != null) {
			$dateFIN_DAT = DateTime::createFromFormat ( 'Y-m-d', $dateFin );
			$dateFIN_DAT->setTime(0,0,0);
		}
		else {
			$dateFIN_DAT = DateTime::createFromFormat('Y-m-d', '9999-12-31');
		}
		
		$dateDEBUT_DAT = DateTime::createFromFormat('Y-m-d', $dateDebut);
		$dateDEBUT_DAT->setTime(0,0,0);	
		

		
		
		
		if($today >= $dateDEBUT_DAT && $dateFIN_DAT >= $today) {
			// si aujourd'hui est égal à la date de début et si date de fin est supérieur ou égale à aujourd'hui, on met en cours
			$statut ='En cours';
			$requeteInsertReservation->bindParam ( ':r_statut', $statut);
			$decreasePlaceDispo = true;
		}
		else if ($today < $dateDEBUT_DAT) {
			// si ajourd'hui est supérieur à la date de début, on met réservé
			$statut ='Réservée';
			$requeteInsertReservation->bindParam(':r_statut', $statut);
			$decreasePlaceDispo = false;
		}
		else if ($today > $dateFIN_DAT) {
			// si la date d'aujourd'hui est supérieur à la date de fin de la réservation
			$statut ='Dépassée';
			$requeteInsertReservation->bindParam(':r_statut', $statut);
			$decreasePlaceDispo = false;
		}
		
		
		$requeteInsertReservation->bindParam ( ':p_id', $id_personne );
		$requeteInsertReservation->bindParam ( ':pi_id', $pieceID );
		$requeteInsertReservation->execute ();
		
		// A décommenter si on utilise pas le trigger de l'insertion de réservation
		/*if ($decreasePlaceDispo == true) {
			// si la réservation est en cours, on met les places disponibles correspondant à la pièce id - 1.
			$requeteUpdateNBPlacesDispo = self::$pdo->prepare("UPDATE t_piece SET pi_nb_places_dispo = pi_nb_places_dispo - 1 WHERE pi_id = :pi_id");
			$requeteUpdateNBPlacesDispo->bindParam(':pi_id', $pieceID);
			$requeteUpdateNBPlacesDispo->execute();
		}*/
	}

	catch ( PDOException $e ) {
		$e->getMessage ();
	}
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static function UpdateNB_PlaceDispos () {
	// utilisé dans modification personne
	
	self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
	// incrémentation, on prend les réservations qui ont une date de fin inférieur à la date d'aujourd'hui avec le statut 'En cours'
	$requeteSelectReservationDateFinDepasse = self::$pdo->prepare("SELECT r_id, pi_id FROM t_reservation WHERE r_date_fin < CURDATE() AND r_statut ='En cours'");
	$requeteSelectReservationDateFinDepasse->execute();
	
	
	$requeteDecreasePlaceDispo = self::$pdo->prepare("UPDATE t_piece SET pi_nb_places_dispo = pi_nb_places_dispo + 1 WHERE pi_id = :pi_id");
	$requeteUpdateStatutDepassee = self::$pdo->prepare("UPDATE t_reservation SET r_statut = 'Dépassée' WHERE r_id = :r_id");
	foreach ($requeteSelectReservationDateFinDepasse as $row) {
		// on met à jour les places disponibles, incrémente de + 1
		$piece_id = $row->pi_id;
		$requeteDecreasePlaceDispo->bindParam(':pi_id', $piece_id);
		$requeteDecreasePlaceDispo->execute();
		
		// on met à jour le statut de 'En cours' à 'Dépassée'
		$reservation_id = $row->r_id;
		$requeteUpdateStatutDepassee->bindParam(':r_id', $reservation_id);
		$requeteUpdateStatutDepassee->execute();
	}
	

	

	
	// décrémentation, on prend les réservations qui ont une date inférieur ou égale à celles d'aujourd'hui avec le statut 'Réservée'
	$requeteSelectReservationDateDebutAujourdhui = self::$pdo->prepare("SELECT r_id, pi_id FROM t_reservation WHERE r_date_debut <= CURDATE() AND r_statut='Réservée'");
	$requeteSelectReservationDateDebutAujourdhui->execute();
	
	
	$requeteIncreasePlaceDispo = self::$pdo->prepare("UPDATE t_piece SET pi_nb_places_dispo = pi_nb_places_dispo - 1 WHERE pi_id = :pi_id");
	$requeteUpdateStatutEnCours = self::$pdo->prepare("UPDATE t_reservation SET r_statut = 'En cours' WHERE r_id = :r_id");
	
	foreach ($requeteSelectReservationDateDebutAujourdhui as $row) {
		// on met à jour les places disponibles, incrémente de -1
		$piece_id = $row->pi_id;
		$requeteIncreasePlaceDispo->bindParam(':pi_id', $piece_id);
		$requeteIncreasePlaceDispo->execute();
		
		// on met à jour le statut de en 'Réservée à 'En cours'
		$reservation_id = $row->r_id;
		$requeteUpdateStatutEnCours->bindParam(':r_id', $reservation_id);
		$requeteUpdateStatutEnCours->execute();
	}
}





static function modif_ordinateur($id,$p_id,$hostname,$adresse_ipv4,$adresse_mac,$type,$serviceTag,$numero_inventaire,$date_achat,$garantie,$processeur,$memoire,$dhcp,$commentaire) {
	try { 
        // utilisé dans ModifierDiscipline.php
        $requeteUpdateDiscipline = self::$pdo->prepare("UPDATE t_ordinateur SET  p_id= :p_id,hostname = :hostname,adresse_ipv4 = :adresse_ipv4, adresse_mac = :adresse_mac, type = :type, serviceTag = :serviceTag, numero_inventaire = :numero_inventaire, date_achat = :date_achat, garantie = :garantie, processeur = :processeur, memoire = :memoire, dhcp = :dhcp, commentaire = :commentaire WHERE o_id = :o_id");
        //echo "UPDATE t_ordinateur SET  p_id= $p_id,hostname = $hostname WHERE o_id = $id";die;
        $requeteUpdateDiscipline ->bindParam('o_id', $id);
        $requeteUpdateDiscipline ->bindParam('p_id', $p_id);
        $requeteUpdateDiscipline ->bindParam('hostname', $hostname);
        $requeteUpdateDiscipline ->bindParam('adresse_ipv4', $adresse_ipv4);
        $requeteUpdateDiscipline ->bindParam('adresse_mac', $adresse_mac);
        $requeteUpdateDiscipline ->bindParam('type', $type);
        $requeteUpdateDiscipline ->bindParam('serviceTag', $serviceTag);
        $requeteUpdateDiscipline ->bindParam('numero_inventaire', $numero_inventaire);
        $requeteUpdateDiscipline ->bindParam('date_achat', $date_achat);
        $requeteUpdateDiscipline ->bindParam('garantie', $garantie);
        $requeteUpdateDiscipline ->bindParam('processeur', $processeur);
        $requeteUpdateDiscipline ->bindParam('memoire', $memoire);
        $requeteUpdateDiscipline ->bindParam('dhcp', $dhcp);
        $requeteUpdateDiscipline ->bindParam('commentaire', $commentaire);
        $requeteUpdateDiscipline ->execute();

	 }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();

        }

}





static function delete_responsable_equipe($idEquipe,$tabmembres) {
        try {
        foreach ($tabmembres as $numHistorique)  {
                $reqdeleteDiscipline = self::$pdo->prepare(" DELETE FROM t_equipe_responsable WHERE equipe_id = :eq_id AND numHistorique = :numHistorique");
                $reqdeleteDiscipline->bindParam(':eq_id', $idEquipe);
                $reqdeleteDiscipline->bindParam(':numHistorique', $numHistorique);
                $reqdeleteDiscipline->execute();

        }

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
                die;

        }

}







static function delete_membre_equipe($idEquipe,$tabmembres) {
        try {
        foreach ($tabmembres as $numHistorique)  {
		$reqdeleteDiscipline = self::$pdo->prepare(" DELETE FROM t_membres_equipe2 WHERE eq_id = :eq_id AND numHistorique = :numHistorique");
	        $reqdeleteDiscipline->bindParam(':eq_id', $idEquipe);
	        $reqdeleteDiscipline->bindParam(':numHistorique', $numHistorique);
       		$reqdeleteDiscipline->execute();

	}

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
		die;

        }

}

static function supprime_personne_equipe($numHistorique,$tabequipe) {
        try {
        foreach ($tabequipe as $eq_id)  {
		$reqdeleteDiscipline = self::$pdo->prepare(" DELETE FROM t_membres_equipe2 WHERE eq_id = :eq_id AND numHistorique = :numHistorique");
	        $reqdeleteDiscipline->bindParam(':eq_id', $eq_id);
	        $reqdeleteDiscipline->bindParam(':numHistorique', $numHistorique);
       		$reqdeleteDiscipline->execute();

	}

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
		die;

        }

}

static function ajoute_membre_equipe($id_equipe,$tabmembres) {
        try {
        foreach ($tabmembres as $numHistorique)  {
	 	$requeteInsertDiscipline = self::$pdo->prepare ( "INSERT INTO t_membres_equipe2 (eq_id,numHistorique) VALUES (:eq_id,:numHistorique)" );
                $requeteInsertDiscipline->bindParam ( ':eq_id', $id_equipe );
                $requeteInsertDiscipline->bindParam ( ':numHistorique', $numHistorique );
                $requeteInsertDiscipline->execute ();
	}

	 }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
		die;

        }

}
static function ajoute_responsable_equipe($id_equipe,$row) {
        try {
                $requete = self::$pdo->prepare ( "INSERT INTO t_equipe_responsable (equipe_id,numHistorique) VALUES (:eq_id,:numHistorique)" );
                $requete->bindParam ( ':eq_id', $id_equipe );
                $requete->bindParam ( ':numHistorique', $row );
                $requete->execute ();

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();

        }

}


static function ajoute_personne_equipe($numHistorique,$tabequipe) {
        try {
        foreach ($tabequipe as $eq_id)  {
	 	$requeteInsertDiscipline = self::$pdo->prepare ( "INSERT INTO t_membres_equipe2 (eq_id,numHistorique) VALUES (:eq_id,:numHistorique)" );
                $requeteInsertDiscipline->bindParam ( ':eq_id', $eq_id );
                $requeteInsertDiscipline->bindParam ( ':numHistorique', $numHistorique );
                $requeteInsertDiscipline->execute ();
	}

	 }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
		die;

        }

}

static function daoEquipeModifie($id,$eq_nom,$eq_nom_long,$eq_verticale,$resp_id) {
        try {
        // utilisé dans ModifierDiscipline.php
// Si le nouveau responsable est défini, on le met à jour
	if (isset($resp_id)) {
		$requeteUpdateDiscipline = self::$pdo->prepare("UPDATE t_equipe SET  eq_nom= :eq_nom,eq_nom_long = :eq_nom_long,eq_verticale = :eq_verticale, resp_id = :resp_id  WHERE eq_id = :eq_id");
	        $requeteUpdateDiscipline ->bindParam('resp_id', $resp_id);
	}
	 else $requeteUpdateDiscipline = self::$pdo->prepare("UPDATE t_equipe SET  eq_nom= :eq_nom,eq_nom_long = :eq_nom_long,eq_verticale = :eq_verticale  WHERE eq_id = :eq_id");
	
        $requeteUpdateDiscipline ->bindParam('eq_id', $id);
        $requeteUpdateDiscipline ->bindParam('eq_nom', $eq_nom);
        $requeteUpdateDiscipline ->bindParam('eq_nom_long', $eq_nom_long);
        $requeteUpdateDiscipline ->bindParam('eq_verticale', $eq_verticale);
        $requeteUpdateDiscipline ->execute();

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();

        }

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoTheseModifie( $theseId,$p_these_directeur1,$p_these_directeur2,$ecoleDoctorale,$intitule,$tfinancement,$m2obtenu, $laboaccueil, $etsInscription,$etsCoTutelle,$theseDebut,$theseSoutenance,$abandon,$apresThese,$modifiePar,$modifieDate) {
	try {

//echo "\nthseùodie".$theseId.' '.$intitule.' '.$financement;die;
     $rq = self::$pdo->prepare("UPDATE theses
                         SET
				p_these_directeur1 = :p_these_directeur1,
				p_these_directeur2 = :p_these_directeur2,
				ecoleDoctorale = :ecoleDoctorale,
				intitule  = :intitule,
				tfinancement= :tfinancement,
				m2obtenu= :m2obtenu,
				laboaccueil= :laboaccueil,
				etsInscription= :etsInscription,
				etsCoTutelle= :etsCoTutelle,
				theseDebut  = :theseDebut,
				theseSoutenance = :theseSoutenance,
				abandon = :abandon,
				apresThese = :apresThese,
				modifiePar = :modifiePar,
				modifieDate = :modifieDate

                         WHERE theseId = :theseId");


    $rq->bindParam(':theseId', $theseId);
    $rq->bindParam(':p_these_directeur1', $p_these_directeur1);
    $rq->bindParam(':p_these_directeur2', $p_these_directeur2);
    $rq->bindParam(':ecoleDoctorale', $ecoleDoctorale);
    $rq->bindParam(':intitule', $intitule);
    $rq->bindParam(':tfinancement', $tfinancement);
    $rq->bindParam(':m2obtenu', $m2obtenu);
    $rq->bindParam(':laboaccueil', $laboaccueil);
    $rq->bindParam(':etsInscription', $etsInscription);
    $rq->bindParam(':etsCoTutelle', $etsCoTutelle);
    $rq->bindParam(':theseDebut', $theseDebut);
   $rq->bindParam(':theseSoutenance', $theseSoutenance);
   $rq->bindParam(':abandon', $abandon);
   $rq->bindParam(':apresThese', $apresThese);
   $rq->bindParam(':modifiePar', $modifiePar);
   $rq->bindParam(':modifieDate', $modifieDate);



        $rq->execute();

  }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
        die;

        }


}


/////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoStageModifie ($stageId,  $encadrant, $stag_intitule, $eqId, $dateDebut, $dateFin, $etablissement, $fId, $autreformation, $t_conv_id, $gratification, $listeFinancementCredit, $listeFinancementCreditNouveau, $financementEntite, $missionFrance, $dateMissionFrance , $missionEtranger, $dateMissionEtranger, $demandeBureau, $demandePoste, $laboChimie,$salleBlanche, $conventionEnvoyee, $conventionRevenue, $scanConvention, $dossierGratif,  $montantGratif,$montantTransport,$numMissionnaire,$refFournisseur,$numBCSifac, $dateDossierGratif,  $parDossierGratif ,$stageModifiePar,$stageModifieDate) {


        try {
        $rq = self::$pdo->prepare("UPDATE stage
			 SET  
				p_stag_encadrant =:p_stag_encadrant,
				stag_intitule =:stag_intitule,
				eq_id =:eq_id,
				dateDebut =:dateDebut,
				dateFin =:dateFin,
				etablissement =:etablissement,
				f_id =:f_id,
				autreformation =:autreformation,
				t_conv_id =:t_conv_id,
				gratification =:gratification,
				financementEntite =:financementEntite,
				missionFrance =:missionFrance,
				dateMissionFrance =:dateMissionFrance,
				missionEtranger =:missionEtranger,
				dateMissionEtranger =:dateMissionEtranger,
				demandeBureau =:demandeBureau,
				demandePoste =:demandePoste,
				laboChimie =:laboChimie,
				salleBlanche =:salleBlanche,
				conventionEnvoyee =:conventionEnvoyee,
				conventionRevenue =:conventionRevenue,
				scanConvention =:scanConvention,
				dossierGratif =:dossierGratif,
				montantGratif =:montantGratif,
				montantTransport =:montantTransport,
				numMissionnaire  =:numMissionnaire,
				refFournisseur =:refFournisseur,
				numBCSifac =:numBCSifac,
				dateDossierGratif =:dateDossierGratif,
				parDossierGratif =:parDossierGratif,
				stageModifiePar = :stageModifiePar,
			        stageModifieDate = :stageModifieDate

			 WHERE stageId = :stageId");


        $rq->bindParam(':stageId', $stageId);
        $rq->bindParam(':p_stag_encadrant', $encadrant);
        $rq->bindParam(':stag_intitule', $stag_intitule);
        $rq->bindParam(':eq_id', $eqId);
        $rq->bindParam(':dateDebut', $dateDebut);
        $rq->bindParam(':dateFin', $dateFin);
        $rq->bindParam(':etablissement', $etablissement);
        $rq->bindParam(':f_id', $fId);
        $rq->bindParam(':autreformation', $autreformation);
        $rq->bindParam(':t_conv_id', $t_conv_id);
        $rq->bindParam(':gratification', $gratification);

        $rq->bindParam(':financementEntite', $financementEntite);
        $rq->bindParam(':missionFrance', $missionFrance);
        $rq->bindParam(':dateMissionFrance', $dateMissionFrance);
        $rq->bindParam(':missionEtranger', $missionEtranger);
        $rq->bindParam(':dateMissionEtranger', $dateMissionEtranger);
        $rq->bindParam(':demandeBureau', $demandeBureau);
        $rq->bindParam(':demandePoste', $demandePoste);
        $rq->bindParam(':laboChimie', $laboChimie);
        $rq->bindParam(':salleBlanche', $salleBlanche);
        $rq->bindParam(':conventionEnvoyee', $conventionEnvoyee);
        $rq->bindParam(':conventionRevenue', $conventionRevenue);
        $rq->bindParam(':scanConvention', $scanConvention);
        $rq->bindParam(':dossierGratif', $dossierGratif);
        $rq->bindParam(':montantGratif', $montantGratif);
        $rq->bindParam(':montantTransport', $montantTransport);
        $rq->bindParam(':numMissionnaire', $numMissionnaire);
        $rq->bindParam(':refFournisseur', $refFournisseur);
        $rq->bindParam(':numBCSifac', $numBCSifac);
        $rq->bindParam(':dateDossierGratif', $dateDossierGratif);
        $rq->bindParam(':parDossierGratif', $parDossierGratif);

        $rq->bindParam(':stageModifiePar', $stageModifiePar);
        $rq->bindParam(':stageModifieDate', $stageModifieDate);
        $rq ->execute();




 $del = self::$pdo->prepare ("DELETE FROM stageFinancement WHERE stageId LIKE :stageId ");
 $del->bindParam ( ':stageId', $stageId );
 $del->execute ();

foreach ($listeFinancementCredit as $row)  {
     $requeteInsertion = self::$pdo->prepare("INSERT INTO stageFinancement
        (stageId,financementCredit,dateDebut,dateFin)
         VALUES
(:stageId,:financementCredit,:dateDebut,:dateFin)
         ");
        $requeteInsertion->bindParam(':stageId', $stageId);
        $requeteInsertion->bindParam(':financementCredit', $row->financementCredit);
        $requeteInsertion->bindParam(':dateDebut', $row->dateDebut);
        $requeteInsertion->bindParam(':dateFin', $row->dateFin);
        $requeteInsertion->execute();
}


foreach ($listeFinancementCreditNouveau as $row)  {
 $requeteInsertion = self::$pdo->prepare("INSERT INTO stageFinancement
        (stageId,financementCredit,dateDebut,dateFin)
         VALUES
(:stageId,:financementCredit,:dateDebut,:dateFin)
         ");
        $requeteInsertion->bindParam(':stageId', $stageId);
        $requeteInsertion->bindParam(':financementCredit', $row->financementCredit);
        $requeteInsertion->bindParam(':dateDebut', $row->dateDebut);
        $requeteInsertion->bindParam(':dateFin', $row->dateFin);
        $requeteInsertion->execute();


}

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
	die;

        }

}






static function daoEquipeAjoute($eq_nom,$eq_nom_long,$eq_verticale) {
        // utilisé dans AjouterEquipe.php
        $requeteInsertion = Model::$pdo->prepare("INSERT INTO t_equipe (eq_nom,eq_nom_long,eq_verticale) VALUES (:eq_nom,:eq_nom_long,:eq_verticale) ");
        $requeteInsertion->bindParam(':eq_nom', $eq_nom);
        $requeteInsertion->bindParam(':eq_nom_long', $eq_nom_long);
        $requeteInsertion->bindParam(':eq_verticale', $eq_verticale);
        $requeteInsertion->execute();
}


static function insert_ordinateur($p_id,$hostname,$adresse_ipv4,$adresse_mac,$type,$serviceTag,$numero_inventaire,$date_achat,$garantie,$processeur,$memoire,$dhcp,$commentaire) {
	// utilisé dans AjouterDiscipline.php
	$requeteInsertion = Model::$pdo->prepare("INSERT INTO t_ordinateur (p_id,hostname,adresse_ipv4,adresse_mac,type,serviceTag,numero_inventaire,date_achat,garantie,processeur,memoire,dhcp,commentaire) VALUES (:p_id,:hostname,:adresse_ipv4,:adresse_mac,:type,:serviceTag,:numero_inventaire,:date_achat,:garantie,:processeur,:memoire,:dhcp,:commentaire) "); 
	$requeteInsertion->bindParam(':p_id', $p_id);
	$requeteInsertion->bindParam(':hostname', $hostname);
	$requeteInsertion->bindParam(':adresse_ipv4', $adresse_ipv4);
	$requeteInsertion->bindParam(':adresse_mac', $adresse_mac);
	$requeteInsertion->bindParam(':type', $type);
	$requeteInsertion->bindParam(':serviceTag', $serviceTag);
	$requeteInsertion->bindParam(':numero_inventaire', $numero_inventaire);
	$requeteInsertion->bindParam(':date_achat', $date_achat);
	$requeteInsertion->bindParam(':garantie', $garantie);
	$requeteInsertion->bindParam(':processeur', $processeur);
	$requeteInsertion->bindParam(':memoire', $memoire);
	$requeteInsertion->bindParam(':dhcp', $dhcp);
	$requeteInsertion->bindParam(':commentaire', $commentaire);
	$requeteInsertion->execute();
}





static function insertion_piece($site, $numero_piece, $nombre_places) {
		// utilisé dans ajouter pièce
	// vérification si déjà existant

	$requeteSelectionPieceExistante = self::$pdo->prepare("SELECT * FROM t_piece WHERE pi_numero = :pi_numero AND sit_id = :sit_id");

	$requeteSelectionPieceExistante->bindParam(':pi_numero', $numero_piece);
	$requeteSelectionPieceExistante->bindParam(':sit_id', $site);

	$requeteSelectionPieceExistante->execute();

	if ($requeteSelectionPieceExistante->rowCount() > 0) {
		return;
	}
	else {

try {
		$sth = self::$pdo->prepare("INSERT INTO t_piece (pi_numero,  sit_id) VALUES (:pi_numero, :sit_id)");
		$sth->bindParam(':pi_numero', $numero_piece);
		$sth->bindParam(':sit_id', $site);
		$sth->execute();



		//L'id de la piece 
		 $sth = self::$pdo->prepare("SELECT pi_id FROM t_piece P WHERE pi_numero = :pi_numero AND sit_id = :sit_id ");
                $sth->bindParam(':pi_numero', $numero_piece);
                $sth->bindParam(':sit_id', $site);
                $sth->execute();
                $objet = $sth->fetchObject ();
	       $id_piece=$objet->pi_id ;





//		echo "insertion piece $id_piece";

		for( $i= 1 ; $i <= $nombre_places ; $i++ ) {
			$requeteInsertionPl = self::$pdo->prepare("INSERT INTO t_place(pi_id) VALUES (:pi_id)");
			$requeteInsertionPl->bindParam(':pi_id', $id_piece);
			$requeteInsertionPl->execute();
//			echo "insertion place piece $id_piece";

		}

}
catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();

        }




	}

}

static function daoTutelleAjoute($nom) {
        // utilisé dans AjouterSite.php
        $requeteInsertionSite = Model::$pdo->prepare("INSERT INTO t_tutelle (t_nom) VALUES (:t_nom)");
        $requeteInsertionSite->bindParam(':t_nom', $nom);
        $requeteInsertionSite->execute();

}
static function daoStatutAjoute($nom) {
        // utilisé dans AjouterSite.php
        $requeteInsertionSite = Model::$pdo->prepare("INSERT INTO t_statut2 (st_nom) VALUES (:st_nom)");
        $requeteInsertionSite->bindParam(':st_nom', $nom);
        $requeteInsertionSite->execute();

}
static function daoCollegeAjoute($nom) {
        // utilisé dans AjouterSite.php
        $requeteInsertionSite = Model::$pdo->prepare("INSERT INTO collegeElection (collegeNom) VALUES (:collegeNom)");
        $requeteInsertionSite->bindParam(':collegeNom', $nom);
        $requeteInsertionSite->execute();

}



static function daoRessourceAjoute($nom) {
	$requeteInsertionSite = Model::$pdo->prepare("INSERT INTO ressourcesMessage (nom) VALUES (:nom)");
	$requeteInsertionSite->bindParam(':nom', $nom);
	$requeteInsertionSite->execute();
}

static function daoRessourceSupprime($rid) {
	$requeteInsertionSite = Model::$pdo->prepare("DELETE FROM ressourcesMessage WHERE rid = :rid");
	$requeteInsertionSite->bindParam(':rid', $rid);
	$requeteInsertionSite->execute();
}

static function daoSiteAjoute($nom_site) {
	// utilisé dans AjouterSite.php
	$requeteInsertionSite = Model::$pdo->prepare("INSERT INTO t_site (sit_nom) VALUES (:sit_nom)");
	$requeteInsertionSite->bindParam(':sit_nom', $nom_site);
	$requeteInsertionSite->execute();

}
static function daoTypeAjoute($st_id,$nom) {
        // utilisé dans AjouterType.php
        $requeteInsertionSite = Model::$pdo->prepare("INSERT INTO t_type_personnel2 (st_id,ty_nom) VALUES (:st_id,:ty_nom)");
        $requeteInsertionSite->bindParam(':st_id', $st_id);
        $requeteInsertionSite->bindParam(':ty_nom', $nom);
        $requeteInsertionSite->execute();

}



static function insert_tutelle($tutelle) {
	// utilisé dans AjouterTutelle.php
	$requeteInsertionSite = Model::$pdo->prepare("INSERT INTO t_tutelle (t_nom) VALUES (:t_nom)");
	$requeteInsertionSite->bindParam(':t_nom', $tutelle);
	$requeteInsertionSite->execute();

}


static function insert_typePersonnel($type_personnel) {
	// utilisé dans ajouterTypePersonne.php
	$requeteInsertionSite = Model::$pdo->prepare("INSERT INTO t_type_personnel (ty_nom) VALUES (:ty_nom)");
	$requeteInsertionSite->bindParam(':ty_nom', $type_personnel);
	$requeteInsertionSite->execute();

}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////





static function recuperer_piece ($id_piece) {
	// utilisé dans ModificationPiece.php
	try {
		$requeteRecuperationPiece = self::$pdo->prepare("SELECT P.pi_numero,  P.sit_id ,count(PL.pl_id) as nbplaces FROM t_piece P,t_place PL WHERE P.pi_id = :pi_id  AND P.pi_id LIKE PL.pi_id");
		$requeteRecuperationPiece->bindParam(':pi_id', $id_piece);
		$requeteRecuperationPiece->execute();
		$objetPiece = $requeteRecuperationPiece->fetchObject ();
		return $objetPiece;
	}
	catch (PDOException $e) {
		echo 'Error :' .$e->getMessage();
			
	}
}



//  Model::updatePiece($id_piece, $site,  $nombre_places,$diff_places);

static function updatePiece($id_piece, $site, $nombre_places, $diff_places) {
	// utilisé dans ModificationPiece.php
	try {
			


		 for( $i= 1 ; $i <= $diff_places ; $i++ ) {
                        $requeteInsertionPl = self::$pdo->prepare("INSERT INTO t_place(pi_id) VALUES (:pi_id)");
                        $requeteInsertionPl->bindParam(':pi_id', $id_piece);
                        $requeteInsertionPl->execute();

                }


	}
	catch (PDOException $e) {
		echo 'Error' .$e->getMessage();
		die;
	}
}






static function  nombre_de_place_total_pr_salle ($id_piece) {
	// utilisé dans ModificationPiece.php
	// requete place dispo
	$requetePlaceDispo = Model::$pdo->prepare("SELECT pi_nb_places_dispo FROM t_piece WHERE pi_id = :pi_id");
	$requetePlaceDispo->bindParam(':pi_id', $id_piece);
	$requetePlaceDispo->execute();
	$nombrePlaceDispoMoinsAvecOccupation = $requetePlaceDispo->fetchColumn();


	// places occupÃ©es actuellement
	$requetePlaceOccupe = Model::$pdo->prepare("SELECT COUNT(*) FROM `t_reservation` WHERE r_date_debut <= CURDATE() AND r_date_fin >= CURDATE() AND pi_id = :pi_id");
	$requetePlaceOccupe->bindParam(':pi_id', $id_piece);
	$requetePlaceOccupe->execute();
	$nombreRequetePlaceOccupe = $requetePlaceOccupe->fetchColumn();

	$requetePlaceOccupeSansDateFin = Model::$pdo->prepare("SELECT COUNT(*) FROM `t_reservation` WHERE r_date_debut <= CURDATE() AND r_date_fin is null AND pi_id = :pi_id");
	$requetePlaceOccupeSansDateFin->bindParam(':pi_id', $id_piece);
	$requetePlaceOccupeSansDateFin->execute();
	$nombreRequetePlaceOccupeSansDateFin = $requetePlaceOccupeSansDateFin->fetchColumn();

	$nombreTotaldePlaces = $nombrePlaceDispoMoinsAvecOccupation + $nombreRequetePlaceOccupe + $nombreRequetePlaceOccupeSansDateFin; // places disponibles quand la salle est vide

	return $nombreTotaldePlaces;
}



static function nombre_de_place_a_tel_date ($id_piece, $date_debut, $date_fin) {
	// utilisé dans ModificationPiece.php
	$requeteSelectReservations = Model::$pdo->prepare("SELECT r_id, r_date_debut, r_date_fin, p_id, pi_id FROM t_reservation WHERE r_date_fin >= :r_date_debut AND r_date_debut <= :r_date_fin AND t_reservation.pi_id = :pi_id GROUP BY p_id");
	$requeteSelectReservations->bindParam('r_date_fin', $date_fin);
	$requeteSelectReservations->bindParam(':r_date_debut', $date_debut);
	$requeteSelectReservations->bindParam(':pi_id', $id_piece);
	$requeteSelectReservations->execute();
	$nombreDeReservationEnCoursDurantCettePeriode = $requeteSelectReservations->rowCount();



	$nombreTotaldePlaces = Model::nombre_de_place_total_pr_salle($id_piece);

	$nombreDePlacesLibresAvecReservations = $nombreTotaldePlaces - $nombreDeReservationEnCoursDurantCettePeriode;

	return $nombreDePlacesLibresAvecReservations;
}


static function retourne_reservations_places_a_tel_date ($id_place, $date_debut, $date_fin) {
        // utilisé dans ModificationPiece.php
$requeteSelectReservations = Model::$pdo->prepare("SELECT r_id, r_date_debut, r_date_fin, t_reservation2.p_id, t_reservation2.pl_id,t_personne.p_nom,p_prenom   FROM t_reservation2,t_personne  WHERE r_date_fin >= :r_date_debut AND r_date_debut <= :r_date_fin AND t_reservation2.pl_id = :pl_id AND t_personne.p_id = t_reservation2.p_id ORDER BY r_date_debut");


        $requeteSelectReservations->bindParam(':r_date_fin', $date_fin);
        $requeteSelectReservations->bindParam(':r_date_debut', $date_debut);
        $requeteSelectReservations->bindParam(':pl_id', $id_place);

        $requeteSelectReservations->execute();

        return $requeteSelectReservations;

}





static function retourne_reservations_a_tel_date ($id_piece, $date_debut, $date_fin) {
	// utilisé dans ModificationPiece.php
$requeteSelectReservations = Model::$pdo->prepare("SELECT r_id, r_date_debut, r_date_fin, t_reservation2.p_id, t_reservation2.pl_id,t_personne.p_nom,p_prenom   FROM t_reservation2,t_place,t_personne  WHERE r_date_fin >= :r_date_debut AND r_date_debut <= :r_date_fin AND t_reservation2.pl_id = t_place.pl_id AND t_personne.p_id = t_reservation2.p_id AND t_place.pi_id = :pi_id AND r_date_fin !='9999-12-31'");


//	$requeteSelectReservations = Model::$pdo->prepare("SELECT r_id, r_date_debut, r_date_fin, t_personne.p_id, p_prenom, p_nom, t_piece.pi_id, pi_numero FROM t_reservation2 JOIN t_personne ON t_reservation2.p_id = t_personne.p_id JOIN t_piece ON t_reservation2.pi_id = t_piece.pi_id  WHERE r_date_fin >= :r_date_debut AND r_date_debut <= :r_date_fin AND t_reservation2.pi_id = :pi_id AND r_date_fin !='9999-12-31'");
	$requeteSelectReservations->bindParam(':r_date_fin', $date_fin);
	$requeteSelectReservations->bindParam(':r_date_debut', $date_debut);
	$requeteSelectReservations->bindParam(':pi_id', $id_piece);
	$requeteSelectReservations->execute();
	return $requeteSelectReservations;
}

static function retourne_reservations_avec_date_fin_indefini ($id_piece,$date_debut,$annee) {
	// utilisé dans ModificationPiece.php
	$requeteSelectReservationsSansDateFin = Model::$pdo->prepare("SELECT r_id, r_date_debut, r_date_fin, r_statut, t_personne.p_id, p_prenom, p_nom, t_piece.pi_id, pi_numero FROM t_reservation JOIN t_personne ON t_reservation.p_id = t_personne.p_id JOIN t_piece ON t_reservation.pi_id = t_piece.pi_id WHERE (YEAR(r_date_debut) = :annee_periode OR r_date_debut <= :debut_periode) AND r_date_fin ='9999-12-31' AND  t_reservation.pi_id = :pi_id");
	$requeteSelectReservationsSansDateFin->bindParam(':pi_id', $id_piece);
	$requeteSelectReservationsSansDateFin->bindParam(':debut_periode', $date_debut);
	$requeteSelectReservationsSansDateFin->bindParam(':annee_periode', $annee);
	$requeteSelectReservationsSansDateFin->execute();


	return $requeteSelectReservationsSansDateFin;
}

//static function retourne_reservations_avec_date_fin_indefini ($id_piece) {
	// utilisé dans ModificationPiece.php
//	$requeteSelectReservationsSansDateFin = Model::$pdo->prepare("SELECT r_id, r_date_debut, r_date_fin, r_statut, t_personne.p_id, p_prenom, p_nom, t_piece.pi_id, pi_numero FROM t_reservation JOIN t_personne ON t_reservation.p_id = t_personne.p_id JOIN t_piece ON t_reservation.pi_id = t_piece.pi_id WHERE r_date_fin ='9999-12-31' AND  t_reservation.pi_id = :pi_id");
//	$requeteSelectReservationsSansDateFin->bindParam(':pi_id', $id_piece);
//	$requeteSelectReservationsSansDateFin->execute();
//	return $requeteSelectReservationsSansDateFin;
//}



static function recup_all_reservation_correspondant_a_id_piece($id_piece) {
        // utilisé dans ModificationPiece.php
	try {
	        $requeteSelectAllReservation = self::$pdo->prepare("SELECT r_id, r_date_debut,r_date_fin, r_statut, p_nom, p_prenom FROM t_reservation JOIN t_personne ON t_reservation.p_id = t_personne.p_id WHERE t_reservation.pi_id = :pi_id");
        	$requeteSelectAllReservation->bindParam(':pi_id', $id_piece);
	        $requeteSelectAllReservation->execute();
	        return $requeteSelectAllReservation;
	} catch (PDOException $e) {
		echo 'Error' .$e->getMessage();
		die;
	}
	
}


static function recup_all_personnes_correspondant_a_id_piece($id_piece) {
	// utilisé dans ModificationPiece.php
	try {
		$requeteSelectAllReservation = self::$pdo->prepare("SELECT p_id,pi_numero, p_nom, p_prenom FROM t_personne JOIN t_piece ON t_personne.pi_id = t_piece.pi_id WHERE t_personne.pi_id = :pi_id");
		$requeteSelectAllReservation->bindParam(':pi_id', $id_piece);
		$requeteSelectAllReservation->execute();
		return $requeteSelectAllReservation;
	} catch (PDOException $e) {
		echo 'Error' .$e->getMessage();
		die;
	}

}




static function recuperer_ordi($id) {
	// utilisé dans ModifierOrdinateur.php
	try {
   		self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
		$requeteSelect = self::$pdo->prepare("SELECT * FROM t_ordinateur WHERE t_ordinateur.o_id = :o_id");
		$requeteSelect->bindParam(':o_id', $id);
		$requeteSelect->execute();
		$objet = $requeteSelect->fetch();
		return $objet;

	} catch (PDOException $e) {
		echo 'Error' .$e->getMessage();
	}
}



static function rechercheDoublon($nom,$prenom) {
        // utilisé dans ModifierOrdinateur.php
        try {

                self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                $requeteSelect = self::$pdo->prepare("SELECT * FROM t_personne WHERE t_personne.p_nom = :nom AND t_personne.p_prenom = :prenom");
                $requeteSelect->bindParam(':nom', $nom);
                $requeteSelect->bindParam(':prenom', $prenom);
                $requeteSelect->execute();
                $objet = $requeteSelect->fetch();
                return $objet;

        } catch (PDOException $e) {
                echo 'Error' .$e->getMessage();
        }
}




static function recuperer_equipe($id) {
        // utilisé dans ModifierEquipe.php
        try {
                $requeteSelect = Model::$pdo->prepare("SELECT * FROM t_equipe WHERE t_equipe.eq_id = :eq_id");
                $requeteSelect->bindParam(':eq_id', $id);
                $requeteSelect->execute();
                $objet = $requeteSelect->fetchObject();
                return $objet;

        } catch (PDOException $e) {
                echo 'Error' .$e->getMessage();
        }
}

static function recuperer_equipe_personne($numHistorique) {
        // utilisé dans ModificationPersonne.php
        try {
                self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                $requeteSelect = Model::$pdo->prepare("SELECT t_equipe.eq_id,t_equipe.eq_nom FROM historique,t_membres_equipe2,t_equipe
			 WHERE historique.numHistorique = :numHistorique
			 AND historique.numHistorique  LIKE t_membres_equipe2.numHistorique
			 AND t_membres_equipe2.eq_id LIKE t_equipe.eq_id");
                $requeteSelect->bindParam(':numHistorique', $numHistorique);
                $requeteSelect->execute();
                return $requeteSelect;

        } catch (PDOException $e) {
                echo 'Error' .$e->getMessage();
        }
}







static function recuperer_site($id) {
// utilisé dans ModifierSite.php
	$requeteSelectionSite = Model::$pdo->prepare("SELECT * FROM t_site WHERE t_site.sit_id = :sit_id");
	$requeteSelectionSite->bindParam(':sit_id', $id);
	$requeteSelectionSite->execute();
	$objetSite = $requeteSelectionSite->fetchObject();
	return $objetSite;
}

static function daoTypeModifie ($st_id,$ty_id,$nom) {
try {
	// utilisé dans ModifierSite.php
	$requeteUpdate = Model::$pdo->prepare("UPDATE t_type_personnel2 SET ty_nom = :ty_nom WHERE st_id = :st_id AND ty_id = :ty_id");
	$requeteUpdate->bindParam(':ty_nom', $nom);
	$requeteUpdate->bindParam(':st_id', $st_id);
	$requeteUpdate->bindParam(':ty_id', $ty_id);
	$requeteUpdate->execute();
  } catch (PDOException $e) {
                echo 'Error' .$e->getMessage();
        }

}
static function daoSiteModifie ($id,$nom_site) {
try {
	// utilisé dans ModifierSite.php
	$requeteUpdateSite = Model::$pdo->prepare("UPDATE t_site SET sit_nom = :sit_nom WHERE sit_id = :sit_id");
	$requeteUpdateSite->bindParam(':sit_nom', $nom_site);
	$requeteUpdateSite->bindParam(':sit_id', $id);
	$requeteUpdateSite->execute();
  } catch (PDOException $e) {
                echo 'Error' .$e->getMessage();
        }

}
static function daoTutelleModifie ($id,$nom) {
try {
        // utilisé dans ModifierSite.php
        $requeteUpdateSite = Model::$pdo->prepare("UPDATE t_tutelle SET t_nom = :t_nom WHERE t_id = :t_id");
        $requeteUpdateSite->bindParam(':t_nom', $nom);
        $requeteUpdateSite->bindParam(':t_id', $id);
        $requeteUpdateSite->execute();
  } catch (PDOException $e) {
                echo 'Error' .$e->getMessage();
        }

}

static function daoStatutModifie ($id,$nom) {
try {
        // utilisé dans ModifierSite.php
        $requeteUpdateSite = Model::$pdo->prepare("UPDATE t_statut2 SET st_nom = :st_nom WHERE st_id = :st_id");
        $requeteUpdateSite->bindParam(':st_nom', $nom);
        $requeteUpdateSite->bindParam(':st_id', $id);
        $requeteUpdateSite->execute();
  } catch (PDOException $e) {
                echo 'Error' .$e->getMessage();
        }

}
static function daoCollegeModifie ($id,$nom) {
try {
        // utilisé dans ModifierSite.php
        $requeteUpdateSite = Model::$pdo->prepare("UPDATE collegeElection SET collegeNom = :collegeNom WHERE collegeId = :collegeId");
        $requeteUpdateSite->bindParam(':collegeNom', $nom);
        $requeteUpdateSite->bindParam(':collegeId', $id);
        $requeteUpdateSite->execute();
  } catch (PDOException $e) {
                echo 'Error' .$e->getMessage();
        }

}



static function recup_piece_site($id) {
	// utilisé dans ModifierSite.php
	$requeteSelectSalleSite = self::$pdo->prepare("SELECT sit_nom, pi_numero, pi_id FROM t_site JOIN t_piece ON t_site.sit_id = t_piece.sit_id WHERE t_site.sit_id = :sit_id");
	$requeteSelectSalleSite->bindParam('sit_id', $id);
	$requeteSelectSalleSite->execute();
	return $requeteSelectSalleSite;
}
static function recup_personne_site($id) {
	// utilisé dans ModifierSite.php
	$requeteSelectPersonneSite = self::$pdo->prepare("SELECT p_id, sit_nom, p_nom, p_prenom FROM t_site JOIN t_personne ON t_personne.sit_id = t_site.sit_id WHERE t_site.sit_id = :sit_id");
	$requeteSelectPersonneSite->bindParam('sit_id', $id);
	$requeteSelectPersonneSite->execute();
	return $requeteSelectPersonneSite;
}




static function recuperer_tutelle($id) {
	// utilisé dans ModifierTuttelle.php
	try {
		$requeteSelectionTutelle = Model::$pdo->prepare("SELECT * FROM t_tutelle WHERE t_tutelle.t_id = :t_id");
		$requeteSelectionTutelle->bindParam(':t_id', $id);
		$requeteSelectionTutelle->execute();
		$objetTutelle = $requeteSelectionTutelle->fetchObject();
		return $objetTutelle;
	} catch (PDOException $e) {
		echo 'Error' .$e->getMessage();
		die;
	}
}

static function update_tutelle ($tutelle, $id) {
	// utilisé dans ModifierTuttelle.php
	try {
		$requeteUpdateTutelle = Model::$pdo->prepare("UPDATE t_tutelle SET t_nom = :t_nom WHERE t_tutelle.t_id = :t_id");
		$requeteUpdateTutelle->bindParam(':t_nom', $tutelle);
		$requeteUpdateTutelle->bindParam(':t_id', $id);
		$requeteUpdateTutelle->execute();
			
	} catch (PDOException $e) {
		echo 'Error' .$e->getMessage();
	}
}

static function recup_personne_tutelle($id) {
	// utilisé dans ModifierTuttelle.php
	try {
		$requeteSelectPersonneTutelle = Model::$pdo->prepare("SELECT t_tutelle.t_id, p_nom, p_id, p_prenom FROM t_tutelle JOIN t_personne ON t_tutelle.t_id = t_personne.t_id WHERE t_tutelle.t_id = :t_id");
		$requeteSelectPersonneTutelle->bindParam(':t_id', $id);
		$requeteSelectPersonneTutelle->execute();
		return $requeteSelectPersonneTutelle;

	} catch (PDOException $e) {
		echo 'Error' .$e->getMessage();
	}

}



static function recuperer_type_personnel($id) {
	// utilisé dans ModifierTyoePersonnel.php
	$requeteSelectionTypePersonnel = Model::$pdo->prepare("SELECT * FROM t_type_personnel WHERE t_type_personnel.ty_id = :ty_id");
	$requeteSelectionTypePersonnel->bindParam(':ty_id', $id);
	$requeteSelectionTypePersonnel->execute();
	$objetTypePersonnel = $requeteSelectionTypePersonnel->fetchObject();
	return $objetTypePersonnel;
}

static function update_type_personnel($type_personnel, $id) {
	// utilisé dans ModifierTyoePersonnel.php
	try {
		$requeteUpdateTypePersonnel = self::$pdo->prepare("UPDATE t_type_personnel SET ty_nom = '$type_personnel' WHERE ty_id = $id");
		$requeteUpdateTypePersonnel->execute();
	} catch (PDOException $e) {
		echo 'Error' .$e->getMessage();
	}

}

static function recup_persone_typeP($id) {
	// utilisé dans ModifierTypePersonnel.php
	try {
		$requeteUpdateTypePersonnel = self::$pdo->prepare("SELECT t_type_personnel.ty_id, p_id, p_nom, p_prenom FROM t_type_personnel JOIN t_personne ON t_personne.ty_id = t_type_personnel.ty_id WHERE t_type_personnel.ty_id  = :ty_id");
		$requeteUpdateTypePersonnel->bindParam('ty_id', $id);
		$requeteUpdateTypePersonnel->execute();
		return $requeteUpdateTypePersonnel;
	} catch (PDOException $e) {
		echo 'Error' .$e->getMessage();
	}
}


static function selection_id_personne_ajouter($nom, $prenom, $email) {
	// utilisé dans AjouterPersonne.php
	// utilisé pour sélectionner l'id de la personne existante ou la personne qui vient d'être ajouté
	try {
			
		$req_selection_id = self::$pdo->prepare ( "SELECT p_id FROM t_personne WHERE p_nom = :p_nom AND p_prenom= :p_prenom AND
				p_mail = :p_mail" );
		$req_selection_id->bindParam(':p_nom', $nom);
		$req_selection_id->bindParam(':p_prenom', $prenom);
		$req_selection_id->bindParam(':p_mail', $email);
		$req_selection_id->execute ();
		$resultat_personne_id = $req_selection_id->fetchColumn();
		return $resultat_personne_id;
	} catch ( PDOException $e ) {
		echo 'Error' . $e->getMessage ();
		die;
	}
}




static function deleteEquipe ($id) {
        // utilisé dans SuppressionDiscipline.php
        $reqdelete = self::$pdo->prepare(" DELETE FROM t_equipe WHERE t_equipe.eq_id = :id");
        $reqdelete->bindParam(':id', $id);
        $reqdelete->execute();
}


static function deletePiece ($id_piece) {
// utilisé dans SuppressionPiece.php

	$reqdeletePiece = self::$pdo->prepare(" DELETE FROM t_piece WHERE t_piece.pi_id = :pi_id");
	$reqdeletePiece->bindParam(':pi_id', $id_piece);
	$reqdeletePiece->execute();

	$reqdeletePlace =  self::$pdo->prepare(" DELETE FROM t_place WHERE t_place.pi_id = :pi_id");
	$reqdeletePlace->bindParam(':pi_id', $id_piece);
	$reqdeletePlace->execute();


}


static function deleteSite ($id_site) {
// utilisé dans deleteSite.php

	$reqdeletePiece = self::$pdo->prepare(" DELETE FROM t_site WHERE t_site.sit_id = :sit_id");
	$reqdeletePiece->bindParam(':sit_id', $id_site);
	$reqdeletePiece->execute();

}


static function deleteTutelle ($idTutelle) {
	// utilisé dans SuppressionTutelle.php

	$reqdeletePiece = self::$pdo->prepare(" DELETE FROM t_tutelle WHERE t_tutelle.t_id = :t_id");
	$reqdeletePiece->bindParam(':t_id', $idTutelle);
	$reqdeletePiece->execute();

}


static function deleteTypePersonnel ($id_type_personnel) {


	$reqdeletePiece = self::$pdo->prepare(" DELETE FROM t_type_personnel WHERE t_type_personnel.ty_id = :ty_id");
	$reqdeletePiece->bindParam(':ty_id', $id_type_personnel);
	$reqdeletePiece->execute();



}







}

Model::set_static ();
?>
