<?php
require_once 'config.php';
class ModelCahier {
	public static $pdo;
	public static function set_static() {
		$conf=new Conf;
                $host =$conf->getHostname ();
                $dbname = $conf->getDatabase ();
                $login = $conf->getLogin ();
                $pass = $conf->getPassword ();
		
		try {
			// Connexion à la base de données
			// Le dernier argument sert à ce que toutes les chaines de charactères
			// en entrée et sortie de MySql soit dans le codage UTF-8
			self::$pdo = new PDO ( "mysql:host=$host;dbname=$dbname", $login, $pass, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8" 
			) );
			
			// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
			self::$pdo->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $ex ) {
			echo $ex->getMessage ();
			die ( 'Problème lors de la connexion à la base de données' );
		}
	}

//	public static $pdospip;
//	public static function set_static_spip() {
//		$conf=new ConfSpip;
//                $host =$conf->getHostname ();
//                $dbname = $conf->getDatabase ();
//                $login = $conf->getLogin ();
//                $pass = $conf->getPassword ();
		
//		try {
			// Connexion à la base de données
			// Le dernier argument sert à ce que toutes les chaines de charactères
			// en entrée et sortie de MySql soit dans le codage UTF-8
//			self::$pdospip = new PDO ( "mysql:host=$host;dbname=$dbname", $login, $pass, array (
//					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8" 
//			) );
			
			// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
//			self::$pdospip->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
//		} catch ( PDOException $ex ) {
//			echo $ex->getMessage ();
//			die ( 'Problème lors de la connexion à la base de données Spip' );
//		}
//	}
	


//                        ModelCahier::ajoute_projet($tab["cahierNum"],$tab['listeProjets']);

////////////////////////////////////////////////////////////////////////////////////////////////////////
static function ajoute_projet($cahierNum,$listeProjets) {
        try {
        foreach ($listeProjets as $projet_id)  {
                $requete = self::$pdo->prepare ( "INSERT INTO cahierlaboProjet (cahierNum,projet_id) VALUES (:cahierNum,:projet_id)" );
                $requete->bindParam ( ':cahierNum', $cahierNum );
                $requete->bindParam ( ':projet_id', $projet_id );
                $requete->execute ();
        }

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();

        }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////
static function supprime_projet($cahierNum,$listeProjets) {
        try {
        foreach ($listeProjets as $projet_id)  {
                $requete = self::$pdo->prepare ( "DELETE FROM  cahierlaboProjet WHERE cahierNum = :cahierNum AND projet_id = :projet_id" );
                $requete->bindParam ( ':cahierNum', $cahierNum );
                $requete->bindParam ( ':projet_id', $projet_id );
                $requete->execute ();
        }

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();

        }

}


////////////////////////////////////////////////////////////////////////////////////////////////////////
//static function insert_cahier($cahierNum,$modele,$p_id,$dateDotation,$fonction,$eq_id,$dateRetour,$localisation,$commentaires) {
static function insert_cahier($cahierNum,$modele,$fonction,$eq_id,$localisation,$commentaires,$utilisateurnouveau) {


        // utilisé dans AjouterDiscipline.php
        $requeteInsertion = self::$pdo->prepare("INSERT INTO cahierlabo
        (cahierNum,modele,fonction,eq_id,localisation,commentaires)
         VALUES
(:cahierNum,:modele,:fonction,:eq_id,:localisation,:commentaires)
         ");
        $requeteInsertion->bindParam(':cahierNum', $cahierNum);
        $requeteInsertion->bindParam(':modele', $modele);
        $requeteInsertion->bindParam(':fonction', $fonction);
        $requeteInsertion->bindParam(':eq_id', $eq_id);
        $requeteInsertion->bindParam(':localisation', $localisation);
        $requeteInsertion->bindParam(':commentaires', $commentaires);
        $requeteInsertion->execute();


if ($utilisateurnouveau->p_id  != NULL) {
        $requete = self::$pdo->prepare("INSERT INTO cahierlaboAppartient
        (p_id,cahierNum,dateDotation,dateRetour)
         VALUES
        (:p_id,:cahierNum,:dateDotation,:dateRetour)
         ");

        $requete->bindParam(':cahierNum', $cahierNum);
        $requete->bindParam(':p_id', $utilisateurnouveau->p_id);
        $requete->bindParam(':dateDotation', $utilisateurnouveau->dateDotation);
        $requete->bindParam(':dateRetour', $utilisateurnouveau->dateRetour);
        $requete->execute();
}
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function recuperation_cahier($dateDotation,$dateRetour,$eq_id,$p_id) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );



$clausewhere="1";

if (!($dateDotation == 'T')) $clausewhere .= " AND year(dateDotation) like :dateDotation";
if (!($dateRetour == 'T')) $clausewhere .= " AND year(dateRetour) like :dateRetour";
if (!($eq_id == 'T')) $clausewhere .= " AND eq_id like :eq_id";
if (!($p_id == 'T')) $clausewhere .= " AND p_id like :p_id";

                $rq = self::$pdo->prepare (
                         "SELECT DISTINCT cahierlabo.cahierNum FROM cahierlabo,cahierlaboAppartient
			  WHERE cahierlabo.cahierNum = cahierlaboAppartient.cahierNum AND ".$clausewhere." 
			  ORDER BY cahierlabo.cahierNum"
                );

			  //ORDER BY INET_ATON(cahierlabo.cahierNum)"
			 //LEFT JOIN cahierlaboAppartient ON cahierlabo.cahierNum LIKE cahierlaboAppartient.cahierNum
			 //LEFT JOIN t_personne ON cahierlaboAppartient.p_id  LIKE t_personne.p_id 

if (!($dateDotation == 'T')) $rq->bindParam(':dateDotation', $dateDotation);
if (!($dateRetour == 'T')) $rq->bindParam(':dateRetour', $dateRetour);
if (!($eq_id == 'T')) $rq->bindParam(':eq_id', $eq_id);
if (!($p_id == 'T')) $rq->bindParam(':p_id', $p_id);
                        $rq->execute ();
                        return $rq;

}






//////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function modif_cahier($cahierNum,$modele,$fonction,$eq_id,$localisation,$commentaires,$listeutilisateur,$utilisateurnouveau) {
        try {
        // utilisé dans ModifierDiscipline.php
        $requete = self::$pdo->prepare("UPDATE cahierlabo
         SET  modele = :modele,fonction = :fonction,eq_id = :eq_id,localisation = :localisation,commentaires = :commentaires
        WHERE cahierNum = :cahierNum
        ");
        $requete->bindParam(':cahierNum', $cahierNum);
        $requete->bindParam(':modele', $modele);
        $requete->bindParam(':fonction', $fonction);
        $requete->bindParam(':eq_id', $eq_id);
        $requete->bindParam(':localisation', $localisation);
        $requete->bindParam(':commentaires', $commentaires);
        $requete->execute();




 $del = self::$pdo->prepare ("DELETE FROM cahierlaboAppartient  WHERE cahierNum LIKE :cahierNum ");
 $del->bindParam ( ':cahierNum', $cahierNum );
 $del->execute ();

foreach ($listeutilisateur as $row)  {
     $requeteInsertion = self::$pdo->prepare("INSERT INTO cahierlaboAppartient
        (cahierNum,p_id,dateDotation,dateRetour)
         VALUES
(:cahierNum,:p_id,:dateDotation,:dateRetour)
         ");
        $requeteInsertion->bindParam(':cahierNum', $cahierNum);
        $requeteInsertion->bindParam(':p_id', $row->p_id);
        $requeteInsertion->bindParam(':dateDotation', $row->dateDotation);
        $requeteInsertion->bindParam(':dateRetour', $row->dateRetour);
        $requeteInsertion->execute();
}


     $requeteInsertion = self::$pdo->prepare("INSERT INTO cahierlaboAppartient
        (cahierNum,p_id,dateDotation,dateRetour)
         VALUES
(:cahierNum,:p_id,:dateDotation,:dateRetour)
         ");
        $requeteInsertion->bindParam(':cahierNum', $cahierNum);
        $requeteInsertion->bindParam(':p_id', $utilisateurnouveau->p_id);
        $requeteInsertion->bindParam(':dateDotation', $utilisateurnouveau->dateDotation);
        $requeteInsertion->bindParam(':dateRetour', $utilisateurnouveau->dateRetour);
        $requeteInsertion->execute();







         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();

        }

}



//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function cahierlaboProjetListe($cahierNum) {
//serge
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
              //self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );

                $rq = self::$pdo->prepare (
                         "SELECT  * FROM cahierlaboProjet
			 WHERE cahierlaboProjet.cahierNum = :cahierNum
			  "
                );
			 $rq->bindParam(':cahierNum', $cahierNum);
                        $rq->execute ();
	                return $rq;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function recuperation_un_cahier($cahierNum) {
              //self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );

                $rq = self::$pdo->prepare (
                         "SELECT  * FROM cahierlabo
			 WHERE cahierlabo.cahierNum = :cahierNum
			  "
                );
			 $rq->bindParam(':cahierNum', $cahierNum);
                        $rq->execute ();
			$objet = $rq->fetch();
	                return $objet;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function recuperation_un_cahier_appartient($cahierNum) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = self::$pdo->prepare (
                         "SELECT  * FROM cahierlaboAppartient
			 LEFT JOIN t_personne ON cahierlaboAppartient.p_id  LIKE t_personne.p_id 
			 WHERE cahierlaboAppartient.cahierNum = :cahierNum
			  "
                );
			 $rq->bindParam(':cahierNum', $cahierNum);
                        $rq->execute ();
	                return $rq;
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////

  static function listeParUtilisateur($id) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = self::$pdo->prepare (
                         "SELECT  cahierlabo.cahierNum,modele,CONCAT (p_nom,' ',p_prenom) AS appartient,dateDotation,fonction,eq_nom,dateRetour,commentaires FROM cahierlabo
				LEFT JOIN cahierlaboAppartient ON  cahierlabo.cahierNum LIKE cahierlaboAppartient.cahierNum
				LEFT JOIN t_equipe ON cahierlabo.eq_id  LIKE t_equipe.eq_id
				LEFT JOIN t_personne ON cahierlaboAppartient.p_id  LIKE t_personne.p_id

			 WHERE cahierlaboAppartient.p_id  LIKE :p_id  

			  ORDER BY INET_ATON(cahierlabo.cahierNum)"
                );

    $rq->bindParam ( ':p_id', $id );


                        $rq->execute ();
                        return $rq;

}


  static function mesStagiairesEtDoctorants($id) {
// aussi les contractuels et le chercheur
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = self::$pdo->prepare ("

SELECT  cahierlabo.cahierNum,modele,CONCAT (p1.p_nom,' ',p1.p_prenom) AS appartient,ty_nom AS type,CONCAT(p2.p_nom,' ',p2.p_prenom) AS encadrant,CONCAT (p3.p_nom,' ',p3.p_prenom,' ',IFNULL(p4.p_nom,''),' ',IFNULL(p4.p_prenom,'')) AS directeur,CONCAT(p5.p_nom,' ',p5.p_prenom) AS superieur,dateDotation,fonction,eq_nom,dateRetour,commentaires FROM cahierlabo
LEFT JOIN cahierlaboAppartient ON cahierlabo.cahierNum = cahierlaboAppartient.cahierNum
LEFT JOIN t_personne p1 ON cahierlaboAppartient.p_id = p1.p_id
LEFT JOIN historique h ON p1.p_id = h.p_id

LEFT JOIN stage ON h.stageId = stage.stageId
LEFT JOIN t_personne p2 ON stage.p_stag_encadrant = p2.p_id

LEFT JOIN theses ON h.theseId = theses.theseId
LEFT JOIN t_personne p3 ON theses.p_these_directeur1 = p3.p_id 
LEFT JOIN t_personne p4 ON theses.p_these_directeur2 = p4.p_id

LEFT JOIN contractuel ON h.contractuel_id  = contractuel.contractuel_id
LEFT JOIN t_personne p5 ON contractuel.contractuel_sup_id  = p5.p_id

LEFT JOIN t_equipe ON cahierlabo.eq_id  LIKE t_equipe.eq_id
LEFT JOIN t_type_personnel2 ON h.ty_id LIKE t_type_personnel2.ty_id AND h.st_id LIKE t_type_personnel2.st_id

WHERE (stage.p_stag_encadrant like :p_id  OR (theses.p_these_directeur1 like :p_id OR theses.p_these_directeur2 like :p_id ) OR contractuel.contractuel_sup_id LIKE :p_id OR cahierlaboAppartient.p_id LIKE :p_id)

			  ORDER BY INET_ATON(cahierlabo.cahierNum)"
		);


//AND p1.archive NOT LIKE '1'


    $rq->bindParam ( ':p_id', $id );


                        $rq->execute ();
                        return $rq;

}


 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function entete() {


$entete=array('cahierNum','modele','appartient','type','encadrant','directeur1','directeur2','dateDotation','fonction','eq_nom','dateRetour','commentaires');

return $entete;
}


  static function pourLequipe($eq_id) {
// les cahiers des membres de l'equipe
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                $rq = self::$pdo->prepare ("
SELECT  cahierlabo.cahierNum,modele,CONCAT (p1.p_nom,' ',p1.p_prenom) AS appartient,CONCAT('MEMBRE') AS type,CONCAT('') AS encadrant,CONCAT('') AS directeur1,CONCAT('') AS directeur2,dateDotation,fonction,eq_nom,dateRetour,commentaires FROM cahierlabo
LEFT JOIN cahierlaboAppartient ON cahierlabo.cahierNum = cahierlaboAppartient.cahierNum
LEFT JOIN t_personne p1 ON cahierlaboAppartient.p_id = p1.p_id


LEFT JOIN historique ON historique.p_id = p1.p_id
LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique



LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id
WHERE t_equipe.eq_id like :eq_id  AND p1.archive NOT LIKE '1'
                ");
    $rq->bindParam ( ':eq_id', $eq_id );
                        $rq->execute ();
                        return $rq;
}



  static function cahier_equipe_membre($id) {
// les cahiers des membres de l'equipe par le responsable
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                $rq = self::$pdo->prepare ("
SELECT  cahierlabo.cahierNum,modele,CONCAT (p1.p_nom,' ',p1.p_prenom) AS appartient,CONCAT('MEMBRE') AS type,CONCAT('') AS encadrant,CONCAT('') AS directeur1,CONCAT('') AS directeur2,dateDotation,fonction,eq_nom,dateRetour,commentaires FROM cahierlabo
LEFT JOIN cahierlaboAppartient ON cahierlabo.cahierNum = cahierlaboAppartient.cahierNum
LEFT JOIN t_personne p1 ON cahierlaboAppartient.p_id = p1.p_id
LEFT JOIN t_membres_equipe ON p1.p_id = t_membres_equipe.p_id
LEFT JOIN t_equipe ON t_membres_equipe.eq_id = t_equipe.eq_id
WHERE t_equipe.resp_id like :p_id  AND p1.st_id NOT LIKE '2'
                ");
    $rq->bindParam ( ':p_id', $id );
                        $rq->execute ();
                        return $rq;
}


  static function cahier_equipe_stagiaire($id) {
// les cahiers des stagiaires  de l'équipe
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                $rq = self::$pdo->prepare ("
SELECT  cahierlabo.cahierNum,modele,CONCAT (p1.p_nom,' ',p1.p_prenom) AS appartient,CONCAT('STAGIAIRE') AS type,CONCAT (p2.p_nom,' ',p2.p_prenom) AS encadrant ,CONCAT('') AS directeur1,CONCAT('') AS directeur2,dateDotation,fonction,eq_nom,dateRetour,commentaires FROM cahierlabo
LEFT JOIN cahierlaboAppartient ON cahierlabo.cahierNum = cahierlaboAppartient.cahierNum
LEFT JOIN t_personne p1 ON cahierlaboAppartient.p_id = p1.p_id
LEFT JOIN stage ON p1.p_id = stage.p_id
LEFT JOIN t_personne p2 ON stage.p_stag_encadrant = p2.p_id

LEFT JOIN t_membres_equipe ON  stage.p_stag_encadrant = t_membres_equipe.p_id
LEFT JOIN t_equipe ON t_membres_equipe.eq_id = t_equipe.eq_id

WHERE t_equipe.resp_id like :p_id  AND p1.st_id NOT LIKE '2'
                ");
    $rq->bindParam ( ':p_id', $id );
                        $rq->execute ();
                        return $rq;
}




  static function cahier_equipe_doctorant($id) {
// les cahiers des  doctorants de l'équipe
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                $rq = self::$pdo->prepare ("

 SELECT  cahierlabo.cahierNum,modele,CONCAT (p1.p_nom,' ',p1.p_prenom) AS appartient,CONCAT('DOCTORANT') AS type,CONCAT('') AS encadrant,CONCAT (p3.p_nom,' ',p3.p_prenom) AS directeur1,CONCAT (p4.p_nom,' ',p4.p_prenom) AS directeur2 ,dateDotation,fonction,eq_nom,dateRetour,commentaires FROM cahierlabo
LEFT JOIN cahierlaboAppartient ON cahierlabo.cahierNum = cahierlaboAppartient.cahierNum
LEFT JOIN t_personne p1 ON cahierlaboAppartient.p_id = p1.p_id
LEFT JOIN theses ON cahierlaboAppartient.p_id = theses.p_id
LEFT JOIN t_personne p3 ON theses.p_these_directeur1 = p3.p_id 
LEFT JOIN t_personne p4 ON theses.p_these_directeur2 = p4.p_id
LEFT JOIN t_membres_equipe ON (theses.p_these_directeur1 = t_membres_equipe.p_id) OR (theses.p_these_directeur2 = t_membres_equipe.p_id)
LEFT JOIN t_equipe ON t_membres_equipe.eq_id = t_equipe.eq_id
WHERE t_equipe.resp_id like :p_id  AND p1.st_id NOT LIKE '2'
                ");
    $rq->bindParam ( ':p_id', $id );
                        $rq->execute ();
                        return $rq;
}


}


ModelCahier::set_static ();
//ModelCahier::set_static_spip ();
?>

