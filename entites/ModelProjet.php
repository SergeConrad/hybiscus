<?php
//recuperation_liste_parequipe_preselection
require_once 'config.php';
class ModelProjet {
	public static $pdo;
	public static function set_static() {
 		$conf=new Conf;
                $host =$conf->getHostname ();
                $dbname = $conf->getDatabase ();
                $login = $conf->getLogin ();
                $pass = $conf->getPassword ();

		
		try {
			// Connexion à la base de données
			// Le dernier argument sert à ce que toutes les chaines de charactères
			// en entrée et sortie de MySql soit dans le codage UTF-8
			self::$pdo = new PDO ( "mysql:host=$host;dbname=$dbname", $login, $pass, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8" 
			) );
			
			// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
			self::$pdo->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $ex ) {
			echo $ex->getMessage ();
			die ( 'Problème lors de la connexion à la base de données' );
		}
	}




////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoProjetModifieAssocie($projet_id,$liste) {
 $del = self::$pdo->prepare ("DELETE FROM projet_recherche_personnel_associe  WHERE projet_id LIKE :projet_id ");
 $del->bindParam ( ':projet_id', $projet_id );
 $del->execute ();

foreach ($liste as $row)  {
     $requeteInsertion = self::$pdo->prepare("INSERT INTO projet_recherche_personnel_associe
        (projet_id,p_id)
         VALUES
(:projet_id,:p_id)
         ");
        $requeteInsertion->bindParam(':projet_id', $projet_id);
        $requeteInsertion->bindParam(':p_id', $row);
        $requeteInsertion->execute();
}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoProjetModifieEquipe($projet_id,$liste) {
 $del = self::$pdo->prepare ("DELETE FROM projet_recherche_equipes  WHERE projet_id LIKE :projet_id ");
 $del->bindParam ( ':projet_id', $projet_id );
 $del->execute ();

foreach ($liste as $row)  {
     $requeteInsertion = self::$pdo->prepare("INSERT INTO projet_recherche_equipes
        (projet_id,equipe_id)
         VALUES
(:projet_id,:eq_id)
         ");
        $requeteInsertion->bindParam(':projet_id', $projet_id);
        $requeteInsertion->bindParam(':eq_id', $row);
        $requeteInsertion->execute();
}
}


////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoProjetAjouteAssocie($projet_id,$p_id) {
        try {

                $requete = self::$pdo->prepare ( "INSERT INTO projet_recherche_personnel_associe (projet_id,p_id) VALUES (:projet_id,:p_id)" );
                $requete->bindParam ( ':projet_id', $projet_id );
                $requete->bindParam ( ':p_id', $p_id );
                $requete->execute ();

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();

        }

}


////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoProjetAjouteEquipe($projet_id,$eq_id) {
        try {

                $requete = self::$pdo->prepare ( "INSERT INTO projet_recherche_equipes(projet_id,equipe_id) VALUES (:projet_id,:eq_id)" );
                $requete->bindParam ( ':projet_id', $projet_id );
                $requete->bindParam ( ':eq_id', $eq_id );
                $requete->execute ();

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();

        }

}


////////////////////////////////////////////////////////////////////////////////////////////////////////

static function supprime_cahier($projet_id,$listeProjetCahier) {
        try {
        foreach ($listeProjetCahier as $cahierNum)  {
                $requete = self::$pdo->prepare ( "DELETE FROM  cahierlaboProjet WHERE cahierNum = :cahierNum AND projet_id = :projet_id" );
                $requete->bindParam ( ':cahierNum', $cahierNum );
                $requete->bindParam ( ':projet_id', $projet_id );
                $requete->execute ();
        }

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();

        }

}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function recuperation_un_projet_associe($projet_id) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = self::$pdo->prepare (
                         "SELECT  * FROM projet_recherche_personnel_associe
                         LEFT JOIN t_personne ON projet_recherche_personnel_associe.p_id  LIKE t_personne.p_id
                         WHERE projet_recherche_personnel_associe.projet_id = :projet_id
                          "
                );
                         $rq->bindParam(':projet_id', $projet_id);
                        $rq->execute ();
                        return $rq;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function recuperation_un_projet_equipe($projet_id) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                $rq = self::$pdo->prepare (
                         "SELECT  * FROM projet_recherche_equipes
                         LEFT JOIN t_equipe ON projet_recherche_equipes.equipe_id  LIKE t_equipe.eq_id
                         WHERE projet_recherche_equipes.projet_id = :projet_id
                          "
                );
                         $rq->bindParam(':projet_id', $projet_id);
                        $rq->execute ();
                        return $rq;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////
static function ajoute_cahier($projet_id,$listeCahier) {
        try {
        foreach ($listeCahier as $cahierNum)  {
                $requete = self::$pdo->prepare ( "INSERT INTO cahierlaboProjet (cahierNum,projet_id) VALUES (:cahierNum,:projet_id)" );
                $requete->bindParam ( ':cahierNum', $cahierNum );
                $requete->bindParam ( ':projet_id', $projet_id );
                $requete->execute ();
        }

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();

        }

}




//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function projetCahier($projet_id) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
              //self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );

                $rq = self::$pdo->prepare (
                         "SELECT  * FROM cahierlaboProjet
                         WHERE cahierlaboProjet.projet_id = :projet_id
                          "
                );
                         $rq->bindParam(':projet_id', $projet_id);
                        $rq->execute ();
                        return $rq;
}





static function daoProjetSupprime($projet_id) {
        $requeteInsertion = Model::$pdo->prepare("
		DELETE FROM  projets_recherche
		WHERE projet_id=:projet_id
	"); 
        $requeteInsertion->bindParam(':projet_id', $projet_id);
        $requeteInsertion->execute();
}

 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoProjetModifie($projet_id,$nom_porteur,$coordinateurProjet,$coordOuPartenaire,$institut_rattachement,$organisme_gestionnaire,$financeur,$nom_financeur,$type,$acronyme,$partenaire_prive,$sujet_projet,$nbrecrutement,$nbstagiaire,$preselection,$dateSoumission) {




        // utilisé dans AjouterEquipe.php
        $requeteInsertion = Model::$pdo->prepare("UPDATE  projets_recherche
		SET	nom_porteur =:nom_porteur,
			coordinateurProjet = :coordinateurProjet,
			coordOuPartenaire =:coordOuPartenaire,
			institut_rattachement =:institut_rattachement,
			organisme_gestionnaire =:organisme_gestionnaire,
			financeur =:financeur,
			nom_financeur =:nom_financeur,
			type =:type,
			acronyme =:acronyme,
			partenaire_prive =:partenaire_prive,
			sujet_projet =:sujet_projet,
			nbrecrutement =:nbrecrutement,
			nbstagiaire =:nbstagiaire,
			preselection =:preselection, 
			dateSoumission =:dateSoumission 
		WHERE projet_id=:projet_id
	"); 

        $requeteInsertion->bindParam(':nom_porteur', $nom_porteur);
        $requeteInsertion->bindParam(':coordinateurProjet', $coordinateurProjet);
        $requeteInsertion->bindParam(':coordOuPartenaire', $coordOuPartenaire);
        $requeteInsertion->bindParam(':institut_rattachement', $institut_rattachement);
        $requeteInsertion->bindParam(':organisme_gestionnaire', $organisme_gestionnaire);
        $requeteInsertion->bindParam(':financeur', $financeur);
        $requeteInsertion->bindParam(':nom_financeur', $nom_financeur);
        $requeteInsertion->bindParam(':type', $type);
        $requeteInsertion->bindParam(':acronyme', $acronyme);
        $requeteInsertion->bindParam(':partenaire_prive', $partenaire_prive);
        $requeteInsertion->bindParam(':sujet_projet', $sujet_projet);
        $requeteInsertion->bindParam(':nbrecrutement', $nbrecrutement);
        $requeteInsertion->bindParam(':nbstagiaire', $nbstagiaire);
        $requeteInsertion->bindParam(':preselection', $preselection);
        $requeteInsertion->bindParam(':dateSoumission', $dateSoumission);
        $requeteInsertion->bindParam(':projet_id', $projet_id);
        $requeteInsertion->execute();
}



 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoProjetModifiePartie2($projet_id,$dateDebut,$dateFin,$duree,$montant,$partHsm,$partenaires,$commentaires,$nbrecrutement2,$nbstagiaire2 ) {



        // utilisé dans AjouterEquipe.php
        $requeteInsertion = Model::$pdo->prepare("UPDATE  projets_recherche
                SET
  			dateDebut =:dateDebut ,
  			dateFin =:dateFin ,
  			duree =:duree ,
  			montant =:montant ,
  			partHsm =:partHsm ,
  			partenaires =:partenaires ,
 			commentaires =:commentaires, 
			nbrecrutement2 =:nbrecrutement2,
			nbstagiaire2 =:nbstagiaire2

                WHERE projet_id=:projet_id
        ");


        $requeteInsertion->bindParam(':dateDebut', $dateDebut);
        $requeteInsertion->bindParam(':dateFin', $dateFin);
        $requeteInsertion->bindParam(':duree', $duree);
        $requeteInsertion->bindParam(':montant', $montant);
        $requeteInsertion->bindParam(':partHsm', $partHsm);
        $requeteInsertion->bindParam(':partenaires', $partenaires);
        $requeteInsertion->bindParam(':commentaires', $commentaires);
        $requeteInsertion->bindParam(':nbrecrutement2', $nbrecrutement2);
        $requeteInsertion->bindParam(':nbstagiaire2', $nbstagiaire2);

        $requeteInsertion->bindParam(':projet_id', $projet_id);
        $requeteInsertion->execute();
}



 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//static function daoProjetAjoute($equipe_id,$nom_porteur,$coordinateurProjet,$coordOuPartenaire,$institut_rattachement,$organisme_gestionnaire,$financeur,$nom_financeur,$type,$acronyme,$partenaire_prive,$sujet_projet,$nbrecrutement,$nbstagiaire,$preselection,$dateSoumission) {
static function daoProjetAjoute($nom_porteur,$coordinateurProjet,$coordOuPartenaire,$institut_rattachement,$organisme_gestionnaire,$financeur,$nom_financeur,$type,$acronyme,$partenaire_prive,$sujet_projet,$nbrecrutement,$nbstagiaire,$preselection,$dateSoumission) {


        // utilisé dans AjouterEquipe.php
        $requeteInsertion = Model::$pdo->prepare("INSERT INTO projets_recherche
		(nom_porteur,coordinateurProjet,coordOuPartenaire,institut_rattachement,organisme_gestionnaire,financeur,nom_financeur,type,acronyme,partenaire_prive,sujet_projet,nbrecrutement,nbstagiaire,preselection,dateSoumission) 
		VALUES
		(:nom_porteur,:coordinateurProjet,:coordOuPartenaire,:institut_rattachement,:organisme_gestionnaire,:financeur,:nom_financeur,:type,:acronyme,:partenaire_prive,:sujet_projet,:nbrecrutement,:nbstagiaire,:preselection,:dateSoumission)   
	"); 

        $requeteInsertion->bindParam(':nom_porteur', $nom_porteur);
        $requeteInsertion->bindParam(':coordinateurProjet', $coordinateurProjet);
        $requeteInsertion->bindParam(':coordOuPartenaire', $coordOuPartenaire);
        $requeteInsertion->bindParam(':institut_rattachement', $institut_rattachement);
        $requeteInsertion->bindParam(':organisme_gestionnaire', $organisme_gestionnaire);
        $requeteInsertion->bindParam(':financeur', $financeur);
        $requeteInsertion->bindParam(':nom_financeur', $nom_financeur);
        $requeteInsertion->bindParam(':type', $type);
        $requeteInsertion->bindParam(':acronyme', $acronyme);
        $requeteInsertion->bindParam(':partenaire_prive', $partenaire_prive);
        $requeteInsertion->bindParam(':sujet_projet', $sujet_projet);
        $requeteInsertion->bindParam(':nbrecrutement', $nbrecrutement);
        $requeteInsertion->bindParam(':nbstagiaire', $nbstagiaire);
        $requeteInsertion->bindParam(':preselection', $preselection);
        $requeteInsertion->bindParam(':dateSoumission', $dateSoumission);
        $requeteInsertion->execute();
}


//////////////////////////////////////////////////////////////////////////////////
       static function daoProjetTableau($id) {
  // utilisé dans modification personne

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                $requeteSelect = self::$pdo->prepare("SELECT *  FROM projets_recherche WHERE projets_recherche.projet_id = :projet_id");
                $requeteSelect->bindParam(':projet_id', $id);
                $requeteSelect->execute();
                $tableau = $requeteSelect->fetch();
                return $tableau;

                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}




 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste() {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                 //$requeteSelect = self::$pdo->prepare ( "SELECT  * FROM projets_recherche ORDER BY eq_verticale DESC, eq_nom" );
                 $requeteSelect = self::$pdo->prepare ( "SELECT  * FROM projets_recherche ORDER BY acronyme" );


                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste_soumis() {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                 $requeteSelect = self::$pdo->prepare ( "SELECT financeur_nom,count(*) AS nb FROM `projets_recherche`,projets_recherche_financeur WHERE preselection like 'soumis' AND projets_recherche.financeur like projets_recherche_financeur.financeur_id GROUP BY financeur ORDER BY financeur_nom" );


                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste_acceptes() {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                 $requeteSelect = self::$pdo->prepare ( "SELECT financeur_nom,count(*) AS nb FROM `projets_recherche`,projets_recherche_financeur WHERE preselection like 'accepté' AND projets_recherche.financeur like projets_recherche_financeur.financeur_id GROUP BY financeur ORDER BY financeur_nom " );


                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }


 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste_refuses() {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                 $requeteSelect = self::$pdo->prepare ( "SELECT financeur_nom,count(*) AS nb FROM `projets_recherche`,projets_recherche_financeur WHERE preselection like 'refusé' AND projets_recherche.financeur like projets_recherche_financeur.financeur_id GROUP BY financeur" );


                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }


 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste_parchercheur_entete() {
//  SELECT COLUMN_NAME
//                FROM INFORMATION_SCHEMA.COLUMNS
//                WHERE table_name = :table_name");


$entete=array('equipe_id','nom_porteur','coordinateurProjet','coordOuPartenaire','institut_rattachement','organisme_gestionnaire','financeur','nom_financeur','type','acronyme','partenaire_prive','sujet_projet','nbrecrutement','nbstagiaire','preselection','dateSoumission','dateDebut','dateFin','duree','montant','partHsm','partenaires','commentaires','nbrecrutement2','nbstagiaire2');

return $entete;
}

 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste_parchercheur_export($id) {
                try {



                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                 $requeteSelect = self::$pdo->prepare ( "
           		SELECT  eq_nom,CONCAT (p_nom,' ',p_prenom) AS nom_porteur,coordinateurProjet,coordOuPartenaire,t1.t_nom AS institut_rattachement,t2.t_nom AS organisme_gestionnaire,financeur,nom_financeur,type,acronyme,partenaire_prive,sujet_projet,nbrecrutement,nbstagiaire,preselection,dateSoumission,dateDebut,dateFin,duree,montant,partHsm,partenaires,commentaires 
						FROM projets_recherche
                        LEFT JOIN t_equipe ON t_equipe.eq_id = projets_recherche.equipe_id
                        LEFT JOIN t_personne ON t_personne.p_id = nom_porteur
                        LEFT JOIN t_tutelle t1 ON t1.t_id = institut_rattachement
			LEFT JOIN t_tutelle t2 ON t2.t_id = organisme_gestionnaire
                        WHERE nom_porteur like :nom_porteur ORDER BY acronyme") ;

                        //LEFT JOIN t_tutelle t2 ON t2.t_id = organisme_gestionnaire
                 $requeteSelect->bindParam(':nom_porteur', $id);


                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }


 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste_parchercheur($id) {
                try {



                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                 //$requeteSelect = self::$pdo->prepare ( "SELECT  * FROM projets_recherche ORDER BY eq_verticale DESC, eq_nom" );
                 $requeteSelect = self::$pdo->prepare ( "
SELECT  projets_recherche.projet_id,GROUP_CONCAT(x.eq_nom) AS equipes,acronyme,dateSoumission,preselection,CONCAT (p_nom,' ',p_prenom) AS nom_porteur,coordOuPartenaire,coordinateurProjet,financeur_nom,type_nom,t2.t_nom AS organisme_gestionnaire
 FROM projets_recherche
LEFT JOIN t_personne ON t_personne.p_id = nom_porteur
LEFT JOIN t_tutelle t2 ON t2.t_id = organisme_gestionnaire

LEFT JOIN projets_recherche_financeur prf  ON prf.financeur_id = financeur
LEFT JOIN projets_recherche_type prt  ON prt.type_id = type

LEFT JOIN (SELECT t.eq_nom ,u.projet_id
                FROM  projets_recherche u,projet_recherche_equipes s, t_equipe t WHERE u.projet_id like s.projet_id AND s.equipe_id like t.eq_id
               ) x on x.projet_id = projets_recherche.projet_id
LEFT JOIN projet_recherche_personnel_associe  prpa   ON prpa.projet_id = projets_recherche.projet_id
                
      WHERE (nom_porteur like :p_id OR prpa.p_id like :p_id)
  GROUP BY projet_id
 ORDER BY acronyme
			");
                 $requeteSelect->bindParam(':p_id', $id);

////////// ANCIENNE VERSION
//			SELECT  *
//			FROM projets_recherche,projet_recherche_personnel_associe 
//			WHERE projet_recherche_personnel_associe.projet_id like projets_recherche.projet_id 
//			AND ( nom_porteur like :nom_porteur OR projet_recherche_personnel_associe.p_id like :nom_porteur)



                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }



 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste_paradmin_export() {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                 $requeteSelect = self::$pdo->prepare ( "
                        SELECT  eq_nom,CONCAT (p_nom,' ',p_prenom) AS nom_porteur,coordinateurProjet,coordOuPartenaire,t1.t_nom AS institut_rattachement,t2.t_nom AS organisme_gestionnaire,financeur,nom_financeur,type,acronyme,partenaire_prive,sujet_projet,nbrecrutement,preselection,dateDebut,dateFin,duree,montant,partHsm,partenaires,commentaires
                                                FROM projets_recherche
                        LEFT JOIN t_equipe ON t_equipe.eq_id = projets_recherche.equipe_id
                        LEFT JOIN t_personne ON t_personne.p_id = nom_porteur
                        LEFT JOIN t_tutelle t1 ON t1.t_id = institut_rattachement
                        LEFT JOIN t_tutelle t2 ON t2.t_id = organisme_gestionnaire") ;

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste_paradmin() {
                try {



                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                 $requeteSelect = self::$pdo->prepare ( "SELECT  *
                        FROM projets_recherche");

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste_parequipe($id) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                 $requeteSelect = self::$pdo->prepare ( "
select projet_id,acronyme,p_nom,coordinateurProjet,coordOuPartenaire,t1.t_nom as institut, t2.t_nom as organisme,dateDebut,dateFin,preselection,projets_recherche_financeur.financeur_nom from projets_recherche
LEFT JOIN t_personne ON t_personne.p_id = projets_recherche.nom_porteur
LEFT JOIN t_tutelle t1 ON projets_recherche.institut_rattachement = t1.t_id
LEFT JOIN t_tutelle t2 ON projets_recherche.organisme_gestionnaire = t2.t_id
LEFT JOIN projets_recherche_financeur ON projets_recherche.financeur = projets_recherche_financeur.financeur_id
WHERE equipe_id like :equipe_id 
ORDER BY preselection,acronyme

");
                 $requeteSelect->bindParam(':equipe_id', $id);


                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste_parequipe_preselection($id,$preselection,$dateSoumission,$dateDebut,$porteur,$associe) {
//sergeequipe
// associe et left join plantent
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

 //       error_log("PARAM recuperation_liste_parequipe_preselection $id,$preselection,$dateSoumission,$dateDebut") ;

$clausewhere="1";
$clauseleft=" ";

/////////////////////////////////////////////////////// POUR UNE EQUIPE OU POUR TOUTES LES EQUIPES ?
if (!($id == 'T')) $clausewhere .= " AND projet_recherche_equipes.equipe_id like :equipe_id AND projet_recherche_equipes.projet_id LIKE projets_recherche.projet_id";
else $clauseleft .= "LEFT JOIN (SELECT s.projet_id,GROUP_CONCAT(s.equipe_id) AS equipe_id
		FROM  projet_recherche_equipes s GROUP BY s.projet_id
		) x on x.projet_id = projets_recherche.projet_id ";


if (!($id == 'T')) $clausechamp = " projet_recherche_equipes.equipe_id";
else $clausechamp = "x.equipe_id";




if (!($preselection == 'T')) $clausewhere .= " AND preselection like :preselection";
if (!($dateSoumission == 'T')) $clausewhere .= " AND year(dateSoumission) like :dateSoumission";
if (!($dateDebut == 'T')) $clausewhere .= " AND year(dateDebut) like :dateDebut";



if (!($porteur == 'T'))   $clausewhere .= " AND projets_recherche.nom_porteur like :p_id ";
//if (!($associe == 'T')) 	$clausewhere .= " AND projet_recherche_personnel_associe.p_id like :p_id AND projet_recherche_personnel_associe.projet_id LIKE projets_recherche.projet_id";
//if (!($associe == 'T')) 	$clauseleft .= " LEFT JOIN projet_recherche_personnel_associe pra ON pra.projet_id = projets_recherche.projet_id AND pra.p_id like :p_id ";
if (!($associe == 'T')) 	$clauseleft .= " right join (select * from projet_recherche_personnel_associe where p_id = :p_id) t2 on projets_recherche.projet_id = t2.projet_id";






//        error_log("PARAM recuperation_liste_parequipe_preselection $clausewhere") ;


$clausefrom = " FROM projets_recherche";
if (!($id == 'T')) $clausefrom  .= " ,projet_recherche_equipes ";

//$requeteSelect = self::$pdo->prepare ( "SELECT  projets_recherche.projet_id,GROUP_CONCAT(projet_recherche_equipes.equipe_id),acronyme,dateSoumission,preselection,nom_porteur,coordOuPartenaire,coordinateurProjet,financeur,type,organisme_gestionnaire ".$clausefrom.$clauseleft." WHERE ".$clausewhere." ORDER BY acronyme");
//PASBON//$requeteSelect = self::$pdo->prepare ( "SELECT  projets_recherche.projet_id,projets_recherche.equipe_id,acronyme,dateSoumission,preselection,nom_porteur,coordOuPartenaire,coordinateurProjet,financeur,type,organisme_gestionnaire ".$clausefrom.$clauseleft." WHERE ".$clausewhere." ORDER BY acronyme");
//$requeteSelect = self::$pdo->prepare ( "SELECT  projets_recherche.projet_id,x.equipe_id,acronyme,dateSoumission,preselection,nom_porteur,coordOuPartenaire,coordinateurProjet,financeur,type,organisme_gestionnaire ".$clausefrom.$clauseleft." WHERE ".$clausewhere." ORDER BY acronyme");
$requeteSelect = self::$pdo->prepare ( "SELECT  projets_recherche.projet_id,".$clausechamp.",acronyme,dateSoumission,preselection,nom_porteur,coordOuPartenaire,coordinateurProjet,financeur,type,organisme_gestionnaire ".$clausefrom.$clauseleft." WHERE ".$clausewhere." ORDER BY acronyme");

//echo  "SELECT  projets_recherche.projet_id,equipe_id,acronyme,dateSoumission,preselection,nom_porteur,coordOuPartenaire,coordinateurProjet,financeur,type,organisme_gestionnaire ".$clausefrom.$clauseleft." WHERE ".$clausewhere." ORDER BY acronyme";
//die;





if (!($id == 'T')) $requeteSelect->bindParam(':equipe_id', $id);
if (!($preselection == 'T')) $requeteSelect->bindParam(':preselection', $preselection);
if (!($dateSoumission == 'T')) $requeteSelect->bindParam(':dateSoumission', $dateSoumission);
if (!($dateDebut == 'T')) $requeteSelect->bindParam(':dateDebut', $dateDebut);
if (!($porteur == 'T')) $requeteSelect->bindParam(':p_id', $porteur);
if (!($associe == 'T')) $requeteSelect->bindParam(':p_id', $associe);
$requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }
//



 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_liste_parequipe_entete() {
//  SELECT COLUMN_NAME
//                FROM INFORMATION_SCHEMA.COLUMNS
//                WHERE table_name = :table_name");


$entete=array('equipe_id','nom_porteur','coordinateurProjet','coordOuPartenaire','institut_rattachement','organisme_gestionnaire','financeur','nom_financeur','type','acronyme','partenaire_prive','sujet_projet','nbrecrutement','preselection','dateDebut','dateFin','duree','montant','partHsm','partenaires','commentaires');

return $entete;
}



 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static function recuperation_liste_parequipe_preselection_export($id,$preselection){
//LEFT JOIN (SELECT s.projet_id,GROUP_CONCAT(s.equipe_id) AS equipe
//		FROM  projet_recherche_equipes s GROUP BY s.projet_id
//		) x on x.projet_id = projets_recherche.projet_id

		try {
       if ($id== 'T') if ($preselection =='T') $condition = " 1";
                               else  $condition = " preselection like :preselection";   
      else if ($preselection =='T')  $condition = "  equipe_id like :equipe_id";
                     else $condition = " preselection like :preselection AND equipe_id like :equipe_id"; 


                      self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                 $requeteSelect = self::$pdo->prepare ( "
                        SELECT  eq_nom,CONCAT (p_nom,' ',p_prenom) AS nom_porteur,coordinateurProjet,coordOuPartenaire,t1.t_nom AS institut_rattachement,t2.t_nom AS organisme_gestionnaire,financeur_nom AS financeur,nom_financeur,type,acronyme,partenaire_prive,sujet_projet,nbrecrutement,nbstagiaire,preselection,dateDebut,dateFin,duree,montant,partHsm,partenaires,commentaires,nbrecrutement2,nbstagiaire2
                                                FROM projets_recherche
                        LEFT JOIN t_equipe ON t_equipe.eq_id = projets_recherche.equipe_id
                        LEFT JOIN projets_recherche_financeur ON  projets_recherche.financeur = projets_recherche_financeur.financeur_id
                        LEFT JOIN t_personne ON t_personne.p_id = nom_porteur
                        LEFT JOIN t_tutelle t1 ON t1.t_id = institut_rattachement
                        LEFT JOIN t_tutelle t2 ON t2.t_id = organisme_gestionnaire
                        WHERE ".$condition) ;

       if ($id== 'T') if ($preselection =='T') $condition = " 1";
                               else  $requeteSelect->bindParam(':preselection', $preselection);
      else if ($preselection =='T')  $requeteSelect->bindParam(':equipe_id', $id);
                     else {
                 			$requeteSelect->bindParam(':equipe_id', $id);
                                        $requeteSelect->bindParam(':preselection', $preselection);
			}

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }

}





 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        static function recuperation_liste_parequipe_export($id) {
                try {



                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                 $requeteSelect = self::$pdo->prepare ( "
                        SELECT  eq_nom,CONCAT (p_nom,' ',p_prenom) AS nom_porteur,coordinateurProjet,coordOuPartenaire,t1.t_nom AS institut_rattachement,t2.t_nom AS organisme_gestionnaire,financeur,nom_financeur,type,acronyme,partenaire_prive,sujet_projet,nbrecrutement,preselection,dateDebut,dateFin,duree,montant,partHsm,partenaires,commentaires
                                                FROM projets_recherche
                        LEFT JOIN t_equipe ON t_equipe.eq_id = projets_recherche.equipe_id
                        LEFT JOIN t_personne ON t_personne.p_id = nom_porteur
                        LEFT JOIN t_tutelle t1 ON t1.t_id = institut_rattachement
                        LEFT JOIN t_tutelle t2 ON t2.t_id = organisme_gestionnaire
                        WHERE equipe_id like :equipe_id") ;

                        //LEFT JOIN t_tutelle t2 ON t2.t_id = organisme_gestionnaire
                 $requeteSelect->bindParam(':equipe_id', $id);


                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

       // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_prfinanceur() {
                try {
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                        $requete = self::$pdo->prepare ( "
                        SELECT *  
                        FROM projets_recherche_financeur
			ORDER BY financeur_nom
                        " );
                        $requete->execute ();

                        return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }
        }

     // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_prtype() {
                try {
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                        $requete = self::$pdo->prepare ( "
                        SELECT *
                        FROM projets_recherche_type
                        ORDER BY type_nom
                        " );
                        $requete->execute ();

                        return $requete;
                } catch ( PDOException $e ) {
                        echo 'Error ' . $e->getMessage ();
                }
        }


       // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoPRFinanceurAjoute($nom) {
        $requeteInsertion = Model::$pdo->prepare("INSERT INTO projets_recherche_financeur (financeur_nom) VALUES (:financeur_nom)");
        $requeteInsertion->bindParam(':financeur_nom', $nom);
        $requeteInsertion->execute();

}

       // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoPRTypeAjoute($nom) {
        $requeteInsertion = Model::$pdo->prepare("INSERT INTO projets_recherche_type (type_nom) VALUES (:type_nom)");
        $requeteInsertion->bindParam(':type_nom', $nom);
        $requeteInsertion->execute();

}


       // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 static function daoPRFinanceurGetTableau($id) {

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelect = self::$pdo->prepare ( "SELECT financeur_id ,financeur_nom FROM projets_recherche_financeur WHERE financeur_id = :financeur_id" );
                        $requeteSelect->bindParam(':financeur_id', $id);
                        $requeteSelect->execute ();
                        $objetPersonne = $requeteSelect->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

       // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 static function daoPRTypeGetTableau($id) {

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
                        $requeteSelect = self::$pdo->prepare ( "SELECT type_id ,type_nom FROM projets_recherche_type WHERE type_id = :type_id" );
                        $requeteSelect->bindParam(':type_id', $id);
                        $requeteSelect->execute ();
                        $objetPersonne = $requeteSelect->fetch ();
                        return $objetPersonne;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}


       // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoPRFinanceurModifie ($id,$nom) {
try {
        $requeteUpdate = Model::$pdo->prepare("UPDATE projets_recherche_financeur SET financeur_nom = :financeur_nom WHERE financeur_id = :financeur_id");
        $requeteUpdate->bindParam(':financeur_nom', $nom);
        $requeteUpdate->bindParam(':financeur_id', $id);
        $requeteUpdate->execute();
  } catch (PDOException $e) {
                echo 'Error' .$e->getMessage();
        }

}


       // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoPRTypeModifie ($id,$nom) {
try {
        $requeteUpdate = Model::$pdo->prepare("UPDATE projets_recherche_type SET type_nom = :type_nom WHERE type_id = :type_id");
        $requeteUpdate->bindParam(':type_nom', $nom);
        $requeteUpdate->bindParam(':type_id', $id);
        $requeteUpdate->execute();
  } catch (PDOException $e) {
                echo 'Error' .$e->getMessage();
        }

}


 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_equipe_1param($eq_id,$preselection,$dateSoumission,$dateDebut) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );



$clausewhere="1";

if (!($id == 'T')) $clausewhere .= " AND equipe_id like :equipe_id";
if (!($preselection == 'T')) $clausewhere .= " AND preselection like :preselection";
if (!($dateSoumission == 'T')) $clausewhere .= " AND year(dateSoumission) like :dateSoumission";
if (!($dateDebut == 'T')) $clausewhere .= " AND year(dateDebut) like :dateDebut";

        error_log("PARAM recuperation_liste_parequipe_preselection $clausewhere") ;

error_log("REQUETE SELECT  * FROM projets_recherche WHERE ".$clausewhere." ORDER BY acronyme");
$requeteSelect = self::$pdo->prepare ( "SELECT  * FROM projets_recherche WHERE ".$clausewhere." ORDER BY acronyme");
if (!($id == 'T')) $requeteSelect->bindParam(':equipe_id', $id);
if (!($preselection == 'T')) $requeteSelect->bindParam(':preselection', $preselection);
if (!($dateSoumission == 'T')) $requeteSelect->bindParam(':dateSoumission', $dateSoumission);
if (!($dateDebut == 'T')) $requeteSelect->bindParam(':dateDebut', $dateDebut);
$requeteSelect->execute ();



#                 $requeteSelect = self::$pdo->prepare ("

#select projet_id,acronyme,eq_nom, YEAR(projets_recherche.dateSoumission) AS anneeSoumission,p_nom,coordOuPartenaire,coordinateurProjet,
#projets_recherche_financeur.financeur_nom,projets_recherche_type.type_nom,
#t2.t_nom as organisme,preselection from projets_recherche
#LEFT JOIN t_personne ON t_personne.p_id = projets_recherche.nom_porteur
#LEFT JOIN t_tutelle t2 ON projets_recherche.organisme_gestionnaire = t2.t_id
#LEFT JOIN projets_recherche_financeur ON projets_recherche.financeur = projets_recherche_financeur.financeur_id
#LEFT JOIN t_equipe ON t_equipe.eq_id like projets_recherche.equipe_id

#LEFT JOIN projets_recherche_type ON projets_recherche.type = projets_recherche_type.type_id
#WHERE equipe_id like :equipe_id
#AND projets_recherche.preselection like :preselection
#AND year(projets_recherche.dateSoumission) like :dateSoumission
#AND year(projets_recherche.dateDebut) like :dateDebut
#ORDER BY preselection,acronyme



#");





#         $requeteSelect->bindParam(':equipe_id', $eq_id);
#         $requeteSelect->bindParam(':preselection', $preselection);
#         $requeteSelect->bindParam(':dateSoumission', $dateSoumission);
#         $requeteSelect->bindParam(':dateDebut', $dateDebut);



                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }


 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_equipe_param($eq_id,$porteur,$preselection,$dateSoumission,$dateDebut) {
                try {
                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
//sergee

//        error_log ("AAA static function recuperation_equipe_param($eq_id,$porteur,$preselection,$datesoumission,$datedebut)");

$clausewhere="1";
$clausefrom=" ";

//if (!($eq_id == 'T')) $clausewhere .= " AND equipe_id like :equipe_id";
if (!($eq_id == 'T')) $clausewhere .=" AND projets_recherche.projet_id like projet_recherche_equipes.projet_id AND projet_recherche_equipes.equipe_id like :equipe_id";

if (!($eq_id == 'T')) $clausefrom .=" ,projet_recherche_equipes ";

if (!($preselection == 'T')) $clausewhere .= " AND preselection like :preselection";
if (!($dateSoumission == 'T')) $clausewhere .= " AND year(dateSoumission) like :dateSoumission";
if (!($dateDebut == 'T')) $clausewhere .= " AND year(dateDebut) like :dateDebut";



$requeteSelect = self::$pdo->prepare ("
	SELECT type_nom,count(*) as nb FROM projets_recherche_type,projets_recherche ".$clausefrom."
	WHERE projets_recherche.type like projets_recherche_type.type_id
    	AND projets_recherche.coordOuPartenaire like :coordOuPartenaire
	AND ".$clausewhere." 
    	GROUP BY type_nom");




$requeteSelect->bindParam(':coordOuPartenaire', $porteur);
if (!($eq_id == 'T')) $requeteSelect->bindParam(':equipe_id', $eq_id);
if (!($preselection == 'T')) $requeteSelect->bindParam(':preselection', $preselection);
if (!($dateSoumission == 'T')) $requeteSelect->bindParam(':dateSoumission', $dateSoumission);
if (!($dateDebut == 'T')) $requeteSelect->bindParam(':dateDebut', $dateDebut);
$requeteSelect->execute ();
                        return $requeteSelect;


                        $requeteSelect->execute ();
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        static function recuperation_equipe_param($eq_id,$porteur,$preselection) {
//                try {
//                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

//                 $requeteSelect = self::$pdo->prepare (" 
//SELECT financeur_nom,count(*) as nb FROM projets_recherche_financeur,projets_recherche
//WHERE
//	projets_recherche.financeur like projets_recherche_financeur.financeur_id
//    AND projets_recherche.equipe_id like :eq_id
//    AND projets_recherche.coordOuPartenaire like :coordOuPartenaire
//    AND projets_recherche.preselection like :preselection
//    GROUP BY financeur_nom");

//         $requeteSelect->bindParam(':eq_id', $eq_id);
//         $requeteSelect->bindParam(':coordOuPartenaire', $porteur);
//         $requeteSelect->bindParam(':preselection', $preselection);
//                        $requeteSelect->execute ();
//                        return $requeteSelect;
//                } catch ( PDOException $e ) {
//                        echo 'Error :' . $e->getMessage ();
//                }
//        }
 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_fin12mois($eq_id,$today,$dans12mois) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                $requeteSelect = self::$pdo->prepare ( "
	SELECT  * FROM projets_recherche WHERE equipe_id like :eq_id AND preselection like 'accepté'
        AND dateFin > :today AND dateFin < :dans12mois
	" );


         $requeteSelect->bindParam(':eq_id', $eq_id);
         $requeteSelect->bindParam(':today', $today);
         $requeteSelect->bindParam(':dans12mois', $dans12mois);

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }


// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_projet_montant_annee($eq_id,$anneedebut,$anneefin) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

//sergeequipe

$requeteSelect = self::$pdo->prepare ( "

SELECT year(dateDebut) AS year,sum(montant) AS sum ,GROUP_CONCAT(projets_recherche.acronyme) AS debug FROM `projets_recherche`

LEFT JOIN projet_recherche_equipes ON projet_recherche_equipes.projet_id  LIKE projets_recherche.projet_id


WHERE projet_recherche_equipes.equipe_id like :eq_id
AND ( year(dateDebut) >= :anneedebut)
AND ( year(dateDebut) <= :anneefin)
AND ((preselection like 'accepté') OR (preselection like 'terminé'))
 group by year(dateDebut)

");



         $requeteSelect->bindParam(':anneedebut', $anneedebut);
         $requeteSelect->bindParam(':anneefin', $anneefin);
         $requeteSelect->bindParam(':eq_id', $eq_id);




                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_projet_parttutelle_annee($eq_id,$anneedebut,$anneefin) {
//serge
                try {
//sergequipe

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


$requeteSelect = self::$pdo->prepare ( "
SELECT year(dateDebut) AS year, t_tutelle.t_nom,sum(partHsm) as hsm ,GROUP_CONCAT(pr.acronyme) AS debug
FROM `projets_recherche` pr,t_tutelle,projet_recherche_equipes pre

WHERE pr.organisme_gestionnaire=t_tutelle.t_id
AND pre.projet_id like pr.projet_id
AND pre.equipe_id like :eq_id
AND ( year(dateDebut) >= :anneedebut)
AND ( year(dateDebut) <= :anneefin)
AND ((preselection like 'accepté') OR (preselection like 'terminé'))
 group by year(dateDebut),t_tutelle.t_nom

");


//SELECT year(dateDebut) AS year, t_tutelle.t_nom,sum(partHsm) as hsm ,GROUP_CONCAT(projets_recherche.acronyme) AS debug 
//FROM `projets_recherche`,t_tutelle
//LEFT JOIN projet_recherche_equipes ON projet_recherche_equipes.projet_id  LIKE projet_id
//WHERE projets_recherche.organisme_gestionnaire=t_tutelle.t_id
//AND projet_recherche_equipes.equipe_id like :eq_id
//AND ( year(dateDebut) >= :anneedebut)
//AND ( year(dateDebut) <= :anneefin)
//AND ((preselection like 'accepté') OR (preselection like 'terminé'))
// group by year(dateDebut),t_tutelle.t_nom


//SELECT t_tutelle.t_nom,count(*) AS nb,sum(partHsm) as hsm,GROUP_CONCAT(projets_recherche.acronyme) AS debug
//FROM `projets_recherche`,t_tutelle
//WHERE projets_recherche.organisme_gestionnaire=t_tutelle.t_id
//AND projets_recherche.equipe_id like :eq_id
//AND (
//         (dateDebut <:date1 AND dateFin  >:date2 )
//        OR (dateDebut >=:date1 AND dateDebut  <:date2 )
//        OR (dateFin >:date1 AND dateFin  <=:date2 )
//)
//AND ((preselection like 'accepté') OR (preselection like 'terminé'))
//GROUP BY t_nom




         $requeteSelect->bindParam(':anneedebut', $anneedebut);
         $requeteSelect->bindParam(':anneefin', $anneefin);
         $requeteSelect->bindParam(':eq_id', $eq_id);




                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_projet_annee($eq_id,$date1,$date2) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );



//sergeequipe
$requeteSelect = self::$pdo->prepare ( "



SELECT year(dateDebut) AS year,t_tutelle.t_nom,count(*) AS nb,GROUP_CONCAT(projets_recherche.acronyme)
FROM `projets_recherche`,t_tutelle,projet_recherche_equipes pre
WHERE projets_recherche.organisme_gestionnaire=t_tutelle.t_id
AND pre.projet_id like projets_recherche.projet_id
AND pre.equipe_id like :eq_id
AND ( year(dateDebut) >= :anneedebut)
AND ( year(dateDebut) <= :anneefin)
AND ((preselection like 'accepté') OR (preselection like 'terminé'))
group by year(dateDebut),t_tutelle.t_nom

");





//$date1=date("$annee-01-01");
//$date2=date("$annee-12-31");

         $requeteSelect->bindParam(':anneedebut', $date1);
         $requeteSelect->bindParam(':anneefin', $date2);
         $requeteSelect->bindParam(':eq_id', $eq_id);




                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_projet_annee_financeur($eq_id,$annee) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );


                 $requeteSelect = self::$pdo->prepare ( "
SELECT projets_recherche_financeur.financeur_nom,count(*) AS nb
FROM `projets_recherche`,projets_recherche_financeur
WHERE projets_recherche.financeur=projets_recherche_financeur.financeur_id
AND projets_recherche.equipe_id like :eq_id
AND (
         (dateDebut <:date1 AND dateFin  >:date2 )
        OR (dateDebut >=:date1 AND dateDebut  <:date2 )
        OR (dateFin >:date1 AND dateFin  <=:date2 )
)

GROUP BY financeur_nom
");

$date1=date("$annee-01-01");
$date2=date("$annee-12-31");

         $requeteSelect->bindParam(':date1', $date1);
         $requeteSelect->bindParam(':date2', $date2);
         $requeteSelect->bindParam(':eq_id', $eq_id);

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_projet_annee_type($eq_id,$annee) {
//serge
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );



                 $requeteSelect = self::$pdo->prepare ( "
SELECT projets_recherche_type.type_nom,count(*) as nb FROM projets_recherche_type,projets_recherche
 WHERE projets_recherche.type like projets_recherche_type.type_id
AND projets_recherche.equipe_id like :eq_id
AND (
         (dateDebut <:date1 AND dateFin  >:date2 )
        OR (dateDebut >=:date1 AND dateDebut  <:date2 )
        OR (dateFin >:date1 AND dateFin  <=:date2 )
)

GROUP BY type_nom
");

$date1=date("$annee-01-01");
$date2=date("$annee-12-31");

         $requeteSelect->bindParam(':date1', $date1);
         $requeteSelect->bindParam(':date2', $date2);
         $requeteSelect->bindParam(':eq_id', $eq_id);

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }







}

ModelProjet::set_static ();
?>

