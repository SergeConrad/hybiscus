<?php
require_once 'config.php';
class ModelPersonne {
	public static $pdo;
	public static function set_static() {
 		$conf=new Conf;
                $host =$conf->getHostname ();
                $dbname = $conf->getDatabase ();
                $login = $conf->getLogin ();
                $pass = $conf->getPassword ();

		
		try {
			// Connexion à la base de données
			// Le dernier argument sert à ce que toutes les chaines de charactères
			// en entrée et sortie de MySql soit dans le codage UTF-8
			self::$pdo = new PDO ( "mysql:host=$host;dbname=$dbname", $login, $pass, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8" 
			) );
			
			// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
			self::$pdo->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch ( PDOException $ex ) {
			echo $ex->getMessage ();
			die ( 'Problème lors de la connexion à la base de données' );
		}
	}



/////////////////////////////////////////////////////////:
static function daoPersonneSupprime($id) {
        $requete = ModelPersonne::$pdo->prepare("
                DELETE FROM  t_personne
                WHERE p_id=:p_id
        ");
        $requete->bindParam(':p_id', $id);
        $requete->execute();

        $requete = ModelPersonne::$pdo->prepare("
                DELETE FROM historique 
                WHERE p_id=:p_id
        ");
        $requete->bindParam(':p_id', $id);
        $requete->execute();
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoHistoriqueSupprime($numHistorique ) {
 $del = self::$pdo->prepare ("DELETE FROM historique  WHERE numHistorique LIKE :numHistorique ");
 $del->bindParam ( ':numHistorique', $numHistorique );
 $del->execute ();

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoCddCreation($numHistorique,$contractuel_sup_id,$contractuel_sujet_recherche,$contractuel_t_id ) {

        $rq = ModelPersonne::$pdo->prepare( "INSERT INTO contractuel (contractuel_sup_id,contractuel_sujet_recherche, contractuel_t_id ) VALUES (:contractuel_sup_id,:contractuel_sujet_recherche,:contractuel_t_id ) ");
        $rq->bindParam(':contractuel_sup_id', $contractuel_sup_id);
        $rq->bindParam(':contractuel_sujet_recherche', $contractuel_sujet_recherche);
        $rq->bindParam(':contractuel_t_id', $contractuel_t_id);
        $rq->execute();
        $contractuel_id = ModelPersonne::$pdo->lastInsertId();

  	$rq2 = self::$pdo->prepare("UPDATE historique
                         SET contractuel_id = :contractuel_id
                         WHERE numHistorique = :numHistorique");
        $rq2->bindParam(':contractuel_id', $contractuel_id);
        $rq2->bindParam(':numHistorique', $numHistorique);
        $rq2 ->execute();

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoInviteCreation($numHistorique,$invite_par_id,$invite_commentaire ) {

        $rq = ModelPersonne::$pdo->prepare( "INSERT INTO invite (invite_par_id,invite_commentaire ) VALUES (:invite_par_id,:invite_commentaire ) ");
        $rq->bindParam(':invite_par_id', $invite_par_id);
        $rq->bindParam(':invite_commentaire', $invite_commentaire);
        $rq->execute();
        $id = ModelPersonne::$pdo->lastInsertId();

  	$rq2 = self::$pdo->prepare("UPDATE historique
                         SET invite_id = :invite_id
                         WHERE numHistorique = :numHistorique");
        $rq2->bindParam(':invite_id', $id);
        $rq2->bindParam(':numHistorique', $numHistorique);
        $rq2 ->execute();
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoCddModifie($contractuel_id,$contractuel_sup_id,$contractuel_sujet_recherche,$contractuel_t_id,$dateDebut,$dateFin) {
  $rq = ModelPersonne::$pdo->prepare("UPDATE contractuel SET contractuel_sup_id = :contractuel_sup_id,contractuel_sujet_recherche = :contractuel_sujet_recherche,contractuel_t_id =:contractuel_t_id,dateDebut = :dateDebut, dateFin = :dateFin WHERE contractuel_id = :contractuel_id");
        $rq->bindParam(':contractuel_id', $contractuel_id);
        $rq->bindParam(':contractuel_sup_id', $contractuel_sup_id);
        $rq->bindParam(':contractuel_sujet_recherche', $contractuel_sujet_recherche);
        $rq->bindParam(':contractuel_t_id', $contractuel_t_id);
        $rq->bindParam(':dateDebut', $dateDebut);
        $rq->bindParam(':dateFin', $dateFin);
        $rq->execute();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoInviteModifie($invite_id,$invite_par_id,$invite_commentaire) {
  $rq = ModelPersonne::$pdo->prepare("UPDATE invite SET invite_par_id = :invite_par_id,invite_commentaire = :invite_commentaire WHERE invite_id = :invite_id");
        $rq->bindParam(':invite_id', $invite_id);
        $rq->bindParam(':invite_par_id', $invite_par_id);
        $rq->bindParam(':invite_commentaire', $invite_commentaire);
        $rq->execute();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoPermanentCreation($numHistorique,$permanent_c_id,$t_id,$evaluateur) {
        $rq = ModelPersonne::$pdo->prepare( "INSERT INTO permanent (permanent_c_id,t_id,evaluateur) VALUES (:permanent_c_id,:t_id,:evaluateur)");
        $rq->bindParam(':permanent_c_id', $permanent_c_id);
        $rq->bindParam(':t_id', $t_id);
        $rq->bindParam(':evaluateur', $evaluateur);
        $rq->execute();
        $id = ModelPersonne::$pdo->lastInsertId();

  	$rq2 = self::$pdo->prepare("UPDATE historique
                         SET permanent_id = :permanent_id
                         WHERE numHistorique = :numHistorique");
        $rq2->bindParam(':permanent_id', $id);
        $rq2->bindParam(':numHistorique', $numHistorique);
        $rq2 ->execute();
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoPermanentModifie($permanent_id,$permanent_c_id,$t_id,$evaluateur) {

  $rq = ModelPersonne::$pdo->prepare("UPDATE permanent SET permanent_c_id = :permanent_c_id,t_id = :t_id,evaluateur = :evaluateur WHERE permanent_id = :permanent_id");
        $rq->bindParam(':permanent_id', $permanent_id);
        $rq->bindParam(':permanent_c_id', $permanent_c_id);
        $rq->bindParam(':t_id', $t_id);
        $rq->bindParam(':evaluateur', $evaluateur);
        $rq->execute();



}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoHistoriqueModifie($numHistorique,$p_id,$sit_id,$ty_id,$st_id,$dateDebutH,$dateFinH,$stageId,$theseId,$contractuel_id,$permanent_id,$ressource) {
//echo 'nh'.$numHistorique.'pid:'.$p_id .'site:'.$sit_id.'typ:'.$ty_id.'statut'.$st_id.'debut'.$dateDebutH.'fin'.$dateFinH.'stageid:'.$stageId.'theseid:'.$theseId.'contra'.$contractuel_id.'permanent'.$permanent_id;
        $rq = ModelPersonne::$pdo->prepare("UPDATE historique SET p_id = :p_id,sit_id = :sit_id,ty_id = :ty_id,st_id = :st_id,dateDebutH = :dateDebutH,dateFinH = :dateFinH,stageId = :stageId,theseId = :theseId,contractuel_id = :contractuel_id,permanent_id = :permanent_id WHERE numHistorique = :numHistorique");

        $rq->bindParam(':p_id', $p_id);
        $rq->bindParam(':sit_id', $sit_id);
        $rq->bindParam(':ty_id', $ty_id);
        $rq->bindParam(':st_id', $st_id);
        $rq->bindParam(':dateDebutH', $dateDebutH);
        $rq->bindParam(':dateFinH', $dateFinH);
        $rq->bindParam(':stageId', $stageId);
        $rq->bindParam(':theseId', $theseId);
        $rq->bindParam(':contractuel_id', $contractuel_id);
        $rq->bindParam(':permanent_id', $permanent_id);
        $rq->bindParam(':numHistorique', $numHistorique);
        $rq->execute();


//foreach ($ressource as $rid) echo "RESS".$rid;
//die;
        $rq3 = ModelPersonne::$pdo->prepare(" DELETE FROM ressourcesMessageHistorique WHERE numHistorique = :numHistorique ");
        $rq3->bindParam(':numHistorique', $numHistorique);
        $rq3->execute();

foreach ($ressource as $rid) {
        $rq2 = ModelPersonne::$pdo->prepare(" INSERT INTO ressourcesMessageHistorique (rid,numHistorique)
        VALUES (:rid,:numHistorique)");

        $rq2->bindParam(':rid', $rid);
        $rq2->bindParam(':numHistorique', $numHistorique);
        $rq2->execute();

}




}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoHistoriqueAjoute($p_id,$sit_id,$ty_id,$st_id,$dateDebutH,$dateFinH,$ressourcen,$stageId,$theseId,$contractuel_id,$invite_id,$permanent_id) {

//echo $p_id.",".$sit_id.",".$ty_id.",".$st_id.",".$dateDebutH.",".$dateFinH.",".$ressourcen.",".$stageId.",".$theseId.",".$contractuel_id.",".$invite_id.",".$permanent_id;
//echo "pid".$p_id;
//echo "datefin".$dateFinH;

foreach ($ressourcen as $rid) echo "rid".$rid; 
//echo "stageId".$stageId;


$rq = ModelPersonne::$pdo->prepare(" INSERT INTO historique (p_id,sit_id,ty_id,st_id,dateDebutH,dateFinH,stageId,theseId,contractuel_id,permanent_id,invite_id) 
VALUES (:p_id,:sit_id,:ty_id,:st_id,:dateDebutH,:dateFinH,:stageId,:theseId,:contractuel_id,:permanent_id,:invite_id)"); 

        $rq->bindParam(':p_id', $p_id);
        $rq->bindParam(':sit_id', $sit_id);
        $rq->bindParam(':ty_id', $ty_id);
        $rq->bindParam(':st_id', $st_id);
        $rq->bindParam(':dateDebutH', $dateDebutH);
        $rq->bindParam(':dateFinH', $dateFinH);
        $rq->bindParam(':stageId', $stageId);
        $rq->bindParam(':theseId', $theseId);
        $rq->bindParam(':contractuel_id', $contractuel_id);
        $rq->bindParam(':permanent_id', $permanent_id);
        $rq->bindParam(':invite_id', $invite_id);
        $rq->execute();

       $numHistorique = ModelPersonne::$pdo->lastInsertId();
//       echo 'NUMHISTORIQUE'.$numHistorique;
//die;

foreach ($ressourcen as $rid) {
	$rq2 = ModelPersonne::$pdo->prepare(" INSERT INTO ressourcesMessageHistorique (rid,numHistorique) 
	VALUES (:rid,:numHistorique)");

        $rq2->bindParam(':rid', $rid);
        $rq2->bindParam(':numHistorique', $numHistorique);
        $rq2->execute();

} 

return $numHistorique;


}





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoPersonneArchive ($id) {
   try {


 $update_personne = self::$pdo->prepare ("
UPDATE t_personne SET t_personne.archive =  \"1\",t_personne.demandeIntranet = \"0\"  WHERE t_personne.p_id = :p_id
        ");

          $update_personne->bindParam ( ':p_id', $id );
          $update_personne->execute ();
        } catch ( PDOException $e ) {
                echo "Error: " . $e->getMessage ();
                die;
        }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoPersonneDesarchive ($id) {
   try {


 $update_personne = self::$pdo->prepare ("
UPDATE t_personne SET t_personne.archive =  \"0\",t_personne.demandeIntranet = \"0\"  WHERE t_personne.p_id = :p_id
        ");

          $update_personne->bindParam ( ':p_id', $id );
          $update_personne->execute ();
        } catch ( PDOException $e ) {
                echo "Error: " . $e->getMessage ();
                die;
        }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoPersonneAjoute( $sexe,$p_nom, $p_prenom,$dateNaissance, $p_mail,$p_mail_perso,$nationalite, 
$p_tel,$p_tel_perso,$orcid,$IdHAL,$p_page_perso,$p_commentaire,$personneaprevenir,
$sectionCNRS,$collegeCNRSId,$sectionCNU,$collegeCNUId,$commissionIRD,$collegeIRDId,

$demandeIntranet)  {

        $rq = ModelPersonne::$pdo->prepare("INSERT INTO t_personne (sexe,p_nom, p_prenom, dateNaissance, p_mail, p_mail_perso, nationalite,
p_tel,p_tel_perso,orcid,IdHAL, p_page_perso,p_commentaire,personneaprevenir,
sectionCNRS,collegeCNRSId,sectionCNU,collegeCNUId,commissionIRD,collegeIRDId,
demandeIntranet)
VALUES (:sexe,:p_nom, :p_prenom, :dateNaissance,:p_mail, :p_mail_perso, :nationalite,
 :p_tel, :p_tel_perso,:orcid,:IdHAL,:p_page_perso,:p_commentaire,:personneaprevenir,
:sectionCNRS,:collegeCNRSId,:sectionCNU,:collegeCNUId,:commissionIRD,:collegeIRDId,
:demandeIntranet) ");

        $rq->bindParam(':sexe', $sexe);
        $rq->bindParam(':p_nom', $p_nom);
        $rq->bindParam(':p_prenom', $p_prenom);

       if ($dateNaissance != '') {
                //      $chdate = explode('/',$dateNaissance);
                //      $dateNaissance = $chdate[2].'-'.$chdate[1].'-'.$chdate[0];
                 $rq->bindParam ( ':dateNaissance', $dateNaissance );
        }
        else  $rq->bindParam(':dateNaissance', $myNull, PDO::PARAM_NULL);

        $rq->bindParam(':p_mail', $p_mail);
        $rq->bindParam(':p_mail_perso', $p_mail_perso);

       if ($nationalite != '')  $rq->bindParam ( ':nationalite', $nationalite );
        else  $rq->bindParam(':nationalite', $myNull, PDO::PARAM_NULL);


       if ($p_tel != '')  $rq->bindParam ( ':p_tel', $p_tel );
        else  $rq->bindParam(':p_tel', $myNull, PDO::PARAM_NULL);

       if ($p_tel_perso != '')  $rq->bindParam ( ':p_tel_perso', $p_tel_perso );
        else  $rq->bindParam(':p_tel_perso', $myNull, PDO::PARAM_NULL);

       if ($orcid != '')  $rq->bindParam ( ':orcid', $orcid );
        else  $rq->bindParam(':orcid', $myNull, PDO::PARAM_NULL);

       if ($IdHAL != '')  $rq->bindParam ( ':IdHAL', $IdHAL );
        else  $rq->bindParam(':IdHAL', $myNull, PDO::PARAM_NULL);


       if ($p_page_perso != '')  $rq->bindParam ( ':p_page_perso', $p_page_perso );
        else  $rq->bindParam(':p_page_perso', $myNull, PDO::PARAM_NULL);

       if ($p_commentaire != '')  $rq->bindParam ( ':p_commentaire', $p_commentaire );
        else  $rq->bindParam(':p_commentaire', $myNull, PDO::PARAM_NULL);


       if ($personneaprevenir != '')  $rq->bindParam ( ':personneaprevenir', $personneaprevenir );
        else  $rq->bindParam(':personneaprevenir', $myNull, PDO::PARAM_NULL);

       if ($sectionCNRS != '')  $rq->bindParam ( ':sectionCNRS', $sectionCNRS );
        else  $rq->bindParam(':sectionCNRS', $myNull, PDO::PARAM_NULL);
       if ($collegeCNRSId != '')  $rq->bindParam ( ':collegeCNRSId', $collegeCNRSId );
        else  $rq->bindParam(':collegeCNRSId', $myNull, PDO::PARAM_NULL);

       if ($sectionCNU != '')  $rq->bindParam ( ':sectionCNU', $sectionCNU );
        else  $rq->bindParam(':sectionCNU', $myNull, PDO::PARAM_NULL);
       if ($collegeCNUId != '')  $rq->bindParam ( ':collegeCNUId', $collegeCNUId );
        else  $rq->bindParam(':collegeCNUId', $myNull, PDO::PARAM_NULL);

       if ($commissionIRD != '')  $rq->bindParam ( ':commissionIRD', $commissionIRD );
        else  $rq->bindParam(':commissionIRD', $myNull, PDO::PARAM_NULL);
       if ($collegeIRDId != '')  $rq->bindParam ( ':collegeIRDId', $collegeIRDId );
        else  $rq->bindParam(':collegeIRDId', $myNull, PDO::PARAM_NULL);



   $rq->bindParam(':demandeIntranet', $demandeIntranet);

        $rq->execute();
        $id = ModelPersonne::$pdo->lastInsertId();
        return $id;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoInfoAdmin( $p_id,$hdr,$horaireacces,$chartesSignees,$formationSecurite,$numeroCle ,$ficheNouveauArrivant,$sessionInformatique)  {

        $rq = ModelPersonne::$pdo->prepare("UPDATE t_personne SET hdr = :hdr,horaireacces = :horaireacces,chartesSignees = :chartesSignees,formationSecurite = :formationSecurite,numeroCle = :numeroCle,ficheNouveauArrivant = :ficheNouveauArrivant,sessionInformatique = :sessionInformatique WHERE p_id = :p_id");

   	$rq->bindParam(':p_id', $p_id);
       $rq->bindParam ( ':hdr', $hdr );
       if ($horaireacces != '')  $rq->bindParam ( ':horaireacces', $horaireacces );
        else  $rq->bindParam(':horaireacces', $myNull, PDO::PARAM_NULL);

       $rq->bindParam ( ':chartesSignees', $chartesSignees );
       $rq->bindParam ( ':formationSecurite', $formationSecurite );

       if ($numeroCle != '')  $rq->bindParam ( ':numeroCle', $numeroCle );
        else  $rq->bindParam(':numeroCle', $myNull, PDO::PARAM_NULL);

       $rq->bindParam ( ':ficheNouveauArrivant', $ficheNouveauArrivant );
       $rq->bindParam ( ':sessionInformatique', $sessionInformatique );
        $rq->execute();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoPersonneModifie( $p_id,$sexe,$p_nom, $p_prenom,$dateNaissance, $p_mail,$p_mail_perso,$nationalite,
$p_tel,$p_tel_perso,$orcid,$IdHAL,$p_page_perso,$p_commentaire,$personneaprevenir,
$sectionCNRS,$collegeCNRSId,$sectionCNU,$collegeCNUId,$commissionIRD,$collegeIRDId,
$demandeIntranet)  {



        $rq = ModelPersonne::$pdo->prepare("UPDATE t_personne SET sexe = :sexe, p_nom = :p_nom, p_prenom = :p_prenom, dateNaissance = :dateNaissance, p_mail = :p_mail, p_mail_perso = :p_mail_perso, nationalite = :nationalite,
p_tel = :p_tel,p_tel_perso = :p_tel_perso, orcid = :orcid,IdHAL = :IdHAL,p_page_perso = :p_page_perso,p_commentaire = :p_commentaire,personneaprevenir = :personneaprevenir,
sectionCNRS = :sectionCNRS ,collegeCNRSId = :collegeCNRSId ,sectionCNU = :sectionCNU , collegeCNUId = :collegeCNUId , commissionIRD = :commissionIRD , collegeIRDId = :collegeIRDId,
demandeIntranet = :demandeIntranet WHERE p_id = :p_id");


   	$rq->bindParam(':p_id', $p_id);
        $rq->bindParam(':sexe', $sexe);
        $rq->bindParam(':p_nom', $p_nom);
        $rq->bindParam(':p_prenom', $p_prenom);

       if ($dateNaissance != '') {
                //      $chdate = explode('/',$dateNaissance);
                //      $dateNaissance = $chdate[2].'-'.$chdate[1].'-'.$chdate[0];
                 $rq->bindParam ( ':dateNaissance', $dateNaissance );
        }
        else  $rq->bindParam(':dateNaissance', $myNull, PDO::PARAM_NULL);

        $rq->bindParam(':p_mail', $p_mail);
        $rq->bindParam(':p_mail_perso', $p_mail_perso);

       if ($nationalite != '')  $rq->bindParam ( ':nationalite', $nationalite );
        else  $rq->bindParam(':nationalite', $myNull, PDO::PARAM_NULL);


       if ($p_tel != '')  $rq->bindParam ( ':p_tel', $p_tel );
        else  $rq->bindParam(':p_tel', $myNull, PDO::PARAM_NULL);

       if ($p_tel_perso != '')  $rq->bindParam ( ':p_tel_perso', $p_tel_perso );
        else  $rq->bindParam(':p_tel_perso', $myNull, PDO::PARAM_NULL);

       if ($orcid != '')  $rq->bindParam ( ':orcid', $orcid );
        else  $rq->bindParam(':orcid', $myNull, PDO::PARAM_NULL);

       if ($IdHAL != '')  $rq->bindParam ( ':IdHAL', $IdHAL );
        else  $rq->bindParam(':IdHAL', $myNull, PDO::PARAM_NULL);


       if ($p_page_perso != '')  $rq->bindParam ( ':p_page_perso', $p_page_perso );
        else  $rq->bindParam(':p_page_perso', $myNull, PDO::PARAM_NULL);

       if ($p_commentaire != '')  $rq->bindParam ( ':p_commentaire', $p_commentaire );
        else  $rq->bindParam(':p_commentaire', $myNull, PDO::PARAM_NULL);


       if ($personneaprevenir != '')  $rq->bindParam ( ':personneaprevenir', $personneaprevenir );
        else  $rq->bindParam(':personneaprevenir', $myNull, PDO::PARAM_NULL);


       if ($sectionCNRS != '')  $rq->bindParam ( ':sectionCNRS', $sectionCNRS );
        else  $rq->bindParam(':sectionCNRS', $myNull, PDO::PARAM_NULL);
       if ($collegeCNRSId != '')  $rq->bindParam ( ':collegeCNRSId', $collegeCNRSId );
        else  $rq->bindParam(':collegeCNRSId', $myNull, PDO::PARAM_NULL);

       if ($sectionCNU != '')  $rq->bindParam ( ':sectionCNU', $sectionCNU );
        else  $rq->bindParam(':sectionCNU', $myNull, PDO::PARAM_NULL);
       if ($collegeCNUId != '')  $rq->bindParam ( ':collegeCNUId', $collegeCNUId );
        else  $rq->bindParam(':collegeCNUId', $myNull, PDO::PARAM_NULL);

       if ($commissionIRD != '')  $rq->bindParam ( ':commissionIRD', $commissionIRD );
        else  $rq->bindParam(':commissionIRD', $myNull, PDO::PARAM_NULL);
       if ($collegeIRDId != '')  $rq->bindParam ( ':collegeIRDId', $collegeIRDId );
        else  $rq->bindParam(':collegeIRDId', $myNull, PDO::PARAM_NULL);



       $rq->bindParam ( ':demandeIntranet', $demandeIntranet );


        $rq->execute();
}


//$missionFrance, $dateMissionFrance , $missionEtranger, $dateMissionEtranger, $demandeBureau, $demandePoste, $laboChimie,$salleBlanche,
/////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoStageModifie ($stageId,$p_id,  $p_stag_encadrant, $stag_intitule, $etablissement, $fId, $autreformation,
 $t_conv_id, $gratification,$numMissionnaire,  $montantGratif,$montantTransport, $listeFinancementCredit,$listeFinancementCreditNouveau, $financementEntite,
 $conventionEnvoyee, $conventionRevenue, $scanConvention,$numBCSifac,$refFournisseur, $dossierGratif, $dateDossierGratif,  $parDossierGratif ,
 $stageModifiePar,$stageModifieDate,$dateDebut,$dateFin) {


//                                missionFrance =:missionFrance,
//                                dateMissionFrance =:dateMissionFrance,
//                                missionEtranger =:missionEtranger,
//                                dateMissionEtranger =:dateMissionEtranger,
//                                demandeBureau =:demandeBureau,
//                                demandePoste =:demandePoste,
//                                laboChimie =:laboChimie,
//                                salleBlanche =:salleBlanche,

        try {
        $rq = self::$pdo->prepare("UPDATE stage
                         SET
                                p_stag_encadrant =:p_stag_encadrant,
                                stag_intitule =:stag_intitule,
                                etablissement =:etablissement,
                                f_id =:f_id,
                                autreformation =:autreformation,
                                t_conv_id =:t_conv_id,
                                gratification =:gratification,
                                financementEntite =:financementEntite,
                                conventionEnvoyee =:conventionEnvoyee,
                                conventionRevenue =:conventionRevenue,
                                scanConvention =:scanConvention,
                                dossierGratif =:dossierGratif,
                                montantGratif =:montantGratif,
                                montantTransport =:montantTransport,
                                numMissionnaire  =:numMissionnaire,
                                refFournisseur =:refFournisseur,
                                numBCSifac =:numBCSifac,
                                dateDossierGratif =:dateDossierGratif,
                                parDossierGratif =:parDossierGratif,
                                stageModifiePar = :stageModifiePar,
                                stageModifieDate = :stageModifieDate,
				dateDebut = :dateDebut,
				dateFin = :dateFin

                         WHERE stageId = :stageId");

        $rq->bindParam(':stageId', $stageId);
        $rq->bindParam(':p_stag_encadrant', $p_stag_encadrant);
        $rq->bindParam(':stag_intitule', $stag_intitule);
        $rq->bindParam(':etablissement', $etablissement);
        $rq->bindParam(':f_id', $fId);
        $rq->bindParam(':autreformation', $autreformation);
        $rq->bindParam(':t_conv_id', $t_conv_id);
        $rq->bindParam(':gratification', $gratification);

        $rq->bindParam(':financementEntite', $financementEntite);

//        $rq->bindParam(':missionFrance', $missionFrance);
//        $rq->bindParam(':dateMissionFrance', $dateMissionFrance);
//        $rq->bindParam(':missionEtranger', $missionEtranger);
//        $rq->bindParam(':dateMissionEtranger', $dateMissionEtranger);
//        $rq->bindParam(':demandeBureau', $demandeBureau);
//        $rq->bindParam(':demandePoste', $demandePoste);
//        $rq->bindParam(':laboChimie', $laboChimie);
//        $rq->bindParam(':salleBlanche', $salleBlanche);
        $rq->bindParam(':conventionEnvoyee', $conventionEnvoyee);
        $rq->bindParam(':conventionRevenue', $conventionRevenue);
        $rq->bindParam(':scanConvention', $scanConvention);
        $rq->bindParam(':dossierGratif', $dossierGratif);
        $rq->bindParam(':montantGratif', $montantGratif);
        $rq->bindParam(':montantTransport', $montantTransport);
        $rq->bindParam(':numMissionnaire', $numMissionnaire);
        $rq->bindParam(':refFournisseur', $refFournisseur);
        $rq->bindParam(':numBCSifac', $numBCSifac);
        $rq->bindParam(':dateDossierGratif', $dateDossierGratif);
        $rq->bindParam(':parDossierGratif', $parDossierGratif);

        $rq->bindParam(':stageModifiePar', $stageModifiePar);
        $rq->bindParam(':stageModifieDate', $stageModifieDate);
        $rq->bindParam(':dateDebut', $dateDebut);
        $rq->bindParam(':dateFin', $dateFin);
        $rq ->execute();




 $del = self::$pdo->prepare ("DELETE FROM stageFinancement WHERE stageId LIKE :stageId ");
 $del->bindParam ( ':stageId', $stageId );
 $del->execute ();

foreach ($listeFinancementCredit as $row)  {
     $requeteInsertion = self::$pdo->prepare("INSERT INTO stageFinancement
        (stageId,financementCredit,dateDebut,dateFin)
         VALUES
(:stageId,:financementCredit,:dateDebut,:dateFin)
         ");
        $requeteInsertion->bindParam(':stageId', $stageId);
        $requeteInsertion->bindParam(':financementCredit', $row->financementCredit);
        $requeteInsertion->bindParam(':dateDebut', $row->dateDebut);
        $requeteInsertion->bindParam(':dateFin', $row->dateFin);
        $requeteInsertion->execute();
}

// Juste les nouveaux
if ($listeFinancementCreditNouveau->financementCredit  != '') {
 $requeteInsertion = self::$pdo->prepare("INSERT INTO stageFinancement
        (stageId,financementCredit,dateDebut,dateFin)
         VALUES
(:stageId,:financementCredit,:dateDebut,:dateFin)
         ");
        $requeteInsertion->bindParam(':stageId', $stageId);
        $requeteInsertion->bindParam(':financementCredit', $listeFinancementCreditNouveau->financementCredit);
        $requeteInsertion->bindParam(':dateDebut', $listeFinancementCreditNouveau->dateDebut);
        $requeteInsertion->bindParam(':dateFin', $listeFinancementCreditNouveau->dateFin);
        $requeteInsertion->execute();
}

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
        die;

        }

}




/////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoStageVideCreation ($p_id,$numHistorique) {
        try {
        $rq = self::$pdo->prepare("INSERT INTO  stage	(p_id)
			VALUES 
			(:p_id)
			");
       $rq->bindParam(':p_id', $p_id);
        $rq ->execute();
	$stageId = ModelPersonne::$pdo->lastInsertId();

     $rq2 = self::$pdo->prepare("UPDATE historique
                         SET
                                stageId = :stageId

                         WHERE numHistorique = :numHistorique");
        $rq2->bindParam(':stageId', $stageId);
        $rq2->bindParam(':numHistorique', $numHistorique);
        $rq2 ->execute();
         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
        die;

        }
} 







////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoTheseModifie( $theseId,$p_id,$p_these_directeur1,$p_these_directeur2,$ecoleDoctorale,$intitule,$tfinancement,$m2obtenu, $laboaccueil, $etsInscription,$etsCoTutelle,$theseSoutenance,$abandon,$apresThese,$modifiePar,$modifieDate,$dateDebut,$dateFin) {
       try {

     $rq = self::$pdo->prepare("UPDATE theses
                         SET
                                p_these_directeur1 = :p_these_directeur1,
                                p_these_directeur2 = :p_these_directeur2,
                                ecoleDoctorale = :ecoleDoctorale,
                                intitule  = :intitule,
                                tfinancement= :tfinancement,
                                m2obtenu= :m2obtenu,
                                laboaccueil= :laboaccueil,
                                etsInscription= :etsInscription,
                                etsCoTutelle= :etsCoTutelle,
                                theseSoutenance = :theseSoutenance,
                                abandon = :abandon,
                                apresThese = :apresThese,
                                modifiePar = :modifiePar,
                                modifieDate = :modifieDate,
				dateDebut = :dateDebut,
				dateFin = :dateFin

                         WHERE theseId = :theseId");


    $rq->bindParam(':theseId', $theseId);
    $rq->bindParam(':p_these_directeur1', $p_these_directeur1);
    $rq->bindParam(':p_these_directeur2', $p_these_directeur2);
    $rq->bindParam(':ecoleDoctorale', $ecoleDoctorale);
    $rq->bindParam(':intitule', $intitule);
    $rq->bindParam(':tfinancement', $tfinancement);
    $rq->bindParam(':m2obtenu', $m2obtenu);
    $rq->bindParam(':laboaccueil', $laboaccueil);
    $rq->bindParam(':etsInscription', $etsInscription);
    $rq->bindParam(':etsCoTutelle', $etsCoTutelle);
   $rq->bindParam(':theseSoutenance', $theseSoutenance);
   $rq->bindParam(':abandon', $abandon);
   $rq->bindParam(':apresThese', $apresThese);
   $rq->bindParam(':modifiePar', $modifiePar);
   $rq->bindParam(':modifieDate', $modifieDate);
   $rq->bindParam(':dateDebut', $dateDebut);
   $rq->bindParam(':dateFin', $dateFin);



        $rq->execute();

  }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
        die;

        }


}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoTheseVideCreation( $p_id,$numHistorique) {
                try {
$rq= ModelPersonne::$pdo->prepare("INSERT INTO theses (p_id) VALUES (:p_id)");
        $rq->bindParam(':p_id', $p_id);
        $rq->execute();
        $theseId = ModelPersonne::$pdo->lastInsertId();

        $rq2 = self::$pdo->prepare("UPDATE historique
                         SET
                                theseId = :theseId

                         WHERE numHistorique = :numHistorique");
        $rq2->bindParam(':theseId', $theseId);
        $rq2->bindParam(':numHistorique', $numHistorique);
        $rq2 ->execute();
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
die;
                }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static function daoTheseCreation( $p_id,$numHistorique,$p_these_directeur1,$p_these_directeur2,$ecoleDoctorale,$intitule,$tfinancement,$m2obtenu, $laboaccueil, $etsInscription,$etsCoTutelle,$theseSoutenance,$abandon,$apresThese,$modifiePar,$modifieDate) {



                try {

$rq= ModelPersonne::$pdo->prepare("INSERT INTO theses (p_id,p_these_directeur1,p_these_directeur2,ecoleDoctorale,intitule,tfinancement,m2obtenu, laboaccueil, etsInscription,etsCoTutelle,theseSoutenance,abandon,apresThese,modifiePar,modifieDate) VALUES (:p_id,:p_these_directeur1,:p_these_directeur2,:ecoleDoctorale,:intitule,:tfinancement,:m2obtenu, :laboaccueil, :etsInscription,:etsCoTutelle,:theseSoutenance,:abandon,:apresThese,:modifiePar,:modifieDate)");

        $rq->bindParam(':p_id', $p_id);
        $rq->bindParam(':p_these_directeur1', $p_these_directeur1);
        $rq->bindParam(':p_these_directeur2', $p_these_directeur2);
        $rq->bindParam(':ecoleDoctorale', $ecoleDoctorale);
        $rq->bindParam(':intitule', $intitule);
        $rq->bindParam(':tfinancement', $tfinancement);
        $rq->bindParam(':m2obtenu', $m2obtenu);
        $rq->bindParam(':laboaccueil', $laboaccueil);
        $rq->bindParam(':etsInscription', $etsInscription);
        $rq->bindParam(':etsCoTutelle', $etsCoTutelle);
        $rq->bindParam(':theseSoutenance', $theseSoutenance);
   $rq->bindParam(':abandon', $abandon);
   $rq->bindParam(':apresThese', $apresThese);
        $rq->bindParam(':modifiePar', $modifiePar);
        $rq->bindParam(':modifieDate', $modifieDate);

        $rq->execute();
        $theseId = ModelPersonne::$pdo->lastInsertId();

     $rq2 = self::$pdo->prepare("UPDATE historique
                         SET
                                theseId = :theseId

                         WHERE numHistorique = :numHistorique");
        $rq2->bindParam(':theseId', $theseId);
        $rq2->bindParam(':numHistorique', $numHistorique);
        $rq2 ->execute();

                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
die;
                }


}



//SELECT historique.numHistorique,p_id,sit_id,ty_id,st_id,dateDebutH,dateFinH,stageId,theseId,contractuel_id,permanent_id,invite_id FROM historique WHERE  p_id = :p_id ORDER BY dateDebutH DESC"); 

        static function daoPersonneHistoriqueTableau($id_personne) {

                try {
                       self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                        $requeteSelectChampsPers = self::$pdo->prepare ( "




SELECT historique.numHistorique,p_id,sit_id,ty_id,st_id,dateDebutH,dateFinH,stageId,theseId,contractuel_id,permanent_id,invite_id,GROUP_CONCAT(rid SEPARATOR ';') AS ressources FROM historique
LEFT JOIN  ressourcesMessageHistorique ON historique.numHistorique = ressourcesMessageHistorique.numHistorique
 WHERE  p_id = :p_id GROUP BY numHistorique ORDER BY dateDebutH DESC"); 



//SELECT historique.numHistorique,p_id,sit_id,ty_id,st_id,dateDebutH,dateFinH,stageId,theseId,contractuel_id,permanent_id,invite_id,GROUP_CONCAT(rid SEPARATOR ';') AS ressources FROM historique,ressourcesMessageHistorique WHERE historique.numHistorique = ressourcesMessageHistorique.numHistorique AND p_id = :p_id ORDER BY dateDebutH DESC"); 

                        $requeteSelectChampsPers->bindParam(':p_id', $id_personne);
                        $requeteSelectChampsPers->execute ();
                        return $requeteSelectChampsPers;
                } catch ( PDOException $e ) {
                        echo 'Error' . $e->getMessage ();
                }

}

//SELECT * FROM historique,ressourcesMessageHistorique WHERE historique.numHistorique = ressourcesMessageHistorique.numHistorique AND p_id = :p_id ORDER BY dateDebutH DESC" );






static function ajoute_personne_equipe($numHistorique,$tabequipe) {
        try {
        foreach ($tabequipe as $eq_id)  {
                $requeteInsertDiscipline = self::$pdo->prepare ( "INSERT INTO t_membres_equipe2 (eq_id,numHistorique) VALUES (:eq_id,:numHistorique)" );
                $requeteInsertDiscipline->bindParam ( ':eq_id', $eq_id );
                $requeteInsertDiscipline->bindParam ( ':numHistorique', $numHistorique );
                $requeteInsertDiscipline->execute ();
        }

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
                die;

        }

}


static function ajoute_membre_equipe($id_equipe,$tabmembres) {
        try {
        foreach ($tabmembres as $numHistorique)  {
                $requeteInsertDiscipline = self::$pdo->prepare ( "INSERT INTO t_membres_equipe2 (eq_id,numHistorique) VALUES (:eq_id,:numHistorique)" );
                $requeteInsertDiscipline->bindParam ( ':eq_id', $id_equipe );
                $requeteInsertDiscipline->bindParam ( ':numHistorique', $numHistorique );
                $requeteInsertDiscipline->execute ();
        }

         }
        catch (PDOException $e) {
                echo 'Error :' .$e->getMessage();
                die;

        }

}



//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  recuperer_personneRessourceListe($numHistorique){
    $rq = Model::$pdo->prepare (
	 "SELECT rid FROM ressourcesMessageHistorique
	 WHERE numHistorique LIKE :numHistorique ");




                        $rq->bindParam(':numHistorique', $numHistorique);
                        $rq->execute ();

$stack = array();
foreach ($rq as $r) array_push($stack,$r->rid);
return($stack);
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////
  static function  getPersonne($nom,$prenom) {
              self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );



$prenomcomposes = explode(" ",$prenom);
$nomcomposes = explode(" ",$nom);

                $rq = Model::$pdo->prepare (
                         "SELECT p1.p_id,historique.numHistorique,theses.theseId,t_equipe.eq_id, p1.p_nom, p1.p_prenom,p1.sexe,eq_nom,p1.dateNaissance,m2obtenu,dateDebutH,theseSoutenance,CONCAT (p2.p_nom,' ',p2.p_prenom) AS directeur1,CONCAT (p3.p_nom,' ',p3.p_prenom) AS directeur2,tfinancement,levenshtein(TRIM(p1.p_nom), :nom) AS levenshtein1  ,levenshtein(TRIM(p1.p_prenom), :prenom) AS levenshtein2
                                FROM t_personne p1
                                LEFT JOIN historique ON historique.p_id = p1.p_id
                                LEFT JOIN t_membres_equipe2 ON historique.numHistorique = t_membres_equipe2.numHistorique
                                LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id
				LEFT JOIN theses ON historique.theseId = theses.theseId
				LEFT JOIN t_personne p2 ON theses.p_these_directeur1 = p2.p_id
				LEFT JOIN t_personne p3 ON theses.p_these_directeur2 = p3.p_id
                                WHERE
 ((levenshtein(TRIM(p1.p_nom), :nom) BETWEEN 0 AND 2 ) OR (levenshtein(TRIM(p1.p_nom), :nomcomposes) BETWEEN 0 AND 1) OR (p1.p_nom LIKE CONCAT(:nom,'%')))
 AND ((levenshtein(TRIM(p1.p_prenom), :prenom) BETWEEN 0 AND 2  ) OR (levenshtein(TRIM(p1.p_prenom), :prenomcomposes) BETWEEN 0 AND 1)OR (p1.p_prenom LIKE CONCAT(:prenom,'%')))
                        ");
                $rq->bindParam ( ':nom', $nom );
                $rq->bindParam ( ':prenom', $prenom );
                $rq->bindParam ( ':prenomcomposes', $prenomcomposes[0] );
                $rq->bindParam ( ':nomcomposes', $nomcomposes[0] );




               $rq->execute ();
                $f = $rq ->fetchObject();
                        return $f;

}
 // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_effectif($fromClause,$whereClause) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
                 $requeteSelect = self::$pdo->prepare ( "
select DISTINCT st_nom,count(*) as nb from t_personne,historique,t_statut2".$fromClause."
WHERE t_personne.p_id like historique.p_id
AND historique.st_id like t_statut2.st_id 
".$whereClause."
AND dateDebutH = (SELECT MAX(dateDebutH) FROM historique WHERE historique.p_id=t_personne.p_id) 
GROUP BY st_nom

");


                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }



// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        static function recuperation_effectif_sexe($fromClause,$whereClause) {
                try {

                        self::$pdo->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );

                 $requeteSelect = self::$pdo->prepare ( "
select sexe,count(*) as nb from t_personne,historique".$fromClause."
WHERE t_personne.p_id like historique.p_id
".$whereClause."
AND dateDebutH = (SELECT MAX(dateDebutH) FROM historique WHERE historique.p_id=t_personne.p_id)
GROUP BY sexe

");
//select sexe,count(*) as nb from t_personne,historique WHERE t_personne.p_id like historique.p_id ".$whereClause." AND t_personne.archive = 0 AND dateDebutH = (SELECT MAX(dateDebutH) FROM historique WHERE historique.p_id=t_personne.p_id) GROUP BY sexe
//
//select sexe,count(*) as nb from t_personne,historique WHERE t_personne.p_id like historique.p_id AND historique.numHistorique like t_membres_equipe2.numHistorique ".$whereClause." AND t_personne.archive = 0 AND dateDebutH = (SELECT MAX(dateDebutH) FROM historique WHERE historique.p_id=t_personne.p_id) GROUP BY sexe
//select sexe,count(*) as nb from t_personne,historique WHERE t_personne.p_id like historique.p_id AND t_personne.archive = 0 AND dateDebutH = (SELECT MAX(dateDebutH) FROM historique WHERE historique.p_id=t_personne.p_id) AND sexe IS  NOT NULL GROUP BY sexe

                        $requeteSelect->execute ();

                        return $requeteSelect;
                } catch ( PDOException $e ) {
                        echo 'Error :' . $e->getMessage ();
                }
        }




}

ModelPersonne::set_static ();
?>

