<?php





class FormInformationsPersonnelles
{

// TAG 1 pour une creation
  private $creation;

  private $p_id;
  private $sexe;
  private $p_nom;
  private $p_prenom;
  private $dateNaissance;
  private $p_email;
  private $p_email_perso;
  private $nationalite;

  private $p_tel;
  private $p_tel_perso;
  private $orcid;
  private $IdHAL;
//  private $hdr;
  private $p_page_perso;
  private $p_commentaire;
  private $personneaprevenir;
//  private $horaireacces;
private $sectionCNRS;
private $collegeCNRSId;
private $sectionCNU;
private $collegeCNUId;
private $commissionIRD;
private $collegeIRDId;



 
// parcours nouvel arrivant
// private $chartesSignees;
 //private $formationSecurite;
// private $ficheNouveauArrivant;
// private $numeroCle;
// private $sessionInformatique;




// demande validation
 private $demandeIntranet;

public function __construct($tab)  {
  $err="";

   if (empty ( $tab ["p_nom"] )) $err=$err." nom";
   if (empty ( $tab ["p_prenom"] )) $err=$err." prenom";
   if (empty ( $tab ["p_mail"] )) $err=$err." email";
   if (empty ( $tab ["dateNaissance"] )) $err=$err." dateNaissance";
   if (empty ( $tab ["nationalite"] )) $err=$err." nationalite";


if (!($err==""))   throw new Exception($err);
  
else {

	$this->creation = $tab['creation'];


if ($this->creation=="0")        $this->p_id = $tab['p_id'];

        $this->sexe = $tab['sexe'];
        $this->p_nom = $tab['p_nom'];
        $this->p_prenom = $tab['p_prenom'];
        $this->p_email = $tab['p_mail'];
        $this->p_email_perso = $tab['p_mail_perso'];
        $this->dateNaissance = $tab['dateNaissance'];
        $this->nationalite = $tab['nationalite'];











        $this->p_tel = $tab['p_tel'];
        $this->p_tel_perso = $tab['p_tel_perso'];
        $this->orcid = $tab['orcid'];
        $this->IdHAL = $tab['IdHAL'];
        $this->p_page_perso = $tab['p_page_perso'];
        $this->p_commentaire = $tab['p_commentaire'];
        $this->personneaprevenir = $tab['personneaprevenir'];
$this->sectionCNRS= $tab['sectionCNRS'];
$this->collegeCNRSId= $tab['collegeCNRSId'];
$this->sectionCNU= $tab['sectionCNU'];
$this->collegeCNUId= $tab['collegeCNUId'];
$this->commissionIRD= $tab['commissionIRD'];
$this->collegeIRDId= $tab['collegeIRDId'];


//        $this->horaireacces = $tab['horaireacces'];


$this->demandeIntranet=$tab['demandeIntranet'];
  }
}


public function setCreation($arg)  { $this->creation = $arg;  }
public function setId($id)  {    $this->p_id = $id;  }
public function setSexe($sexe)  {    $this->sexe = $sexe;  }
public function setNom($nom)  {    $this->p_nom = $nom;  }
public function setPrenom($prenom)  {    $this->p_prenom = $prenom;  }
public function setDateNaissance($date)  {    $this->dateNaissance = $date;  }
public function setEmail($email)  {    $this->p_email = $email;  }
public function setEmailPerso($email)  {    $this->p_email_perso = $email;  }
public function setNationalite($nationalite)  {    $this->nationalite = $nationalite;  }


public function setTel($tel)  {   $this->p_tel = $tel;  }
public function setTelPerso($tel)  {   $this->p_tel_perso = $tel;  }
public function setOrcid($orcid)  {   $this->orcid = $orcid;  }
public function setIdHAL($IdHAL)  {   $this->IdHAL = $IdHAL;  }
public function setPagePerso($page)  {  $this->p_page_perso = $page ;  }
public function setCommentaire($comm)  {  $this->p_commentaire = $comm ;  }
public function setPersonneAPrevenir($p)  {    $this->personneaprevenir = $p;  }


public function setDemandeIntranet($arg) { $this->demandeIntranet = $arg;}





public function getCreation()  { return  $this->creation ;  }
public function getId()  { return  $this->p_id ;  }
public function getSexe()  { return  $this->sexe ;  }
public function getNom()  { return  $this->p_nom ;  }
public function getPrenom()  {  return  $this->p_prenom ;  }
public function getDateNaissance()  {  return  $this->dateNaissance ;  }
public function getEmail()  {  return  $this->p_email ;  }
public function getEmailPerso()  {  return  $this->p_email_perso ;  }
public function getNationalite()  {  return  $this->nationalite ;  }


public function getTel()  {  return  $this->p_tel ;  }
public function getTelPerso()  {  return  $this->p_tel_perso ;  }
public function getOrcid()  {  return  $this->orcid ;  }
public function getIdHAL()  {  return  $this->IdHAL ;  }
public function getPagePerso()  {  return  $this->p_page_perso ;  }
public function getCommentaire()  {  return  $this->p_commentaire ;  }
public function getPersonneAPrevenir()  {  return  $this->personneaprevenir;  }

public function getsectionCNRS()  {  return  $this->sectionCNRS;  }
public function getcollegeCNRSId()  {  return  $this->collegeCNRSId;  }
public function getsectionCNU()  {  return  $this->sectionCNU;  }
public function getcollegeCNUId()  {  return  $this->collegeCNUId;  }
public function getcommissionIRD()  {  return  $this->commissionIRD;  }
public function getcollegeIRDId()  {  return  $this->collegeIRDId;  }



public function getDemandeIntranet() { return $this->demandeIntranet;}


}
?>
