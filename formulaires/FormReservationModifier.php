<?php





class FormReservationModifier
{

  private $r_id;
  private $date_fin;

public function __construct($id,$datedebut,$datefin)  {
  $err="";

if ($datefin<$datedebut) $err=$err. "Date de début avant date de fin";
if (DateTime::createFromFormat('Y-m-d', $datefin) == FALSE) $err=$err. "Format date de fin invalide"; 

if (!($err==""))   throw new Exception($err);
  

else {
        $this->r_id = $id;
        $this->date_fin = $datefin;
      
  }
}
public function getDateFin()  {  return  $this->date_fin ;  }
public function getId()  {  return  $this->r_id ;  }

}
?>
