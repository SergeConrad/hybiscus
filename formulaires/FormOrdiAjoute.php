<?php





class FormOrdiAjoute
{

  private $p_id;
  private $hostname;
  private $adresse_ipv4;
  private $adresse_mac;
  private $type;
  private $serviceTag;
  private $numero_inventaire;
  private $date_achat;
  private $garantie;
  private $processeur;
  private $memoire;
  private $dhcp;
  private $commentaire;


public function __construct($tab)  {
  $err="";

   if (empty ( $tab ["hostname"] )) $err=$err." hostname";
if (!($err==""))   throw new Exception($err);
  

else {
        $this->p_id = $tab['p_id'];
        $this->hostname = $tab['hostname'];
        $this->adresse_ipv4 = $tab['adresse_ipv4'];
        $this->adresse_mac = $tab['adresse_mac'];
        $this->type = $tab['type'];
        $this->serviceTag = $tab['serviceTag'];
        $this->numero_inventaire = $tab['numero_inventaire'];
        $this->date_achat = $tab['date_achat'];
        $this->garantie = $tab['garantie'];
        $this->processeur = $tab['processeur'];
        $this->memoire = $tab['memoire'];
        $this->dhcp = $tab['dhcp'];
        $this->commentaire = $tab['commentaire'];

      
  }
}
public function getp_id()  { return  $this->p_id ;  }
public function gethostname()  { return  $this->hostname ;  }
public function getadresse_ipv4()  { return  $this->adresse_ipv4 ;  }
public function getadresse_mac()  { return  $this->adresse_mac ;  }
public function gettype()  { return  $this->type ;  }
public function getserviceTag()  { return  $this->serviceTag ;  }
public function getnumero_inventaire()  { return  $this->numero_inventaire ;  }
public function getdate_achat()  { return  $this->date_achat ;  }
public function getgarantie()  { return  $this->garantie ;  }
public function getprocesseur()  { return  $this->processeur ;  }
public function getmemoire()  { return  $this->memoire ;  }
public function getdhcp()  { return  $this->dhcp ;  }
public function getcommentaire()  { return  $this->commentaire ;  }

}
?>
