<?php





class FormProjet
{


//FLAGS
  private $iscreation;



  private $projet_id;
  private $equipe_id;
  private $nom_porteur;
  private $coordinateurProjet; // serge
  private $coordOuPartenaire;
  private $institut_rattachement;
  private $organisme_gestionnaire;
  private $financeur;
  private $nom_financeur;
  private $type;
  private $acronyme;
  private $partenaire_prive;
  private $sujet_projet;
  private $nbrecrutement;
  private $nbstagiaire;
  //private $phase;
  private $preselection;


  private $dateSoumission;
  private $dateDebut;
  private $dateFin;
  private $duree;
  private $montant;
  private $partHsm;
  private $partenaires;
  private $commentaires;
  private $nbrecrutement2;
  private $nbstagiaire2;




private $listeassocie;
private $associenouveau;

private $listeequipe;
private $equipenouveau;


public function __construct($tab)  {

  $err="";

//   if (empty ( $tab ["equipe_id"] )) $err=$err." Equipe";
   if (empty ( $tab ["coordOuPartenaire"] )) $err=$err." Coord ou Partenaire";
   if (empty ( $tab ["institut_rattachement"] )) $err=$err." Institut Rattachement ";
   if (empty ( $tab ["acronyme"] )) $err=$err." Acronyme ";
   if (empty ( $tab ["financeur"] )) $err=$err." Financeur";
   if (empty ( $tab ["partenaire_prive"] )) $err=$err." Partenaire privé";
if (!($err==""))   throw new Exception($err);
  

else {
//FLAGS
        $this->iscreation = $tab['iscreation'];



        $this->projet_id = $tab['projet_id'];
        $this->equipe_id = $tab['equipe_id'];
        $this->nom_porteur = $tab['nom_porteur'];
        $this->coordinateurProjet = $tab['coordinateurProjet'];
        $this->coordOuPartenaire = $tab['coordOuPartenaire'];
        $this->institut_rattachement = $tab['institut_rattachement'];
        $this->organisme_gestionnaire = $tab['organisme_gestionnaire'];
        $this->financeur = $tab['financeur'];
        $this->nom_financeur = $tab['nom_financeur'];
        $this->type = $tab['type'];
        $this->acronyme = $tab['acronyme'];
        $this->partenaire_prive = $tab['partenaire_prive'];
        $this->sujet_projet = $tab['sujet_projet'];
        $this->nbrecrutement = $tab['nbrecrutement'];
        $this->nbstagiaire = $tab['nbstagiaire'];
//        $this->phase = $tab['phase'];
        $this->preselection = $tab['preselection'];



  $this->dateSoumission =$tab['dateSoumission'];
  $this->dateDebut =$tab['dateDebut'];
  $this->dateFin =$tab['dateFin'];
  $this->duree =$tab['duree'];
  $this->montant =$tab['montant'];
  $this->partHsm =$tab['partHsm'];
  $this->partenaires =$tab['partenaires'];
  $this->commentaires =$tab['commentaires'];
        $this->nbrecrutement2 = $tab['nbrecrutement2'];
        $this->nbstagiaire2 = $tab['nbstagiaire2'];

 $this->listeassocie = array();
        for ($i = 0; $i < $tab['nblisteassocie']; $i++) {
                $this->listeassocie[$i]= $tab['p_id'.$i];
        }
        // la liste des nouveaux utilisateurs non enregistrés
        $this->associenouveau= $tab['np_id'];

 $this->listeequipe = array();
        for ($i = 0; $i < $tab['nblisteequipe']; $i++) {
                $this->listeequipe[$i]= $tab['equipe_id'.$i];
        }
        // la liste des nouveaux utilisateurs non enregistrés
        $this->equipenouveau= $tab['nequipe_id'];

      
  }
}
//FLAGS
public function getiscreation()  { return  $this->iscreation ;  }



public function getprojet_id()  { return  $this->projet_id ;  }
public function getequipe_id()  { return  $this->equipe_id ;  }
public function getnom_porteur()  { return  $this->nom_porteur ;  }
public function getcoordinateurProjet()  { return  $this->coordinateurProjet ;  }
public function getcoordOuPartenaire()  { return  $this->coordOuPartenaire ;  }
public function getinstitut_rattachement()  { return  $this->institut_rattachement ;  }
public function getorganisme_gestionnaire()  { return  $this->organisme_gestionnaire ;  }
public function getfinanceur()  { return  $this->financeur ;  }
public function getnom_financeur()  { return  $this->nom_financeur ;  }
public function gettype()  { return  $this->type ;  }
public function getacronyme()  { return  $this->acronyme ;  }
public function getpartenaire_prive()  { return  $this->partenaire_prive ;  }
public function getsujet_projet()  { return  $this->sujet_projet ;  }
public function getnbrecrutement()  { return  $this->nbrecrutement ;  }
public function getnbstagiaire()  { return  $this->nbstagiaire ;  }
//public function getphase()  { return  $this->phase ;  }
public function getpreselection()  { return  $this->preselection ;  }



public function getdateSoumission()  { return  $this->dateSoumission ;  }
public function getdateDebut()  { return  $this->dateDebut ;  }
public function getdateFin()  { return  $this->dateFin ;  }
public function getduree()  { return  $this->duree ;  }
public function getmontant()  { return  $this->montant ;  }
public function getpartHsm()  { return  $this->partHsm ;  }
public function getpartenaires()  { return  $this->partenaires ;  }
public function getcommentaires()  { return  $this->commentaires ;  }
public function getnbrecrutement2()  { return  $this->nbrecrutement2 ;  }
public function getnbstagiaire2()  { return  $this->nbstagiaire2 ;  }

public function getlisteassocie()  { return  $this->listeassocie ;  }
public function getassocienouveau()  { return  $this->associenouveau ;  }
public function getlisteequipe()  { return  $this->listeequipe ;  }
public function getequipenouveau()  { return  $this->equipenouveau ;  }
}
?>
