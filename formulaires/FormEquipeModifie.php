<?php





class FormEquipeModifie
{

  private $eq_id;
  private $eq_nom;
  private $eq_nom_long;
  private $eq_verticale;
  private $resp_id;

public function __construct($tab)  {
  $err="";

   if (empty ( $tab ["eq_nom"] )) $err=$err." acronyme";
   if (empty ( $tab ["eq_nom_long"] )) $err=$err." nom";
if (!($err==""))   throw new Exception($err);
  

else {
	
        $this->eq_id = $tab['eq_id'];
        $this->eq_nom = $tab['eq_nom'];
        $this->eq_nom_long = $tab['eq_nom_long'];
	if (isset($tab['eq_verticale'])) $this->eq_verticale=1;
	else $this->eq_verticale=0;
        $this->resp_id = $tab['resp_id'];

      
  }
}
public function seteq_nom($acronyme)  {    $this->eq_nom = $acronyme;  }
public function seteq_nom_long($nom)  {    $this->eq_nom_long = $nom;  }
public function geteq_id()  { return  $this->eq_id ;  }
public function geteq_nom_long()  { return  $this->eq_nom_long ;  }
public function geteq_nom()  {  return  $this->eq_nom ;  }
public function geteq_verticale()  {  return  $this->eq_verticale ;  }
public function getresp_id()  {  return  $this->resp_id ;  }

}
?>
