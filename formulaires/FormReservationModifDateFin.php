<?php





class FormReservationModifDateFin
{

  private $r_id;
  private $date_fin;

public function __construct($id,$datefinori,$datefin)  {
  $err="";

if ($datefinori<$datefin) $err=$err. "Pas de prolongation de réservation";
if (DateTime::createFromFormat('Y-m-d', $datefin) == FALSE) $err=$err. "Format date de fin invalide"; 

if (!($err==""))   throw new Exception($err);
  

else {
        $this->r_id = $id;
        $this->date_fin = $datefin;
      
  }
}
public function getDateFin()  {  return  $this->date_fin ;  }
public function getId()  {  return  $this->r_id ;  }

}
?>
