<?php














class FormCahier
{


  private $cahierNum;
  private $modele;
  private $fonction;
  private $eq_id;
  private $localisation;
  private $commentaires;

private $listeutilisateur;
private $utilisateurnouveau;



public function __construct($tab)  {
  $err="";

   if (empty ( $tab ["cahierNum"] )) $err=$err." numéro de cahier";
if (!($err==""))   throw new Exception($err);
  

else {
        $this->cahierNum = $tab['cahierNum'];
        $this->modele = $tab['modele'];
        $this->fonction = $tab['fonction'];
        $this->eq_id = $tab['eq_id'];
        $this->localisation = $tab['localisation'];
        $this->commentaires = $tab['commentaires'];


     require_once '../objets/CahierLaboUtilisateur.php';

        $this->listeutilisateur = array();
        for ($i = 0; $i < $tab['nblisteutilisateur']; $i++) {
                $this->listeutilisateur[$i]= new CahierLaboUtilisateur ($tab['p_id'.$i],$tab['cahierNum'],$tab['dateDotation'.$i],$tab['dateRetour'.$i]);
        }
        // la liste des nouveaux utilisateurs non enregistrés
                $this->utilisateurnouveau= new CahierLaboUtilisateur ($_POST['np_id'],$_POST['cahierNum'],$_POST['ndateDotation'],$_POST['ndateRetour']);

      
  }
}
public function getcahierNum()  { return  $this->cahierNum ;  }
public function getmodele()  { return  $this->modele ;  }
public function getfonction()  { return  $this->fonction ;  }
public function geteq_id()  { return  $this->eq_id ;  }
public function getlocalisation()  { return  $this->localisation ;  }
public function getcommentaires()  { return  $this->commentaires ;  }
public function getlisteutilisateur()  { return  $this->listeutilisateur ;  }
public function getutilisateurnouveau()  { return  $this->utilisateurnouveau ;  }


}
?>
