<?php
require_once '../entites/Model.php';

class actionPersonneTousFiltre {
      public function execute($selectSite,$selectStatut,$selectType,$selectEquipe){




/////////////////////////////////////////////////////////////////////////////////////
// Calcul where clause
////////////////////////////////////////////////////////////////////////////////////////////////
if (!(isset($selectTutelle))) $selectTutelle="T";
if (!(isset($selectType))) $selectType="T";
if (!(isset($selectSite))) $selectSite="T";
if (!(isset($selectStatut))) $selectStatut="T";
if (!(isset($selectEquipe))) $selectEquipe="T";

$whereclause="";
if($selectSite !='T') $whereclause = $whereclause ." AND historique.sit_id LIKE ".$selectSite;
if($selectStatut!='T') $whereclause = $whereclause ." AND historique.st_id LIKE ".$selectStatut;
if($selectType!='T') $whereclause = $whereclause ." AND  ty_id LIKE ".$selectType;
if($selectEquipe!='T') $whereclause = $whereclause ." AND  t_membres_equipe2.eq_id LIKE ".$selectEquipe;


/////////////////////////////////////////////////////////////////////////////////////
// order by clause
////////////////////////////////////////////////////////////////////////////////////////////////



// On prend ici que l'historique le plus récent 
//dateDebutH = (
//                                                SELECT MAX(dateDebutH)
//                                                FROM historique
//                                                WHERE p1.p_id LIKE historique.p_id
//                                        )

$reqsql = "
	SELECT p1.p_id, p1.p_nom,p1.p_prenom,p1.demandeIntranet 
	FROM t_personne p1
	LEFT JOIN historique ON historique.p_id = p1.p_id
	LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique
	WHERE 
			dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE p1.p_id LIKE historique.p_id
                                        )
	AND archive NOT LIKE '1'
	AND demandeintranet NOT LIKE '1'

	".$whereclause."
	GROUP BY p_id ORDER BY p_nom";
	

$liste = Model::req_sql($reqsql);



return $liste;





}
}
?>
