<?php
/***************************************************************************\
 *  GES UMR : gestion rh pour les umr                                      *
 *                                                                         *
 *  Copyright (c) 2018                                                     *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/
require_once '../entites/Model.php';

class actionAdminStagiaireFiltre {
      public function execute($year1,$year2){

echo $year1;

if (($year1!=0) && ($year2!=0)) $plus= " AND               year(historique.dateDebutH) >= $year1      AND year(historique.dateDebutH) <= $year2 ";
else $plus=" AND archive NOT LIKE '1' ";

// la date de fin est dans la période
if (($year1!=0) && ($year2!=0)) $plus= " AND (               (year(historique.dateFinH) >= $year1  AND year(historique.dateFinH) <= $year2) 
                                                OR (year(historique.dateDebutH) >= $year1  AND year(historique.dateDebutH) <= $year2)  
                                                OR (year(historique.dateDebutH) < $year1  AND year(historique.dateFinH) > $year2) ) ";
else $plus=" AND archive NOT LIKE '1' ";



/////////////////////////////////////////////////////////////////////////////////////
// Calcul where clause
////////////////////////////////////////////////////////////////////////////////////////////////

//$reqsql="SELECT DISTINCT t_personne.p_id, p_nom, p_prenom,demandeintranet FROM t_personne,historique WHERE t_personne.p_id LIKE historique.p_id AND historique.ty_id LIKE '2' AND historique.st_id LIKE '3' AND archive NOT LIKE '1' AND demandeintranet NOT LIKE '1' ORDER BY p_nom";

$reqsql="
  SELECT p1.p_id, p1.p_nom,p1.p_prenom,p1.demandeIntranet
        FROM t_personne p1
        LEFT JOIN historique ON historique.p_id = p1.p_id
        WHERE
                        dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE p1.p_id LIKE historique.p_id
                                        )
	AND demandeintranet NOT LIKE '1'
	AND historique.ty_id LIKE '2' AND historique.st_id LIKE '3' 
".$plus."
        GROUP BY p1.p_id ORDER BY p_nom";

$liste = Model::req_sql($reqsql);

return $liste;





}
}
?>
