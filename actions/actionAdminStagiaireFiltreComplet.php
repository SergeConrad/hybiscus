<?php
require_once '../entites/Model.php';

class actionAdminStagiaireFiltreComplet {
      public function execute($year1,$year2){
/////////////////////////////////////////////////////////////////////////////////////
// Calcul where clause
////////////////////////////////////////////////////////////////////////////////////////////////


$reqsql="SELECT  p1.p_nom, p1.p_prenom,p1.dateNaissance,
sit_nom,datedebutH,datefinH,GROUP_CONCAT(eq_nom SEPARATOR '/'),
s.stag_intitule,CONCAT (p2.p_nom,' ',p2.p_prenom) AS encadrant ,s.gratification,s.numMissionnaire,s.montantGratif,s.montantTransport,s.financementCredit,s.financementEntite,  s.conventionEnvoyee,s.conventionRevenue,s.numBCSifac,s.refFournisseur,s.dateDossierGratif,s.parDossierGratif,s.dossierGratif

                FROM t_personne AS p1


                LEFT JOIN historique ON p1.p_id = historique.p_id
		LEFT JOIN t_membres_equipe2 ON historique.numHistorique = t_membres_equipe2.numHistorique
		LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id
                RIGHT JOIN stage AS s ON historique.stageId = s.stageId
                LEFT JOIN t_personne AS p2 ON s.p_stag_encadrant = p2.p_id
		LEFT JOIN t_site  ON t_site.sit_id = historique.sit_id

                WHERE
		year(historique.dateDebutH) >= $year1
		AND year(historique.dateDebutH) <= $year2
                GROUP BY p1.p_id ORDER BY dateDebutH";



//$reqsql="
//  SELECT p1.p_id, p1.p_nom,p1.p_prenom,p1.demandeIntranet
//        FROM t_personne p1
//        LEFT JOIN historique ON historique.p_id = p1.p_id
//        WHERE
//                        dateDebutH = (
//                                                SELECT MAX(dateDebutH)
//                                                FROM historique
//                                                WHERE p1.p_id LIKE historique.p_id
//                                        )
//        AND archive NOT LIKE '1'
//        AND demandeintranet NOT LIKE '1'
//        AND historique.ty_id LIKE '2' AND historique.st_id LIKE '3'
//        GROUP BY p1.p_id ORDER BY p_nom";




$liste = Model::req_sql($reqsql);

return $liste;






} // fin  while


}
?>
