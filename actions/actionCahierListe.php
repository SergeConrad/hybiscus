<?php
require_once '../entites/ModelCahier.php';
class actionCahierListe {
      public function execute($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id){

if (!(isset($selectdateDotation)))  $selectdateDotation='T';
if (!(isset($selectdateRetour)))  $selectdateRetour='T';
if (!(isset($selecteq_id)))  $selecteq_id='T';
if (!(isset($selectp_id)))  $selectp_id='T';

  $liste = ModelCahier::recuperation_cahier($selectdateDotation,$selectdateRetour,$selecteq_id,$selectp_id);

return $liste;

}

public function parUtilisateur($id) {
 $liste = ModelCahier::listeParUtilisateur($id);

return $liste;
}

public function mesStagiairesEtDoctorants($id) {
// et contractuels
 $liste = ModelCahier::mesStagiairesEtDoctorants($id);
return $liste;
}


public function cahier_equipe_membre($id) {
 $liste = ModelCahier::cahier_equipe_membre($id); 
return $liste;
}

public function cahier_equipe_stagiaire($id) {
 $liste = ModelCahier::cahier_equipe_stagiaire($id); 
return $liste;
}

public function cahier_equipe_doctorant($id) {
 $liste = ModelCahier::cahier_equipe_doctorant($id); 
return $liste;
}

public function pourLequipe($eq_id) {
 $liste = ModelCahier::pourLequipe($eq_id); 
return $liste;
}

public function entete() {
 $liste = ModelCahier::entete(); 
return $liste;
}


 public function cahierlaboProjetListe($cahierNum) {
 $liste = ModelCahier::cahierlaboProjetListe($cahierNum); 
return $liste;
}




}
?>

