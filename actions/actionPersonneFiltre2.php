<?php
require_once '../entites/Model.php';

class actionPersonneFiltre2 {
      public function execute($selectSite,$selectStatut,$selectType,$selectEquipe){




/////////////////////////////////////////////////////////////////////////////////////
// Calcul where clause
////////////////////////////////////////////////////////////////////////////////////////////////
if (!(isset($selectTutelle))) $selectTutelle="T";
if (!(isset($selectType))) $selectType="T";
if (!(isset($selectSite))) $selectSite="T";
if (!(isset($selectStatut))) $selectStatut="T";
if (!(isset($selectEquipe))) $selectEquipe="T";
if (!(isset($selectDate))) $selectDate=date('Y-m-d');
if (!(isset($selectArchive))) $selectArchive=0;

$whereclause="";
if($selectSite !='T') $whereclause = $whereclause ." AND historique.sit_id LIKE ".$selectSite;
if($selectStatut!='T') $whereclause = $whereclause ." AND historique.st_id LIKE ".$selectStatut;
if($selectType!='T') $whereclause = $whereclause ." AND  historique.ty_id LIKE ".$selectType;
if($selectEquipe!='T') $whereclause = $whereclause ." AND  t_membres_equipe2.eq_id LIKE ".$selectEquipe;


/////////////////////////////////////////////////////////////////////////////////////
// order by clause
////////////////////////////////////////////////////////////////////////////////////////////////


$reqsql = "
	SELECT historique.sit_id,historique.st_id,historique.ty_id,p1.p_id, p1.p_nom,p1.p_prenom,p1.demandeIntranet,p1.p_mail, p1.archive,dateDebutH, dateFinH, CONCAT (p2.p_nom,' ',p2.p_prenom) AS encadrant,
		CONCAT (p3.p_nom,' ',p3.p_prenom) AS directeur1,CONCAT (p4.p_nom,' ',p4.p_prenom) AS directeur2, intitule,
		CONCAT (p5.p_nom,' ',p5.p_prenom) AS superieur,
		ty_nom,contractuel_sujet_recherche
	FROM t_personne p1
	LEFT JOIN historique ON historique.p_id = p1.p_id
	LEFT JOIN t_membres_equipe2 ON t_membres_equipe2.numHistorique = historique.numHistorique


	LEFT JOIN stage ON historique.stageId = stage.stageId
	LEFT JOIN t_personne p2 ON stage.p_stag_encadrant = p2.p_id

	LEFT JOIN theses ON historique.theseId = theses.theseId
	LEFT JOIN t_personne p3 ON theses.p_these_directeur1 = p3.p_id
	LEFT JOIN t_personne p4 ON theses.p_these_directeur2 = p4.p_id

 	LEFT JOIN contractuel ON historique.contractuel_id = contractuel.contractuel_id
	LEFT JOIN t_personne p5 ON contractuel.contractuel_sup_id = p5.p_id
	LEFT JOIN t_type_personnel2 ON historique.ty_id = t_type_personnel2.ty_id AND historique.st_id = t_type_personnel2.st_id





	WHERE 
			dateDebutH = (
                                                SELECT MAX(dateDebutH)
                                                FROM historique
                                                WHERE p1.p_id LIKE historique.p_id
                                        )
	AND p1.demandeintranet NOT LIKE '1'

	".$whereclause."
	GROUP BY p_id ORDER BY p_nom";
	

error_log($reqsql);
$liste = Model::req_sql($reqsql);



return $liste;





}
}
?>
