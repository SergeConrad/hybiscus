

<?php
//require_once '../formulaires/FormPersonneAjoute.php';
require_once '../entites/Model.php';
class actionStageListe {
      public function execute($p_id){


	 $res = Model::daoStageListe($p_id);
       
$listeStage = array();
foreach ($res as $row) {

	$res2 = Model::daoStageFinancement($row['stageId']);
	$listeFinancement = array();
	foreach ($res2 as $row2) {
 	$listeFinancement[$row2['financementId']] = array ("financementCredit" => $row2['financementCredit'],
						"dateDebut" => $row2['dateDebut'],
						"dateFin" => $row2['dateFin']
					);
	}
$listeStage[$row['numHistorique']]=array ( "stageId" => $row['stageId'],
                                       "p_stag_encadrant" => $row['p_stag_encadrant'],
                                       "stag_intitule" => $row['stag_intitule'],
                                        "etablissement" => $row['etablissement'],
                                        "f_id" => $row['f_id'],
                                        "autreformation" => $row['autreformation'],
                                        "t_conv_id" => $row['t_conv_id'],
                                        "gratification" => $row['gratification'],
                                        "financementEntite" => $row['financementEntite'],
                                        "stageModifiePar" => $row['stageModifiePar'],
                                        "stageModifieDate" => $row['stageModifieDate'],
                                        "conventionEnvoyee" => $row['conventionEnvoyee'],
                                        "conventionRevenue" => $row['conventionRevenue'],
                                        "scanConvention" => $row['scanConvention'],
                                        "dossierGratif" => $row['dossierGratif'],
                                        "montantGratif" => $row['montantGratif'],
                                        "montantTransport" => $row['montantTransport'],
                                        "numMissionnaire" => $row['numMissionnaire'],
                                        "refFournisseur" => $row['refFournisseur'],
                                        "numBCSifac" => $row['numBCSifac'],
                                        "dateDossierGratif" => $row['dateDossierGratif'],
                                        "parDossierGratif" => $row['parDossierGratif'],
                                        "listeFinancement" => $listeFinancement,
                                        "dateDebut" => $row['dateDebut'],
                                        "dateFin" => $row['dateFin']
                                     );
}


        return $listeStage;




      }

}
?>
