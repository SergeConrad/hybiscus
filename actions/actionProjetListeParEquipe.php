<?php
require_once '../entites/ModelProjet.php';

class actionProjetListeParEquipe {
public function execute($eq_id) {
 $liste = ModelProjet::recuperation_liste_parequipe($eq_id); 
return $liste;
}


      public function entete(){
        $liste2= ModelProjet::recuperation_liste_parequipe_entete();
        return $liste2;
}
      public function export($id){
        $liste2= ModelProjet::recuperation_liste_parequipe_export($id);
        return $liste2;
}


      public function soumis($id){
	$liste2= ModelProjet::recuperation_equipe_1param($id,"soumis");
	return $liste2;
}
      public function accepte($id){
	$liste2= ModelProjet::recuperation_equipe_1param($id,"accepté");
	return $liste2;
}
      public function refuse($id){
	$liste2= ModelProjet::recuperation_equipe_1param($id,"refusé");
	return $liste2;
}
      public function termine($id){
	$liste2= ModelProjet::recuperation_equipe_1param($id,"terminé");
	return $liste2;
}


      public function soumisporteur($id){
	$liste2= ModelProjet::recuperation_equipe_param($id,"Coordinateur","soumis");
	return $liste2;
}

      public function soumispartenaire($id){
	$liste2= ModelProjet::recuperation_equipe_param($id,"Partenaire","soumis");
	return $liste2;
}
      public function accepteporteur($id){
	$liste2= ModelProjet::recuperation_equipe_param($id,"Coordinateur","accepté");
	return $liste2;
}
      public function acceptepartenaire($id){
	$liste2= ModelProjet::recuperation_equipe_param($id,"Partenaire","accepté");
	return $liste2;
}
      public function refuseporteur($id){
	$liste2= ModelProjet::recuperation_equipe_param($id,"Coordinateur","refusé");
	return $liste2;
}
      public function refusepartenaire($id){
	$liste2= ModelProjet::recuperation_equipe_param($id,"Partenaire","refusé");
	return $liste2;
}

      public function fin12mois($id,$today,$dans12mois){
$liste2= ModelProjet::recuperation_fin12mois($id,$today,$dans12mois);
return $liste2;
}
      public function projetParAnnee($id,$annee){
$liste2= ModelProjet::recuperation_projet_annee($id,$annee);
return $liste2;
}





}
?>


