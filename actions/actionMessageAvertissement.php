<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require_once '../entites/Model.php';
class actionMessageAvertissement {
      public function execute($lrid,$from,$personne,$listeRessource,$listeEquipe){

//Load Composer's autoloader
require '../vendor2/autoload.php';
//require '../vendor2/phpmailer/phpmailer/PHPMailerAutoload.php';




if (($personne->getHistoriquen()->st_idn==3) && ($personne->getHistoriquen()->ty_idn==2)) $statut = "[stagiaire]";
elseif (($personne->getHistoriquen()->st_idn==3) && ($personne->getHistoriquen()->ty_idn==1)) $statut = "[doctorant]";
elseif ($personne->getHistoriquen()->st_idn==1)  $statut = "[permanent]";
elseif (($personne->getHistoriquen()->st_idn==2) && ($personne->getHistoriquen()->ty_idn==1)) $statut = "[cdd chercheur]";
elseif (($personne->getHistoriquen()->st_idn==2) && ($personne->getHistoriquen()->ty_idn==2)) $statut = "[cdd ita]";
elseif (($personne->getHistoriquen()->st_idn==2) && ($personne->getHistoriquen()->ty_idn==3)) $statut = "[post doc]";
elseif (($personne->getHistoriquen()->st_idn==2) && ($personne->getHistoriquen()->ty_idn==4)) $statut = "[ater]";
elseif ($personne->getHistoriquen()->st_idn==4)  $statut = "[invite]";
else $statut= "";




// Construire également le contenu pour prévenir les autres gestionnaires de ressources si besoin (si lrid est renseigné)

     $ressourceNom =array();
     $ressourceMail =array();
                foreach ($listeRessource as $r)  {
     			$ressourceNom[$r->rid] = $r->nom;
			$ressourceMail[$r->rid]  = $r->mail;
		}


// Transformation des objets sql en tableau
$dataEquipe= array();
   foreach ( $listeEquipe as $row )  $dataEquipe[$row->eq_id] = $row->eq_nom;






$equipe="[";
foreach ($personne->getHistoriquen()->eq_idn as $eq) $equipe= $equipe . " ".$dataEquipe[$eq]. " ";
$equipe = $equipe ."]";




$p = Model::daoPersonneTableau($personne->getId());




$contenu = $from." vient d'ajouter : <b>".$p['p_nom']." " .$p['p_prenom']. "</b> ".$p['p_mail']." par l'application nouveaux arrivants<br>";


$contenu = $contenu . "Cette personne sera présente du ". $personne->getHistoriquen()->dateDebutHn . " au ". $personne->getHistoriquen()->dateFinHn ."<br>";
$contenu = $contenu . "STATUT $statut" ."<br>";
$contenu = $contenu . "EQUIPE $equipe" ."<br>";

  $to = Model::messageDestinataire(1);


//foreach ($lrid as $rid) {
//	$contenu = $contenu .'accès demandé pour la ressource :'.$ressourceNom[$rid]."\n";
//	$to= $to .';'.	$ressourceMail[$rid];
//	}




$subject = "[HYBISCUS] $equipe $statut ajout personne";


//$headers = 'From: no-reply@umontpellier.fr' . "\r\n" .
//    'Reply-To: no-reply@umontpellier.fr' . "\r\n" .
//    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $contenu, $headers);


//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'smtp-int.umontpellier.fr';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = false;                                   //Enable SMTP authentication
    $mail->Port       = 25;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //Recipients
    $mail->setFrom('no-reply@umontpellier.fr', 'Hybiscus');
//   $mail->addAddress('serge.conrad@umontpellier.fr', 'Serge Conrad');     //Add a recipient
$tags = explode(';',$to);
foreach($tags as $key) {    
   $mail->addAddress($key);     //Add a recipient
}
foreach ($lrid as $rid) {
	$tags = explode(';',$ressourceMail[$rid]);               //Name is optional
	foreach($tags as $key) {    
	   $mail->addAddress($key);     //Add a recipient
	}
	$contenu = $contenu .'accès demandé pour la ressource :'.$ressourceNom[$rid]."<br>";
	}
//    $mail->addAddress('ellen@example.com');               //Name is optional
//    $mail->addReplyTo('no-reply@umontpellier.fr', 'Information');
//    $mail->addCC('cc@example.com');
//    $mail->addBCC('bcc@example.com');

    //Attachments
//    $mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
//    $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = $subject;
    $mail->Body    = $contenu;
    $mail->AltBody = $contenu;
    $mail->CharSet = "UTF-8";

    $mail->send();
    echo 'Message has been sent';
    error_log( 'DEBUGA Message has been sent');
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    error_log( 'DEBUGA Message has not been sent'.$mail->ErrorInfo);
}








}
}
?>

