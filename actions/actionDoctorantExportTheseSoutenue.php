<?php
require_once '../entites/Model.php';

class actionDoctorantExportTheseSoutenue {
      public function execute($year){
/////////////////////////////////////////////////////////////////////////////////////
// Calcul where clause
////////////////////////////////////////////////////////////////////////////////////////////////




$reqsql="SELECT  p1.p_nom, p1.p_prenom,p1.dateNaissance,t.intitule,CONCAT (p2.p_nom,' ',p2.p_prenom) AS directeur1, CONCAT(p3.p_nom,' ',p3.p_prenom) AS directeur2,GROUP_CONCAT(eq_nom), t.tfinancement, t.m2obtenu,t.ecoleDoctorale,t.laboaccueil,t.theseDebut,t.etsInscription,t.etsCoTutelle,t.theseSoutenance,t.apresThese
                FROM t_personne AS p1


                LEFT JOIN historique ON p1.p_id = historique.p_id
		LEFT JOIN t_membres_equipe2 ON historique.numHistorique = t_membres_equipe2.numHistorique
                LEFT JOIN t_equipe ON t_membres_equipe2.eq_id = t_equipe.eq_id
                RIGHT JOIN theses AS t ON historique.theseId = t.theseId
                LEFT JOIN t_personne AS p2 ON t.p_these_directeur1 = p2.p_id
                LEFT JOIN t_personne AS p3 ON t.p_these_directeur2 = p3.p_id
                WHERE
		year(t.theseSoutenance) = $year
                GROUP BY p_nom";

$liste = Model::req_sql($reqsql);

return $liste;






} // fin  while




}
?>
