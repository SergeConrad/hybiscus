<?php
require_once '../entites/ModelProjet.php';

class actionProjetListeAdmin {
      public function execute($eq_id,$preselection,$datesoumission,$datedebut,$porteur,$associe){

if (!(isset($eq_id)))  $eq_id='T'; 
if (!(isset($preselection)))  $preselection='T'; 
if (!(isset($datesoumission)) OR $datesoumission== "")  $datesoumission='T'; 
if (!(isset($datedebut)) OR $datedebut =="")  $datedebut='T'; 
if (!(isset($porteur)))  $porteur = 'T'; 
if (!(isset($associe)))  $associe='T'; 
$liste2 =ModelProjet::recuperation_liste_parequipe_preselection($eq_id,$preselection,$datesoumission,$datedebut,$porteur,$associe);
return $liste2;
}
      public function entete(){
	$liste2= ModelProjet::recuperation_liste_parchercheur_entete();
	return $liste2;
}
      //public function export($eq_id,$preselection){
      public function export($selecteq_id,$selectpreselection,$selectDate,$selectDateDebut,$selectPersonnel,$selectAssocie){

if (!(isset($selecteq_id)))  $selecteq_id='T'; 
if (!(isset($selectpreselection)))  $selectpreselection='T'; 

$liste2 =ModelProjet::recuperation_liste_parequipe_preselection_export($selecteq_id,$selectpreselection);
//	if (isset($eq_id)&&($eq_id!='T')) $liste2 =ModelProjet::recuperation_liste_parequipe_export($eq_id);
//	else $liste2= ModelProjet::recuperation_liste_paradmin_export();
	return $liste2;
}





    public function termine($eq_id,$datesoumission,$datedebut,$selectPersonnel,$selectAssocie){
        //$liste2= ModelProjet::recuperation_equipe_1param('%',"terminé");
if (!(isset($eq_id)))  $eq_id='T'; 
if (!(isset($datesoumission)))  $datesoumission='T'; 
if (!(isset($datedebut)))  $datedebut='T'; 
#if ($eq_id=='T')  $eq_id='%'; 
#if ($datesoumission=='T')  $datesoumission='%'; 
#if ($datedebut=='T')  $datedebut='%'; 
        #$liste2= ModelProjet::recuperation_equipe_1param($eq_id,"terminé",$datesoumission,$datedebut);
	$liste2 =ModelProjet::recuperation_liste_parequipe_preselection($eq_id,"terminé",$datesoumission,$datedebut,$selectPersonnel,$selectAssocie);
        return $liste2;
}
    public function refuse($eq_id,$datesoumission,$datedebut,$selectPersonnel,$selectAssocie){
if (!(isset($eq_id)))  $eq_id='T'; 
if (!(isset($datesoumission)))  $datesoumission='T'; 
if (!(isset($datedebut)))  $datedebut='T'; 
#if ($eq_id=='T')  $eq_id='%'; 
#if ($datesoumission=='T')  $datesoumission='%'; 
#if ($datedebut=='T')  $datedebut='%'; 
        #$liste2= ModelProjet::recuperation_equipe_1param($eq_id,"refusé",$datesoumission,$datedebut);
	$liste2 =ModelProjet::recuperation_liste_parequipe_preselection($eq_id,"refusé",$datesoumission,$datedebut,$selectPersonnel,$selectAssocie);
        return $liste2;
}
    public function soumis($eq_id,$datesoumission,$datedebut,$selectPersonnel,$selectAssocie){
if (!(isset($eq_id)))  $eq_id='T'; 
if (!(isset($datesoumission)))  $datesoumission='T'; 
if (!(isset($datedebut)))  $datedebut='T'; 

	$liste2 =ModelProjet::recuperation_liste_parequipe_preselection($eq_id,"soumis",$datesoumission,$datedebut,$selectPersonnel,$selectAssocie);
        return $liste2;
}
    public function accepte($eq_id,$datesoumission,$datedebut,$selectPersonnel,$selectAssocie){
if (!(isset($eq_id)))  $eq_id='T'; 
if (!(isset($datesoumission)))  $datesoumission='T'; 
if (!(isset($datedebut)))  $datedebut='T'; 
#if ($eq_id=='T')  $eq_id='%'; 
#if ($datesoumission=='T')  $datesoumission='%'; 
#if ($datedebut=='T')  $datedebut='%'; 
        #$liste2= ModelProjet::recuperation_equipe_1param($eq_id,"accepte",$datesoumission,$datedebut);
	$liste2 =ModelProjet::recuperation_liste_parequipe_preselection($eq_id,"accepte",$datesoumission,$datedebut,$selectPersonnel,$selectAssocie);
        return $liste2;
}


      public function soumisporteur($eq_id,$datesoumission,$datedebut){
        $liste2= ModelProjet::recuperation_equipe_param($eq_id,"Coordinateur","soumis",$datesoumission,$datedebut);
        return $liste2;
}

      public function soumispartenaire($eq_id,$datesoumission,$datedebut){
        $liste2= ModelProjet::recuperation_equipe_param($eq_id,"Partenaire","soumis",$datesoumission,$datedebut);
        return $liste2;
}
      public function accepteporteur($eq_id,$datesoumission,$datedebut){
        $liste2= ModelProjet::recuperation_equipe_param($eq_id,"Coordinateur","accepté",$datesoumission,$datedebut);
        return $liste2;
}
      public function acceptepartenaire($eq_id,$datesoumission,$datedebut){
        $liste2= ModelProjet::recuperation_equipe_param($eq_id,"Partenaire","accepté",$datesoumission,$datedebut);
        return $liste2;
}
      public function refuseporteur($eq_id,$datesoumission,$datedebut){
        $liste2= ModelProjet::recuperation_equipe_param($eq_id,"Coordinateur","refusé",$datesoumission,$datedebut);
        return $liste2;
}
      public function refusepartenaire($eq_id,$datesoumission,$datedebut){
        $liste2= ModelProjet::recuperation_equipe_param($eq_id,"Partenaire","refusé",$datesoumission,$datedebut);
        return $liste2;
}

      public function fin12mois($id,$today,$dans12mois){
$liste2= ModelProjet::recuperation_fin12mois($id,$today,$dans12mois);
return $liste2;
}
      //public function projetParAnnee($id,$annee){
      public function projetParAnnee($id,$anneedebut,$anneefin){
$liste2= ModelProjet::recuperation_projet_annee($id,$anneedebut,$anneefin);
return $liste2;
}
      public function projetMontantParAnnee($id,$anneedebut,$anneefin){
$liste2= ModelProjet::recuperation_projet_montant_annee($id,$anneedebut,$anneefin);
return $liste2;
}
      public function projetMontantParTutelleParAnnee($id,$anneedebut,$anneefin){
$liste2= ModelProjet::recuperation_projet_parttutelle_annee($id,$anneedebut,$anneefin);
//foreach ($liste2 as $row) echo "$row->year $row->t_nom $row->hsm $row->debug";
return $liste2;
}

      public function projetParAnneeFinanceur($id,$annee){
$liste2= ModelProjet::recuperation_projet_annee_financeur($id,$annee);
return $liste2;
}
      public function projetParAnneeType($id,$annee){
$liste2= ModelProjet::recuperation_projet_annee_type($id,$annee);
//var_dump("\n liste2");
//var_dump("\n liste2");
//var_dump("\n liste2");
//var_dump("\n liste2");
//var_dump("\n liste2");
//var_dump(" id $id annee $annee");
//var_dump("\n liste2");
//foreach($liste2 as $l var_dump($l)
//foreach ($liste2 as $row) {
//var_dump($row);
//}


return $liste2;
}


}
?>






