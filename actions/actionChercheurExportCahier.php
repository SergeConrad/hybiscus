<?php
class actionChercheurExportCahier {
      public function execute($liste,$listeSD){
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=cahiers.csv');
// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');
fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));
// output the column headings
fputcsv($output, array('cahierNum','modele','p_id','dateDotation','fonction','eq_id','dateRetour','commentaires'),";");
foreach ( $liste as $row )  {
 $fields = (array) $row;
fputcsv($output,$fields,";");
} // fin  while
fputcsv($output,'\n' );
fputcsv($output, array('cahierNum','modele','p_id','type','encadrant','directeur','dateDotation','fonction','eq_id','dateRetour','commentaires'),";");
foreach ( $listeSD as $row )  {
 $fields = (array) $row;
fputcsv($output,$fields,";");
} // fin  while
}
}
?>
