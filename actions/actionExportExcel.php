<?php
class actionExportExcel {
      public function execute($liste,$entete,$nomfich){
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename='.$nomfich);
// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');
fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));
fputcsv($output, $entete,";");
foreach ( $liste as $row )  {
 $fields = (array) $row;
fputcsv($output, array_map('utf8_decode',array_values($fields)),";"  );
} // fin  while
}
}
?>
