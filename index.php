<?php

/*********************************************************************************************************************************\
 *  Hybiscus : HydroSciences Base d'Information et de Suivi pour les Chercheurs et UtilisateurS                                    *
 *                                                                         							  *
 *  Copyright (c) 2018 2019   							                                              *
 *  Elise  Deme                                                            *
 *  Serge Conrad                                                           *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\************************************************************************************************************************************/

 header( "Location: control/ControleurChercheur.php" );

?>

