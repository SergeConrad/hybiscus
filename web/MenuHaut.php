<?php
require_once("../web/session_start.php");
require_once '../web/head.php';
?>
  <nav id="NavigationBarGeneral">
        
<?php        



echo '<ul>';
echo '  <li><a id="first_link_of_bar_general" href="../control/ControleurChercheur.php?numAction=2">Hybiscus  '. $_SESSION['user'].'</a></li>';




if ($_SESSION['arrivant']==1) {
echo '    <li class="dropdown">';
    echo '<a href="../control/Controleur.php?numAction=4" class="dropbtn">Nouvel Arrivant</a>';
echo '     <div class="dropdown-content">';
echo '      <a href="ControleurPersonne.php?numAction=90">Vous êtes un nouveau stagiaire </a>';
echo '      <a href="ControleurPersonne.php?numAction=95">Vous êtes un nouveau doctorant </a>';
echo '      <a href="ControleurPersonne.php?numAction=100">Vous êtes un nouveau contractuel </a>';
echo '      <a href="ControleurPersonne.php?numAction=105">Vous êtes un nouvel invité </a>';
echo '    </div>';
echo '    </li>    ';
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CHERCHEUR ONLY
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// USE CASE : CHERCEHR DECLARE STAGIAIRE ET DOCTORANTS
// USE CASE : CHERCHEUR CONSULTE STAGIAIRES ET DOCTORANTS
#if (($_SESSION['admin']==0)&&( $_SESSION['personnes']==0)&&($_SESSION['arrivant']==0)) {
echo '    <li class="dropdown">';
    echo '<a href="../control/ControleurChercheur.php?numAction=5" class="dropbtn">Mes entrées</a>';
echo '     <div class="dropdown-content">';
echo '      <a href="ControleurPersonne.php?numAction=303&menuDestination=chercheurmafiche&id='.$_SESSION['p_id'].'">Ma fiche </a>';

echo '    <a href="../control/ControleurChercheur.php?numAction=5" > Mon suivi (personnels & projets) </a>';
echo '      <a href="ControleurPersonne.php?numAction=90">Déclarer un nouveau stagiaire </a>';
echo '      <a href="ControleurPersonne.php?numAction=95">Déclarer un nouveau doctorant </a>';
echo '      <a href="ControleurPersonne.php?numAction=100">Déclarer un nouveau contractuel </a>';
echo '      <a href="ControleurPersonne.php?numAction=105">Déclarer un nouvel invité </a>';
echo '      <a href="ControleurProjet.php?numAction=110&menuDestination=chercheur">Déclarer un nouveau projet </a>';
echo '    </div>';
echo '    </li>    ';
#}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  RESPONSABLE EQUIPE ONLY
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// USE CASE RESPONSABLE GERE MEMBRES EQUIPES
// USE CASE RESPONSABLE GERE CONSULTE STAGIAIRES ET DOCTORANTS 
//if ( isset($_SESSION['responsableEquipe'])) {
//        echo '    <li class="dropdown">';
//        echo '    <a href="../control/ControleurResponsable.php?numAction=300&eq_id='.$_SESSION['responsableEquipe'].'&menuDestination=responsableequipe&nomEquipe='.$_SESSION['responsableEquipeNom'].'" class="dropbtn"> Resp Equipe'.$_SESSION['responsableEquipeNom'].' </a>';
//        echo '     <div class="dropdown-content">';
//        echo '      <a href="ControleurEquipe.php?numAction=112&menuDestination=responsableequipe&id='.$_SESSION['responsableEquipe'].'">Gérer les membres</a>';
//        echo '    <a href="../control/ControleurPersonne.php?numAction=300&selectEquipe='.$_SESSION['responsableEquipe'].'&menuDestination=responsableequipe&nomEquipe='.$_SESSION['responsableEquipeNom'].'" > Stagiaires / Doctorants </a>';
//        echo '    <a href="../control/ControleurResponsable.php?numAction=11&eq_id='.$_SESSION['responsableEquipe'].'&menuDestination=responsableequipe&nomEquipe='.$_SESSION['responsableEquipeNom'].'" > Suivi RH </a>';
//        echo '    <a href="../control/ControleurProjet.php?numAction=100&selecteq_id='.$_SESSION['responsableEquipe'].'&menuDestination=responsableequipe&nomEquipe='.$_SESSION['responsableEquipeNom'].'" > Suivi Projets </a>';
//        echo '    <a href="../control/ControleurResponsable.php?numAction=10&eq_id='.$_SESSION['responsableEquipe'].'&menuDestination=responsableequipe&nomEquipe='.$_SESSION['responsableEquipeNom'].'" > Stagiaires / Doctorants ANCIENNE VERSION</a>';
//        echo '    </div>';
//        echo '    </li>    ';
//}


if ( isset($_SESSION['responsableEquipe'])) {

	$arrayResp= $_SESSION['responsableEquipe'];
	foreach ($arrayResp as $key => $value) {
	        echo '    <li class="dropdown">';
	        echo '    <a href="../control/ControleurPersonne.php?numAction=300&selectEquipe='.$key.'&menuDestination=responsableequipe&nomEquipe='.$value.'" class="dropbtn"> Resp Equipe'.$value.' </a>';
        	echo '     <div class="dropdown-content">';
        	echo '      <a href="ControleurEquipe.php?numAction=112&menuDestination=responsableequipe&id='.$key.'">Gérer les membres</a>';
        	echo '    <a href="../control/ControleurPersonne.php?numAction=300&selectEquipe='.$key.'&menuDestination=responsableequipe&nomEquipe='.$value.'" > Suivi personnels équipe </a>';
        	echo '    <a href="../control/ControleurProjet.php?numAction=100&selecteq_id='.$key.'&menuDestination=responsableequipe&nomEquipe='.$value.'" > Suivi Projets </a>';
	        echo '    </div>';
	        echo '    </li>    ';


	}
}







///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// RECAPITULATIFS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// USE CASE ADMIN GERE PERSONNELS
if ( $_SESSION['personnes']==1) {
   echo '   <li class="dropdown">';
        echo '    <a href="../control/ControleurAdmin.php?numAction=230" class="dropbtn">Recapitulatifs </a>';
        echo '     <div class="dropdown-content">';
        echo '      <a href="ControleurAdmin.php?numAction=230&menuDestination=stagiaire">Gestion stagiaires par gratifications</a>';
        echo '      <a href="ControleurAdmin.php?numAction=235&menuDestination=laboratoire">Parcours Nouveaux Arrivants</a>';
}
elseif ($_SESSION['arrivant']==0) {
   echo '   <li class="dropdown">';
        echo '    <a href="../control/ControleurAdmin.php?numAction=230" class="dropbtn">Recapitulatifs </a>';
        echo '     <div class="dropdown-content">';
        echo '      <a href="ControleurAdmin.php?numAction=230&menuDestination=chercheurstagiaire">Gestion stagiaires par gratifications</a>';
//        echo '      <a href="ControleurAdmin.php?numAction=230&menuDestination=chercheurstag">Gestion stagiaires par gratifications</a>';
//        echo '      <a href="ControleurAdmin.php?numAction=235&menuDestination=chercheurlabo">Parcours Nouveaux Arrivants</a>';
        echo '      <a href="ControleurAdmin.php?numAction=235&menuDestination=chercheurstagiaire">Parcours Nouveaux Arrivants</a>';
}






///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PERSONNES
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if ( $_SESSION['personnes']==1) {
   echo '   <li class="dropdown">';
  //      echo '    <a href="../control/Controleur.php" class="dropbtn">Personnes </a>';
        echo '      <a href="ControleurPersonne.php?numAction=300&menuDestination=laboratoire" class="dropbtn">Personnes</a>';
        echo '     <div class="dropdown-content">';
        echo '      <a href="../control/ControleurPersonne.php?numAction=200">Ajouter une personne</a>';

                require_once '../actions/actionAdminValidationFiltre.php';
                $action =  new actionAdminValidationFiltre;
                $liste =  $action->execute();
$count = $liste->rowCount();

//require_once '../entites/Model.php';
//$rq = Model::$pdo->prepare ( 'select *  from t_personne where demandeintranet like "1"');
//$rq->execute();
//$count = $rq->rowCount();

        echo '      <a href="ControleurPersonne.php?numAction=340">En attente de validation ('.$count.')</a>';
        echo '      <a href="ControleurPersonne.php?numAction=300&menuDestination=laboratoire"><b>Gestion globale personnels</b></a>';
        echo '      <a href="ControleurPersonne.php?numAction=220&menuDestination=stagiaire">Gestion stagiaires</a>';
        echo '      <a href="ControleurPersonne.php?numAction=260&menuDestination=doctorant">Gestion doctorants</a>';
        echo '      <a href="ControleurPersonne.php?numAction=280&menuDestination=cdd">Gestion contractuels, postdoc, ater</a>';
        echo '      <a href="ControleurPersonne.php?numAction=240&menuDestination=permanent">Gestion permanents</a>';
        echo '      <a href="ControleurPersonne.php?numAction=330&menuDestination=invite">Gestion invités</a>';
        echo '      <a href="ControleurPersonne.php?numAction=320&menuDestination=archive"><b>Gestion archives</b></a>';
        echo '      <a href="ControleurPersonne.php?numAction=350&menuDestination=anonyme"><b>données anonymisées</b></a>';

        echo '    </div>';

        echo '    </li>    ';

//serge
   echo '   <li class="dropdown">';
        echo '    <a href="../control/ControleurProjet.php?numAction=100&menuDestination=admin" class="dropbtn">Projets </a>';
        echo '     <div class="dropdown-content">';
        echo '      <a href="../control/ControleurProjet.php?numAction=110&menuDestination=admin">Ajouter un projet</a>';
        echo '      <a href="../control/ControleurProjet.php?numAction=100&menuDestination=admin" >Liste Projets </a>';

     require_once '../actions/actionEquipeRechercheListe.php';
        $action =  new actionEquipeRechercheListe;
        $listeEquipe2 = $action->execute();
        
        
        foreach ( $listeEquipe2 as $row )  {
//echo ' <a href ="../control/ControleurResponsable.php?numAction=12&eq_id='.$row->eq_id.'&menuDestination=responsableequipe&nomEquipe='.$row->eq_nom.'"> Liste Projets'.$row->eq_nom.' </a>';
        echo '    <a href="../control/ControleurResponsable.php?numAction=11&eq_id='.$row->eq_id.'&menuDestination=responsableequipe&nomEquipe='.$row->eq_nom.'" > Suivi RH '.$row->eq_nom.'</a>';

}


        echo '    </div>';

        echo '    </li>    ';


}
//elseif ($_SESSION['arrivant']==0)  
// {
//   echo '   <li class="dropdown">';
//        echo '    <a href="../control/Controleur.php" class="dropbtn">Personnes </a>';
//        echo '     <div class="dropdown-content">';
//        echo '      <a href="ControleurAdmin.php?numAction=300&menuDestination=chercheurlabo"><b>Gestion globale personnels</b></a>';
//        echo '      <a href="ControleurAdmin.php?numAction=220&menuDestination=chercheurstag">Gestion stagiaires</a>';
//        echo '      <a href="ControleurAdmin.php?numAction=260&menuDestination=chercheurdoct">Gestion doctorants</a>';
//        echo '      <a href="ControleurAdmin.php?numAction=280&menuDestination=chercheurcont">Gestion contractuels, postdoc, ater</a>';
//        echo '      <a href="ControleurAdmin.php?numAction=240&menuDestination=chercheurperm">Gestion permanents</a>';
//        echo '      <a href="ControleurAdmin.php?numAction=330&menuDestination=chercheurinvi">Gestion invités</a>';
//        echo '      <a href="ControleurAdmin.php?numAction=320&menuDestination=chercheurarch"><b>Gestion archives</b></a>';
//        echo '      <a href="ControleurAdmin.php?numAction=350&menuDestination=chercheuranon"><b>données anonymisées</b></a>';


//        echo '    </div>';

//        echo '    </li>    ';
//}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//SALLES 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if ( $_SESSION['personnes']==1) {
        echo '    <li class="dropdown">';
        echo '      <a href="ControleurSalles.php?numAction=164&menuDestination=admin" class="dropbtn">Salles </a>';
        echo '     <div class="dropdown-content">';
        echo '      <a href="ControleurSalles.php?numAction=164&menuDestination=admin">Salles </a>';
        echo '      <a href="ControleurSalles.php?numAction=160&menuDestination=admin">Ajouter une pièce</a>';
        echo '    </div>';
        echo '    </li>    ';

}
elseif ($_SESSION['arrivant']==0) 
 {
        echo '    <li class="dropdown">';
        echo '            <a href="../control/ControleurSalles.php" class="dropbtn">Salles </a>';
        echo '     <div class="dropdown-content">';
        echo '      <a href="ControleurSalles.php?numAction=164&menuDestination=chercheur">Salles </a>';
        echo '    </div>';
        echo '    </li>    ';

}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EQUIPES
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// USE CASE ADMIN GERE EQUIPE 
if ( $_SESSION['equipe']==1) {
	echo '    <li class="dropdown">';
	echo '    <a href="../control/ControleurEquipe.php" class="dropbtn">Gestion Equipes </a>';
	echo '     <div class="dropdown-content">';
	echo '      <a href="ControleurEquipe.php?numAction=114&menuDestination=adminequipe">équipe</a>';
	echo '      <a href="ControleurEquipe.php?numAction=110">Ajouter une équipe</a>';
	echo '    </div>';
	echo '    </li>    ';
}
elseif ($_SESSION['arrivant']==0) 
 {
	echo '    <li class="dropdown">';
	echo '    <a href="../control/ControleurEquipe.php" class="dropbtn">Gestion Equipes </a>';
	echo '     <div class="dropdown-content">';
	echo '      <a href="ControleurEquipe.php?numAction=114&menuDestination=chercheurequipe">équipe</a>';
	echo '    </div>';
	echo '    </li>    ';
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ORDI 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// USE CASE ADMIN GERE ORDI 
if ( $_SESSION['ordi']==1) {
	echo '    <li class="dropdown">';
	echo '    <a href="../control/Controleur.php" class="dropbtn">Gestion Ordinateur </a>';
	echo '     <div class="dropdown-content">';
	echo      ' <a href="ControleurOrdi.php?numAction=174">ordinateur</a>';
      	echo '<a href="ControleurOrdi.php?numAction=170">Ajouter un ordinateur</a>';
	echo '</div>';
	echo '    </li>    ';
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CAHIERS 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// USE CASE ADMIN GERE CAHIERS 
if ( $_SESSION['cahiersLabo']==1) {
        echo '    <li class="dropdown">';
        echo '    <a href="../control/ControleurCahier.php?numAction=174&menuDestination=admincahier" class="dropbtn">Cahiers Laboratoire  </a>';
        echo '     <div class="dropdown-content">';
        echo      ' <a href="ControleurCahier.php?numAction=174&menuDestination=admincahier">cahiers</a>';
        echo '<a href="ControleurCahier.php?numAction=170">Ajouter un cahier</a>';
        echo '</div>';
        echo '    </li>    ';
}
elseif ($_SESSION['arrivant']==0)  {
        echo '    <li class="dropdown">';
        echo '    <a href="../control/ControleurCahier.php?numAction=174&menuDestination=chercheurcahier" class="dropbtn">Cahiers Laboratoire  </a>';
        echo '     <div class="dropdown-content">';
        echo      ' <a href="ControleurCahier.php?numAction=174&menuDestination=chercheurcahier">cahiers</a>';
        echo '</div>';
        echo '    </li>    ';
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// APPLICATIONS 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// USE CASE ADMIN GERE PARAMETRES APPLICATIONS 
if ($_SESSION['admin']==1) {

echo '  <li class="dropdown">';
echo '    <a href="../control/ControleurParam.php" class="dropbtn">Paramètres</a>';
echo '     <div class="dropdown-content">';
	echo '      <a href="ControleurParam.php?numAction=150">Ajouter un statut</a>';
	echo '      <a href="ControleurParam.php?numAction=130">Ajouter un type de personnel</a>';
	echo '      <a href="ControleurParam.php?numAction=140">Ajouter une tutelle</a>';
	echo '      <a href="ControleurParam.php?numAction=154">Modifier un statut</a>';
	echo '      <a href="ControleurParam.php?numAction=134">Modifier un type de personnel</a>';
	echo '      <a href="ControleurParam.php?numAction=144">Modifier une tutelle</a>';
	echo '      <a href="ControleurParam.php?numAction=145">Financeurs pour les projets de recherche</a>';
//	echo '      <a href="ControleurParam.php?numAction=149">Modifier un financeur pour les projets de recherche</a>';
	echo '      <a href="ControleurParam.php?numAction=160">Types pour les projets de recherche</a>';
//	echo '      <a href="ControleurParam.php?numAction=164">Modifier un type pour les projets de recherche</a>';
	echo '      <a href="ControleurParam.php?numAction=200">Ajouter / Modifier les colleges electoraux</a>';
	echo '<a href="ControleurParam.php?numAction=120">Ajouter un site</a>';
	echo '  <a href="ControleurParam.php?numAction=124">Modifier un site</a>';
	echo '  <a href="ControleurParam.php?numAction=500">Liste des administrateurs de la base de données</a>';
	echo '  <a href="ControleurParam.php?numAction=507">Gerer les destinataires des mails d\'avertissement</a>';
//	echo '  <a href="ControleurParam.php?numAction=511">Generer le login spip</a>';
//	echo '  <a href="ControleurAdmin.php?numAction=1200">Import des fichiers excel HCERES</a>';
echo ' </div>';
echo '    </li>';
}
?>



    
<?php
//echo  '    <li><a href="http://www.hydrosciences.org/spip.php?rubrique31">Annuaire public</a></li>';
echo '    <li><a href="http://www.hydrosciences.org/spip.php?rubrique25">Intranet</a></li>';
//echo '<li><a href="Controleur.php?numAction=2">Déconnexion '. $_SESSION['user'].'</a>';
?>
    
  
</ul>
</nav>
