-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 14, 2019 at 11:04 AM
-- Server version: 5.5.64-MariaDB
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd_annuaireV6`
--

-- --------------------------------------------------------

--
-- Table structure for table `cahierlabo`
--

CREATE TABLE IF NOT EXISTS `cahierlabo` (
  `cahierNum` varchar(10) NOT NULL,
  `modele` varchar(2) DEFAULT NULL,
  `fonction` varchar(30) DEFAULT NULL,
  `eq_id` int(11) DEFAULT NULL,
  `localisation` varchar(100) DEFAULT NULL,
  `commentaires` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `cahierlaboAppartient`
--

CREATE TABLE IF NOT EXISTS `cahierlaboAppartient` (
  `p_id` int(11) NOT NULL,
  `cahierNum` varchar(10) NOT NULL,
  `dateDotation` date DEFAULT NULL,
  `dateRetour` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `contractuel`
--

CREATE TABLE IF NOT EXISTS `contractuel` (
  `contractuel_id` int(11) NOT NULL,
  `contractuel_sup_id` int(11) DEFAULT NULL,
  `contractuel_sujet_recherche` varchar(60) DEFAULT NULL,
  `contractuel_t_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `droitsadmin`
--

CREATE TABLE IF NOT EXISTS `droitsadmin` (
  `user` varchar(60) NOT NULL,
  `admin` int(11) DEFAULT NULL,
  `ordi` int(11) NOT NULL,
  `equipe` int(11) DEFAULT NULL,
  `personnes` int(11) DEFAULT NULL,
  `reservations` int(11) NOT NULL,
  `cahiersLabo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `historique`
--

CREATE TABLE IF NOT EXISTS `historique` (
  `numHistorique` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `sit_id` int(11) NOT NULL COMMENT 'Site',
  `ty_id` int(11) NOT NULL COMMENT 'Type',
  `st_id` int(11) NOT NULL COMMENT 'Statut',
  `dateDebutH` date DEFAULT NULL,
  `dateFinH` date DEFAULT NULL,
  `stageId` int(11) DEFAULT NULL,
  `theseId` int(11) DEFAULT NULL,
  `contractuel_id` int(11) DEFAULT NULL,
  `permanent_id` int(11) DEFAULT NULL,
  `invite_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `invite`
--

CREATE TABLE IF NOT EXISTS `invite` (
  `invite_id` int(11) NOT NULL,
  `invite_par_id` int(11) DEFAULT NULL,
  `invite_commentaire` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `meta`
--

CREATE TABLE IF NOT EXISTS `meta` (
  `nom` varchar(255) NOT NULL,
  `valeur` varchar(767) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meta`
--


--
-- Table structure for table `ressourcesMessage`
--

CREATE TABLE IF NOT EXISTS `ressourcesMessage` (
  `rid` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `message` varchar(767) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ressourcesMessage`
--

INSERT INTO `ressourcesMessage` (`rid`, `nom`, `mail`, `message`) VALUES
(1, 'bienvenue', '', '');


--
-- Table structure for table `ressourcesMessageHistorique`
--

CREATE TABLE IF NOT EXISTS `ressourcesMessageHistorique` (
  `rid` int(11) NOT NULL,
  `numHistorique` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `ressourcesMessagePJ`
--

CREATE TABLE IF NOT EXISTS `ressourcesMessagePJ` (
  `pj_id` int(30) NOT NULL,
  `rid` int(11) NOT NULL,
  `pj` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permanent`
--

CREATE TABLE IF NOT EXISTS `permanent` (
  `permanent_id` int(11) NOT NULL,
  `permanent_c_id` int(11) DEFAULT NULL,
  `t_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `stage`
--

CREATE TABLE IF NOT EXISTS `stage` (
  `stageId` int(11) NOT NULL,
  `p_id` int(11) DEFAULT NULL,
  `p_stag_encadrant` int(11) NOT NULL,
  `stag_intitule` varchar(50) DEFAULT NULL,
  `eq_id` int(11) DEFAULT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL,
  `etablissement` varchar(30) DEFAULT NULL,
  `f_id` int(11) DEFAULT NULL,
  `autreformation` varchar(50) DEFAULT NULL,
  `t_conv_id` int(11) DEFAULT NULL,
  `gratification` tinyint(1) NOT NULL,
  `financementCredit` varchar(30) DEFAULT NULL,
  `financementEntite` varchar(30) DEFAULT NULL,
  `missionFrance` tinyint(1) DEFAULT NULL,
  `dateMissionFrance` varchar(100) DEFAULT NULL,
  `missionEtranger` tinyint(1) DEFAULT NULL,
  `dateMissionEtranger` varchar(100) DEFAULT NULL,
  `demandeBureau` tinyint(1) NOT NULL,
  `demandePoste` tinyint(1) NOT NULL,
  `laboChimie` tinyint(1) DEFAULT NULL,
  `salleBlanche` tinyint(1) DEFAULT NULL,
  `conventionEnvoyee` tinyint(1) DEFAULT NULL,
  `conventionRevenue` tinyint(1) DEFAULT NULL,
  `scanConvention` varchar(50) DEFAULT NULL,
  `dossierGratif` tinyint(1) DEFAULT NULL,
  `montantGratif` float DEFAULT NULL,
  `montantTransport` float DEFAULT NULL,
  `numMissionnaire` varchar(6) DEFAULT NULL,
  `refFournisseur` varchar(8) DEFAULT NULL,
  `numBCSifac` varchar(15) DEFAULT NULL,
  `dateDossierGratif` date DEFAULT NULL,
  `parDossierGratif` varchar(25) DEFAULT NULL,
  `stageModifiePar` int(11) DEFAULT NULL,
  `stageModifieDate` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `stageFinancement`
--

CREATE TABLE IF NOT EXISTS `stageFinancement` (
  `financementId` int(11) NOT NULL,
  `stageId` int(11) NOT NULL,
  `financementCredit` varchar(30) NOT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `theses`
--

CREATE TABLE IF NOT EXISTS `theses` (
  `theseId` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `p_these_directeur1` int(11) DEFAULT NULL,
  `p_these_directeur2` int(11) DEFAULT NULL,
  `ecoleDoctorale` varchar(50) DEFAULT NULL,
  `intitule` varchar(300) DEFAULT NULL,
  `tfinancement` varchar(150) DEFAULT NULL,
  `m2obtenu` varchar(150) DEFAULT NULL,
  `laboaccueil` varchar(60) DEFAULT NULL,
  `etsInscription` varchar(60) DEFAULT NULL,
  `theseDebut` varchar(4) DEFAULT NULL,
  `theseSoutenance` date DEFAULT NULL,
  `abandon` smallint(6) NOT NULL DEFAULT '0',
  `apresThese` varchar(100) DEFAULT NULL,
  `modifiePar` int(11) DEFAULT NULL,
  `modifieDate` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `t_corps`
--

CREATE TABLE IF NOT EXISTS `t_corps` (
  `permanent_c_id` int(11) NOT NULL,
  `ty_id` int(11) NOT NULL,
  `corps` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_corps`
--

INSERT INTO `t_corps` (`permanent_c_id`, `ty_id`, `corps`) VALUES
(1, 4, 'AJT'),
(2, 4, 'TECH'),
(3, 4, 'AI'),
(4, 4, 'IE'),
(5, 4, 'IR'),
(6, 2, 'MCF'),
(7, 2, 'PR'),
(8, 1, 'CR'),
(9, 1, 'DR'),
(10, 5, 'MCU'),
(11, 5, 'PU');

-- --------------------------------------------------------

--
-- Table structure for table `t_ecole_doctorale`
--

CREATE TABLE IF NOT EXISTS `t_ecole_doctorale` (
  `ed_id` int(11) NOT NULL,
  `ed_nom` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_ecole_doctorale`
--

INSERT INTO `t_ecole_doctorale` (`ed_id`, `ed_nom`) VALUES
(1, 'GAIA'),
(2, 'Autre');

-- --------------------------------------------------------

--
-- Table structure for table `t_equipe`
--

CREATE TABLE IF NOT EXISTS `t_equipe` (
  `eq_id` int(11) NOT NULL,
  `resp_id` int(11) DEFAULT NULL,
  `eq_nom` varchar(25) NOT NULL,
  `eq_nom_long` varchar(100) NOT NULL,
  `eq_verticale` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `t_formation_stagiaire`
--

CREATE TABLE IF NOT EXISTS `t_formation_stagiaire` (
  `f_id` int(11) NOT NULL,
  `intitule` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_formation_stagiaire`
--

INSERT INTO `t_formation_stagiaire` (`f_id`, `intitule`) VALUES
(1, 'M1 Sciences de l''eau Montpellier'),
(2, 'M1 Sciences de l''eau Montpellier'),
(3, 'M2 Sciences de l''eau Montpellier'),
(4, 'POLYTECH Montpellier 3 ème année'),
(5, 'POLYTECH Montpellier  4ème année'),
(6, 'L3 pro Chimie de l''environnment Montpellier'),
(7, 'L3 pro GPTP Montpellier'),
(8, 'DUT chimie Sète - Montpellier');

-- --------------------------------------------------------



-- --------------------------------------------------------

--
-- Table structure for table `t_membres_equipe2`
--

CREATE TABLE IF NOT EXISTS `t_membres_equipe2` (
  `eq_id` int(11) NOT NULL,
  `numHistorique` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `t_ordinateur`
--

CREATE TABLE IF NOT EXISTS `t_ordinateur` (
  `o_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `hostname` varchar(20) NOT NULL,
  `adresse_ipv4` varchar(15) NOT NULL,
  `adresse_mac` varchar(17) NOT NULL,
  `type` varchar(30) NOT NULL,
  `serviceTag` varchar(10) NOT NULL,
  `numero_inventaire` varchar(12) NOT NULL,
  `date_achat` date NOT NULL,
  `garantie` tinyint(4) NOT NULL,
  `processeur` varchar(25) NOT NULL,
  `memoire` varchar(12) NOT NULL,
  `dhcp` tinyint(1) NOT NULL,
  `commentaire` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `t_personne`
--

CREATE TABLE IF NOT EXISTS `t_personne` (
  `p_id` int(11) NOT NULL,
  `sexe` varchar(1) DEFAULT NULL,
  `p_nom` varchar(100) NOT NULL,
  `p_prenom` varchar(100) NOT NULL,
  `dateNaissance` date DEFAULT NULL,
  `p_mail` varchar(254) NOT NULL,
  `p_tel` varchar(54) DEFAULT NULL,
  `p_tel_perso` varchar(20) DEFAULT NULL,
  `orcid` varchar(19) DEFAULT NULL,
  `hdr` int(1) DEFAULT NULL,
  `p_page_perso` varchar(2083) DEFAULT NULL,
  `p_commentaire` varchar(180) DEFAULT NULL,
  `nationalite` varchar(2) DEFAULT NULL,
  `personneaprevenir` varchar(60) DEFAULT NULL,
  `horaireacces` varchar(2) DEFAULT NULL,
  `p_article_spip` int(5) DEFAULT NULL,
  `pi_id` int(11) DEFAULT NULL,
  `ed_id` int(11) NOT NULL,
  `loginspip` varchar(20) DEFAULT NULL,
  `demandeIntranet` tinyint(1) NOT NULL,
  `archive` int(11) NOT NULL DEFAULT '0',
  `anonyme` int(1) DEFAULT NULL,
  `chartesSignees` tinyint(1) NOT NULL,
  `formationSecurite` tinyint(1) NOT NULL,
  `numeroCle` varchar(10) DEFAULT NULL,
  `ficheNouveauArrivant` tinyint(1) NOT NULL,
  `sessionInformatique` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `t_piece`
--

CREATE TABLE IF NOT EXISTS `t_piece` (
  `pi_id` int(11) NOT NULL,
  `pi_numero` varchar(25) NOT NULL,
  `pi_nb_places` int(11) NOT NULL,
  `pi_nb_places_dispo` int(11) NOT NULL,
  `sit_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `t_place`
--

CREATE TABLE IF NOT EXISTS `t_place` (
  `pl_id` int(11) NOT NULL,
  `pi_id` int(11) NOT NULL,
  `o_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `t_reservation2`
--

CREATE TABLE IF NOT EXISTS `t_reservation2` (
  `r_id` int(11) NOT NULL,
  `r_date_debut` date NOT NULL,
  `r_date_fin` date NOT NULL,
  `p_id` int(11) NOT NULL,
  `pl_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `t_site`
--

CREATE TABLE IF NOT EXISTS `t_site` (
  `sit_id` int(11) NOT NULL,
  `sit_nom` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `t_statut2`
--

CREATE TABLE IF NOT EXISTS `t_statut2` (
  `st_id` int(11) NOT NULL,
  `st_nom` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_statut2`
--

INSERT INTO `t_statut2` (`st_id`, `st_nom`) VALUES
(3, 'Etudiant'),
(4, 'Invité'),
(2, 'Non-Permament'),
(1, 'Permanent');

-- --------------------------------------------------------

--
-- Table structure for table `t_tutelle`
--

CREATE TABLE IF NOT EXISTS `t_tutelle` (
  `t_id` int(11) NOT NULL,
  `t_nom` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



-- --------------------------------------------------------

--
-- Table structure for table `t_type_personnel2`
--

CREATE TABLE IF NOT EXISTS `t_type_personnel2` (
  `st_id` int(11) NOT NULL,
  `ty_id` int(11) NOT NULL,
  `ty_nom` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_type_personnel2`
--

INSERT INTO `t_type_personnel2` (`st_id`, `ty_id`, `ty_nom`) VALUES
(1, 1, 'chercheur'),
(1, 2, 'enseignant chercheur / past'),
(1, 3, 'prag'),
(1, 4, 'ita'),
(1, 5, 'personnel hospitalier'),
(1, 6, 'autre'),
(2, 1, 'cdd chercheur'),
(2, 2, 'cdd ita'),
(2, 3, 'post doctorant'),
(2, 4, 'ater'),
(3, 1, 'doctorant'),
(3, 2, 'stagiaire'),
(4, 1, 'invité ec c'),
(4, 2, 'invité ita');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cahierlabo`
--
ALTER TABLE `cahierlabo`
  ADD PRIMARY KEY (`cahierNum`);

--
-- Indexes for table `cahierlaboAppartient`
--
ALTER TABLE `cahierlaboAppartient`
  ADD PRIMARY KEY (`p_id`,`cahierNum`);

--
-- Indexes for table `contractuel`
--
ALTER TABLE `contractuel`
  ADD PRIMARY KEY (`contractuel_id`);

--
-- Indexes for table `droitsadmin`
--
ALTER TABLE `droitsadmin`
  ADD PRIMARY KEY (`user`);

--
-- Indexes for table `historique`
--
ALTER TABLE `historique`
  ADD PRIMARY KEY (`numHistorique`);

--
-- Indexes for table `invite`
--
ALTER TABLE `invite`
  ADD PRIMARY KEY (`invite_id`);

--
-- Indexes for table `meta`
--
ALTER TABLE `meta`
  ADD PRIMARY KEY (`nom`,`valeur`);

--
-- Indexes for table `ressourcesMessage`
--
ALTER TABLE `ressourcesMessage`
  ADD PRIMARY KEY (`rid`);

-- Indexes for table `ressourcesMessageHistorique`
--
ALTER TABLE `ressourcesMessageHistorique`
  ADD PRIMARY KEY (`rid`,`numHistorique`);

--
-- Indexes for table `ressourcesMessagePJ`
--
ALTER TABLE `ressourcesMessagePJ`
  ADD PRIMARY KEY (`pj_id`);

--
-- Indexes for table `permanent`
--
ALTER TABLE `permanent`
  ADD PRIMARY KEY (`permanent_id`);

--
-- Indexes for table `stage`
--
ALTER TABLE `stage`
  ADD PRIMARY KEY (`stageId`);

--
-- Indexes for table `stageFinancement`
--
ALTER TABLE `stageFinancement`
  ADD PRIMARY KEY (`financementId`,`stageId`);

--
-- Indexes for table `theses`
--
ALTER TABLE `theses`
  ADD PRIMARY KEY (`theseId`);

--
-- Indexes for table `t_corps`
--
ALTER TABLE `t_corps`
  ADD PRIMARY KEY (`permanent_c_id`);

--
-- Indexes for table `t_ecole_doctorale`
--
ALTER TABLE `t_ecole_doctorale`
  ADD PRIMARY KEY (`ed_id`);

--
-- Indexes for table `t_equipe`
--
ALTER TABLE `t_equipe`
  ADD PRIMARY KEY (`eq_id`);

--
-- Indexes for table `t_formation_stagiaire`
--
ALTER TABLE `t_formation_stagiaire`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `t_ordinateur`
--
ALTER TABLE `t_ordinateur`
  ADD PRIMARY KEY (`o_id`);

--
-- Indexes for table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `t_piece`
--
ALTER TABLE `t_piece`
  ADD PRIMARY KEY (`pi_id`),
  ADD KEY `fk_t_piece_sit_id` (`sit_id`);

--
-- Indexes for table `t_place`
--
ALTER TABLE `t_place`
  ADD PRIMARY KEY (`pl_id`);

--
-- Indexes for table `t_reservation2`
--
ALTER TABLE `t_reservation2`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `t_site`
--
ALTER TABLE `t_site`
  ADD PRIMARY KEY (`sit_id`),
  ADD UNIQUE KEY `sit_nom` (`sit_nom`);


--
-- Indexes for table `t_statut2`
--
ALTER TABLE `t_statut2`
  ADD PRIMARY KEY (`st_id`),
  ADD UNIQUE KEY `st_nom` (`st_nom`);

--
-- Indexes for table `t_tutelle`
--
ALTER TABLE `t_tutelle`
  ADD PRIMARY KEY (`t_id`),
  ADD UNIQUE KEY `t_nom` (`t_nom`);


--
-- Indexes for table `t_type_personnel2`
--
ALTER TABLE `t_type_personnel2`
  ADD PRIMARY KEY (`st_id`,`ty_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contractuel`
--
ALTER TABLE `contractuel`
  MODIFY `contractuel_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `historique`
--
ALTER TABLE `historique`
  MODIFY `numHistorique` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `invite`
--
ALTER TABLE `invite`
  MODIFY `invite_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `permanent`
--
ALTER TABLE `permanent`
  MODIFY `permanent_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `stage`
--
ALTER TABLE `stage`
  MODIFY `stageId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `stageFinancement`
--
ALTER TABLE `stageFinancement`
  MODIFY `financementId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `theses`
--
ALTER TABLE `theses`
  MODIFY `theseId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `t_corps`
--
ALTER TABLE `t_corps`
  MODIFY `permanent_c_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `t_ecole_doctorale`
--
ALTER TABLE `t_ecole_doctorale`
  MODIFY `ed_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_equipe`
--
ALTER TABLE `t_equipe`
  MODIFY `eq_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `t_formation_stagiaire`
--
ALTER TABLE `t_formation_stagiaire`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `t_ordinateur`
--
ALTER TABLE `t_ordinateur`
  MODIFY `o_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `t_piece`
--
ALTER TABLE `t_piece`
  MODIFY `pi_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `t_place`
--
ALTER TABLE `t_place`
  MODIFY `pl_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `t_reservation2`
--
ALTER TABLE `t_reservation2`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `t_site`
--
ALTER TABLE `t_site`
  MODIFY `sit_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `t_statut2`
--
ALTER TABLE `t_statut2`
  MODIFY `st_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `t_tutelle`
--
ALTER TABLE `t_tutelle`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `ressourcesMessage`
--
ALTER TABLE `ressourcesMessage`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ressourcesMessagePJ`
--
ALTER TABLE `ressourcesMessagePJ`
  MODIFY `pj_id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_piece`
--
ALTER TABLE `t_piece`
  ADD CONSTRAINT `fk_t_piece_sit_id` FOREIGN KEY (`sit_id`) REFERENCES `t_site` (`sit_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
