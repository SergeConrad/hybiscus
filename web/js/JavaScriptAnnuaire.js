/**
 * 
 */

  $.datepicker.regional['fr'] = {clearText: 'Effacer', clearStatus: '',
		    closeText: 'Fermer', closeStatus: 'Fermer sans modifier',
		    prevText: '<Préc', prevStatus: 'Voir le mois précédent',
		    nextText: 'Suiv>', nextStatus: 'Voir le mois suivant',
		    currentText: 'Courant', currentStatus: 'Voir le mois courant',
		    monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
		    'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		    monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
		    'Jul','Aoû','Sep','Oct','Nov','Déc'],
		    monthStatus: 'Voir un autre mois', yearStatus: 'Voir un autre année',
		    weekHeader: 'Sm', weekStatus: '',
		    dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		    dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		    dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		    dayStatus: 'Utiliser DD comme premier jour de la semaine', dateStatus: 'Choisir le DD, MM d',
		    dateFormat: 'dd/mm/yy', firstDay: 0, 
		    initStatus: 'Choisir la date', isRTL: false};
		 $.datepicker.setDefaults($.datepicker.regional['fr']);
  $( ".dateNaiss" ).datepicker({
	changeYear: true,
	changeMonth: true,
	yearRange: '-100:+0',
beforeShow: function(){    
           $(".ui-datepicker").css('font-size', 20) 
    }
	});
  
  
  $( ".date" ).datepicker({
	  dateFormat: "yy-mm-dd"
	});
  
  var selectedValSite = "";
  var dateDebut = "";
  var dateFin = "";


  function updateSelectSallesV2() {
   	

  	var select = document.getElementById("selection_salle");
  if (selectedValSite != "" && dateDebut != "") {

   	$(select)
    .empty()
    .append('<option selected="selected" disabled="disabled" value="">Sélectionnez une salle existante.</option>')
  }

  if(dateFin != "") {
  	

  if(dateDebut > dateFin) {
  	alert('La date de début ne peut pas être supérieur à la date de fin');
  	 return; 
  	 }
  }
  if (selectedValSite == "" && dateDebut == "" && dateFin == "") { // si la date de début et la date de fin sont nulles.
  	return;
  }
  	else if (selectedValSite != "" && dateDebut != "" && dateFin == "") {// si la date de début n'est pas nulle, si la date de fin est nulle 
  		
  		$.post('post_recuperation_salles.php', { siteNom: selectedValSite, dateDebut: dateDebut}, 
  				    function(returnedData){
  		
  				         var json = returnedData;
  			         var arr = $.parseJSON(json);

  			
  			         for (index=0 ; index < arr.length; index++) {
  				         if (arr[index].pi_nb_places_dispo != 0) {
  					          
  	 						var option = document.createElement('option');
  	 						option.value = arr[index].pi_numero;
  	 						option.text = "n°" + arr[index].pi_numero +"; "+   "    NB PLACES:"+ arr[index].pi_nb_places_dispo;
  	 						select.appendChild(option);
  				         }
  							
  					
  	 					}

  			         for (index=0 ; index < arr.length; index++) {
  				         if (arr[index].pi_nb_places_dispo == 0) {
  					          
  	 						var option = document.createElement('option');
  	 						option.value = arr[index].pi_numero;
  	 						option.text = "n°" + arr[index].pi_numero +"; "+   "    NB PLACES:"+ arr[index].pi_nb_places_dispo;
  	 						select.appendChild(option);
  	 						$("#selection_salle option[value=" + arr[index].pi_numero + "]").attr("disabled", true);
  				         }
  							
  				  
  	 					}
  	})
  	}
  	else if (selectedValSite != "" && dateDebut != "" && dateFin != "") { // si la date de début est nulle et la date de fin n'est pas nulle
  		$.post('post_recuperation_salles.php', { siteNom: selectedValSite, dateDebut: dateDebut, dateFin :dateFin}, 
  			    function(returnedData){
  	
  			         var json = returnedData;
  		         var arr = $.parseJSON(json);


  		         for (index=0 ; index < arr.length; index++) {
  			  		if(arr[index].pi_nb_places_dispo != 0) {
   						var option = document.createElement('option');
   						option.value = arr[index].pi_numero;
   						option.text = "n°" + arr[index].pi_numero +"; "+   "    NB PLACES:"+ arr[index].pi_nb_places_dispo;
   						select.appendChild(option);
  			  		}

   					}
  		         for (index=0 ; index < arr.length; index++) {
  			         if (arr[index].pi_nb_places_dispo == 0) {
  				          
   						var option = document.createElement('option');
   						option.value = arr[index].pi_numero;
   						option.text = "n°" + arr[index].pi_numero +"; "+   "    NB PLACES:"+ arr[index].pi_nb_places_dispo;
   						select.appendChild(option);
   						$("#selection_salle option[value=" + arr[index].pi_numero + "]").attr("disabled", true);
  			         }
  						
  			  
   					}
  				
  })
  	

  	}

  }

  	$("#reservation_site input[type='radio']").click(function() {
  		selectedValSite = $("#reservation_site input[type='radio']:checked").val();
      	updateSelectSallesV2();
  	});


  	$('input[name=dateDebut]').change(function() {
          dateDebut = $('input[name=dateDebut]').val();
          updateSelectSallesV2();
      });

  	$('input[name=dateFin]').change(function() {
          dateFin = $('input[name=dateFin]').val();
          updateSelectSallesV2();
      });














  	// pas la même chose que modification piece attention ! modif personne
  $('.link_suppression_reservation').click(function(event){
  	
  	   event.preventDefault(); // prevent default behavior of link click
  	   // now make an AJAX request to server_side_file.php by passing some data
  	   if (confirm('Etes-vous sur de vouloir supprimer cette réservation ?')) {
  	   
  	   
  	   urlComplete = ($(this).attr('href'));
  	   urlpage = urlComplete.substring(0, urlComplete.indexOf('?'));
  	   valeurparametre = urlComplete.substring(urlComplete.indexOf('=') + 1, urlComplete.length);
  	   



  		   // now make an AJAX request to server_side_file.php by passing some data
  		   $.post(urlpage, {id : valeurparametre}, function(response){
  		      //now you've got `response` from server, play with it like
  		      
  		      alert(response);
  		      if (response == 'La réservation a été supprimé avec succès') {


  			   	   ligne = $(event.target).parent().parent();
  					ligne.remove();

  					var rows = document.getElementById('reservations').getElementsByTagName('tbody')[0].getElementsByTagName('tr').length;
  					if(rows == 1)
  					{
  						($(".PremierReservation").parent().parent()).remove();
  						$( "<p>Aucune réservation affectée pour cette personne.</p>" ).insertAfter( "#reservation_existante" );
  					}
  				
  				   
  		      }
  		   });
  		  

  		 

  	


  		 
  	   }
  		   
  	   

  	});


  $('.link_suppression_discipline').click(function(event){
  	
  	   event.preventDefault(); // prevent default behavior of link click
  	   // now make an AJAX request to server_side_file.php by passing some data
  	   if (confirm('Etes-vous sur de vouloir supprimer cette discipline ?')) {
  	   

  	   
  	   urlComplete = ($(this).attr('href'));
  	   urlpage = urlComplete.substring(0, urlComplete.indexOf('?'));
  	   valeurparametre1 = urlComplete.substring(urlComplete.indexOf('=') + 1, urlComplete.indexOf('&'));
  	   urlApresPremierParametre = urlComplete.substring(urlComplete.indexOf('&'), urlComplete.length);
  	   valeurparametre2 = urlApresPremierParametre.substring(urlApresPremierParametre.indexOf('=') +1, urlComplete.length);
  	   



  		   // now make an AJAX request to server_side_file.php by passing some data
  		   $.post(urlpage, {idDiscipline : valeurparametre1, idPersonne : valeurparametre2}, function(response){
  		      //now you've got `response` from server, play with it like
  		      
  		      alert(response);

  		      if (response == 'La discipline affectée à cette personne a été supprimé avec succès.') {

  		   	   ligne = $(event.target).parent().parent();
  				ligne.remove();

  				var rows = document.getElementById('disciplines').getElementsByTagName('tbody')[0].getElementsByTagName('tr').length;
  				if(rows == 1)
  				{
  					($(".PremierDiscipline").parent().parent()).remove();
  					$( "<p>Aucune discipline affectée pour cette personne.</p>" ).insertAfter( "#disciplines_affectes" );
  				}
  				
  			
  			   

  		      }

  		      
  		   });
   
  	   }

  	});










  var url = window.location.href;

  // Will only work if string in href matches with location
  $('.cemenu a[href="'+ url +'"]').addClass('active');

  // Will also work for relative and absolute hrefs
  $('.cemenu a').filter(function() {
      return this.href == url;
  }).addClass('active');


  function ConfirmationSupprimerPersonne() {
  	if (!confirm('Etes-vous sur de vouloir supprimer cette personne ? Toutes les informations relatives à cette personne et ses réservations s\'il en existe vont être supprimées')) {
  	event.preventDefault();
  	return;
  	}
  }


  //capture submit
  $('#SuppressionPhoto').submit(function() {
  	event.preventDefault();
  	if(confirm('Etes vous-sur de vouloir supprimer la photo de cette personne ?')) {
  	
       $theForm = $(this);

       // send xhr request
       $.ajax({
           type: $theForm.attr('method'),
           url: $theForm.attr('action'),
           data: $theForm.serialize(),
           success: function(data) {
               alert('La photo a bien été supprimée');
  			$('#SuppressionPhoto').remove();
           }
       });
  	}
       // prevent submitting again
       return false;
  });


  //capture submit
  $('#SuppressionCV').submit(function() {
  	event.preventDefault();
  	if(confirm('Etes vous-sur de vouloir supprimer le CV de cette personne ?')) {
  	
       $theForm = $(this);

       // send xhr request
       $.ajax({
           type: $theForm.attr('method'),
           url: $theForm.attr('action'),
           data: $theForm.serialize(),
           success: function(data) {
               alert('Le CV a bien été supprimé');
  			$('#SuppressionCV').remove();
           }
       });
  	}
       // prevent submitting again
  	
       return false;
  });



  ////////////////////////////////
  //fixscroll.js:
  //call loadP and unloadP when body loads/unloads and scroll position will not move
  function getScrollXY() {
   var x = 0, y = 0;
   if( typeof( window.pageYOffset ) == 'number' ) {
       // Netscape
       x = window.pageXOffset;
       y = window.pageYOffset;
   } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
       // DOM
       x = document.body.scrollLeft;
       y = document.body.scrollTop;
   } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
       // IE6 standards compliant mode
       x = document.documentElement.scrollLeft;
       y = document.documentElement.scrollTop;
   }
   return [x, y];
  }
          
  function setScrollXY(x, y) {
   window.scrollTo(x, y);
  }
  function createCookie(name,value,days) {
  	if (days) {
  		var date = new Date();
  		date.setTime(date.getTime()+(days*24*60*60*1000));
  		var expires = "; expires="+date.toGMTString();
  	}
  	else var expires = "";
  	document.cookie = name+"="+value+expires+"; path=/";
  }

  function readCookie(name) {
  	var nameEQ = name + "=";
  	var ca = document.cookie.split(';');
  	for(var i=0;i < ca.length;i++) {
  		var c = ca[i];
  		while (c.charAt(0)==' ') c = c.substring(1,c.length);
  		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  	}
  	return null;
  }
  function loadP(pageref){
  	x=readCookie(pageref+'x');
  	y=readCookie(pageref+'y');
  	setScrollXY(x,y)
  }
  function unloadP(pageref){
  	s=getScrollXY()
  	createCookie(pageref+'x',s[0],0.1);
  	createCookie(pageref+'y',s[1],0.1);
  }
  
  
  
  
  function ConfirmationSupprimerPiece() {
		if (!confirm('Etes-vous sur de vouloir supprimer cette pièce ?')) {
		event.preventDefault();
		return;
		}
	}
  

  function ConfirmationSupprimerDiscipline() {
		if (!confirm('Etes-vous sur de vouloir supprimer cette discipline ?')) {
		event.preventDefault();
		return;
		}
	}

  function ConfirmationSupprimerOrdinateur() {
		if (!confirm('Etes-vous sur de vouloir supprimer cet ordinateur ?')) {
		event.preventDefault();
		return;
		}
	}

  function ConfirmationSupprimerEquipe() {
		if (!confirm('Etes-vous sur de vouloir supprimer cette équipe ?')) {
		event.preventDefault();
		return;
		}
	}
  

  function ConfirmationSupprimerSite() {
  if(!confirm('Etes-vous sur de vouloir supprimer ce site ?')) {
  	event.preventDefault();
  	return;
  }
  	
  }
  
  function ConfirmationSupprimerStatut() {
		if (!confirm('Etes-vous sur de vouloir supprimer ce statut ?')) {
		event.preventDefault();
		return;
		}
	}
  

  function ConfirmationSupprimerTutelle() {
		if (!confirm('Etes-vous sur de vouloir supprimer cette tutelle ?')) {
		event.preventDefault();
		return;
		}
	}
  
	function ConfirmationSupprimerTypePersonnel() {
		if (!confirm('Etes-vous sur de vouloir supprimer ce type de personnel ?')) {
		event.preventDefault();
		return;
		}
	}

	// pas la même chose que modification personne attention !
	$('.modif_res_piece').click(function(event){
		
		   event.preventDefault(); // prevent default behavior of link click

		   var event_target = ($(event.target));
			var tableau_courant = event_target.parent().parent().parent().parent();
	 		var id_courant = tableau_courant[0].id;


		
		   // now make an AJAX request to server_side_file.php by passing some data
		   if (confirm('Etes-vous sur de vouloir supprimer cette réservation ?')) {
		   urlComplete = ($(this).attr('href'));
		   urlpage = urlComplete.substring(0, urlComplete.indexOf('?'));
		   valeurparametre = urlComplete.substring(urlComplete.indexOf('=') + 1, urlComplete.length);
		   



			   // now make an AJAX request to server_side_file.php by passing some data
			   $.post(urlpage, {id : valeurparametre}, function(response){
			      //now you've got `response` from server, play with it like
			      
			      alert(response);
			      if (response == 'La réservation a été supprimé avec succès') {
			    		location.reload();	
					 //  ligne = $(event.target).parent().parent();
						
					//	ligne.remove();

						

						// var rows = document.getElementById(id_courant).getElementsByTagName('tbody')[0].getElementsByTagName('tr').length;
						
//	 					//if(rows == 1)
//	 					{
//	 						tableau_courant.remove();
//	 						$( "<p>Aucune rÃ©servation effectuÃ©e pour cette personne.</p>" ).insertAfter( "#reservation_existante" );


//	 						$form = $('<form method="post" action="SuppressionPiece.php" class="inline"></form>');
		//					$form.append('<input type="hidden" name="id" value="<?php echo $id_piece; ?>">');
//	 						$form.append(' <button type="submit" name="submit_param" value="submit_value" class="button" onclick="ConfirmationSupprimerPiece()"> Supprimer cette piÃ¨ce</button>');
							
//	 						$form.insertAfter('#tte_reservation');
							
//	 						$('#suppression').remove();
//	 						$('#tte_reservation').remove();
								
//	 						}
					
							
						   

					//   nbrplace_dispo_form = $( "input[name='nombre_places']" ).val();
					  // nbrplace_dispo_form = parseInt(nbrplace_dispo_form) + 1;
					  // $( "input[name='nombre_places']" ).val(nbrplace_dispo_form);
			      }
			   });
			  

			 

		


			 
		   }
			   
		   

		});
		
var container = $('#ce');
scrollToLienActif = $('.active');

$.fn.exists = function () {
    return this.length !== 0;
}

if (scrollToLienActif.exists()) {
	container.scrollTop(
		    scrollToLienActif.offset().top - container.offset().top + container.scrollTop()
		);
}




 $(document).ready(function() {

jQuery.expr[':'].contains = function(a, i, m) { 
  return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0; 
};

      $("#searchInput").keyup(function(){
	//hide all the rows
          $("#ce").find(".cemenu").hide();

	//split the current value of searchInput
      var data = this.value.split(" ");
	
	//create a jquery object of the rows
          var jo = $("#ce").find("li");
          
	//Recusively filter the jquery object to get results.
          $.each(data, function(i, v){
              jo = jo.filter("*:contains('"+v+"')");
          });
        //show the rows that match.
          jo.show();
     //Removes the placeholder text  
   
      }).focus(function(){
          this.value="";
          $(this).css({"color":"black"});
          $(this).unbind('focus');
      }).css({"color":"#C0C0C0"});

  });
  

  


